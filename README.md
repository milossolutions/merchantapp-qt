# TwintMerchantApp

TwintMerchantApp is ...


# Version
1.0.0

# Building

## Script
buildClient.py script can be used to perform automatic build. It's written in Python.

### Requirements
* Python 3 interpreter (https://www.python.org)
* Qt 5 
* MinGW (Windows only)
* Inno Setup 5 (Windows only)

### Flags

```sh
-q <QT_BINARY_DIR>     Adds <QT_BINARY_DIR> to the system PATH.
-m <MAKE BINARY_DIR>   Adds <MAKE BINARY_DIR> to the system PATH.
-l <QML_DIR>           Used as the argument value --qmldir make command.
-i <INNO_SETUP_DIR>    Adds <INNO_SETUP_DIR> to the system PATH (Windows only).
```

### Example call

```sh
python deployClient.py -q "E:/Qt/5.6/mingw49_32/bin" -m "E:/Qt/Tools/mingw492_32/bin" -l "E:/Qt/5.6/mingw49_32/qml" -i "D:/Program Files (x86)/Inno Setup 5"
```

### Script Output
```sh
Releases/release                       Compilation output with necessary libraries.
Releases/release/TwintMerchant.dmg     DMG Package.
Packages/TwintMerchant-setup.exe       Windows installer.
```