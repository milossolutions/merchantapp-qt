import os
import platform
import argparse
import subprocess
import shutil

# global variables
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
RELEASES_DIR = os.path.join(SCRIPT_DIR, "Releases")

MAKE_BINARY = "mingw32-make" if platform.system() == "Windows" else "make"

BUILD_DIR = None
QT_BIN_DIR = None
INNO_SETUP_DIR = None
QML_DIR = None
BUILD_TYPE = "release"

APP_NAME = "TWINT Merchant.exe" if platform.system() == "Windows" else "TWINT Merchant.app"
DEPLOY_NAME = "windeployqt" if platform.system() == "Windows" else "macdeployqt"

JOBS = 1

def check_opts():
    global BUILD_DIR
    global QT_BIN_DIR
    global INNO_SETUP_DIR
    global QML_DIR

    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--qt-path", metavar='QT_BIN_PATH')
    parser.add_argument("-m", "--make-path", metavar='MAKE_BIN_PATH')
    parser.add_argument("-l", "--qml-path", metavar='QML_PATH')

    if platform.system() == "Windows":
        parser.add_argument("-i", "--innosetup-path", metavar='INNO_SETUP_PATH')

    args = parser.parse_args()

    if args.make_path is not None:
        print("Adding Make bin directory", args.make_path, "to PATH")
        os.environ['PATH'] = args.make_path + os.path.pathsep + os.environ['PATH']
        
    if args.qt_path is not None:
        print("Adding Qt bin directory", args.qt_path, "to PATH")
        os.environ['PATH'] = args.qt_path + os.path.pathsep + os.environ['PATH']
        QT_BIN_DIR = args.qt_path
        
    if args.qml_path is not None:
        print("Adding Qml directory", args.qml_path, "to PATH")
        os.environ['PATH'] = args.qml_path + os.path.pathsep + os.environ['PATH']
        QML_DIR = args.qml_path
        
    if platform.system() == "Windows":
        if args.innosetup_path is not None:
            print("Adding Inno Setup bin directory", args.innosetup_path, "to PATH")
            os.environ['PATH'] = args.innosetup_path + os.path.pathsep + os.environ['PATH']
            INNO_SETUP_DIR = args.innosetup_path
    
    BUILD_DIR = os.path.join(SCRIPT_DIR, "build", BUILD_TYPE)

def build_client():
    print("Building Client...")
    build_path = os.path.join(SCRIPT_DIR)

    # make clean
    subprocess.call([MAKE_BINARY, "clean"], cwd=build_path)

    # qmake
    config = "CONFIG+=" + BUILD_TYPE
    result = subprocess.call(["qmake", config], cwd=build_path)
    if result != 0:
        print("Client qmake step has failed!")
        exit(1)

    # make
    jobs = "-j" + str(JOBS)
    result = subprocess.call([MAKE_BINARY, jobs], cwd=build_path)
    if result != 0:
        print("Client make step has failed!")
        exit(1)

    print("done\n")
    
def check_executable():
    cmd = "where" if platform.system() == "Windows" else "which"

    needed_executable = ["qmake", ]

    for executable in needed_executable:
        print("Testing " + executable + "...")
        try:
            subprocess.call([executable, "--version"])
            print(executable, "found\n")
        except:
            print(executable, "not found\n")
            exit(1)
            
def prepare_dirs():
    if not os.path.exists(RELEASES_DIR):
        os.makedirs(RELEASES_DIR)

    if os.path.exists(os.path.join(RELEASES_DIR, BUILD_TYPE)):
        shutil.rmtree(os.path.join(RELEASES_DIR, BUILD_TYPE))
    os.makedirs(os.path.join(RELEASES_DIR, BUILD_TYPE))
    
def copy_executable():
    print("Copying executable into Releases directory...")
    if platform.system() == "Windows":
        shutil.copyfile(os.path.join(BUILD_DIR, APP_NAME), os.path.join(RELEASES_DIR, BUILD_TYPE, APP_NAME))
    else:
        shutil.copytree(os.path.join(BUILD_DIR, APP_NAME), os.path.join(RELEASES_DIR, BUILD_TYPE, APP_NAME))
    print("done\n")    
    
def qt_deployment():
    print("Qt deployment...")

    if platform.system() == "Windows":
        result = subprocess.call([DEPLOY_NAME, APP_NAME, "--qmldir", QML_DIR], cwd=os.path.join(RELEASES_DIR, BUILD_TYPE))
        if result != 0:
            print("  error during Qt deployment step!\n")
    else:
        result = subprocess.call([DEPLOY_NAME, APP_NAME, ''.join(['-qmldir=', QML_DIR])], cwd=os.path.join(RELEASES_DIR, BUILD_TYPE))
        if result != 0:
            print("  error during Qt deployment step!\n")

    print("done\n")
    
def copy_libraries():
    print("Copying Open Ssl binaries into Releases directory...")
    if platform.system() == "Windows":
        copy_libraries_win()

    print("done\n")
    
def copy_libraries_win():
    src_files = os.listdir(os.path.join(SCRIPT_DIR, "winLibs"))
    for file_name in src_files:
        full_file_name = os.path.join(os.path.join(SCRIPT_DIR, "winLibs"), file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, os.path.join(RELEASES_DIR, BUILD_TYPE, file_name))
    
def create_package():
    if platform.system() == "Windows":
        create_package_win()
    else:
        create_package_osx()
    
def create_package_win():
    print("Creating windows installer...")
    result = subprocess.call(["iscc", "win-setup.iss"])

    if result != 0:
        print("  error during creating windows installer step!\n")

    print("done\n")


def create_package_osx():
    print("Creating DMG package")
    result = subprocess.call(["hdiutil", "create", "-srcfolder", "TWINT Merchant.app", "TwintMerchant.dmg"], cwd=os.path.join(RELEASES_DIR, BUILD_TYPE))
    if result != 0:
        print("  error during creating DMG step!\n") 
   
if __name__ == '__main__':  
    check_opts()
    check_executable()
    build_client()
    prepare_dirs()
    copy_executable()
    qt_deployment()
    copy_libraries()
    create_package()
    exit(0)
