import QtQuick 2.4
import QtQuick.Controls 1.3

import "qrc:/qml/components"
import "qrc:/qml/screens"

import milosolutions.com 1.0

ScreenInterface {
    id: qrCodeScreen

    property string orderNumberString: currentOrderItem.orderToken
    signal backButtonClicked

    Component.onCompleted: {
        setCodePaymentText()
    }

    function setCodePaymentText(){
        if(Settings.deviceType === "NONE"){
            codePaymentText.visible = true
        }else{
            if(ApplicationStatus.beaconConnectionStatus){
                codePaymentText.visible = false
            }else{
                codePaymentText.visible = true
            }
        }
    }

    function setOrderValue(ordeValue){
        priceValueComponent.priceValueString = ordeValue
    }
    Connections {
        target: Settings

        onDeviceTypeChanged: {
            setCodePaymentText()
        }
    }

    Connections {
        target: ApplicationStatus

        onBeaconConnectionStatusChanged: {
            setCodePaymentText()
        }

        onPaymentTimeout: {
            qrCodeScreen.backButtonClicked()
            currentOrderItem.clearData()
        }
    }

    Connections {
        target: currentOrderItem

        onImageDataChanged: {
            qrCode.source = "image://qrCodeImage/" + currentOrderItem.orderToken + ".png"
        }
    }

    Text {
        id: codePaymentText

        text: qsTr("Payment only possible using QR code") + Settings.emptyString
        color: appStyle.defaultTextColor
        opacity: 0.4
        wrapMode: Text.Wrap
        //renderType: Text.NativeRendering

        visible: false

        anchors {
            top: priceValueComponent.top
            left: priceValueComponent.left
            right: priceValueComponent.right
            leftMargin: appStyle.applicationWidth * 0.08
            rightMargin: appStyle.applicationWidth * 0.08
        }

        font {
            family: appStyle.heavyFontFamily
            pixelSize: 14 * appStyle.fontScale
        }
    }

    PriceValueComponent {
        id: priceValueComponent
        height: appStyle.applicationHeight * 0.24
        width: appStyle.applicationWidth * 0.9

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.02
            horizontalCenter: parent.horizontalCenter
        }
    }

    Image {
        id: qrCode
        height: appStyle.applicationHeight * 0.35
        fillMode: Image.PreserveAspectFit
        sourceSize {
            height: qrCode.height
            width: qrCode.height
        }
        anchors {
            top: priceValueComponent.bottom
            topMargin: appStyle.applicationHeight * 0.01
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: orderNumberText
        text: orderNumberString
        //renderType: Text.NativeRendering

        font {
            family: appStyle.lightFontFamily
            pixelSize: 22 * appStyle.fontScale
        }

        anchors {
            top: qrCode.bottom
            topMargin: appStyle.applicationHeight * (-0.01)
            horizontalCenter: qrCode.horizontalCenter
        }
    }

    RectangleButton {
        id: confirmButton
        height: appStyle.applicationHeight * 0.1
        buttonText: qsTr("Abbrechen") + Settings.emptyString
        upStateColor: "#a6a6a6"
        downStateColor: "#808080"
        anchors {
            bottom: parent.bottom
            bottomMargin: appStyle.applicationHeight * 0.02
            horizontalCenter: parent.horizontalCenter
        }

        onButtonClicked: {
            UiController.showMessage(Enums.ErrorPopUp, qsTr("The payment is terminated") + Settings.emptyString)
            qrCodeScreen.backButtonClicked()
            currentOrderItem.clearData()
        }
    }
}
