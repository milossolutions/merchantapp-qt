import QtQuick 2.5
import QtQuick.Controls 1.4

import "qrc:/qml/components"
import "qrc:/qml/screens"

ScreenInterface {
    id: errorScreenPopUp

    anchors.fill: parent

    property int windowMessageType: 0

    signal closeClicked
    signal goToClicked

    function setFields(){
        if(windowMessageType === 0){

        }else if(windowMessageType === 1){

        }
    }

    property string errorTitleStringText: "tytul"
    property string errorSubTitleStringText:"jakis tam text"
    property string errorInputFieldStringtext: "Error"
    property string errorBottomStringtext: "Bottom error"

    SvgButton {
        id: closeButton

        height: appStyle.applicationHeight * 0.035

        downState: "qrc:/images/CloseButton"
        upState: "qrc:/images/CloseButton"

        opacity: 0.4

        anchors {
            top: parent.top
            right: line.right
            topMargin: appStyle.defCompSpacing
        }

        onClicked: {
            errorScreenPopUp.closeClicked()
        }
    }

    Rectangle {
        id: line
        color: "#262626"
        opacity: 0.2
        height: 1
        width: appStyle.applicationWidth * 0.9

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.08
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: errorTitleText
        text: errorTitleStringText
        //renderType: Text.NativeRendering

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.heavyFontFamily
        }

        anchors {
            top: line.bottom
            topMargin: appStyle.applicationHeight * 0.06
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: errorSubTitleText
        color: appStyle.defaultTextColor
        text: errorSubTitleStringText
        //renderType: Text.NativeRendering

        opacity: 0.8

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }

        anchors {
            top: errorTitleText.bottom
            topMargin: appStyle.applicationHeight * 0.04
            horizontalCenter: parent.horizontalCenter
        }
    }

    Rectangle {
        id: errorInputField
        width: parent.width
        height: appStyle.applicationHeight * 0.08
        color: appStyle.errorScreenColor

        anchors {
            top: errorSubTitleText.bottom
            topMargin: appStyle.applicationHeight * 0.04
            horizontalCenter: parent.horizontalCenter
        }

    }

    Text {
        id: errorInputFieldText
        text: errorInputFieldStringtext
        color: "white"
        //renderType: Text.NativeRendering

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }

        anchors {
            left: line.left
            verticalCenter: errorInputField.verticalCenter
        }
    }

    SvgButton {
        id: goToButton

        height: appStyle.applicationHeight * 0.05

        downState: "qrc:/images/CameraButton"
        upState: "qrc:/images/CameraButton"

        opacity: 0.4

        anchors {
            right: line.right
            verticalCenter: errorInputField.verticalCenter
        }

        onClicked: {
            errorScreenPopUp.goToClicked()
        }
    }

    Text {
        id: bottomText
        text: errorBottomStringtext
        color: appStyle.redTextColor
        //renderType: Text.NativeRendering

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.defaultFontFamily
            italic: true
            bold: true
        }

        anchors {
            top: errorInputField.bottom
            topMargin: appStyle.applicationHeight * 0.04
            left: line.left
        }
    }
}
