import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2

import "qrc:/qml/screens"
import "qrc:/qml/components"

import milosolutions.com 1.0

ScreenInterface {
    id: numpadScreen

    signal confirmClicked();
    signal backClicked();

    function setPaymentValue(value) {
        paymentScreen.setPrice(value)
    }

    onVisibleChanged: {
        ApplicationStatus.setCouponScreen(numpadScreen.visible)
    }

    WaitingComponent {
        id: waitingComponent
        z: 10
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.1
        }

        visible: false
    }

    Payment {
        id: paymentScreen

        width: appStyle.applicationWidth * 0.9
        height: parent.height * 0.68

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.08
            horizontalCenter: parent.horizontalCenter
            bottom: confirmButtom.top
            bottomMargin: appStyle.applicationHeight * 0.02
        }

        onStateChanged: {
            if(paymentScreen.state === "validValues"){
                numpadScreen.state = ""
            }else if(paymentScreen.state === "invalidValues"){
                numpadScreen.state = "invalidValues"
            }
        }

        onKeyboardVisibilityChanged: {
            if(keyboardVisibility){
                paymentScreen.anchors.bottom = virtualNumpad.top
            } else {
                paymentScreen.anchors.bottom = confirmButtom.top
            }
        }

    }

    RectangleButton {
        id: confirmButtom
        buttonText: qsTr("Collect") + Settings.emptyString
        z: 0

        anchors {
            bottom: cancelButton.top
            bottomMargin: appStyle.applicationHeight * 0.02
            horizontalCenter: parent.horizontalCenter
        }

        onButtonClicked: {
            confirmButtom.forceActiveFocus()
            numpadScreen.confirmClicked();
            UiController.startOrder(paymentScreen.getTotalPrice(),paymentScreen.getCheckedCoupons());
        }
    }

    RectangleButton {
        id: cancelButton
        buttonText: qsTr("Cancel") + Settings.emptyString
        z: 0
        upStateColor: "#a6a6a6"
        downStateColor: "#808080"

        anchors {
            bottom: numpadScreen.bottom
            bottomMargin: appStyle.applicationHeight * 0.02
            horizontalCenter: parent.horizontalCenter
        }

        onButtonClicked: {
            numpadScreen.backClicked()
            UiController.showMessage(Enums.ErrorPopUp, qsTr("The payment is terminated") + Settings.emptyString)
            currentOrderItem.clearData()
        }
    }


    VirtualNumpad {
        id: virtualNumpad
        width: appStyle.applicationWidth
        catchButton: true
        visible: paymentScreen.keyboardVisibility
        keyboardColumnState: "showButton"
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }

        onKeyboardClick: {
            paymentScreen.receivedKeyboardEvent(value)
        }

        onOkButtonClick: {
            paymentScreen.clearKeyboardFocus()
        }
    }

    states: [
        State {
            name: "invalidValues"
            PropertyChanges {
                target: confirmButtom
                enabled: false
                opacity: 0.4
            }
        }
    ]
}
