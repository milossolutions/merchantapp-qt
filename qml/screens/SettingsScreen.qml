import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

import "qrc:/qml/components"
import "qrc:/qml/screens"

import milosolutions.com 1.0

ScreenInterface {
    id: settingsScreen

    height: appStyle.applicationHeight
    width: appStyle.applicationWidth

    signal closeClicked
    signal leaveClicked
    signal logoutUser
    property string certificatePath: qsTr("")
    property var pathArray: []

    property string numericRadioStringText: qsTr("Numerical code") + Settings.emptyString
    property string aplhanumericRadioStringText: qsTr("Alphanumerical code") + Settings.emptyString

    onCertificatePathChanged: {
        settingsScreen.pathArray = certificatePath.split("/")
    }

    Connections {
        target: UiController

        onErrorCheckCallback: {
            if(errorCheckEnum === Enums.PasswordCheck){
                if(value === Enums.WrongPassword){
                    certificatePasswordField.state = "errorPasswordOccured"
                    certificatePasswordField.textField.focus = true

                    contentScroll.contentY = certificatePasswordField.y
                    buttonHandler(true)
                }else if(value === Enums.ImportSuccess){
                    if(deviceTypeComboBox.currentIndex === 0){
                        Settings.setDeviceType("USB")
                        Settings.setUsbPort(usbPortField.inputString)

                        UiController.performUsbCheck()
                    }else{
                        performSystemStatusCheck()
                    }
                }else if(value === Enums.FileNotExist){
                    if(Settings.certificateData === ""){
                        contentScroll.contentY = certificateField.y
                        buttonHandler(true)
                    }else{
                        if(deviceTypeComboBox.currentIndex === 0){
                            Settings.setDeviceType("USB")
                            Settings.setUsbPort(usbPortField.inputString)

                            UiController.performUsbCheck()
                        }else{
                            performSystemStatusCheck()
                        }
                    }
                }
            }else if(errorCheckEnum === Enums.UsbCheck){
                if(value){
                    performSystemStatusCheck()
                }else{
                    usbPortField.state = "errorUsbOccured"
                    if(settingsScreen.visible){
                        usbPortField.textField.focus = true
                    }

                    contentScroll.contentY = usbPortField.y
                    buttonHandler(true)
                }
            }else if(errorCheckEnum === Enums.SystemStatusCheck){
                if(value){
                    //saveSettings()
                    if(!Settings.isSelectUser){
                        UiController.enrollCashRegister("EPOS")
                    }
                    contentScroll.contentY = 0

                    closeClicked()

                    refreshDeviceModel()
                }else{
                    ApplicationStatus.setCurrentCashRegister("")
                    merchantUuidField.state = "errorMerchantUuid"
                    if(settingsScreen.visible){
                        merchantUuidField.textField.focus = true
                    }

                    contentScroll.contentY = merchantUuidField.y
                    ApplicationStatus.setCredentialsStatus(false)
                }
                buttonHandler(true)
            }
        }
    }

    //function disables button while settings are being save and enables them after finishing
    function buttonHandler(enable){
        if(enable){
            saveButtom.buttonState = ""
            cancelButtom.buttonState = ""
        }else{
            saveButtom.buttonState = "disabled"
            cancelButtom.buttonState = "disabled"
        }
    }

    function performPasswordCheck(){
        Settings.setCerfiticatePath(settingsScreen.certificatePath)
        Settings.setCertificatePassword(certificatePasswordField.inputString)

        UiController.performPasswordCheck();
    }

    function performUsbCheck(){
        Settings.setDeviceType("USB")
        Settings.setUsbPort(usbPortField.inputString)
    }

    function performSystemStatusCheck(){
        Settings.setCashRegisterId(cashRegisterIdField.inputString)
        Settings.setMerchantUuid(merchantUuidField.inputString)

        UiController.checkSystemStatus()
    }

    //Function required to change language of device types
    function refreshDeviceModel(){
        typeModel.clear()
        typeModel.append({type:0, text: qsTr("TWINT Beacon")})
        typeModel.append({type: 1, text: qsTr("No TWINT Beacon")})

        if(Settings.deviceType === "USB"){
            deviceTypeComboBox.currentIndex = 0
        }else{
            deviceTypeComboBox.currentIndex = 1
        }
    }

    function saveSettings(){
        //Reset input field states
        certificatePasswordField.state = "passwordType"
        usbPortField.state = ""
        merchantUuidField.state = ""

        if (runOnStartupCheckBox.isChecked()) {
            Settings.createAutorunRegistry(true);
        } else {
            Settings.createAutorunRegistry(false);
        }
        Settings.setMerchantAlias("")
        Settings.setCashRegisterId(cashRegisterIdField.inputString)
        Settings.setMerchantUuid(merchantUuidField.inputString)
        Settings.setCerfiticatePath(settingsScreen.certificatePath)
        Settings.setCertificatePassword(certificatePasswordField.inputString)
        Settings.setUsbPort(usbPortField.inputString)
        Settings.setKeackonUrl(wifiUrlField.inputString)
        Settings.setUrlPort(wifiPortField.inputString)
        Settings.setRunOnStartup(runOnStartupCheckBox.isChecked())

        if(deviceTypeComboBox.currentIndex === 0){
            Settings.setDeviceType("USB")
        }else if(deviceTypeComboBox.currentIndex === 1){
            Settings.setDeviceType("NONE")
        }

        if(englishRadio.checked === true){
            Settings.setLanguage(qsTr("en"))
        }else if(germanRadio.checked === true){
            Settings.setLanguage(qsTr("de"))
        }else if(frenchRadio.checked === true){
            Settings.setLanguage(qsTr("fr"))
        }else if(italianRadio.checked === true){
            Settings.setLanguage(qsTr("it"))
        }else{

        }

        if(selectUserCheckBox.isChecked() === true){
            //Disconnect beacon device on changing cash register type
            Settings.setIsSelectUser(true)
        }else if(selectUserCheckBox.isChecked() === false){
            Settings.setIsSelectUser(false)
        }

        if(showKeyboardCheckBox.isChecked() === true){
            Settings.setShowKeyboard(true)
        }else if(showKeyboardCheckBox.isChecked() === false){
            Settings.setShowKeyboard(false)
        }

        if(numericRadio.checked === true){
            Settings.setCashRegisterIdType(qsTr("Numeric"))
        }else if(aplhanumericRadio.checked === true){
            Settings.setCashRegisterIdType(qsTr("Alphanumeric"))
        }

        UiController.logoutUser()
        settingsScreen.logoutUser()

        Settings.storeSettings();

//        if(!Settings.isSelectUser){
//            UiController.enrollCashRegister("EPOS")
//        }

        UiController.updateSystemTray();

//        contentScroll.contentY = 0

//        closeClicked()

//        refreshDeviceModel()
    }

    function loadSettings(){
        //merchantAliasField.inputString = Settings.merchantAlias
        cashRegisterIdField.inputString = Settings.cashRegisterId
        merchantUuidField.inputString = Settings.merchantUuid
        settingsScreen.certificatePath = Settings.cerfiticatePath
        certificatePasswordField.inputString = Settings.certificatePassword
        usbPortField.inputString = Settings.usbPort
        wifiUrlField.inputString = Settings.keackonUrl
        wifiPortField.inputString = Settings.urlPort

        //ToDo Check why settings don't load default value
        if(Settings.deviceType === qsTr("")){
            deviceTypeComboBox.currentIndex = 0
        }else{
            if(Settings.deviceType === "USB"){
                deviceTypeComboBox.currentIndex = 0
            }else {
                deviceTypeComboBox.currentIndex = 1
            }
        }

        if(Settings.runOnStartup){
            runOnStartupCheckBox.state = "Checked"
        }else{
            runOnStartupCheckBox.state = "UnChecked"
        }

        if(Settings.isSelectUser){
            selectUserCheckBox.state = "Checked"
        }else{
            selectUserCheckBox.state = "UnChecked"
        }

        if(Settings.showKeyboard){
            showKeyboardCheckBox.state = "Checked"
        }else{
            showKeyboardCheckBox.state = "UnChecked"
        }

        if(Settings.language === qsTr("en")){
            englishRadio.checked = true
        }else if(Settings.language === qsTr("de")){
            germanRadio.checked = true
        }else if(Settings.language === qsTr("fr")){
            frenchRadio.checked = true
        }else if(Settings.language === qsTr("it")){
            italianRadio.checked = true
        }

        if(Settings.cashRegisterIdType === qsTr("Numeric")){
            numericRadio.checked = true
        }else if(Settings.cashRegisterIdType === qsTr("Alphanumeric")){
            aplhanumericRadio.checked = true
        }
    }

    Component.onCompleted: {
        loadSettings()
    }

    Flickable {
        id: contentScroll
        width: appStyle.applicationWidth * 0.90

        boundsBehavior:
            Flickable.StopAtBounds

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        contentHeight: settingsColumn.height

        property alias settingsColumnAlias: settingsColumn

        Column {
            id: settingsColumn

            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            width: appStyle.applicationWidth * 0.85
            spacing: appStyle.applicationHeight * 0.03

            Connections {
                target: ApplicationStatus
                onDiscoverdPortChanged: usbPortField.textField.text = discoverdPort
            }

            Item {
                width: appStyle.applicationWidth * 0.85
                height: appStyle.applicationHeight * 0.02
            }

            SvgButton {
                id: twintLogo
                height: appStyle.applicationHeight * 0.09
                downState: "qrc:/images/LoginLogoImage"
                upState: "qrc:/images/LoginLogoImage"

                onClicked: {
                    loadSettings()
                    closeClicked()
                }

                anchors {
                    horizontalCenter: cashRegisterIdField.horizontalCenter
                }

                Text {
                    id: logoText

                    text: qsTr("TWINT MERCHANT") + Settings.emptyString
                    color: "#262626"
                    opacity: 0.4
                    //renderType: Text.NativeRendering

                    anchors {
                        top: twintLogo.bottom
                        topMargin: appStyle.applicationHeight * 0.02
                        horizontalCenter: parent.horizontalCenter
                    }

                    font {
                        family: appStyle.heavyFontFamily
                        pixelSize: 12 * appStyle.fontScale
                    }
                }
            }

            Item {
                width: appStyle.applicationWidth * 0.85
                height: appStyle.applicationHeight * 0.06
            }

            Text {
                id: titleText

                text: qsTr("Setup") + Settings.emptyString
                color: appStyle.defaultTextColor
                opacity: 0.4
                //renderType: Text.NativeRendering

                anchors {
                    horizontalCenter: cashRegisterIdField.horizontalCenter
                }

                font {
                    family: appStyle.heavyFontFamily
                    pixelSize: 21 * appStyle.fontScale
                }
            }

            TwoLevelInput {
                id: cashRegisterIdField
                labelText: qsTr("Cash Register Id") + Settings.emptyString
            }

            TwoLevelInput {
                id: merchantUuidField
                labelText: qsTr("Merchant UUID") + Settings.emptyString
                inputType: "uuid"
            }

            TwoLevelInput {
                id: certificateField
                labelText: qsTr("Certificate") + Settings.emptyString
                textField.text: settingsScreen.pathArray[pathArray.length-1]

                onCertificatePathChanged: {
                    settingsScreen.certificatePath = path
                }
            }

            ButtonWithBorder {
                id: browseButton
                upStateColor: "white"
                downStateColor: appStyle.defaultTextColor
                upStateTextColor: appStyle.defaultTextColor
                downStateTextColor: "white"


                buttonText: qsTr("Browse Certificate") + Settings.emptyString

                borderColor: appStyle.defaultTextColor
                opacity: 0.4
                width: appStyle.applicationWidth * 0.75
                height: appStyle.applicationHeight * 0.07

                anchors {
                    horizontalCenter: cashRegisterIdField.horizontalCenter
                }

                onButtonClicked: {
                    fileDialog.open()
                }
            }

            TwoLevelInput {
                id: certificatePasswordField
                labelText: qsTr("Password") + Settings.emptyString
                state: "passwordType"
                inputType: "password"
            }

            CustomComboBox {
                id: deviceTypeComboBox
                property int deviceType

                anchors {
                    left: cashRegisterIdField.left
                }

                width: appStyle.applicationWidth * 0.9
                height: appStyle.applicationHeight * 0.05
                z: state === "expanded" ? 5 : 0
                property int lastIndex: 0

                onLastIndexChanged: {
                    if(lastIndex === 0){
                        usbPortField.visible = true
                        wifiUrlField.visible = false
                        wifiPortField.visible = false
                        scanUsbButton.visible = true
                    }else if(lastIndex === 1){
                        usbPortField.visible = false
                        wifiUrlField.visible = false
                        wifiPortField.visible = false
                        scanUsbButton.visible = false
                    }
                }

                onIndexChanged: {
                    deviceTypeComboBox.deviceType = model.get(currentIndex).type;
                    lastIndex = currentIndex;

                }

                model: ListModel {
                    id: typeModel

                    ListElement {
                        type: 0
                        text: qsTr("TWINT Beacon")
                    }
                    ListElement {
                        type: 1
                        text: qsTr("No TWINT Beacon")
                    }
                }
            }

            TwoLevelInput {
                id: usbPortField
                labelText: qsTr("USB port") + Settings.emptyString
                isDialogType: true
            }

            TwoLevelInput {
                id: wifiUrlField
                labelText: qsTr("Beacon URL") + Settings.emptyString
                visible: false
            }

            TwoLevelInput {
                id: wifiPortField
                labelText: qsTr("URL Port") + Settings.emptyString
                visible: false
            }

            ButtonWithBorder {
                id: scanUsbButton
                upStateColor: "white"
                downStateColor: appStyle.defaultTextColor
                upStateTextColor: appStyle.defaultTextColor
                downStateTextColor: "white"


                buttonText: qsTr("Connect TWINT Beacon") + Settings.emptyString

                borderColor: appStyle.defaultTextColor
                opacity: 0.4
                width: appStyle.applicationWidth * 0.75
                height: appStyle.applicationHeight * 0.07

                anchors {
                    horizontalCenter: titleText.horizontalCenter
                }

                onButtonClicked: {
                    UiController.discoverUsbPort();
                }
            }

            Row {
                width: parent.width
                spacing: appStyle.applicationWidth * 0.03
                height: appStyle.applicationHeight * 0.04

                SvgCheckbox {
                    id: runOnStartupCheckBox

                    height: appStyle.applicationHeight * 0.04
                    width: height

                    onToggled: {

                    }
                }

                Text {
                    id: runOnStartupText
                    text: qsTr("Let TWINT solution run \n actively in the background") + Settings.emptyString
                    color: appStyle.defaultTextColor
                    opacity: 0.5
                    //renderType: Text.NativeRendering

                    anchors {
                        verticalCenter: runOnStartupCheckBox.verticalCenter
                    }

                    font {
                        family: appStyle.defaultFontFamily
                        pixelSize: 14 * appStyle.fontScale
                    }
                }
            }

            Row {
                width: parent.width
                spacing: appStyle.applicationWidth * 0.03
                height: appStyle.applicationHeight * 0.04

                SvgCheckbox {
                    id: showKeyboardCheckBox

                    height: appStyle.applicationHeight * 0.04
                    width: height

                    onToggled: {

                    }
                }

                Text {
                    id: showKeyboardText
                    text: qsTr("Show keyboard") + Settings.emptyString
                    color: appStyle.defaultTextColor
                    opacity: 0.5
                    //renderType: Text.NativeRendering

                    anchors {
                        verticalCenter: showKeyboardCheckBox.verticalCenter
                    }

                    font {
                        family: appStyle.defaultFontFamily
                        pixelSize: 14 * appStyle.fontScale
                    }
                }
            }

            Row {
                width: parent.width
                spacing: appStyle.applicationWidth * 0.03
                height: appStyle.applicationHeight * 0.04

                SvgCheckbox {
                    id: selectUserCheckBox

                    height: appStyle.applicationHeight * 0.04
                    width: height

                    onToggled: {

                    }
                }

                Text {
                    id: selectUserText
                    text: qsTr("Select User") + Settings.emptyString
                    color: appStyle.defaultTextColor
                    opacity: 0.5
                    //renderType: Text.NativeRendering

                    anchors {
                        verticalCenter: selectUserCheckBox.verticalCenter
                    }

                    font {
                        family: appStyle.defaultFontFamily
                        pixelSize: 14 * appStyle.fontScale
                    }
                }
            }

            Column {
                id: userInputTypeColumn

                spacing: appStyle.applicationHeight * 0.02

                visible: selectUserCheckBox.isChecked()

                CustomExclusiveGroup { id: userInputType }

                SettingsRadioButton {
                    id: numericRadio
                    text: numericRadioStringText
                    exclusiveGroup: userInputType
                }

                SettingsRadioButton {
                    id: aplhanumericRadio
                    text: aplhanumericRadioStringText
                    exclusiveGroup: userInputType
                }
            }

            Text {
                id: languageTextLabel
                text: qsTr("Language") + Settings.emptyString
                color: appStyle.defaultTextColor
                opacity: 0.5
                //renderType: Text.NativeRendering

                anchors {
                    left: cashRegisterIdField.left
                }

                font {
                    family: appStyle.defaultFontFamily
                    pixelSize: 14 * appStyle.fontScale
                }
            }

            Column {
                id: languageInputTypeColumn

                spacing: appStyle.applicationHeight * 0.02

                CustomExclusiveGroup { id: languageInputType }

                SettingsRadioButton {
                    id: englishRadio
                    text: qsTr("English")
                    exclusiveGroup: languageInputType
                }

                SettingsRadioButton {
                    id: germanRadio
                    text: qsTr("Deutsch")
                    exclusiveGroup: languageInputType
                }

                SettingsRadioButton {
                    id: frenchRadio
                    text: qsTr("Français")
                    exclusiveGroup: languageInputType
                }

                SettingsRadioButton {
                    id: italianRadio
                    text: qsTr("Italiano")
                    exclusiveGroup: languageInputType
                }
            }

            RectangleButton {
                id: saveButtom

                width: appStyle.applicationWidth * 0.80
                height: appStyle.applicationHeight * 0.07

                buttonText: qsTr("Connect with Beacon") + Settings.emptyString

                anchors {
                    horizontalCenter: cashRegisterIdField.horizontalCenter
                }

                onButtonClicked: {
                    certificateField.state = ""
                    buttonHandler(false)
                    saveSettings()
                    performPasswordCheck()
                }
            }

            RectangleButton {
                id: cancelButtom

                width: appStyle.applicationWidth * 0.80
                height: appStyle.applicationHeight * 0.07

                upStateColor: "#a6a6a6"
                downStateColor: "#808080"

                buttonText: qsTr("Cancel") + Settings.emptyString

                anchors {
                    horizontalCenter: cashRegisterIdField.horizontalCenter
                }


                onButtonClicked: {
                    contentScroll.contentY = 0
                    loadSettings()
                    closeClicked()
                }
            }

            Text {
                id: applicationVersionText

                text: "Application Version " + Settings.applicationVersion
                color: "#262626"
                opacity: 0.4

                anchors {
                    right: cancelButtom.right
                }

                font {
                    family: appStyle.heavyFontFamily
                    pixelSize: 12 * appStyle.fontScale
                }
            }

            Text {
                id: firmwareVersionText

                text: "Firmware Version " + ApplicationStatus.firmwareVersion
                color: "#262626"
                opacity: 0.4

                visible: ApplicationStatus.firmwareVersion !== "" ? true : false

                anchors {
                    right: cancelButtom.right
                }

                font {
                    family: appStyle.heavyFontFamily
                    pixelSize: 12 * appStyle.fontScale
                }
            }

            Item {
                width: appStyle.applicationWidth * 0.85
                height: appStyle.applicationHeight * 0.01
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose certificate"
        selectExisting: true
        nameFilters: [ "*.p12" ]
        onAccepted: {
            settingsScreen.certificatePath = UiController.getFilePath(fileUrl)
            certificateField.textField.text = settingsScreen.pathArray[pathArray.length-1]
            fileDialog.close()
        }
        onRejected: {
            fileDialog.close()
        }
        Component.onCompleted: visible = false
    }

    ErrorScreenPopUp {
        id: errorScreenPopUp
        z: 101
        visible: false

        onCloseClicked: {
            errorScreenPopUp.visible = false
        }
    }

}

