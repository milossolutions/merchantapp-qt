import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2

import "qrc:/qml/components"
import milosolutions.com 1.0

ScreenInterface {
    id: keyboardScreen
    width: appStyle.applicationWidth

    Component.onCompleted: {
        setPaymentPosibleStatus()
        setErrorText()
    }

    property bool paymentPosibleStatus: false

    signal pairingByBeacon(var value);
    signal setCashRegisterInputState(var value)
    signal keyboardClickEvent()

    function setPaymentPosibleStatus(){
        if(!ApplicationStatus.currentEnrollStatus){
            paymentPosibleStatus = false
        }else{
            paymentPosibleStatus = true
        }
    }

    function setErrorText(){
        if(!ApplicationStatus.hostConnection){
            errorText.text = qsTr("Please make sure your system is connected to the internet") + Settings.emptyString
            errorText.visible = true;
            return;
        }

        if(Settings.merchantUuid === ""){
            errorText.text = qsTr("First configure the app. To do so, click on the settings symbol.") + Settings.emptyString
            errorText.visible = true;
            return;
        }else{
            if(!ApplicationStatus.credentialsStatus){
                errorText.text = qsTr("First configure the app. To do so, click on the settings symbol.") + Settings.emptyString
                errorText.visible = true;
                return;
            }else{
                if(Settings.deviceType === "NONE"){
                    errorText.visible = false;
                    return;
                }else{
                    if(ApplicationStatus.beaconConnectionStatus === Enums.Connected){
                        errorText.visible = false;
                        return;
                    }else if(ApplicationStatus.beaconConnectionStatus === Enums.Connecting){
                        errorText.text = qsTr("Beacon connecting...") + Settings.emptyString
                        errorText.visible = true;
                        return;
                    }else if(ApplicationStatus.beaconConnectionStatus === Enums.NotConnected){
                        errorText.text = qsTr("Beacon not connected") + Settings.emptyString
                        errorText.visible = true;
                        return;
                    }
                }
            }
        }
    }

    Connections {
        target: Settings

        onIsSelectUserChanged: {
            setPaymentPosibleStatus()
        }

        onDeviceTypeChanged: {
            setErrorText()
        }
    }

    Connections {
        target: ApplicationStatus

        onCurrentEnrollStatusChanged: {
            setPaymentPosibleStatus()
        }

        onBeaconConnectionStatusChanged: {
            setErrorText()
        }

        onCredentialsStatusChanged: {
            setErrorText()
        }

        onHostConnectionChanged: {
            setErrorText()
        }
    }

    WaitingComponent {
        id: waitingComponent
        z: 10
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.1
        }

        visible: false
    }

    Text {
        id: errorText
        color: appStyle.redTextColor
        text: ""
        visible: false
        wrapMode: Text.Wrap
        width: priceField.width
        horizontalAlignment: Text.AlignHCenter

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.heavyFontFamily
        }

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.03
            horizontalCenter: parent.horizontalCenter
        }
    }

    InputComponent {
        id: priceField
        width: appStyle.applicationWidth * 0.9
        height: appStyle.applicationHeight * 0.13
        enabled: ApplicationStatus.currentEnrollStatus
        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.1
            horizontalCenter: parent.horizontalCenter
        }

        onTextChanged: {
            if(text>0){
                confirmButtom.buttonState = ""
            }else{
                confirmButtom.buttonState = "disabled"
            }
        }

        textField.validator: RegExpValidator { regExp: /([0-9]?[0-9]?[0-9]?[0-9]?)?([.,][0-9]?[0-9]?)?/ }
    }

    VirtualNumpad {
        id: virtualNumpad
        width: appStyle.applicationWidth

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: priceField.bottom
            topMargin: appStyle.applicationHeight * 0.02
        }
    }

    RectangleButton {
        id: confirmButtom

        buttonText: qsTr("Collect") + Settings.emptyString

        anchors {
            bottom: parent.bottom
            bottomMargin: appStyle.applicationHeight * 0.02
            horizontalCenter: parent.horizontalCenter
        }

        buttonState: "disabled"

        onButtonClicked: {
            if(ApplicationStatus.credentialsStatus){
                if(Settings.isSelectUser){
                    if(ApplicationStatus.currentCashRegister){
                        UiController.performRegisterCheckIn()
                        currentOrderItem.setOrderValue(Number(priceField.text).toFixed(2))
                        priceField.textField.text = ""
                    }else{
                        keyboardScreen.setCashRegisterInputState(true)
                    }
                }else{
                    UiController.performRegisterCheckIn()
                    currentOrderItem.setOrderValue(Number(priceField.text).toFixed(2))
                    priceField.textField.text = ""
                }
            }
        }
    }
}
