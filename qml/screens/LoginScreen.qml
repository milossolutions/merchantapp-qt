import QtQuick 2.4

import "qrc:/qml/screens"
import "qrc:/qml/components"

ScreenInterface {
    id: loginScreen

    property string login: loginField.text
    property string password: passwordField.text
    signal loginClicked

    SvgButton {
        id: loginLogo
        downState: "qrc:/images/LoginLogoImage"
        upState: "qrc:/images/LoginLogoImage"
        height: appStyle.loginLogoHeight
        fillMode: Image.PreserveAspectFit

        anchors {
            top: parent.top
            topMargin: parent.height * 0.04
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: twintHandlerText
        color: appStyle.defaultTextColor
        text: Settings.applicationName
        //renderType: Text.NativeRendering

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }

        anchors {
            top: loginLogo.bottom
            topMargin: appStyle.applicationHeight * 0.03
            horizontalCenter: parent.horizontalCenter
        }
    }

    InputComponent{
        id: loginField
        anchors {
            top: twintHandlerText.top
            topMargin: appStyle.defInputMargin
            horizontalCenter: parent.horizontalCenter
        }
    }

    InputComponent{
        id: passwordField
        isPasswordType: true
        anchors {
            top: loginField.bottom
            topMargin: appStyle.defInputMargin
            horizontalCenter: parent.horizontalCenter
        }
    }

    RectangleButton {
        id: confirmButton
        downStateColor: "#00BD62"
        anchors {
            top: passwordField.bottom
            topMargin: appStyle.defInputMargin
            horizontalCenter: parent.horizontalCenter
        }
        onButtonClicked: {
            loginScreen.loginClicked()
        }
    }

    Text {
        id: twintBottomText
        text: qsTr("TWINT Handlerportal") + Settings.emptyString
        color: "#252525"
        opacity: 0.5
        //renderType: Text.NativeRendering

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.defaultFontFamily
            underline: true
        }

        anchors {
            top: confirmButton.bottom
            topMargin: appStyle.applicationHeight * 0.09
            horizontalCenter: parent.horizontalCenter
        }
    }

    function setUserData(login, password) {
        loginField.setTextFiledText(login)
        passwordField.setTextFiledText(password)
    }

    function logIn() {
        loginScreen.loginClicked()
    }
}
