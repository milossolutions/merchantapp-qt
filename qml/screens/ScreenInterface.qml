import QtQuick 2.0

Item {
    id: screenItem
    width: appStyle.applicationWidth
    height: appStyle.applicationHeight
    property string backgroundColor: appStyle.defaultBgColor

    property int pageId: 0
    property double backgroundOpacity: 1

    MouseArea {
        id: mouseArea

        anchors {
            fill: parent
        }

        onClicked: {
            mouseArea.forceActiveFocus()
        }
    }

    Rectangle {
        id: background
        color: screenItem.backgroundColor
        opacity: backgroundOpacity
        anchors {
            fill: parent
        }
    }
}
