import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

import QMLEnums 1.0

import "qrc:/qml/screens"
import "qrc:/qml/components"


ScreenInterface {
    id: detailsScreen

    property var lastYPostition: 0

    signal elementClicked(int elementIndex);


    width: appStyle.applicationWidth

    anchors {
        top: parent.top
        bottom: parent.bottom
        horizontalCenter: parent.horizontalCenter
    }

    onHeightChanged: {
        lastYPostition = billList.contentY
        billList.model = 0;
        billList.model = newOrderListModel
        billList.contentY = lastYPostition
    }

    ListView {
        id: billList
        height: parent.height
        width: appStyle.applicationWidth
        model: newOrderListModel
        boundsBehavior: Flickable.StopAtBounds
        clip: true
        bottomMargin: appStyle.applicationHeight * 0.04

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        section {
            property: "orderDate"
            delegate: NewBillSectionDelegate {
            }
        }

        delegate: NewBillListDelegate {
            MouseArea {
                anchors {
                    fill: parent
                }
                onClicked: {
                    if(!isSumarryItem){
                        detailsScreen.elementClicked(index)
                    }
                }
            }
        }
    }
}
