import QtQuick 2.5
import QtQuick.Controls 1.4

import "qrc:/qml/components"
import "qrc:/qml/screens"

ScreenInterface {
    id: gearSettingsScreen
    backgroundOpacity: 0.5
    backgroundColor: "#262626"
    visible: false

    signal closeSettings
    signal settingsClicked
    signal exitClicked

    Rectangle {
        id: triangleBackground
        color: "#f5f5f5"

        height: appStyle.applicationHeight * 0.03
        width: height

        transform: Rotation {  angle: 45}

        anchors {
            bottom: backgroundRectangle.top
            bottomMargin: -appStyle.applicationHeight * 0.015
            right: backgroundRectangle.right
            rightMargin: appStyle.applicationWidth * 0.005
        }
    }

    Rectangle {
        id: backgroundRectangle
        radius: 8
        color: "#f5f5f5"
        width: appStyle.applicationWidth * 0.7
        height: (appStyle.applicationHeight * 0.16) + 1
        z:109

        anchors {
            fill: baRectangle
        }
    }
    Rectangle {
        id: baRectangle
        radius: 8
        color: "#f5f5f5"
        width: appStyle.applicationWidth * 0.7
        height: (appStyle.applicationHeight * 0.16) + 1
        z:110

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.08
            right: parent.right
            rightMargin: (appStyle.applicationWidth * 0.03) + ((appStyle.realApplicationWidth - appStyle.applicationWidth) / 2)
        }
        Column {
            id: buttonColumn

            anchors {
                fill: parent
            }

            GearSettingsButton {
                id: settingsButton
                buttonTextString: qsTr("Settings") + Settings.emptyString
                onButtonClicked: {
                    gearSettingsScreen.settingsClicked()
                }
            }

            Rectangle {
                width: parent.width
                height: 1
                color: "#808080"
                opacity: 0.5
            }

            GearSettingsButton {
                id: exitButton
                buttonTextString: qsTr("Exit") + Settings.emptyString
                onButtonClicked: {
                    gearSettingsScreen.exitClicked()
                }
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onReleased: {
            gearSettingsScreen.closeSettings()
        }
    }

    states: [
        State {
            name: "first"
            PropertyChanges {
                target: gearSettingsScreen
                opacity: 1
            }
        },
        State {
            name: "second"
        }
    ]

    transitions: [
        Transition {
            from: "first";
            to: "second";
            enabled: true

            onRunningChanged: {
                if( running === false){
                    gearSettingsScreen.visible = false
                }
            }
            PropertyAnimation {
                target: gearSettingsScreen
                duration: 200
                property: "opacity"
                from: 1
                to: 0
            }
        }
    ]
}
