import QtQuick 2.5
import QtQuick.Controls 1.4

import "qrc:/qml/components"
import "qrc:/qml/screens"

ScreenInterface {
    id: reverseConfirmationPopUp

    backgroundOpacity: 0.35
    backgroundColor: "#262626"

    signal confirmClicked
    signal cancelClicked

    Rectangle {
        id: popUpRectangle
        color: "white"
        width: appStyle.applicationWidth * 0.70
        height: appStyle.applicationHeight * 0.30

        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        Text {
            id: titleText
            color: appStyle.defaultTextColor
            text: qsTr("Reversal") + Settings.emptyString
            //renderType: Text.NativeRendering

            font {
                pixelSize: 16 * appStyle.fontScale
                family: appStyle.heavyFontFamily
                bold: true
            }

            anchors {
                left: parent.left
                top: parent.top
                leftMargin: parent.height * 0.1
                topMargin: parent.height * 0.15
            }
        }

        Text {
            id: questionText
            color: appStyle.defaultTextColor
            text: qsTr("Are you sure?") + Settings.emptyString
            //renderType: Text.NativeRendering

            font {
                pixelSize: 16 * appStyle.fontScale
                family: appStyle.heavyFontFamily
            }

            anchors {
                left: parent.left
                top: titleText.bottom
                leftMargin: parent.height * 0.1
                topMargin: parent.height * 0.15
            }
        }

        TransparentTextButton {
            id: cancelButton
            buttonTextString: qsTr("Cancel") + Settings.emptyString
            anchors {
                left: parent.left
                bottom: parent.bottom
            }

            onButtonClicked: {
                reverseConfirmationPopUp.cancelClicked()
            }
        }

        TransparentTextButton {
            id: confirmButton
            buttonTextString: qsTr("Ok") + Settings.emptyString
            upStateButtonColor: appStyle.greenTextColor
            anchors {
                right: parent.right
                bottom: parent.bottom
            }

            onButtonClicked: {
                reverseConfirmationPopUp.confirmClicked()
            }
        }
    }
}
