import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QMLEnums 1.0

import "qrc:/qml/components"

ScreenInterface {
    id: cashRegisterPopUp

    state: "Login"
    backgroundOpacity: 0.7
    backgroundColor: "#262626"

    signal closeClicked

    property bool activeButton: false


    Component.onCompleted: {
        setValidator()
    }

    onVisibleChanged: {
        if(cashRegisterPopUp.visible){
            UiController.showSystemKeyboard();
            cashRegisterField.inputFieldAlias.forceActiveFocus()
        }
    }

    function setValidator(){
        if(Settings.cashRegisterIdType === "Numeric"){
            cashRegisterField.textField.validator = numericValidator
        }else {
            cashRegisterField.textField.validator = alphanumericValidator
        }
    }

    function logoutCalled(){
        cashRegisterPopUp.state = "Login"
        cashRegisterField.textField.text = ""
        ApplicationStatus.setCurrentCashRegister("")
    }

    RegExpValidator {
        id: numericValidator
        regExp: /^\d{1,3}$/
    }

    RegExpValidator {
        id: alphanumericValidator
        regExp: /^([a-zA-Z0-9_-]){1,15}$/
    }

    Connections {
        target: Settings

        onCashRegisterIdTypeChanged: {
            setValidator()
        }
    }

    Rectangle {
        id: popUpRectangle
        radius: 8
        color: "#f5f5f5"
        width: appStyle.applicationWidth * 0.66
        height: appStyle.applicationHeight * 0.27

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.11
        }

        Text {
            id: titleText

            color: appStyle.greenTextColor
            opacity: 0.8
            //renderType: Text.NativeRendering

            font {
                pixelSize: 13 * appStyle.fontScale
                family: appStyle.defaultFontFamily
            }

            anchors {
                top: popUpRectangle.top
                left: popUpRectangle.left
                topMargin: appStyle.defCompSpacing
                leftMargin: appStyle.defCompSpacing
            }
        }

        Text {
            id: placeholderText
            color: appStyle.defaultTextColor
            text: qsTr("Insert Username") + Settings.emptyString
            opacity: 0.5
            //renderType: Text.NativeRendering

            visible: true

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 17 * appStyle.fontScale
            }

            anchors {
                bottom: loginButton.top
                bottomMargin: popUpRectangle.height * 0.24
                horizontalCenter: cashRegisterField.horizontalCenter
            }
        }

        InputComponent {
            id: cashRegisterField
            width: popUpRectangle.width * 0.9
            height: popUpRectangle.height * 0.23

            fontPixelSize: 17

            anchors {
                bottom: loginButton.top
                bottomMargin: popUpRectangle.height * 0.15
                horizontalCenter: popUpRectangle.horizontalCenter
            }

            onTextChanged: {
                if(Settings.cashRegisterIdType === "Numeric"){
                    if(cashRegisterField.text.length === 3){
                        loginButton.enabled = true
                        loginButton.buttonAlias.state = ""
                    }else{
                        if(cashRegisterField.text.length > 0){
                            placeholderText.visible = false
                        }else{
                            placeholderText.visible = true
                        }

                        loginButton.enabled = false
                        loginButton.buttonAlias.state = "disabled"
                    }
                }else{
                    if(cashRegisterField.text.length > 0){
                        placeholderText.visible = false
                        loginButton.enabled = true
                        loginButton.buttonAlias.state = ""
                    }else{
                        placeholderText.visible = true
                        loginButton.enabled = false
                        loginButton.buttonAlias.state = "disabled"
                    }
                }
            }

        }

        ButtonWithBorder {
            id: loginButton
            enabled: false
            upStateColor: "white"
            downStateColor: "#737373"
            upStateTextColor: "#737373"
            downStateTextColor: "white"

            borderColor: "#737373"
            width: popUpRectangle.width * 0.9
            height: popUpRectangle.height * 0.23

            anchors {
                horizontalCenter: popUpRectangle.horizontalCenter
                bottom: popUpRectangle.bottom
                bottomMargin: appStyle.defCompSpacing
            }

            Component.onCompleted: buttonAlias.state = "disabled"

            onButtonClicked: {
                if(cashRegisterPopUp.state === "Login"){
                    ApplicationStatus.setCurrentCashRegister(cashRegisterField.text)
                    cashRegisterPopUp.state = "Logout"
                    cashRegisterPopUp.closeClicked()
                    UiController.enrollCashRegister("EPOS")
                }else{
                    cashRegisterPopUp.state = "Login"
                    cashRegisterField.textField.text = ""
                    ApplicationStatus.setCurrentCashRegister("")
                    cashRegisterPopUp.closeClicked()
                    UiController.logoutUser()
                }
            }
        }

        SvgButton {
            id: closeButton
            height: popUpRectangle.height * 0.1

            downState: "qrc:/images/CloseButton"
            upState: "qrc:/images/CloseButton"

            anchors {
                top: parent.top
                right: parent.right
                topMargin: appStyle.defCompSpacing
                rightMargin: appStyle.defCompSpacing
            }

            onClicked: {
                cashRegisterPopUp.closeClicked()
            }
        }
    }

    states: [
        State {
            name: "Login"
            PropertyChanges {
                target: titleText
                text: qsTr("User Login") + Settings.emptyString
            }
            PropertyChanges {
                target: loginButton
                buttonText: qsTr("Login") + Settings.emptyString
            }
            PropertyChanges {
                target: cashRegisterField.textField
                enabled: true
            }
            PropertyChanges {
                target: placeholderText
                visible: true
            }
        },
        State {
            name: "Logout"
            PropertyChanges {
                target: titleText
                text: qsTr("User Logout") + Settings.emptyString
            }
            PropertyChanges {
                target: loginButton
                buttonText: qsTr("Logout") + Settings.emptyString
            }
            PropertyChanges {
                target: cashRegisterField.textField
                enabled: false
            }
        }
    ]
}
