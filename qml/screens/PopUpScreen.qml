import QtQuick 2.4

import "qrc:/qml/screens"
import "qrc:/qml/components"

ScreenInterface {
    id: popUpScreen
    width: appStyle.applicationWidth
    height: appStyle.applicationHeight
    backgroundColor: appStyle.confirmScreenColor

    signal confirmClicked();

    property string informationText: qsTr("text")

    SvgButton {
        id: image
        height: appStyle.applicationHeight * 0.17
        downState: "qrc:/images/DoneBigImage"
        upState: "qrc:/images/DoneBigImage"

        anchors {
            top: parent.top
            topMargin: popUpScreen.height * 0.35
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: popUpText
        color: "#FFFFFF"
        text: informationText
        wrapMode: Text.Wrap
        horizontalAlignment: Text.AlignHCenter
        //renderType: Text.NativeRendering

        font {
            pixelSize: 17 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }

        anchors {
            top: image.bottom
            topMargin: appStyle.defInputMargin
            left: popUpScreen.left
            right: popUpScreen.right
        }
    }

    RectangleButton {
        id: confirmButton
        width: appStyle.applicationWidth
        upStateColor: "#FFFFFF"
        downStateColor: "#FFFFFF"
        textColor: appStyle.defaultTextColor
        buttonRadius: 0

        buttonText: qsTr("Schliessen") + Settings.emptyString

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        MouseArea {
            id: confirmButtonMouseArea
            anchors.fill: parent

            onClicked: {
                popUpScreen.confirmClicked();
            }
        }
    }


    onVisibleChanged: {
        if (visible == true && state == "ErrorPopUp") {
            UiController.cancelCheckIn("PAYMENT_ABORT")
        }
    }

    states: [
        State{
            name: "ErrorPopUp"
            PropertyChanges {
                target: popUpScreen
                backgroundColor: appStyle.errorScreenColor
            }
            PropertyChanges {
                target: image
                downState: "qrc:/images/XButtonImage"
                upState: "qrc:/images/XButtonImage"
            }
            PropertyChanges {
                target: popUpText
            }
        },
        State{
            name: "ConfirmPopUp"
            PropertyChanges {
                target: popUpScreen
                backgroundColor: appStyle.confirmScreenColor
            }
            PropertyChanges {
                target: image
                downState: "qrc:/images/DoneBigImage"
                upState: "qrc:/images/DoneBigImage"
            }
            PropertyChanges {
                target: popUpText
            }
        }

    ]
}
