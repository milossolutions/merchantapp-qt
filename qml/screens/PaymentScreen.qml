import QtQuick 2.4
import QtQuick.Controls 1.3

import "qrc:/qml/components"
import "qrc:/qml/screens"
import milosolutions.com 1.0


ScreenInterface {
    id: paymentScreen

    signal setCashRegisterInputState(var value)
    signal keyboardClickEvent()


    function replacePageToDefault(){
        stackView.replacePage(Enums.KeyboardScreen)
    }

    CustomStackView {
        id: stackView
        anchors.fill: parent

        initializationList: [
            {
                "pageId": Enums.QrCodeScreen,
                "pageSource": "qrc:/qml/screens/QrCodeScreen.qml"
            },
            {
                "pageId": Enums.KeyboardScreen,
                "pageSource": "qrc:/qml/screens/KeyboardScreen.qml"
            },
        ]

        onInitialized: {
            stackView.pushPage(Enums.KeyboardScreen)

            qrCodePageHandlers.target = stackView.items[Enums.QrCodeScreen]
            keyboardPageHandlers.target = stackView.items[Enums.KeyboardScreen]
        }

    }

    WaitingComponent {
        id: waitComponent

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.1
            horizontalCenter: parent.horizontalCenter
        }

        visible: false
    }

    Connections {
        id: keyboardPageHandlers
        ignoreUnknownSignals: true

        onPairingByBeacon: {
            stackView.items[Enums.NumpadScreen].setPaymentValue(value)
            stackView.pushPage(Enums.NumpadScreen)
        }

        onSetCashRegisterInputState: {
            paymentScreen.setCashRegisterInputState(value)
        }

        onKeyboardClickEvent: {
            paymentScreen.keyboardClickEvent()
        }
    }

    Connections {

    }

    Connections {
        id: qrCodePageHandlers
        ignoreUnknownSignals: true

        onBackButtonClicked: {
            stackView.replacePage(Enums.KeyboardScreen)
        }
    }

    Connections {
        target: currentOrderItem

        onImageDataChanged: {
            stackView.replacePage(Enums.QrCodeScreen)
        }
    }

    Connections {
        target: UiController

        onShowMessage: {
            stackView.goBackToFirst();
            stackView.replacePage(Enums.KeyboardScreen)
        }
    }
}
