import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

import "qrc:/qml/screens"
import "qrc:/qml/components"
import milosolutions.com 1.0

ScreenInterface {
    id: mainWindow

    signal detailsElementClicked(var elementIndex)
    signal backClicked
    signal cashierNameClicked
    signal loginComboClicked
    signal gearButtonClicked

    property alias changePageAlias: changePageBar

    function replacePageToDefault(){
        stackView.items[Enums.PaymentScreen].replacePageToDefault()
    }

    HeaderBar {
        id: statusBar

        width: appStyle.applicationWidth

        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }

        onBackClicked: {
            mainWindow.backClicked()
        }

        onCashierNameClicked: {
            mainWindow.cashierNameClicked()
        }

        onLoginComboClicked: {
            mainWindow.loginComboClicked()
        }

        onGearButtonClicked: {
            mainWindow.gearButtonClicked()
        }
    }

    Rectangle {
        id: line
        color: "#26262a"
        opacity: 0.1
        height: 1
        width: parent.width

        anchors {
            bottom: statusBar.bottom
            left: parent.left
            right: parent.right
        }
    }

    ChangePageBar {
        id: changePageBar

        width: appStyle.applicationWidth * 0.9

        property int currentPageId: 0

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: statusBar.bottom
        }

        onPageChanged: {
            if( pageNumber === 0 ){
                currentPageId = 0
                stackView.replacePage(Enums.PaymentScreen, true)
            }else if( pageNumber === 1 ){
                currentPageId = 1
                stackView.replacePage(Enums.DetailsScreen, true)
            }
        }
    }

    CustomStackView {
        id: stackView
        anchors {
            top: changePageBar.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        initializationList: [
            {
                "pageId": Enums.PaymentScreen,
                "pageSource": "qrc:/qml/screens/PaymentScreen.qml"
            },
            {
                "pageId": Enums.DetailsScreen,
                "pageSource": "qrc:/qml/screens/DetailsScreen.qml"
            }
        ]

        onInitialized: {
            paymentPageHandlers.target = stackView.items[Enums.PaymentScreen]
            detailsPageHandlers.target = stackView.items[Enums.DetailsScreen]
        }

    }

    Connections {
        id: paymentPageHandlers
        ignoreUnknownSignals: true

        onSetCashRegisterInputState: {
            statusBar.loginComboBoxAlias.state = "invalidState"
        }

        onKeyboardClickEvent: {
            statusBar.loginComboBoxAlias.state = "invalidState"
        }

    }

    Connections {
        id: detailsPageHandlers
        ignoreUnknownSignals: true

        onElementClicked: {
            mainWindow.detailsElementClicked(elementIndex)
        }
    }
}
