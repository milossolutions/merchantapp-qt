import QtQuick 2.5

import QMLEnums 1.0

import "qrc:/qml/components"

ScreenInterface {
    id : transactionsDetailsScreeen

    signal backClicked();
    signal cancelClicked();

    property int elementIndex: 0

    function refreshValues(){
        cashierText.text = qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.CashierName))
        dateText.text = qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.OrderDate))
        timeText.text = qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.OrderHour))
        totalValueText1.text = Number(newOrderListModel.getTotalPrice(elementIndex)).toFixed(2)
        rabatValueText.text = Number(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.Discount)).toFixed(2)
        totalValueText2.text = qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.Value))
        trxValueText.text = qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.OrderId))

        if(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.Discount) > 0.){
            rabatText.visible = true
            rabatValueText.visible = true
            rabatLine.visible = true
        }else{
            rabatText.visible = false
            rabatValueText.visible = false
            rabatLine.visible = false
        }
    }

    Text {
        id: cashierText
        text: qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.CashierName))
        color: appStyle.defaultTextColor
        width: appStyle.applicationWidth * 0.23
        horizontalAlignment: Text.AlignHCenter
        //renderType: Text.NativeRendering

        anchors {
            left: parent.left
            leftMargin: appStyle.applicationWidth * 0.05
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.04
        }

        font {
            pixelSize: 11 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }
    }

    SimpleLine {
        id: cashierLine
        opacity: 0.1
        color: appStyle.defaultTextColor
        width: parent.width

        anchors {
            top: cashierText.bottom
            bottom: undefined // reset anchor for simple line
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: dateText
        text: qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.OrderDate))
        color: appStyle.defaultTextColor
        horizontalAlignment: Text.AlignLeft
        //renderType: Text.NativeRendering

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
            bold: true
        }

        anchors {
            top: cashierLine.top
            topMargin: appStyle.applicationHeight * 0.02
            right: timeText.left
            rightMargin: appStyle.applicationWidth * 0.05
        }
    }

    Text {
        id: timeText
        text: qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.OrderHour))
        color: appStyle.defaultTextColor
        horizontalAlignment: Text.AlignRight
        //renderType: Text.NativeRendering

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
        anchors {
            bottom: dateText.bottom
            right: totalLine2.right
        }
    }

    Text {
        id: totalText1
        text: qsTr("Zwischentotal") + Settings.emptyString
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            left: totalLine1.left
            top: dateText.bottom
            topMargin: appStyle.applicationHeight * 0.05
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily //MEDIUM
        }
    }

    Text {
        id: totalValueText1
        text: ""
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            right: totalLine1.right
            bottom: totalText1.bottom
        }

        font {
            pixelSize: 21 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    SimpleLine {
        id: totalLine1
        opacity: 0.5
        color: appStyle.defaultTextColor

        width: appStyle.applicationWidth * 0.9

        anchors {
            top: totalValueText1.bottom
            topMargin: appStyle.applicationHeight * 0.01
            bottom: undefined // reset anchor for simple line
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: rabatText
        text: qsTr("Discount") + Settings.emptyString
        color: "#00d66f"
        //renderType: Text.NativeRendering

        visible: false

        anchors {
            left: totalLine1.left
            top: totalLine1.top
            topMargin: appStyle.applicationHeight * 0.03
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily //MEDIUM
        }
    }

    Text {
        id: rabatValueText
        text: ""
        color: "#00d66f"
        visible: false
        //renderType: Text.NativeRendering

        anchors {
            right: rabatLine.right
            verticalCenter: rabatText.verticalCenter
        }

        font {
            pixelSize: 21 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    SimpleLine {
        id: rabatLine
        opacity: 0.5
        color: "#00d66f"
        visible: false

        width: appStyle.applicationWidth * 0.9

        anchors {
            top: rabatText.bottom
            topMargin: appStyle.applicationHeight * 0.01
            bottom: undefined // reset anchor for simple line
            horizontalCenter: parent.horizontalCenter
        }
    }

    Text {
        id: totalText2
        text: qsTr("TOTAL") + Settings.emptyString
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            left: totalLine2.left
            top: rabatText.bottom
            topMargin: appStyle.applicationHeight * 0.08
        }

        font {
            pixelSize: 18 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }
    }

    Text {
        id: reversedTextLabel
        text: qsTr("Reversal") + Settings.emptyString
        color: "#db485a"
        //renderType: Text.NativeRendering

        visible: false

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }

        anchors {
            right: totalValueText2.right
            bottom: totalValueText2.top
            bottomMargin: -(parent.height * 0.02)
        }
    }

    Text {
        id: totalValueText2
        text: qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.Value))
        color: "#000000"
        //renderType: Text.NativeRendering

        anchors {
            right: totalLine2.right
            top: rabatText.bottom
            topMargin: appStyle.applicationHeight * 0.1 - 32 * appStyle.fontScale
        }

        font {
            pixelSize: 50 * appStyle.fontScale
            family: appStyle.lightFontFamily
            bold: true
        }
    }

    SimpleLine {
        id: totalLine2
        color: appStyle.defaultTextColor

        width: appStyle.applicationWidth * 0.9

        anchors {
            top: totalValueText2.bottom
            bottom: undefined // reset anchor for simple line
            horizontalCenter: parent.horizontalCenter
        }
    }

    Column {
        id: textColumn
        spacing:appStyle.applicationHeight * 0.004
        anchors {
            top: totalLine2.bottom
            topMargin: appStyle.applicationHeight * 0.02
            right: valueColumn.left
            rightMargin: appStyle.applicationWidth * 0.02
        }

        Row {
            id: trxRow

            Text {
                id: trxText
                text: qsTr("TRX-ID:")
                horizontalAlignment: Text.horizontalAlignment
                //renderType: Text.NativeRendering

                font {
                    pixelSize: 11 * appStyle.fontScale
                    family: appStyle.lightFontFamily
                }
            }
        }
    }

    Column {
        id: valueColumn
        spacing:appStyle.applicationHeight * 0.004
        anchors {
            top: totalLine2.bottom
            topMargin: appStyle.applicationHeight * 0.02
            right: totalLine2.right
            //rightMargin: appStyle.applicationWidth * 0.05
        }

        Row {
            id: trxValueRow

            Text {
                id: trxValueText
                text: qsTr(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.OrderId))
                horizontalAlignment: Text.horizontalAlignment
                //renderType: Text.NativeRendering

                font {
                    pixelSize: 11 * appStyle.fontScale
                    family: appStyle.lightFontFamily
                }
            }
        }
    }

    RectangleButton {
        id: cancelButton
        buttonText: qsTr("Stornierung") + Settings.emptyString
        downStateColor: "#db485a"
        upStateColor: "#db485a"
        height: appStyle.applicationHeight * 0.08
        anchors {
            bottom: backButton.top
            bottomMargin: appStyle.applicationWidth * 0.004
            horizontalCenter: parent.horizontalCenter
        }

        onButtonClicked: {
            console.log("Cancel order")
            transactionsDetailsScreeen.cancelClicked();
        }
    }

    RectangleButton {
        id: backButton
        buttonText: qsTr("Back") + Settings.emptyString
        upStateColor: "#a6a6a6"
        downStateColor: "#808080"
        height: appStyle.applicationHeight * 0.08
        anchors {
            bottom: parent.bottom
            bottomMargin: appStyle.applicationWidth * 0.02
            horizontalCenter: parent.horizontalCenter
        }
        onButtonClicked: {
            transactionsDetailsScreeen.backClicked();
        }
    }

    onVisibleChanged: {
        if (visible == true) {
            if(newOrderListModel.getTransactionDetails(elementIndex, ModelRole.BillStatus) === "2"){
                transactionsDetailsScreeen.state = "Details3"
            } else {
                transactionsDetailsScreeen.state = "Details2"
            }
        }
    }

    states: [
        State{
            name: "Details2"
            PropertyChanges {
                target: totalText1
                visible: true
            }
            PropertyChanges {
                target: totalValueText1
                visible: true
            }
            PropertyChanges {
                target: totalLine1
                visible: true
            }
            PropertyChanges {
                target: cancelButton
                visible: true
            }
            PropertyChanges {
                target: totalValueText2
                color: "#000000"
            }
            PropertyChanges {
                target: reversedTextLabel
                visible: false
            }
        },
        State{
            name: "Details3"
            PropertyChanges {
                target: totalText1
                visible: true
            }
            PropertyChanges {
                target: totalValueText1
                visible: true
            }
            PropertyChanges {
                target: totalLine1
                visible: true
            }
            PropertyChanges {
                target: cancelButton
                visible: false
            }
            PropertyChanges {
                target: totalValueText2
                color: "#db485a"
            }
            PropertyChanges {
                target: reversedTextLabel
                visible: true
            }
        }
    ]
}
