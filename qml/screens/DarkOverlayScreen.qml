import QtQuick 2.4

ScreenInterface {
    id: blockingOverlay
    backgroundColor: "#000000"
    z: 1000
    backgroundOpacity: 0.8
    visible: ApplicationStatus.mandatoryUpdateRequired
    anchors {
        fill: parent
    }

    Text {
        id: errorText
        color: appStyle.whiteTextColor
        text: qsTr("This version is no longer supported. You need to install the new version before you can use TWINT again.") + Settings.emptyString
        wrapMode: Text.Wrap
        width: appStyle.applicationWidth * 0.9
        horizontalAlignment: Text.AlignHCenter
        z: 1000

        font {
            pixelSize: 20 * appStyle.fontScale
            family: appStyle.heavyFontFamily
        }

        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.21
            horizontalCenter: parent.horizontalCenter
        }
    }
}
