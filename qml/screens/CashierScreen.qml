import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

import "qrc:/qml/screens"
import "qrc:/qml/components"

ScreenInterface {
    id: cashierScreen

    Text{
        id: placeholderText
        text: qsTr("<Cashier Screen Placeholder>")
        //renderType: Text.NativeRendering
        font {
            pixelSize: 14 * appStyle.fontScale
        }
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }
}
