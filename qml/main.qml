import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2

import "qrc:/qml/screens"
import "qrc:/qml/components"
import milosolutions.com 1.0
import QMLEnums 1.0

ApplicationWindow {
    id: mainView
    title: (Qt.platform.os == "windows") ? (qsTr("TWINT Windows solution") + Settings.emptyString) : (qsTr("TWINT OS X solution") + Settings.emptyString)
    width: (dpiScale > 1) ? Math.min((appStyle.defaultWidth * dpiScale), (screenWidth * 0.9)) : appStyle.defaultWidth
    height: (dpiScale > 1) ? Math.min((appStyle.defaultHeight * dpiScale), (screenHeight * 0.9)) : appStyle.defaultHeight
    minimumHeight: appStyle.defaultHeight
    minimumWidth: appStyle.defaultWidth

    visible: true

    property int visibilityHandler: 2

    Component.onCompleted: {
        if(dpiScale > 1){
            mainView.height = Math.min((appStyle.defaultHeight * dpiScale), (screenHeight * 0.9))
            mainView.width = Math.min((appStyle.defaultWidth * dpiScale), (screenWidth * 0.9))
        }

        if(Qt.platform.os != "windows"){
            mainView.menuBar = menuBar
        }
    }

    onClosing: {
        UiController.quitClicked()
        close.accepted = false
    }

    //Implementation of frameless window with resize mode
    //    flags: Qt.MSWindowsFixedSizeDialogHint | Qt.FramelessWindowHint

    //    Item {
    //        id: resizeItem
    //        width: 20
    //        height: 20
    //        z: 115
    //        anchors {
    //            right: parent.right
    //            bottom: parent.bottom
    //        }

    //        MouseArea {
    //            id: testMouse
    //            anchors.fill: parent
    //            cursorShape: Qt.SizeFDiagCursor

    //            property point lastMousePos: Qt.point(0, 0)

    //            onPressed: {
    //                lastMousePos = Qt.point(mouseX, mouseY);
    //            }

    //            onPositionChanged: {
    //                var widthChange = mouseX - lastMousePos.x
    //                var heightChange = mouseY - lastMousePos.y

    //                if((mainView.width + widthChange) > appStyle.defaultWidth){
    //                    mainView.width += widthChange
    //                }

    //                if((mainView.height + heightChange) > appStyle.defaultHeight){
    //                    mainView.height += heightChange
    //                }
    //            }
    //        }
    //    }

    onVisibilityChanged: {
        if(visibility!=3){
            visibilityHandler = visibility
        }
    }

    onWidthChanged: {
        appStyle.realApplicationWidth = width
        appStyle.applicationHeight = height
        appStyle.applicationWidth = appStyle.applicationHeight * appStyle.defaultWindowRatio

        if(appStyle.applicationWidth >= width){
            appStyle.applicationWidth = width
            appStyle.applicationHeight = appStyle.applicationWidth / appStyle.defaultWindowRatio
        }
    }

    onHeightChanged: {
        appStyle.realApplicationWidth = width
        appStyle.applicationHeight = height
        appStyle.applicationWidth = appStyle.applicationHeight * appStyle.defaultWindowRatio

        if(appStyle.applicationWidth >= width){
            appStyle.applicationWidth = width
            appStyle.applicationHeight = appStyle.applicationWidth / appStyle.defaultWindowRatio
        }
    }

    property alias appStyle: styleLoader.item

    MenuBar {
        id: menuBar
        Menu {
            title: qsTr("Settings") + Settings.emptyString
            MenuItem {
                text: qsTr("Preferences") + Settings.emptyString
                onTriggered: {
                    if(mainStackView.currentItem.pageId !== Enums.SettingsScreen){
                        mainStackView.pushPageWithoutAnimation(Enums.SettingsScreen)
                    }
                }
            }
        }
    }

    Connections {
        target: QApp
        onMessageReceived: {
            UiController.appIsAlreadyRunning()
        }
    }

    FontLoader {
        source: "qrc:/fonts/MarkPro"
        name: "MarkPro"
    }

    FontLoader {
        source: "qrc:/fonts/MarkPro-LightZ"
        name: "MarkPro-LightZ"
    }

    FontLoader {
        source: "qrc:/fonts/MarkPro-HeavyZ"
        name: "MarkPro-HeavyZ"
    }

    Loader {
        id: styleLoader
        source: "qrc:/ApplicationStyle"
    }

    Connections {
        target: UiController

        onRaiseWindow: {
            if(!mainView.active){
                mainView.showMinimized();
                if(visibilityHandler===4){
                    mainView.showMaximized()
                }else{
                    mainView.show()
                }
                if(mainStackView.currentItem.pageId !== Enums.MainWindow){
                    mainStackView.replacePage(Enums.MainWindow)
                }else{
                    if(mainStackView.currentItem.changePageAlias.currentPageId === 1){
                        mainStackView.currentItem.changePageAlias.state = "PaymentChecked"
                    }
                }
            }
        }

        onSettingsClicked: {
            mainStackView.pushPageWithoutAnimation(Enums.SettingsScreen)
        }

        onMinimalizeClicked: {
            mainView.hide();
        }

        onShowClicked: {
            mainView.show();
        }
    }

    ScreenInterface {
        id: background
        anchors {
            fill: parent
            //margins:10
        }
    }

    DarkOverlayScreen {
        id: blockingOverlay
    }

    CashRegisterPopUp {
        id: cashRegisterPopUp
        visible: false
        z: 100

        anchors {
            fill: parent
            //margins: 10
        }

        onCloseClicked: {
            cashRegisterPopUp.visible = false
        }
    }

    GearSettingsScreen {
        id: gearSettings
        z: 100

        anchors {
            fill: parent
            //margins: 10
        }

        onCloseSettings: {
            gearSettings.state = "second"
        }

        onSettingsClicked: {
            mainStackView.pushPageWithoutAnimation(Enums.SettingsScreen)
            gearSettings.state = "second"
        }

        onExitClicked: {
            UiController.exitButtonClicked()
            gearSettings.state = "second"
        }
    }

    ReverseConfirmationPopUp {
        id: reverseConfirmationPopUp
        visible: false
        z: 100

        anchors {
            fill: parent
            //margins: 10
        }

        onConfirmClicked: {
            UiController.reverseOrder(newOrderListModel.getTransactionDetails(mainStackView.items[Enums.TransactionsDetailsScreen].elementIndex, ModelRole.Value), newOrderListModel.getTransactionDetails(mainStackView.items[Enums.TransactionsDetailsScreen].elementIndex, ModelRole.OrderId));
            reverseConfirmationPopUp.visible = false
        }

        onCancelClicked: {
            reverseConfirmationPopUp.visible = false
        }
    }

    CustomStackView {
        id: mainStackView
        width: appStyle.applicationWidth
        anchors {
            fill: parent
            //margins: 10
        }

        initializationList: [
            {
                "pageId": Enums.MainWindow,
                "pageSource": "qrc:/qml/screens/MainWindow.qml"
            },
            {
                "pageId": Enums.TransactionsDetailsScreen,
                "pageSource": "qrc:/qml/screens/TransactionsDetailsScreen.qml"
            },
            {
                "pageId": Enums.PopUpScreen,
                "pageSource": "qrc:/qml/screens/PopUpScreen.qml"
            },
            {
                "pageId": Enums.SettingsScreen,
                "pageSource": "qrc:/qml/screens/SettingsScreen.qml"
            },
            {
                "pageId": Enums.NumpadScreen,
                "pageSource": "qrc:/qml/screens/NumpadScreen.qml"
            }
        ]

        onInitialized: {
            mainStackView.pushPage(Enums.MainWindow)

            mainWindowPageHandlers.target = mainStackView.items[Enums.MainWindow]
            transactionsDetailsPageHandlers.target = mainStackView.items[Enums.TransactionsDetailsScreen]
            popUpScreenHandlers.target = mainStackView.items[Enums.PopUpScreen]
            settingsScreenHandlers.target = mainStackView.items[Enums.SettingsScreen]
            numpadScreenHandlers.target = mainStackView.items[Enums.NumpadScreen]
        }
    }

    Component{
        id: mainWindowComponent
        MainWindow{
            id:mainWindow
        }
    }

    Connections {
        id: mainWindowPageHandlers
        ignoreUnknownSignals: true

        onDetailsElementClicked: {
            mainStackView.items[Enums.TransactionsDetailsScreen].elementIndex = elementIndex;
            mainStackView.items[Enums.TransactionsDetailsScreen].refreshValues()
            mainStackView.pushPage(Enums.TransactionsDetailsScreen)
        }
        onBackClicked: {
            mainStackView.popPageWithoutAnimation()
        }

        onCashierNameClicked: {
            mainStackView.pushPageWithoutAnimation(Enums.SettingsScreen);
        }

        onLoginComboClicked: {
            cashRegisterPopUp.visible = true
        }

        onGearButtonClicked: {
            gearSettings.state = "first"
            gearSettings.visible = true
        }
    }

    Connections {
        id: transactionsDetailsPageHandlers
        ignoreUnknownSignals: true

        onBackClicked: {
            mainStackView.popPageWithoutAnimation()
        }

        onCancelClicked: {
            reverseConfirmationPopUp.visible = true
        }
    }

    Connections {
        id: popUpScreenHandlers
        ignoreUnknownSignals: true

        onConfirmClicked: {
            mainStackView.pop()
            mainStackView.replacePage(Enums.MainWindow)
        }

    }

    Connections {
        id: settingsScreenHandlers
        ignoreUnknownSignals: true

        onCloseClicked: {
            mainStackView.pop()
        }

        onLogoutUser: {
            cashRegisterPopUp.logoutCalled()
        }
    }

    Connections {
        target: ApplicationStatus

        onPairingOnGoingStatusChanged: {
            if((mainStackView.currentPageId === Enums.NumpadScreen) && (!ApplicationStatus.pairingOnGoingStatus)){
                mainStackView.replacePage(Enums.MainWindow)
                mainStackView.items[Enums.MainWindow].replacePageToDefault()
            }
        }
    }

    Connections {
        id: numpadScreenHandlers
        ignoreUnknownSignals: true

        onConfirmClicked: {
        }

        onBackClicked: {
            //UiController.cancelCheckIn("PAYMENT_ABORT")
            mainStackView.replacePage(Enums.MainWindow)
            mainStackView.items[Enums.MainWindow].replacePageToDefault()
        }
    }

    Connections {
        target: UiController

        onShowMessage: {
            console.log("Show message: " + popUpType + " " + message)
            mainStackView.items[Enums.PopUpScreen].informationText = message
            if (popUpType == 0) {
                mainStackView.items[Enums.PopUpScreen].state = "ConfirmPopUp"
            } else {
                mainStackView.items[Enums.PopUpScreen].state = "ErrorPopUp"
            }

            mainStackView.pushPageWithoutAnimation(Enums.PopUpScreen)
        }

        onShowCouponsList: {
            mainStackView.replacePage(Enums.NumpadScreen)
        }
    }
}
