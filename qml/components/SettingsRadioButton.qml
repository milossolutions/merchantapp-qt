import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

RadioButton {
    id: radioButton

    style: RadioButtonStyle {
        label: Text {
            id: radioButtonText
            text: control.text
            color: control.checked ? "#00d66f" : "#262626"
            opacity: control.checked ? 1 : 0.4
            //renderType: Text.NativeRendering

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 14 * appStyle.fontScale
            }
        }


        indicator: Rectangle {
            width: appStyle.applicationHeight * 0.04
            height: appStyle.applicationHeight * 0.04
            radius: 100
            border.color: control.checked ? "#00d66f" : "#262626"
            border.width: 1
            opacity: control.checked ? 1: 0.4
            Rectangle {
                anchors.fill: parent
                visible: control.checked
                color: "#00d66f"
                radius: 100
                anchors.margins: appStyle.applicationHeight * 0.012
            }
        }
    }
}
