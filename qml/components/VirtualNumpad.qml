import QtQuick 2.4

Item {
    id: virtualNumpad
    width: appStyle.applicationWidth
    height: buttonContainer.height + numpadGrid.height

    property bool catchButton: false
    property variant buttonValue: [1,2,3,4,5,6,7,8,9,".",0,"DEL"]
    property variant buttonText: [" ","ABC","DEF","GHI","JKL","MNO","PQRS","TUV","WXYZ","","",""]
    property alias keyboardColumnState: keyboardColumn.state

    signal keyboardClick(var value)
    signal okButtonClick()

    Column {
        id: keyboardColumn

        anchors {
            fill: parent
        }

        states: [
            State{
                name: "showButton"
                PropertyChanges {
                    target: buttonContainer
                    visible: true
                    height: 0.07 * appStyle.applicationHeight
                }
            }
        ]

        Rectangle {
            id: buttonContainer
            visible: false
            height: 0
            width: parent.width
            color: "#f5f5f5"
            focus: false

            MouseArea {
                anchors {
                    top: parent.top
                    bottom: parent.bottom
                    left: parent.left
                    right: okButtom.left
                }
            }

            RectangleButton {
                id: okButtom
                buttonText: "Ok"
                width: 0.3 * appStyle.applicationWidth
                height: 0.055 * appStyle.applicationHeight

                anchors {
                    right: parent.right
                    rightMargin: 0.01 * appStyle.applicationWidth
                    verticalCenter: parent.verticalCenter
                }

                onButtonClicked: {
                    virtualNumpad.okButtonClick()
                }
            }
        }

        Grid {
            id: numpadGrid
            columns: 3
            rows: 4
            height: 0.4 * appStyle.applicationHeight

            anchors {
                left: parent.left
                right: parent.right
            }

            Repeater {
                id: buttonReapeter
                model: virtualNumpad.buttonValue
                NumpadButton {
                    buttonIndex: index
                    buttonTextString: modelData
                    underTextString: buttonText[index]
                    width: parent.width / 3
                    height: appStyle.defRectButtonHeight
                    onButtonClicked: {
                        if(catchButton){
                            keyboardClick(modelData)
                        }else{
                            if(ApplicationStatus.credentialsStatus){
                                if(ApplicationStatus.currentEnrollStatus){
                                    priceField.keyboardClick(modelData)
                                }else{
                                    if(Settings.isSelectUser){
                                        keyboardScreen.keyboardClickEvent()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } 
}
