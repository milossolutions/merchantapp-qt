import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
    id: inputWithLabelComponent
    width: appStyle.applicationWidth * 0.9
    height: appStyle.applicationHeight * 0.07

    property alias inputString: inputField.text

    property alias isActiveFocus: inputField.focus
    property string labelText: qsTr("")

    property alias textField: inputField

    function receivedKeyboardEvent(value){
        if(value === "DEL"){
            inputField.text = inputField.text.slice(0,-1)
            inputField.forceActiveFocus()
        }else{
            inputField.text += value
            if(!inputField.acceptableInput){
                inputField.text = inputField.text.slice(0,-1)
            }
            inputField.forceActiveFocus()
        }
    }

    TextField {
        id: inputField
        horizontalAlignment: TextInput.AlignRight
        width: parent.width
        height: parent.height * 0.6
        echoMode: TextInput.Normal

        anchors {
            top: parent.top
        }

        font {
            family: appStyle.defaultFontFamily
            pixelSize: 22 * appStyle.fontScale
        }

        style: TextFieldStyle {
            textColor: "#595959"
            padding{
                left: 0
                right: 0//clearButton.width + 7
            }
            background: Rectangle {
                border.width: 0
            }
        }

        Text {
            id: inputLabel
            text: labelText
            color: appStyle.defaultTextColor
            opacity: 0.3
            //renderType: Text.NativeRendering

            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
            }

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 16 * appStyle.fontScale
            }

            MouseArea {
                anchors {
                    fill: parent
                }
                onClicked: {
                    inputField.forceActiveFocus()
                }
            }
        }
    }

    Rectangle {
        id: line
        color: "#26262a"
        opacity: isActiveFocus ? 1.0 : 0.1
        height: 1
        width: parent.width

        anchors {
            top: inputField.bottom
            topMargin: parent.height * 0.02
        }
    }
}
