import QtQuick 2.0

Item {
    id: loginItem
    width: appStyle.applicationWidth * 0.55
    height: appStyle.applicationHeight *0.055

    anchors {
        horizontalCenter: parent.horizontalCenter
        verticalCenter: parent.verticalCenter
    }

    signal loginClicked

    Connections {
        target: ApplicationStatus

        onCurrentCashRegisterChanged: {
            loginItem.state = ""
        }
    }

    Rectangle {
        id: loginComboBox

        border {
            color: "#00d66f"
            width: 1
        }

        anchors {
            fill: parent
        }

        Text {
            id: placeholderText
            color: appStyle.defaultTextColor
            text: qsTr("Insert Username") + Settings.emptyString
            opacity: 0.5
            //renderType: Text.NativeRendering

            visible: true

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 14 * appStyle.fontScale
            }

            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
        }

        Text {
            id: cashRegisterId
            color: appStyle.defaultTextColor
            text: ApplicationStatus.currentCashRegister
            //renderType: Text.NativeRendering

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 14 * appStyle.fontScale
            }

            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }

            onTextChanged: {
                if(cashRegisterId.text.length > 0){
                    placeholderText.visible = false
                }else{
                    placeholderText.visible = true
                }
            }
        }

        SvgButton {
            id: arrowButton
            height: appStyle.applicationHeight * 0.02
            sourceSize.width: width * 4
            downState: "qrc:/images/DownArrow"
            upState: "qrc:/images/DownArrow"

            anchors {
                verticalCenter: parent.verticalCenter
                right: parent.right
                rightMargin: appStyle.applicationWidth * 0.03
            }
        }
    }

    MouseArea {
        anchors.fill: parent

        onPressed: {
            loginClicked()
        }
    }

    states: [
        State{
            name: "invalidState"
            PropertyChanges {
                target: loginComboBox
                color: appStyle.errorScreenColor
            }
            PropertyChanges {
                target: placeholderText
                color: "white"
                opacity: 1.0
            }
        }
    ]
}
