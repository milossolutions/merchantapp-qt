import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
    id: inputWithLabelComponent
    width: appStyle.applicationWidth * 0.85
    height: appStyle.applicationHeight * 0.08

    property alias inputString: inputField.text

    property bool isActiveFocus: inputField.activeFocus
    property string labelText: qsTr("")
    property bool isDialogType: false
    property string inputType: "normal"

    property alias textField: inputField

    signal certificatePathChanged(var path)

    state: ""

    states:  [
        State{
            name: "merchantType"
        },
        State{
            name: "certificateType"
            PropertyChanges {
                target: browseButton
                visible: true
                buttonText: "Browse" + Settings.emptyString
            }
        },
        State{
            name: "passwordType"
            PropertyChanges {
                target: inputField
                echoMode: TextInput.Password
                font.letterSpacing: inputWithLabelComponent.width * 0.01
            }
            PropertyChanges {
                target: clearButton
                downState: "qrc:/images/EyeIcon"
                upState: "qrc:/images/EyeIcon"
            }
        },
        State{
            name: "usbType"
            PropertyChanges {
                target: browseButton
                visible: true
                buttonText: "Scan" + Settings.emptyString
            }
        },
        State {
            name: "errorPasswordOccured"
            PropertyChanges {
                target: inputField
                inputFieldBackgroundColor: appStyle.redTextColor
                echoMode: TextInput.Password
                font.letterSpacing: inputWithLabelComponent.width * 0.01
                inputFieldTextColor: "white"
            }
            PropertyChanges {
                target: clearButton
                downState: "qrc:/images/EyeIcon"
                upState: "qrc:/images/EyeIcon"
            }
            PropertyChanges {
                target: errorString
                visible: true
                text: qsTr("The password is not correct.") + Settings.emptyString
            }
            PropertyChanges {
                target: inputWithLabelComponent
                height: appStyle.applicationHeight * 0.12
            }
        },
        State {
            name: "errorUsbOccured"
            PropertyChanges {
                target: inputField
                inputFieldBackgroundColor: appStyle.redTextColor
                inputFieldTextColor: "white"
            }
            PropertyChanges {
                target: errorString
                visible: true
                text: qsTr("USB connection to Beacon not found ") + Settings.emptyString
            }
            PropertyChanges {
                target: inputWithLabelComponent
                height: appStyle.applicationHeight * 0.15
            }
        },
        State {
            name: "errorMerchantUuid"
            PropertyChanges {
                target: inputField
                inputFieldBackgroundColor: appStyle.redTextColor
                inputFieldTextColor: "white"
            }
            PropertyChanges {
                target: errorString
                visible: true
                text: qsTr("The access code or password is not correct.") + Settings.emptyString
            }
            PropertyChanges {
                target: inputWithLabelComponent
                height: appStyle.applicationHeight * 0.12
            }
        }
    ]

    Text {
        id: inputLabel
        text: labelText
        color: appStyle.defaultTextColor
        opacity: 0.3
        z: 10
        //renderType: Text.NativeRendering

        anchors {
            left: inputWithLabelComponent.left
            top: inputWithLabelComponent.top
        }

        font {
            family: appStyle.defaultFontFamily
            pixelSize: 12 * appStyle.fontScale
        }

        MouseArea {
            anchors {
                fill: parent
            }
            onClicked: {
                UiController.showSystemKeyboard()
                inputField.forceActiveFocus()
            }
        }
    }

    TextField {
        id: inputField
        horizontalAlignment: TextInput.AlignHCenter
        width: parent.width
        height: appStyle.applicationHeight * 0.06
        echoMode: TextInput.Normal
        z: 0
        maximumLength: 41

        anchors {
            top: inputLabel.bottom
            topMargin: parent.height * 0.01
        }

        font {
            family: appStyle.defaultFontFamily
            pixelSize: 11 * appStyle.fontScale
        }

        onTextChanged: {
            if(inputWithLabelComponent.inputType == "uuid" ||
                    inputWithLabelComponent.inputType == "password"){
                inputField.text = inputField.text.replace(" ", "")
            }

            if(inputWithLabelComponent.inputType == "uuid"){
                if(inputField.text.length > inputField.displayText.length){
                    if(inputField.text.length == 8
                            || inputField.text.length == 13
                            || inputField.text.length == 18
                            || inputField.text.length == 23){
                        inputField.text = inputField.text + "-";
                    }
                    if(inputField.text.length > 36){
                        inputField.text = inputField.text.slice(0,-1)
                    }
                }
            }

            if(inputWithLabelComponent.state === "errorPasswordOccured"){
                inputWithLabelComponent.state = "passwordType"
            }else if(inputWithLabelComponent.state === "errorUsbOccured"){
                inputWithLabelComponent.state = ""
            }
        }

        onFocusChanged: {
            if(inputField.focus){
                UiController.showSystemKeyboard()
            }
        }


        property string inputFieldBackgroundColor: "white"
        property string inputFieldTextColor: appStyle.defaultTextColor

        style: TextFieldStyle {
            id: inputFieldTextStyle
            textColor: inputField.inputFieldTextColor
            padding{
                top: parent.height * 0.5
                left: 0
                right: 0
            }
            background: Rectangle {
                color: inputField.inputFieldBackgroundColor
                border.width: 0
            }
        }

        SvgButton {
            id: clearButton
            height: inputField.height * 0.6
            downState: "qrc:/images/ClearDownImage"
            upState: "qrc:/images/ClearUpImage"
            opacity: 0.4

            visible: inputWithLabelComponent.isActiveFocus && !inputWithLabelComponent.isDialogType

            anchors {
                right: inputField.right
                rightMargin: 2
                verticalCenter: parent.verticalCenter
            }
            onPressed: {
                if(inputWithLabelComponent.state === "passwordType"
                        || inputWithLabelComponent.state === "errorPasswordOccured"){
                    inputField.echoMode = TextInput.Normal
                }else{
                    inputField.text = ""
                }
            }

            onClicked: {

            }

            onReleased: {
                if(inputWithLabelComponent.state === "passwordType"
                        || inputWithLabelComponent.state === "errorPasswordOccured"){
                    console.log
                    inputField.echoMode = TextInput.Password
                }else{

                }
            }
        }

        RectangleButton {
            id: browseButton
            width: appStyle.applicationWidth * 0.25
            height: parent.height * 0.7
            buttonText: qsTr("Browse") + Settings.emptyString
            upStateColor: "#a6a6a6"
            downStateColor: "#808080"
            visible: false

            buttonFontSize: 14 * appStyle.fontScale

            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
            }

            onButtonClicked: {
                if(inputWithLabelComponent.state === "usbType"){
                    UiController.discoverUsbPort();
                }else{
                    fileDialog.open()
                }
            }
        }

    }

    FileDialog {
        id: fileDialog
        title: "Please choose certificate"
        selectExisting: true
        nameFilters: [ "*.p12" ]
        onAccepted: {
            certificatePathChanged(UiController.getFilePath(fileUrl))
            fileDialog.close()
        }
        onRejected: {
            fileDialog.close()
        }
        Component.onCompleted: visible = false
    }

    Rectangle {
        id: line
        color: "#26262a"
        opacity: isActiveFocus ? 1.0 : 0.1
        height: 1
        width: parent.width
        z: 10

        anchors {
            top: inputField.bottom
        }
    }

    Text {
        id: errorString
        color: appStyle.redTextColor
        visible: false
        //renderType: Text.NativeRendering
        wrapMode: Text.Wrap

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.defaultFontFamily
            italic: true
            bold: true
        }

        anchors {
            top: line.bottom
            topMargin: appStyle.applicationHeight * 0.01
            left: line.left
            right: line.right
        }
    }
}
