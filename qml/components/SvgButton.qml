import QtQuick 2.4

Image {
    id: button
    source: upState
    height: parent.height
    width: height
    fillMode: Image.PreserveAspectFit
    sourceSize.width: width * 2
    sourceSize.height: height * 2

    signal clicked
    signal released
    signal pressed
    property string downState: ""
    property string upState: ""

    MouseArea {
        anchors.fill: parent

        onReleased: {
            button.state = ""
            button.released()
        }
        onPressed: {
            button.state = "Clicked"
            button.clicked()
            button.pressed()
        }
    }

    states: [
        State {
            name: "Clicked"
            PropertyChanges {
                target: button
                source: downState
            }
        }
    ]
}
