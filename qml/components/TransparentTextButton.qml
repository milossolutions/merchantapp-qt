import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: transparentTextButton
    width: parent.width * 0.5
    height: parent.height * 0.3

    property string buttonTextString: qsTr("") + Settings.emptyString

    property string upStateButtonColor: appStyle.defaultTextColor
    property string downStateButtonColor: "#808080"

    signal buttonClicked

    Text {
        id: buttonText
        color: upStateButtonColor
        text: buttonTextString
        //renderType: Text.NativeRendering

        font {
            pixelSize: 17 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }

        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }
    }

    MouseArea {
        id: buttonMouseArea
        anchors.fill: parent

        onPressed: {
            transparentTextButton.state = 'clicked'
        }
        onReleased: {
            transparentTextButton.state = ""
            transparentTextButton.buttonClicked()
        }
    }

    states: [
        State {
            name: "clicked"
            PropertyChanges {
                target: buttonText
                color: downStateButtonColor
            }
        }
    ]
}
