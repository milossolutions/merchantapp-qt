import QtQuick 2.4

Item {
    id: sectionDelegate
    width: appStyle.applicationWidth
    height: summaryItem.height + itemMargin.height + dateItem.height + headerItem.height

    property string sDiscount: newOrderListModel.getDiscountSummary(section)
    property string sValue: newOrderListModel.getValueSummary(section)

    property bool firstElement: newOrderListModel.isFirstSection(section)

    Component.onCompleted: {
        if(newOrderListModel.isFirstSection(section)){
            sectionDelegate.state = "FirstElement"
        }else{
            sectionDelegate.state = "OtherElement"
        }
    }

    Column {
        id: sectionColumn
        width: parent.width
        height: parent.height

        anchors {
            fill: parent
        }

        BillListSummaryItem {
            id: summaryItem
            height: 0
            date: section
            summaryValue: sValue
            summaryDiscount: sDiscount
        }

        Item {
            id: itemMargin
            height: 0
            width: parent.width
            visible: true
        }


        Item {
            id: dateItem
            width: parent.width
            height: appStyle.applicationHeight * 0.06

            Text {
                text: section
                //renderType: Text.NativeRendering
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }

                font {
                    pixelSize: 13 * appStyle.fontScale
                    //should be MarkPro-Bold
                    family: appStyle.heavyFontFamily
                }
            }
        }


        Item {
            id: headerItem
            width: parent.width
            height: appStyle.applicationHeight * 0.04

            Text {
                id: discountText
                text: qsTr("Discount") + Settings.emptyString
                //renderType: Text.NativeRendering

                anchors {
                    right: headerItem.right
                    rightMargin: appStyle.applicationWidth * 0.25
                    verticalCenter: headerItem.verticalCenter
                }

                font {
                    pixelSize: 13 * appStyle.fontScale
                    family: appStyle.defaultFontFamily
                }
            }

            Text {
                id: totalText
                text: qsTr("Total") + Settings.emptyString
                //renderType: Text.NativeRendering

                anchors {
                    right: headerItem.right
                    rightMargin: appStyle.applicationWidth * 0.05
                    verticalCenter: headerItem.verticalCenter
                }

                font {
                    pixelSize: 13 * appStyle.fontScale
                    family: appStyle.defaultFontFamily
                }
            }

            SimpleLine {
                id: headerLine
            }
        }
    }

    states: [
        State {
            name: "FirstElement"
            PropertyChanges{
                target: summaryItem
                visible: false
                height: 0
            }
            PropertyChanges {
                target: itemMargin
                visible: true
                height: 0
            }
        },
        State {
            name: "OtherElement"
            PropertyChanges{
                target: summaryItem
                visible: true
                height: appStyle.applicationHeight * 0.09
            }
            PropertyChanges {
                target: itemMargin
                visible: true
                height: appStyle.applicationHeight * 0.04
            }
        }
    ]
}
