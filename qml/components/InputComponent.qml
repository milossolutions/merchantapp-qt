import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

import "qrc:/qml/components"

Item {
    id: inputFieldItem
    width: appStyle.applicationWidth * 0.9
    height: appStyle.applicationWidthHeight * 0.1

    signal clearButtonClicked

    property bool isActiveFocus: inputField.activeFocus
    property bool errorOccured: false
    property bool isPasswordType: false
    property bool showBottomLine: true
    property string errorText: qsTr("Place holder Text")
    property int applicationHeight: parent.height
    property string text: inputField.text
    property alias textField: inputField
    property int fontPixelSize: 50
    property alias inputFieldAlias: inputField

    function keyboardClick(buttonString){
        if(buttonString === "DEL"){
            textField.text = textField.text.slice(0,-1)
            inputField.forceActiveFocus()
        }else{
            textField.text += buttonString
            inputField.forceActiveFocus()
        }
    }

    onShowBottomLineChanged: {
        line.visible = false
    }

    function setTextFiledText(textString) {
        inputField.text = textString
    }

    TextInput {
        id: inputField
        horizontalAlignment: TextInput.AlignHCenter
        width: parent.width
        height: parent.height * 0.8


        anchors {
            top: parent.top
        }

        onFocusChanged: {
            if(inputField.activeFocus){
                Qt.inputMethod.show()
            }
        }

        font {
            family: appStyle.defaultFontFamily
            pixelSize: inputFieldItem.fontPixelSize * appStyle.fontScale
        }

        onTextChanged: {
            if(!inputField.acceptableInput){
                textField.text = textField.text.slice(0,-1)
            }
            inputField.text = inputField.text.replace(",",".")

        }

        cursorDelegate: CursorDelegateComponent {
            id: cursor
        }
    }

    SvgButton {
        id: clearButton
        height: inputField.height * 0.6
        downState: "qrc:/images/ClearDownImage"
        upState: "qrc:/images/ClearUpImage"

        visible: isActiveFocus

        anchors {
            right: inputField.right
            rightMargin: 2
            verticalCenter: inputField.verticalCenter
        }

        onPressed: {
            inputField.text = ""
        }
    }

    Rectangle {
        id: line
        color: "#26262a"
        opacity: isActiveFocus ? 1.0 : 0.1
        height: 1
        width: parent.width

        anchors {
            top: inputField.bottom
            topMargin: parent.height * 0.02
        }
    }

    Text {
        id: error
        text: errorText
        color: "#db485a"
        visible: errorOccured ? true : false
        //renderType: Text.NativeRendering

        font {
            family: appStyle.heavyFontFamily
            pixelSize: 10 * appStyle.fontScale
        }

        anchors {
            top: line.bottom
            topMargin: 10
            horizontalCenter: parent.horizontalCenter
        }
    }
}
