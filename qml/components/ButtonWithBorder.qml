import QtQuick 2.4

Item{
    id: buttonItem
    width: appStyle.defRectButtonWidth
    height: appStyle.defRectButtonHeight

    signal buttonClicked()
    property string buttonText: qsTr("Ok")

    property string upStateColor: "#00d66f"
    property string downStateColor: "#00BD62"
    property string upStateTextColor: appStyle.defaultTextColor
    property string downStateTextColor: appStyle.defaultTextColor
    property string borderColor: appStyle.defaultBgColor
    property bool buttonClickState: false
    property bool isInactive: false
    property int buttonRadius: appStyle.defaultButtonRadius

    property alias buttonFontSize: buttonTextComponent.font.pixelSize

    property alias buttonAlias: button

    Rectangle {
        id: button
        anchors.fill: parent
        opacity: appStyle.opacityUpState
        color: buttonItem.upStateColor
        radius: buttonItem.buttonRadius

        border {
            width: 1
            color: borderColor
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            onClicked: {
                buttonClicked()
            }
        }

        states: [
            State {
                name: "clicked"
                when: mouseArea.pressed
                PropertyChanges {
                    target: button
                    color: buttonItem.downStateColor
                }
                PropertyChanges {
                    target: buttonTextComponent
                    color: buttonItem.downStateTextColor
                }
            },
            State {
                name: "disabled"
                PropertyChanges {
                    target: button
                    opacity: 0.3
                }
            }
        ]

    }

    Text {
        id: buttonTextComponent
        color: upStateTextColor
        text: buttonText
        //renderType: Text.NativeRendering
        font {
            family: appStyle.lightFontFamily
            pixelSize: 17 * appStyle.fontScale
        }
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
    }
}

