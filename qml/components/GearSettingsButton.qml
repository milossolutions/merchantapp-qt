import QtQuick 2.5

Rectangle {
    id: gearSettingsButton
    width: parent.width
    height: appStyle.applicationHeight * 0.08
    color: "transparent"
    radius: 8
    z:110

    anchors {
        left: parent.left
        right: parent.right
    }

    property string buttonTextString: "NULL"

    signal buttonClicked

    Text {
        id: buttonText

        color: appStyle.defaultTextColor
        text: buttonTextString
        //renderType: Text.NativeRendering

        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: parent.width * 0.1
        }

        font {
            family: appStyle.defaultFontFamily
            pixelSize: 16 * appStyle.fontScale
        }
    }

    MouseArea {
        anchors {
            fill: parent
        }
        onPressed: {
            gearSettingsButton.state = "downState"
            mouse.accepted = true;
        }

        onReleased: {
            gearSettingsButton.state = ""
            gearSettingsButton.buttonClicked()
        }
    }

    states: [
        State {
            name: "downState"
            PropertyChanges {
                target: gearSettingsButton
                color: "#d3d3d3"
            }
        }
    ]
}
