import QtQuick 2.4

import "qrc:/qml/screens"
import "qrc:/qml/components"


ScreenInterface {
    id: changePageBar
    width: parent.width
    height: parent.height * 0.08

    signal pageChanged(int pageNumber)

    Component.onCompleted: {
        state = "PaymentChecked"
    }

    onStateChanged: {
        if(changePageBar.state === "PaymentChecked"){
            changePageBar.pageChanged(0)
        }else if(changePageBar.state === "DetailsChecked"){
            changePageBar.pageChanged(1)
        }
    }

    Row{
        id: buttonRow
        width: appStyle.applicationWidth
        height: appStyle.defRectButtonHeight

        anchors {
            fill: parent
        }

        ChangePageButton {
            id: paymentButton
            buttonTextString: qsTr("Cash Register") + Settings.emptyString

            onButtonClicked: {
                if(paymentButton.selected === false){
                    changePageBar.state = "PaymentChecked"
                }
            }
        }

        ChangePageButton {
            id: detailsButton
            buttonTextString: qsTr("History") + Settings.emptyString

            onButtonClicked: {
                if(detailsButton.selected === false){
                    changePageBar.state = "DetailsChecked"
                }
            }
        }
    }

    states: [
        State {
            name: "PaymentChecked"
            PropertyChanges {
                target: paymentButton
                state: "Checked"
            }
            PropertyChanges {
                target: detailsButton
                state: ""
            }
        },
        State {
            name: "DetailsChecked"
            PropertyChanges {
                target: paymentButton
                state: ""
            }
            PropertyChanges {
                target: detailsButton
                state: "Checked"
            }
        }
    ]
}
