import QtQuick 2.0

Item {
    id: waitingItem
    width: appStyle.applicationWidth < appStyle.applicationHeight ? appStyle.applicationWidth * 0.3 : appStyle.applicationHeight * 0.3
    height: width

    Connections {
        target: ApplicationStatus

        onPaymentOngoingStatusChanged: {
            if(ApplicationStatus.paymentOngoingStatus){
                waitingItem.visible = true
            }else{
                waitingItem.visible = false
            }
        }
    }

    RotationAnimation on rotation {
        from: 0;
        to: 360;
        duration: 1000
        running: true
        loops: Animation.Infinite
    }

    Canvas {
        id: canvas
        anchors.fill: parent
        antialiasing: true

        property color primaryColor: "transparent"
        property color secondaryColor: "#00d66f"

        property real centerWidth: width / 2
        property real centerHeight: height / 2
        property real radius: Math.min(canvas.width, canvas.height) / 2 - lineWidth / 2;
        property real lineWidth: 5
        property real angle: Math.PI / 2

        property real angleOffset: 0

        onPaint: {
            var ctx = getContext("2d");
            ctx.save();

            ctx.clearRect(0, 0, canvas.width, canvas.height);

            ctx.beginPath();
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = primaryColor;
            ctx.arc(canvas.centerWidth,
                    canvas.centerHeight,
                    canvas.radius,
                    angleOffset + canvas.angle,
                    angleOffset + 2*Math.PI);
            ctx.stroke();

            ctx.beginPath();
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = canvas.secondaryColor;
            ctx.arc(canvas.centerWidth,
                    canvas.centerHeight,
                    canvas.radius,
                    canvas.angleOffset,
                    canvas.angleOffset + canvas.angle);
            ctx.stroke();

            ctx.restore();
        }
    }
}
