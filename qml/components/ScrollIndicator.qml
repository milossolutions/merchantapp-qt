import QtQuick 2.5

/*
  Scroll indicator for the Flickable based items
  */
Item {
    id: root

    /*
      Mandatory value, has to be set to make ScrollBar work
      */
    property var flickable

    /*
      Determines if the ScrollBar is vertical or horizontal. It is vertical by default
      */
    property bool vertical : true

    /*
      Color of the Bar background. Transaprent by default
      */
    property color backgroundColor: "transparent"

    /*
      Color of the indicator
      */
    property color indicatorColor: "#00d66f"

    opacity: flickable.moving && (flickable.contentHeight > flickable.height)

    Behavior on opacity {
        NumberAnimation { duration: 500 }
    }

    Rectangle {
        radius: vertical ? (width/2 - 1) : (height/2 - 1)
        color: root.backgroundColor
        opacity: 0.3
        anchors.fill: parent
    }

    Rectangle {
        id: indicator
        color: root.indicatorColor
        radius: vertical ? (width/2 - 1) : (height/2 - 1)

        x: {
            if (vertical)
                return 1
            return ((root.width - indicator.width)  // working/moving area
                    * flickable.contentX / (flickable.contentWidth - flickable.width)) // content position fraction
        }

        y: {
            if (!vertical)
                return 1

            return ((root.height - indicator.height)  // working/moving area
                    * flickable.contentY / (flickable.contentHeight - flickable.height)) // content position fraction
        }

        width: {
            if (vertical)
                return parent.width-2
            return flickable.width * (flickable.width / flickable.contentWidth)
        }

        height: {
            if (!vertical)
                return parent.height-2

            return flickable.height * (flickable.height / flickable.contentHeight)
        }
    }
}
