import QtQuick 2.4

Rectangle {
    id: changePageButton
    width: parent.width / 2
    height: parent.height

    signal buttonClicked
    property bool selected: false
    property string buttonTextString: qsTr("Placeholder")

    Text {
        id: buttonText
        text: buttonTextString
        color: "#262626"
        //renderType: Text.NativeRendering

        font {
            family: appStyle.defaultFontFamily
            pixelSize: 14 * appStyle.fontScale
        }

        anchors {
            bottom: parent.bottom
            bottomMargin: parent.height * 0.15
            horizontalCenter: parent.horizontalCenter
        }
    }

    Rectangle {
        id: line
        color: "#262626"
        height: 0
        width: buttonText.width

        opacity: 0.4

        anchors {
            horizontalCenter: buttonText.horizontalCenter
            bottom: parent.bottom
        }
    }

    states: [
        State {
            name: "Checked"
            PropertyChanges {
                target: line
                color: "#00d66f"
                height: 2
                opacity: 1
            }
            PropertyChanges {
                target: buttonText
                color: "#00d66f"
            }
            PropertyChanges {
                target: changePageButton
                selected: true
            }
        }
    ]

    MouseArea {
        anchors.fill: parent
        onClicked: {
            changePageButton.buttonClicked()
        }
    }

}
