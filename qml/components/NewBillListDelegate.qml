import QtQuick 2.0

Item {
    id: delegate
    width: appStyle.applicationWidth;
    height: appStyle.applicationHeight * 0.09

    anchors {
        horizontalCenter: parent.horizontalCenter
    }

    Component.onCompleted: {
        if(isSumarryItem){
            state = "summaryOrder"
        }else{
            if(billStatus === "2") {
                state = "reversalOrder"
            }else{
                state = "normalOrder"
            }
        }
    }

    Text {
        id: dateText
        text: orderDate
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            left: parent.left
            leftMargin: appStyle.applicationWidth * 0.05
            bottom: simpleLine.top
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.heavyFontFamily
        }
    }

    Text {
        id: hourText
        text: orderHour
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            left: dateText.right
            leftMargin: appStyle.applicationWidth * 0.02
            bottom: simpleLine.top
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    Text {
        id: discountText
        text: Number(discount).toFixed(2)
        color: "#00d66f"
        //renderType: Text.NativeRendering

        onTextChanged: {
            if(discountText.text === "0.00"){
                discountText.visible = false
            }else{
                discountText.visible = true
            }
        }

        anchors {
            right: parent.right
            rightMargin: appStyle.applicationWidth * 0.25
            bottom: simpleLine.top
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    Text {
        id: valueText
        text: value
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            right: parent.right
            rightMargin: appStyle.applicationWidth * 0.05
            bottom: simpleLine.top
        }

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    states: [
        State{
            name: "normalOrder"
            PropertyChanges {
                target: valueText
                color: appStyle.defaultTextColor
            }
        },
        State{
            name: "reversalOrder"
            PropertyChanges {
                target: valueText
                color: appStyle.redTextColor
            }
        },
        State{
            name: "summaryOrder"
            PropertyChanges {
                target: valueText
                color: appStyle.defaultTextColor
            }
            PropertyChanges {
                target: dateText
                text: qsTr("Daily total") + Settings.emptyString
            }
            PropertyChanges {
                target: hourText
                visible: false
            }
            PropertyChanges {
                target: discountText
                text: Number(summaryDiscount).toFixed(2)
            }
            PropertyChanges {
                target: valueText
                text: Number(summaryValue).toFixed(2)
            }
            PropertyChanges {
                target: delegate
                height: appStyle.applicationHeight * 0.13
            }
            PropertyChanges {
                target: simpleLine
                anchors.bottomMargin: appStyle.applicationHeight * 0.04
            }
        }

    ]

    SimpleLine {
        id: simpleLine
    }
}
