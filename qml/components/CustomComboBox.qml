import QtQuick 2.4

Item {
    id: customComboBox
    width: appStyle.defRectButtonWidth
    height: appStyle.defRectButtonHeight
    property bool enabled: true
    property alias model: list.model
    property alias currentIndex: list.currentIndex
    property alias currentItem: list.currentItem
    property string currentText: ""
    property alias count: list.count

    signal indexChanged

    state: "collapsed"

    onFocusChanged: {
        if (!focus)
            state = "collapsed"
    }

    onEnabledChanged: {
        if (!enabled)
            state = "collapsed"
    }

    function isVariableArray(variable) {
        if (variable.constructor === Array) {
            return true;
        }

        return false;
    }

    Item {
        id: comboBg
        anchors.fill: parent

        Text {
            id: label

            color: appStyle.defaultTextColor
            text: qsTr("Beacon Type") + Settings.emptyString
            //renderType: Text.NativeRendering

            opacity: 0.5

            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
            }

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 14 * appStyle.fontScale
            }
        }

        Text {
            id: textValue

            color: appStyle.defaultTextColor
            text: currentText
            //renderType: Text.NativeRendering

            opacity: 0.4

            anchors {
                verticalCenter: parent.verticalCenter
                left: label.right
                leftMargin: customComboBox.width * 0.05
            }

            font {
                family: appStyle.defaultFontFamily
                pixelSize: 14 * appStyle.fontScale
            }
        }

        SvgButton {
            id: arrowButton
            height: appStyle.applicationHeight * 0.02
            sourceSize.width: width * 4
            downState: "qrc:/images/DownArrow"
            upState: "qrc:/images/DownArrow"

            anchors {
                verticalCenter: parent.verticalCenter
                right: parent.right
                rightMargin: customComboBox.width * 0.1
            }
        }

        MouseArea {
            anchors {
                fill: parent
            }

            enabled: customComboBox.enabled
            onClicked: {
                customComboBox.focus = true

                if (customComboBox.state == "collapsed")
                    customComboBox.state = "expanded"
                else
                    customComboBox.state = "collapsed"
            }
        }
    }

    ListView {
        id: list
        clip: true
        boundsBehavior: ListView.StopAtBounds
        z: customComboBox.z + 100

        anchors {
            left: parent.left
            right: parent.right
            top: comboBg.bottom
        }

        property int globalY
        property int maxHeight: appStyle.applicationHeight * 0.25

        function recalculateGlobalY() {
            var par = parent
            globalY = y

            while(par != null) {
                globalY += par.y
                par = par.parent
            }
        }

        Component.onCompleted: recalculateGlobalY()
        onYChanged: recalculateGlobalY()

        Rectangle {
            color: "transparent"
            border {
                width: 0
                color: "lightgray"
            }

            anchors {
                fill: parent
            }
        }

        Behavior on height {
            NumberAnimation { duration: 100 }
        }

        delegate: Rectangle {
            width: list.width
            height: customComboBox.height * 1.2
            color: "white"

            Text {
                color: appStyle.defaultTextColor
                opacity: 0.5
                text: model.text
                //renderType: Text.NativeRendering
                font {
                    family: appStyle.defaultFontFamily
                    pixelSize: 14 * appStyle.fontScale
                }

                anchors {
                    left: parent.left
                    verticalCenter: parent.verticalCenter
                }
            }

            MouseArea {
                anchors {
                    fill: parent
                }

                onClicked: {
                    list.currentIndex = model.index
                    customComboBox.state = "collapsed"
                }
            }
        }

        onCurrentIndexChanged: {
            if (currentIndex == -1 || ( isVariableArray(list.model) && list.model.length === 0))
                return;

            currentText = list.model.get(currentIndex).text

            indexChanged();
        }

        onModelChanged: {
            var count = isVariableArray(model) ? model.length : model.count

            if (count === 0) {
                currentIndex = -1;
                currentText = ""
            } else {
                currentIndex = 0;
                currentText = list.model.get(currentIndex).text
            }
        }
    }

    states: [
        State {
            name: "collapsed"
            PropertyChanges { target: list; height: 0 }
        },
        State {
            name: "expanded"
            PropertyChanges { target: list; height: Math.min(list.contentHeight, list.maxHeight) }
        }
    ]
}

