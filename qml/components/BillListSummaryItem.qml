import QtQuick 2.0

Item {
    id: summaryItem
    visible: false
    width: parent.width
    height: appStyle.applicationHeight * 0.09

    property string date
    property string summaryValue: newOrderListModel.getValueSummary(date)

    property string summaryDiscount: newOrderListModel.getDiscountSummary(date)

    Text {
        id: totalAmountText
        text: qsTr("Daily total") + Settings.emptyString
        //renderType: Text.NativeRendering

        anchors {
            left: summaryLine.left
            bottom: summaryLine.bottom
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }
    }

    Text {
        id: discountValueText
        text: summaryDiscount
        //renderType: Text.NativeRendering

        color: "#00d66f"

        anchors {
            bottom: summaryLine.bottom
            right: summaryItem.right
            rightMargin: appStyle.applicationWidth * 0.25
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    Text {
        id: summaryValueText
        text: summaryValue
        //renderType: Text.NativeRendering

        anchors {
            bottom: summaryLine.bottom
            right: summaryItem.right
            rightMargin: appStyle.applicationWidth * 0.05
        }

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    SimpleLine {
        id: summaryLine
    }
}
