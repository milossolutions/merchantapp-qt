import QtQuick 2.5
import QtQuick.Controls 1.4
import milosolutions.com 1.0

StackView {
    id: stackView
    clip: true

    readonly property bool empty: (depth == 0)
    property bool active: false
    property bool immediateInitialization: true
    property bool initError: false
    property bool initComplete: false
    property var currentPageId: 0

    /*
        Expects array of JS objects where each of them contains
        two <key, value> pairs:

        {
            pageId: <PAGE_ID>,
            pageSource: <SOURCE_FILE_URI>
        }

        pageId will be passed as a 'key' in items map (the one declared below)
        pageSource is an URI to the QML file which would be used to create object
                   used as a value in the items map (the one declared below)

    */
    property var initializationList: []

    /*
        JS dictionary with <key, value> pairs
        key -> might be Globals::Enums::FullScreenPage, Globals::Enums::CentralPage etc.
        value -> created QML item
    */
    property var items: ({})

    signal initialized
    signal initializationFailed
    signal popped(var item)
    signal pushed(var item)

    delegate: StackViewDelegate {

        popTransition: StackViewTransition {
            PropertyAnimation {
                target: exitItem
                duration: 350
                property: "y"
                from: 0
                to: appStyle.applicationHeight
            }

            onStarted: {
                enterItem.visible = true
            }

            onStopped: {
                exitItem.visible = false
                exitItem.y = 0
            }
        }
    }

    Component.onCompleted: if (immediateInitialization) initialize()

    function initialize() {

        var lenght = initializationList.length;
        for (var i = 0; i < lenght; ++i) {

            var modelItem = initializationList[i]
            var component = Qt.createComponent(modelItem.pageSource)

            if (Component.Ready === component.status) {

                var item = component.createObject(stackView, { "pageId": modelItem.pageId, "visible": false })
                if(item == null){
                    console.log("Error occured")
                }

                items[modelItem.pageId] = item

            } else {
                console.warn("StackView initialization failed: " + component.errorString())

                initError = true
                initializationFailed()
            }
        }
        initComplete = true
        initError = false
        initialized()

    }

    function reload() {

        clear()
        initialize()

    }

    function pushPage(pageId) {
        currentPageId = pageId
        push(items[pageId])

    }

    function pushPageWithoutAnimation(pageId) {
        currentPageId = pageId
        push({item:items[pageId], immediate: true})
    }

    function popPageWithoutAnimation(){
        pop({immediate: true})
    }

    function replacePage(pageId, useImmediate) {

        currentPageId = pageId
        var pushedItem = undefined

        if (useImmediate)
            pushedItem = push({item: items[pageId], replace: true, immediate: true})
        else
            pushedItem = push({item: items[pageId], replace: true})

    }

    function goBackToFirst() {
        pop({item:null, immediate: true})
    }
}
