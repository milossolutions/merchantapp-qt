import QtQuick 2.4

Image {
    id: checkBox

    source: unchecked
    height: parent.height
    width: height
    fillMode: Image.PreserveAspectFit
    sourceSize.width: width * 2
    sourceSize.height: height * 2

    signal toggled(bool checked)

    state: 'UnChecked'

    property string unchecked: "qrc:/images/CheckOffImage"
    property string checked: "qrc:/images/CheckOnImage"

    function isChecked() {
        return checkBox.state == 'Checked' ? true : false
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {

            checkBox.state == 'Checked' ? checkBox.state = 'UnChecked' :
                                          checkBox.state = 'Checked'
            checkBox.toggled( checkBox.state == 'Checked' ? true : false)
        }
    }

    states: [
        State {
            name: "Checked"
            PropertyChanges {
                target: checkBox
                source: checked
            }
        },
        State {
            name: "UnChecked"
            PropertyChanges {
                target: checkBox
                source: unchecked
            }
        }
    ]
}
