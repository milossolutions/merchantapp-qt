import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QMLEnums 1.0

FocusScope {
    id: delegat
    height: appStyle.applicationHeight * 0.13
    width: parent.width

    Component.onCompleted: {
        if(Amount === 0.00)
            currentDiscountAmount.text = Amount
    }

    property alias inputHasFocus: currentDiscountAmount.activeFocus

    function receivedKeyboardEvent(value){
        if(value === "DEL"){
            currentDiscountAmount.text = currentDiscountAmount.text.slice(0,-1)
            currentDiscountAmount.forceActiveFocus()
        }else{
            currentDiscountAmount.text += value
            if(!currentDiscountAmount.acceptableInput){
                currentDiscountAmount.text = currentDiscountAmount.text.slice(0,-1)
            }
            currentDiscountAmount.forceActiveFocus()
        }
    }

    TextArea {
        id: title
        text: Title
        focus: true
        frameVisible: false
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        readOnly: true
        textMargin: 0
        width: 150 * appStyle.fontScale
        height: 32 * appStyle.fontScale
        wrapMode: Text.Wrap
        horizontalAlignment: Qt.AlignLeft

        style: TextAreaStyle {
            backgroundColor: "#ffffff"
        }

        anchors {
            top: parent.top
            topMargin: 14 * appStyle.fontScale
            left: parent.left
            right: inputBorder.left
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.heavyFontFamily
        }
    }

    MouseArea {
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: inputBorder.left
        }
        onClicked: {
            forceActiveFocus()
        }
    }


    TextInput {
        id: currentDiscountAmount
        z: 10

        width: 79 * appStyle.fontScale * 1.2
        height: 41 * appStyle.fontScale
        horizontalAlignment: Qt.AlignRight
        //renderType: Text.NativeRendering

        validator: RegExpValidator { regExp: /([0-9]?[0-9]?[0-9]?[0-9]?)?([.,][0-9]?[0-9]?)?/ }

        anchors {
            top: inputBorder.top
            topMargin: inputBorder.height * 0.1
            right: inputBorder.right
            rightMargin: inputBorder.width * 0.05
        }

        font {
            pixelSize: 26 * appStyle.fontScale
            family: appStyle.lightFontFamily

        }

        onTextChanged: {
            var couponValue = text.replace(",", ".")
            if(couponValue > 0.00){
                color = appStyle.greenTextColor
            } else {
                color = appStyle.defaultTextColor
            }

            if(couponValue === "" || couponValue === "."){
                couponValue = 0
            }

            couponListModel.setModelData( index,
                                         couponValue,
                                         CouponModelRole.Amount)
            calculateTotalPrice()
        }

        cursorDelegate: CursorDelegateComponent {
            id: cursor
        }
    }

    Rectangle {
        id: inputBorder
        radius: 4
        z: 0
        color: "transparent"

        width: 79 * appStyle.fontScale * 1.4
        height: 41 * appStyle.fontScale

        anchors {
            top: parent.top
            topMargin: 13 * appStyle.fontScale
            left: parent.right
            leftMargin: -width
        }

        border {
            color: "#D8D8D8"
            width: 1
        }
    }

    SimpleLine {
        opacity: 0.5
        width: parent.width
        anchors {
            bottom: parent.bottom
        }
    }
}
