import QtQuick 2.4

Item{
    id: buttonItem
    width: appStyle.defRectButtonWidth
    height: appStyle.defRectButtonHeight

    signal buttonClicked()
    property string buttonText: qsTr("Ok")
    property string textColor: "white"
    property string upStateColor: "#00d66f"
    property string downStateColor: "#00BD62"
    property bool buttonClickState: false
    property bool isInactive: false
    property int buttonRadius: appStyle.defaultButtonRadius

    property alias buttonFontSize: buttonTextComponent.font.pixelSize

    property alias buttonState: button.state

    Rectangle {
        id: button
        anchors.fill: parent
        opacity: appStyle.opacityUpState
        color: buttonItem.upStateColor
        radius: buttonItem.buttonRadius

        MouseArea {
            anchors.fill: parent
            onPressed: {
                if(button.state !== "disabled"){
                    button.state = 'clicked'
                }
            }
            onReleased: {
                if(button.state !== "disabled"){
                    button.state = ""
                    buttonClicked()
                } else {
                    buttonClicked()
                }
            }
        }

        states: [
            State {
                name: "clicked"
                PropertyChanges {
                    target: button;
                    color: buttonItem.downStateColor
                }
            },
            State {
                name: "disabled"
                PropertyChanges {
                    target: button
                    opacity: 0.3
                    enabled: false
                }
            }

        ]

    }

    Text {
        id: buttonTextComponent
        color: textColor
        text: buttonText
        //renderType: Text.NativeRendering
        font {
            family: appStyle.lightFontFamily
            pixelSize: 17 * appStyle.fontScale
        }
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
    }
}
