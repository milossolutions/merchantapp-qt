import QtQuick 2.4

import "qrc:/qml/screens"

ScreenInterface {
    id: statusBar
    height: parent.height * 0.07

    signal backClicked
    signal cashierNameClicked
    signal loginComboClicked
    signal gearButtonClicked

    property alias loginComboBoxAlias: loginComboBox

    Connections {
        target: Settings

        onIsSelectUserChanged: {
            if(Settings.isSelectUser){
                loginComboBox.visible = true
            }else{
                loginComboBox.visible = false
            }
        }
    }

    //Implementation related with frameless window lets user to move window around on header click
//    MouseArea {
//        anchors {
//            fill: parent
//        }

//        property point lastMousePos: Qt.point(0, 0)

//        onPressed: {
//            lastMousePos = Qt.point(mouseX, mouseY);
//        }

//        onMouseXChanged: {
//            if(mainView.visibility === 4){
//                return;
//            }

//            mainView.x += (mouseX - lastMousePos.x)
//        }

//        onMouseYChanged: {
//            if(mainView.visibility === 4){
//                return;
//            }

//            mainView.y += (mouseY - lastMousePos.y)
//        }
//    }

    SvgButton{
        id: lockButton
        downState: "qrc:/images/HeaderLogoImage"
        upState: "qrc:/images/HeaderLogoImage"
        height: appStyle.headerLogoHeight
        fillMode: Image.PreserveAspectFit
        anchors {
            left: parent.left
            leftMargin: appStyle.applicationWidth * 0.05
            verticalCenter: parent.verticalCenter
        }
    }

    LoginComboBox {
        id: loginComboBox

        visible: Settings.isSelectUser

        onLoginClicked: {
            if(ApplicationStatus.credentialsStatus){
                statusBar.loginComboClicked()
            }
        }
    }

    Image {
        id: gearButton
        height: 0.035 * appStyle.applicationHeight
        width: height
        fillMode: Image.PreserveAspectFit
        sourceSize.width: width * 2
        sourceSize.height: height * 2
        source: "qrc:/images/GearGray"
        visible: true

        anchors {
            right: parent.right
            rightMargin: parent.width * 0.05
            verticalCenter: parent.verticalCenter
        }

        Connections {
            target: gearSettings

            onVisibleChanged: {
                if(gearSettings.visible){
                    gearButton.source = "qrc:/images/GearGreen"
                }else{
                    gearButton.source = "qrc:/images/GearGray"
                }
            }
        }

        MouseArea {
            anchors.fill: parent

            onReleased: {
                statusBar.gearButtonClicked()
            }
        }
    }
}
