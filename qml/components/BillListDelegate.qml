import QtQuick 2.4

Item {
    id: delegate
    width: appStyle.applicationWidth;
    height: appStyle.applicationHeight * 0.09

    Component.onCompleted: {
        if(billStatus === "2"){
            state = "reversalOrder"
        }else{
            state = "normalOrder"
        }
    }

    Text {
        id: dateText
        text: orderDate
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            left: parent.left
            leftMargin: appStyle.applicationWidth * 0.05
            bottom: simpleLine.top
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.heavyFontFamily
        }
    }

    Text {
        id: hourText
        text: orderHour
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            left: dateText.right
            leftMargin: appStyle.applicationWidth * 0.02
            bottom: simpleLine.top
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    Text {
        id: discountText
        text: discount
        color: "#00d66f"
        //renderType: Text.NativeRendering

        anchors {
            right: parent.right
            rightMargin: appStyle.applicationWidth * 0.25
            bottom: simpleLine.top
        }

        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    Text {
        id: valueText
        text: value
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering

        anchors {
            right: parent.right
            rightMargin: appStyle.applicationWidth * 0.05
            bottom: simpleLine.top
        }

        font {
            pixelSize: 16 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
    }

    states: [
        State{
            name: "normalOrder"
            PropertyChanges {
                target: valueText
                color: appStyle.defaultTextColor
            }
        },
        State{
            name: "reversalOrder"
            PropertyChanges {
                target: valueText
                color: appStyle.redTextColor
            }
        }

    ]

    SimpleLine {
        id: simpleLine
    }
}
