import QtQuick 2.0

Item {
    id: toogleButton
    width: appStyle.defRectButtonWidth
    height: appStyle.defRectButtonHeight

    property string upStateColor: "#a6a6a6"
    property string downStateColor: "#808080"

    state: "USB"

    function switchState() {
        if(toogleButton.state === "USB"){
            toogleButton.state = "WIFI"
        }else if(toogleButton.state === "WIFI"){
            toogleButton.state = "USB"
        }
    }

    Rectangle {
        id: button
        opacity: appStyle.opacityUpState
        color: toogleButton.upStateColor
        radius: 4
        height: parent.height * 0.8

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        Text {
            id: deviceTypeText
            //renderType: Text.NativeRendering

            color: "white"

            anchors {
                left: parent.left
                leftMargin: appStyle.applicationWidth * 0.05
                verticalCenter: parent.verticalCenter
            }

            font {
                family: appStyle.heavyFontFamily
                pixelSize: 14 * appStyle.fontScale
            }
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                button.state = 'clicked'
            }
            onReleased: {
                button.state = ""
                switchState()
            }
        }

        states: [
            State {
                name: "clicked"
                PropertyChanges {
                    target: button;
                    color: toogleButton.downStateColor
                }
            }
        ]
    }

    states: [
        State {
            name: "USB"
            PropertyChanges {
                target: deviceTypeText
                text: qsTr("Keacon type: USB") + Settings.emptyString
            }

        },
        State {
            name: "WIFI"
            PropertyChanges {
                target: deviceTypeText
                text: qsTr("Keacon type: WIFI") + Settings.emptyString
            }
        }

    ]
}
