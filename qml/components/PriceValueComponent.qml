import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

Item {
    id: priceValueItem

    property string priceValueString: currentOrderItem.orderValue

    Text {
        id: priceValueText
        text: priceValueString
        opacity: 0.5
        //renderType: Text.NativeRendering

        font {
            family: appStyle.lightFontFamily
            pixelSize: 50 * appStyle.fontScale
        }

        anchors {
            bottom: parent.bottom
            bottomMargin: appStyle.applicationHeight * 0.02
            horizontalCenter: parent.horizontalCenter
        }
    }

    Rectangle {
        id: grayLine
        color: "#26262a"
        opacity: 0.1
        height: 1
        width: parent.width

        anchors {
            top: priceValueText.bottom
            topMargin: appStyle.applicationHeight * 0.02
        }
    }
}
