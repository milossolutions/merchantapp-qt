import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: numpadButton
    signal buttonClicked
    property int buttonIndex: 0
    property string buttonTextString: qsTr("0")
    property string underTextString: qsTr("")

    Column {
        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: buttonText
            text: buttonTextString
            color: "#000000"
            opacity: 0.5
            //renderType: Text.NativeRendering
            font {
                family: appStyle.defaultFontFamily
                pixelSize: 24 * appStyle.fontScale
            }

            anchors {
                horizontalCenter: parent.horizontalCenter
            }
        }
    }

    Rectangle {
        id: buttonDownstate
        height: parent.height
        width: parent.width
        visible: false
        opacity: 0.4
        color: appStyle.defaultTextColor
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            numpadButton.state = "Clicked"
            numpadButton.buttonClicked()
        }
        onReleased: {
            numpadButton.state = ""
        }
    }

    states: [
        State {
            name: "Clicked"
            PropertyChanges {
                target: buttonText
                opacity: appStyle.opacityNPDownState
            }
            PropertyChanges {
                target: buttonDownstate
                visible: true
            }
        }


    ]
}
