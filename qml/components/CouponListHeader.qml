import QtQuick 2.4

Item {
    id: header
    width: parent.width
    height: couponLabel.height + couponDescription.height

    Text {
        id: couponLabel
        width: parent.width
        height: appStyle.applicationHeight * 0.02
        //renderType: Text.NativeRendering
        anchors {
            top: parent.top
        }
        font {
            pixelSize: 14 * appStyle.fontScale
            family: appStyle.defaultFontFamily
        }
        horizontalAlignment: Qt.AlignLeft
        text: qsTr( "COUPONS") + Settings.emptyString
    }

    Text {
        id: couponDescription
        color: "#00d66f"
        width: parent.width
        height: appStyle.applicationHeight * 0.02
        //renderType: Text.NativeRendering

        anchors {
            top: couponLabel.bottom
            topMargin: 2 * appStyle.fontScale
        }
        font {
            pixelSize: 11 * appStyle.fontScale
            family: appStyle.defaultFontFamily
            weight: Font.Bold
        }
        horizontalAlignment: Text.AlignRight
        text: qsTr( "Enter amount") + Settings.emptyString
    }
}
