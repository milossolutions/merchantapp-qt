import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: cursor
    width: 2
    height: parent.cursorRectangle.height
    color: "#00d66f"
    antialiasing: false
    visible: parent.activeFocus && parent.selectionStart === parent.selectionEnd
    state: "on"
    Timer {
        running: true
        repeat: true
        interval: 500
        onTriggered: cursor.state = cursor.state == "on" ? "off" : "on"
    }

    states: [
        State {
            name: "on"
            PropertyChanges { target: cursor; opacity: 1 }
        },
        State {
            name: "off"
            PropertyChanges { target: cursor; opacity: 0 }
        }
    ]

    transitions: [
        Transition {
            from: "on"
            to: "off"
            NumberAnimation { property: "opacity"; duration: 150 }
        }
    ]
}
