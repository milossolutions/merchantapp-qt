import QtQuick 2.0

Rectangle {
    id: headerLine
    height: 1
    width: parent.width * 0.9
    color: "#000000"

    anchors {
        bottom: parent.bottom
        horizontalCenter: parent.horizontalCenter
    }
}
