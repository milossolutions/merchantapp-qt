import QtQuick 2.4

Item {
    id: sectionDelegate
    width: appStyle.applicationWidth
    height: dateItem.height + headerItem.height

    anchors {
        horizontalCenter: parent.horizontalCenter
    }


    Column {
        id: sectionColumn
        width: parent.width
        height: parent.height

        anchors {
            fill: parent
        }

        Item {
            id: dateItem
            width: parent.width
            height: appStyle.applicationHeight * 0.06

            Text {
                text: section
                //renderType: Text.NativeRendering
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }

                font {
                    pixelSize: 13 * appStyle.fontScale
                    //should be MarkPro-Bold
                    family: appStyle.heavyFontFamily
                }
            }
        }


        Item {
            id: headerItem
            width: parent.width
            height: appStyle.applicationHeight * 0.04

            Text {
                id: discountText
                text: qsTr("Discount") + Settings.emptyString
                //renderType: Text.NativeRendering

                anchors {
                    right: headerItem.right
                    rightMargin: appStyle.applicationWidth * 0.25
                    verticalCenter: headerItem.verticalCenter
                }

                font {
                    pixelSize: 13 * appStyle.fontScale
                    family: appStyle.defaultFontFamily
                }
            }

            Text {
                id: totalText
                text: qsTr("Total") + Settings.emptyString
                //renderType: Text.NativeRendering

                anchors {
                    right: headerItem.right
                    rightMargin: appStyle.applicationWidth * 0.05
                    verticalCenter: headerItem.verticalCenter
                }

                font {
                    pixelSize: 13 * appStyle.fontScale
                    family: appStyle.defaultFontFamily
                }
            }

            SimpleLine {
                id: headerLine
            }
        }
    }
}
