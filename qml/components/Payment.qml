import QtQuick 2.4

import "qrc:/qml/components"
import milosolutions.com 1.0

Item {
    id: paymentScreen

    onVisibleChanged: {
        if(visible === true){
            priceField.inputString = currentOrderItem.orderValue
            calculateTotalPrice()
        }
    }

    property bool keyboardVisibility: false
    property alias couponsViewAlias: couponsView
    property bool discountVisibility: false

    // handling prices and user interaction
    // model has also guards
    Connections {
        target: priceField.textField
        onTextChanged: calculateTotalPrice()
    }

    Connections {
        target: UiController

        onShowMessage: {
            if(popUpType === Enums.ConfirmPopUp){
                priceField.textField.text = ""
            }
        }
    }

    function getTotalPrice() {
        calculateTotalPrice()
        return totalPriceValueLabel.text
    }

    function setPrice(value) {
        priceField.textField.text = value;
    }

    function calculateTotalPrice() {
        var discount = couponListModel.getTotalDiscount();
        rabatTotalValueLabel.text = discount.toFixed( 2);

        if(discount > 0.00){
            discountVisibility = true
        }else{
            discountVisibility = false
        }

        var price = priceField.textField.text.replace( ",", ".");
        var totalValue = price - discount;

        totalPriceValueLabel.text = totalValue.toFixed( 2);
    }

    function getCheckedCoupons() {
        return couponListModel.getCheckedCoupons();
    }

    function checkFocus(){
        var hasFocus = false;

        if(couponsView.activeFocus){
            hasFocus = couponsView.currentItem.inputHasFocus;
        }
        paymentScreen.keyboardVisibility = hasFocus
    }

    function receivedKeyboardEvent(value){
        if(couponsView.currentItem.activeFocus){
            couponsView.currentItem.receivedKeyboardEvent(value)
        }
    }

    function clearKeyboardFocus(){
        if(priceField.isActiveFocus){
            priceField.isActiveFocus = false
        }

        if(couponsView.currentItem.activeFocus){
            couponsView.currentItem.focus = false
        }
    }

    InputWithLabel {
        id: priceField
        labelText: qsTr("Zwischentotal") + Settings.emptyString
        enabled: false
        anchors {
            top: parent.top
            topMargin: appStyle.applicationHeight * 0.02
        }
        textField.validator: RegExpValidator { regExp: /([0-9]?[0-9]?[0-9]?[0-9]?)?([.,][0-9]?[0-9]?)?/ }

        onIsActiveFocusChanged: {
            checkFocus()
        }
    }

    // Rabatt area
    Text {
        id: rabatLabel
        color: appStyle.confirmScreenColor
        horizontalAlignment: Qt.AlignLeft
        text: qsTr( "Discount") + Settings.emptyString
        //renderType: Text.NativeRendering
        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.defaultFontFamily
            capitalization: Font.AllUppercase
        }
        anchors {
            top: priceField.bottom
            topMargin: 8 * appStyle.fontScale
        }
    }

    Text {
        id: rabatTotalValueLabel
        color: appStyle.confirmScreenColor
        visible: discountVisibility
        horizontalAlignment: Qt.AlignRight
        text: qsTr( "0.00")
        //renderType: Text.NativeRendering
        font {
            pixelSize: 21 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
        anchors {
            top: priceField.bottom
            right: parent.right
            topMargin: 3 * appStyle.fontScale
        }
    }

    SimpleLine {
        id: rabatLine
        color: appStyle.confirmScreenColor
        width: parent.width
        anchors {
            top: rabatTotalValueLabel.bottom
            bottom: undefined // reset anchor for simple line
            horizontalCenter: parent.horizontalCenter
        }
    }

    // Total price area
    Text {
        id: totalPriceLabel
        color: appStyle.defaultTextColor
        //renderType: Text.NativeRendering
        font {
            pixelSize: 13 * appStyle.fontScale
            family: appStyle.defaultFontFamily
            capitalization: Font.AllUppercase
        }
        anchors {
            top: rabatLine.bottom
            topMargin: 30 * appStyle.fontScale
        }
        horizontalAlignment: Qt.AlignLeft
        text: qsTr( "TOTAL") + Settings.emptyString
    }

    Text {
        id: totalPriceValueLabel
        color: "#595959"
        z: 10
        //renderType: Text.NativeRendering
        font {
            pixelSize: 32 * appStyle.fontScale
            family: appStyle.lightFontFamily
        }
        anchors {
            top: rabatLine.bottom
            topMargin: 11 * appStyle.fontScale
            right: parent.right
        }
        lineHeight: 70
        horizontalAlignment: Qt.AlignRight
        text: qsTr( "0.00")

        onTextChanged: {
            if(text < 0){
                paymentScreen.state = "invalidValues"
            }else{
                paymentScreen.state = "validValues"
            }
        }
    }

    SimpleLine {
        id: totalPriceLine
        width: parent.width
        height: 2
        anchors {
            top: totalPriceLabel.bottom
            bottom: undefined // reset anchor for simple line
            horizontalCenter: parent.horizontalCenter
        }
    }

    // Coupon view
    ListView {
        id: couponsView

        orientation: ListView.Vertical
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        anchors {
            top: totalPriceLine.bottom
            topMargin: appStyle.applicationHeight * 0.025
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
        }

        width: parent.width
        spacing: 8
        onFocusChanged: {
            paymentScreen.checkFocus()
        }

        model: couponListModel
        delegate: CouponListDelegate {
            onInputHasFocusChanged: {
                if(inputHasFocus){
                    couponsView.currentIndex = index
                }
                paymentScreen.checkFocus()
            }
        }
        header: CouponListHeader {}
    }

    states: [
        State {
            name: "invalidValues"
            PropertyChanges {
                target: totalPriceValueLabel
                color: "#db485a"
            }
        },
        State {
            name: "validValues"
            PropertyChanges {
                target: totalPriceValueLabel
                color: "#595959"
            }
        }
    ]
}
