import QtQuick 2.4

Item {
    readonly property int defaultWidth: 320
    readonly property int defaultHeight: 548
    readonly property real defaultWindowRatio: defaultWidth / defaultHeight
    property int applicationWidth: defaultWidth
    property int applicationHeight: defaultHeight
    property real currentWindowRatio: applicationWidth / applicationHeight
    property real realApplicationWidth: applicationWidth


    //font
    property real fontScale: applicationHeight / defaultHeight

    //sizes
    property int defRectButtonWidth: 0.9 * applicationWidth
    property int defRectButtonHeight: 0.1 * applicationHeight
    property int loginLogoHeight: 0.13 * applicationHeight
    property int headerLogoHeight: 0.05 * applicationHeight
    property int headerLockHeight: 0.02 * applicationHeight
    property int horizontalLineHeight: 0.002 * applicationHeight

    //margins
    property int defCompSpacing: 0.02 * applicationHeight
    property int defInputMargin: 0.06 * applicationHeight

    //colors
    readonly property color defaultTextColor:             "#262626"
    readonly property color greenTextColor:               "#00d66f"
    readonly property color redTextColor:                 "#db485a"
    readonly property color defaultBgColor:               "#FFFFFF"
    readonly property color confirmScreenColor:           "#00d66f"
    readonly property color errorScreenColor:             "#db485a"
    readonly property color whiteTextColor:               "#FFFFFF"

    //opacity
    readonly property real opacityUpState : 1.0
    readonly property real opacityDownState : 0.4

    //opacity numpad
    readonly property real opacityNPUpState : 0.4
    readonly property real opacityNPDownState : 0.7

    //fonts
    readonly property string defaultFontFamily: "Mark Offc Pro"
    readonly property string lightFontFamily: "Mark Offc Pro Light"
    readonly property string heavyFontFamily: "Mark Offc Pro Heavy"

    //radius
    readonly property int defaultButtonRadius: 4

    // coupon view
    readonly property int verticalItemSpacing: 8


}
