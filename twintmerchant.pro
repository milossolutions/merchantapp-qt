## Milo Solutions - project file template
#
#
## (c) Milo Solutions, 2016

QT = core qml quick svg xml sql network serialport

CONFIG += c++11

## Set up application version
VERSION=1.7.0.0
## Add version define. You can now use this in C++ code:
#  QString someString(APP_VERSION);
DEFINES+= APP_VERSION=\\\"$$VERSION\\\"

TEMPLATE = app

TARGET = "TWINT Merchant"

#Include 3rd party modules
include(src/qtsingleapplication/src/qtsingleapplication.pri)

# High DPI support for Qt 5.6++
equals(QT_MAJOR_VERSION, 5):greaterThan(QT_MINOR_VERSION, 5) {
    DEFINES += USE_HIGHDPI
}

# OS SPECIFIC RESOURCES
macx {
    ICON = $${PWD}/macicon.icns
}
win32 {
    RC_FILE = icon.rc
}

HEADERS += \
    src/DataManagers/settings.h \
    src/applicationcontroller.h \
    src/DataManagers/globalsettings.h \
    src/Communication/soapmanager.h \
    src/Communication/usbmanager.h \
    src/Communication/wifimanager.h \
    src/DataManagers/datamanager.h \
    src/DataManagers/databasemanager.h \
    src/uicontroller.h \
    src/Communication/communicationmanager.h \
    src/Communication/Messages/messageinterface.h \
    src/Communication/Messages/enrollcashregistermessage.h \
    src/Communication/Messages/requestcheckinmessage.h \
    src/Communication/Messages/monitorcheckinmessage.h \
    src/Communication/Messages/cancelcheckinmessage.h \
    src/Communication/Messages/startordermessage.h \
    src/Communication/Messages/confirmordermessage.h \
    src/Communication/Messages/cancelordermessage.h \
    src/Communication/Messages/findordermessage.h \
    src/Communication/Messages/checksystemstatusmessage.h \
    src/Communication/Messages/monitorordermessage.h \
    src/DataManagers/Items/ResponseItems/cancelcheckinresponseitem.h \
    src/DataManagers/Items/ResponseItems/cancelorderresponseitem.h \
    src/DataManagers/Items/ResponseItems/checksystemstatusresponseitem.h \
    src/DataManagers/Items/ResponseItems/confirmorderresponseitem.h \
    src/DataManagers/Items/ResponseItems/enrollcashregisterresponseitem.h \
    src/DataManagers/Items/ResponseItems/findorderresponseitem.h \
    src/DataManagers/Items/ResponseItems/responseiteminterface.h \
    src/DataManagers/Items/ResponseItems/monitorcheckinresponseitem.h \
    src/DataManagers/Items/ResponseItems/requestcheckinresponseitem.h \
    src/DataManagers/Items/ResponseItems/startorderresponseitem.h \
    src/DataManagers/Items/Types/checkinnotificationtype.h \
    src/DataManagers/Items/Types/merchantinformationtype.h \
    src/DataManagers/Items/Types/customerinformationtype.h \
    src/DataManagers/Items/Types/loyaltytype.h \
    src/DataManagers/Items/Types/coupontype.h \
    src/DataManagers/Items/Types/currencyamounttype.h \
    src/DataManagers/Items/Types/operationresulttype.h \
    src/DataManagers/Items/Types/orderstatustype.h \
    src/DataManagers/Items/Types/ordertype.h \
    src/DataManagers/Items/Types/beaconsecuritytype.h \
    src/DataManagers/Items/ResponseItems/monitororderresponseitem.h \
    src/DataManagers/Items/Types/paymentamounttype.h \
    src/DataManagers/Items/RequestItems/requestiteminterface.h \
    src/DataManagers/Items/RequestItems/cancelcheckinrequestitem.h \
    src/DataManagers/Items/RequestItems/startorderrequestitem.h \
    src/DataManagers/Items/RequestItems/requestcheckinrequestitem.h \
    src/DataManagers/Items/RequestItems/monitororderrequestitem.h \
    src/DataManagers/Items/RequestItems/monitorcheckinrequestitem.h \
    src/DataManagers/Items/RequestItems/findorderrequestitem.h \
    src/DataManagers/Items/RequestItems/enrollcashregisterrequestitem.h \
    src/DataManagers/Items/RequestItems/confirmorderrequestitem.h \
    src/DataManagers/Items/RequestItems/checksystemstatusrequestitem.h \
    src/DataManagers/Items/RequestItems/cancelorderrequestitem.h \
    src/DataManagers/Items/Types/couponlisttype.h \
    src/DataManagers/Items/Types/rejectedcoupontype.h \
    src/DataManagers/Items/Types/couponrejectionreason.h \
    src/DataManagers/Items/Types/orderrequesttype.h \
    src/DataManagers/Items/Types/orderlinktype.h \
    src/DataManagers/Items/Types/findorderbydate.h \
    src/DataManagers/Items/Types/findorderbyreference.h \
    src/DataManagers/Items/Types/typeinterface.h \
    src/DataManagers/Items/iteminterface.h \
    src/DataManagers/Models/currentorderitem.h \
    src/DataManagers/Models/qrcodeimageprovider.h \
    src/DataManagers/Models/couponlistmodel.h \
    src/DataManagers/Models/coupondata.h \
    src/DataManagers/Items/ContainerItems/containeriteminterface.h \
    src/DataManagers/Items/ContainerItems/startordercontainer.h \
    src/crashhandler.h \
    src/DataManagers/Items/ContainerItems/monitorordercontainer.h \
    src/DataManagers/Items/ContainerItems/keconinformation.h \
    src/Communication/abstractdevicemanager.h \
    src/Communication/abstractdevicedata.h \
    src/DataManagers/Items/ContainerItems/requestcheckincontainer.h \
    src/DataManagers/Models/neworderlistmodel.h \
    src/DataManagers/Items/orderlistitem.h \
    src/messagelogger.h \
    src/DataManagers/Items/ContainerItems/monitorcheckincontainer.h \
    src/Communication/versionchecker.h \
    src/Communication/httpdownload.h \
    src/Communication/commandinterface.h \
    src/DataManagers/applicationstatus.h
macx {
HEADERS +=    src/Communication/customsslcertificatereader.h
}

win32 {
HEADERS +=   
}

SOURCES += src/main.cpp \
    src/DataManagers/settings.cpp \
    src/applicationcontroller.cpp \
    src/DataManagers/globalsettings.cpp \
    src/Communication/soapmanager.cpp \
    src/Communication/usbmanager.cpp \
    src/Communication/wifimanager.cpp \
    src/DataManagers/datamanager.cpp \
    src/DataManagers/databasemanager.cpp \
    src/uicontroller.cpp \
    src/Communication/communicationmanager.cpp \
    src/Communication/Messages/messageinterface.cpp \
    src/Communication/Messages/enrollcashregistermessage.cpp \
    src/Communication/Messages/requestcheckinmessage.cpp \
    src/Communication/Messages/monitorcheckinmessage.cpp \
    src/Communication/Messages/cancelcheckinmessage.cpp \
    src/Communication/Messages/startordermessage.cpp \
    src/Communication/Messages/confirmordermessage.cpp \
    src/Communication/Messages/cancelordermessage.cpp \
    src/Communication/Messages/findordermessage.cpp \
    src/Communication/Messages/checksystemstatusmessage.cpp \
    src/Communication/Messages/monitorordermessage.cpp \
    src/DataManagers/Items/ResponseItems/cancelcheckinresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/cancelorderresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/checksystemstatusresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/confirmorderresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/enrollcashregisterresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/findorderresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/responseiteminterface.cpp \
    src/DataManagers/Items/ResponseItems/monitorcheckinresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/requestcheckinresponseitem.cpp \
    src/DataManagers/Items/ResponseItems/startorderresponseitem.cpp \
    src/DataManagers/Items/Types/checkinnotificationtype.cpp \
    src/DataManagers/Items/Types/merchantinformationtype.cpp \
    src/DataManagers/Items/Types/customerinformationtype.cpp \
    src/DataManagers/Items/Types/loyaltytype.cpp \
    src/DataManagers/Items/Types/coupontype.cpp \
    src/DataManagers/Items/Types/currencyamounttype.cpp \
    src/DataManagers/Items/Types/operationresulttype.cpp \
    src/DataManagers/Items/Types/orderstatustype.cpp \
    src/DataManagers/Items/Types/ordertype.cpp \
    src/DataManagers/Items/Types/beaconsecuritytype.cpp \
    src/DataManagers/Items/ResponseItems/monitororderresponseitem.cpp \
    src/DataManagers/Items/Types/paymentamounttype.cpp \
    src/DataManagers/Items/RequestItems/requestiteminterface.cpp \
    src/DataManagers/Items/RequestItems/cancelcheckinrequestitem.cpp \
    src/DataManagers/Items/RequestItems/startorderrequestitem.cpp \
    src/DataManagers/Items/RequestItems/requestcheckinrequestitem.cpp \
    src/DataManagers/Items/RequestItems/monitororderrequestitem.cpp \
    src/DataManagers/Items/RequestItems/monitorcheckinrequestitem.cpp \
    src/DataManagers/Items/RequestItems/findorderrequestitem.cpp \
    src/DataManagers/Items/RequestItems/enrollcashregisterrequestitem.cpp \
    src/DataManagers/Items/RequestItems/confirmorderrequestitem.cpp \
    src/DataManagers/Items/RequestItems/checksystemstatusrequestitem.cpp \
    src/DataManagers/Items/RequestItems/cancelorderrequestitem.cpp \
    src/DataManagers/Items/Types/couponlisttype.cpp \
    src/DataManagers/Items/Types/rejectedcoupontype.cpp \
    src/DataManagers/Items/Types/couponrejectionreason.cpp \
    src/DataManagers/Items/Types/orderrequesttype.cpp \
    src/DataManagers/Items/Types/orderlinktype.cpp \
    src/DataManagers/Items/Types/findorderbydate.cpp \
    src/DataManagers/Items/Types/findorderbyreference.cpp \
    src/DataManagers/Items/Types/typeinterface.cpp \
    src/DataManagers/Items/iteminterface.cpp \
    src/DataManagers/Models/currentorderitem.cpp \
    src/DataManagers/Models/qrcodeimageprovider.cpp \
    src/DataManagers/Models/couponlistmodel.cpp \
    src/DataManagers/Models/coupondata.cpp \
    src/DataManagers/Items/ContainerItems/containeriteminterface.cpp \
    src/DataManagers/Items/ContainerItems/startordercontainer.cpp \
    src/crashhandler.cpp \
    src/DataManagers/Items/ContainerItems/monitorordercontainer.cpp \
    src/DataManagers/Items/ContainerItems/keconinformation.cpp \
    src/Communication/abstractdevicemanager.cpp \
    src/DataManagers/Items/ContainerItems/requestcheckincontainer.cpp \
    src/DataManagers/Models/neworderlistmodel.cpp \
    src/DataManagers/Items/orderlistitem.cpp \
    src/messagelogger.cpp \
    src/DataManagers/Items/ContainerItems/monitorcheckincontainer.cpp \
    src/Communication/versionchecker.cpp \
    src/Communication/httpdownload.cpp \
    src/DataManagers/applicationstatus.cpp
macx {
SOURCES +=    src/Communication/customsslcertificatereader.cpp
}

win32 {
SOURCES +=   
}


OTHER_FILES += \
    bumpVersion.sh \
    README.md \
    .gitignore

## Feature switcher
# Use this system to quickly enable and disable features in your application
# Works the same as built-in qmake configs like win32, unix, mac*, etc.

## To enable feature1 use:
# qmake -config feature1
# OR:
# CONFIG+=feature1
##
## To disable it, use:
# qmake -config no-feature1


# Respect qmake command-line flags:
no-feature1 { CONFIG -= feature1 }

feature1 {
  INCLUDEPATH += template
  HEADERS += template.h
  SOURCES += template.cpp
}

## Platform-specific stuff
#
#ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

#win32 {
#    RC_FILE = $$PWD/resources.qrc
#}

OTHER_FILES += \
    $$PWD/android/AndroidManifest.xml

## Put all build files into build directory
#  This also works with shadow building, so don't worry!
BUILD_DIR = build
OBJECTS_DIR = $$BUILD_DIR
MOC_DIR = $$BUILD_DIR
RCC_DIR = $$BUILD_DIR
UI_DIR = $$BUILD_DIR

debug: DESTDIR = $$PWD/build/debug
release: DESTDIR = $$PWD/build/release

backtraceDebug {
    DEFINES += USE_BACKTRACE

    win32 {
        LIBS += imagehlp.lib
    }
}

## Setting this enables byppas for peer verification, with opens door to man in
# the midle attacks, using only for dev.

useDebugCert {
    DEFINES += USE_DEBUG_CERT
    release: warning( "Using debug certificate in relase build. In poroduction \
                       remove this CONFIG option.")
}

win32 {
    LIBS += -lws2_32 # for socket keep alive
}

#mac:!ios {
#    #ICON = $$PWD/template.icns
#    QMAKE_INFO_PLIST = $$PWD/Info.plist
#}

DISTFILES += \
    TwintMerchant.doxyfile \
    src/simplecrypt/simplecrypt.pri \
    qml/components/ScrollIndicator.qml


lupdate_only {
    SOURCES = \
    qml/*.qml \
    qml/screens/*.qml \
    qml/components/*.qml \
    src/Communication/Messages/messageinterface.cpp \
    src/DataManagers/Models/currentorderitem.cpp \
    src/Communication/soapmanager.cpp \
    src/Communication/usbmanager.cpp \
    src/applicationcontroller.cpp
}

RESOURCES += \
    resources.qrc

QTPLUGIN     += qsvg qsvgicon
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

INCLUDEPATH += $$PWD/src \
               $$PWD/src/Communication \
               $$PWD/src/Communication/Messages \
               $$PWD/src/DataManagers \
               $$PWD/src/DataManagers/Models \
               $$PWD/src/DataManagers/Items \
               $$PWD/src/DataManagers/Items/ResponseItems \
               $$PWD/src/DataManagers/Items/RequestItems \
               $$PWD/src/DataManagers/Items/Types \
               $$PWD/src/DataManagers/Items/ContainerItems

TRANSLATIONS = $$PWD/translations/tr_en.ts \
               $$PWD/translations/tr_de.ts \
               $$PWD/translations/tr_fr.ts \
               $$PWD/translations/tr_it.ts \

macx {
    INCLUDEPATH += /usr/local/opt/openssl/include
    LIBS += -L/usr/local/opt/openssl/lib -lcrypto -lssl
}
