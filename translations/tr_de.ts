﻿<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ApplicationController</name>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="149"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation type="obsolete">Vollbild</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="151"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="255"/>
        <source>TWINT Windows solution</source>
        <translation>TWINT Windows-Lösung</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="256"/>
        <source>The TWINT Windows solution is already open.</source>
        <translation>Die TWINT Windows-Lösung ist bereits geöffnet.</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="257"/>
        <source>Go to TWINT Windows solution</source>
        <translation>Zur TWINT Windows-Lösung</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="259"/>
        <source>TWINT OS X solution</source>
        <translation>TWINT OS X-Lösung</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="260"/>
        <source>The TWINT OS X solution is already open.</source>
        <translation>Die TWINT OS X-Lösung ist bereits geöffnet.</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="261"/>
        <source>Go to TWINT OS X solution</source>
        <translation>Zur TWINT OS X-Lösung</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="317"/>
        <source>A new version of TWINT Windows Solution is available</source>
        <translation>Eine neue Version für TWINT Windows-Lösung ist verfügbar.</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="319"/>
        <source>A new version of TWINT OS X Solution is available</source>
        <translation>Eine neue Version für TWINT OS X-Lösung ist verfügbar.</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="326"/>
        <location filename="../src/applicationcontroller.cpp" line="354"/>
        <source>Download update</source>
        <translation>Update downloaden</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="345"/>
        <source>A new version of TWINT Windows Solution is available. You need to install the new version before you can use TWINT again.</source>
        <translation>Eine neue Version für TWINT Windows-Lösung ist verfügbar. Bevor Sie TWINT wieder nutzen können, muss die neue Version installiert werden.</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="347"/>
        <source>A new version of TWINT OS X Solution is available. You need to install the new version before you can use TWINT again.</source>
        <translation>Eine neue Version für TWINT OS X-Lösung ist verfügbar. Bevor Sie TWINT wieder nutzen können, muss die neue Version installiert werden.</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="351"/>
        <source>Close app</source>
        <translation>App schliessen</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="378"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="244"/>
        <location filename="../src/applicationcontroller.cpp" line="381"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="313"/>
        <source>New Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="323"/>
        <source>Ask later</source>
        <translation>Später nachfragen</translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="385"/>
        <source>Application exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/applicationcontroller.cpp" line="386"/>
        <source>Are you sure to log out?</source>
        <translation>Sind Sie sicher, dass Sie die App verlassen möchten?</translation>
    </message>
    <message>
        <source>Go to merchant app</source>
        <translation type="vanished">Zur Händler-App</translation>
    </message>
    <message>
        <source>TWINT merchant</source>
        <translation type="vanished">TWINT merchant</translation>
    </message>
    <message>
        <source>The TWINT merchant app is already open.</source>
        <translation type="vanished">Die TWINT Händler-App ist bereits geöffnet.</translation>
    </message>
</context>
<context>
    <name>BillListSectionDelegate</name>
    <message>
        <location filename="../qml/components/BillListSectionDelegate.qml" line="75"/>
        <source>Discount</source>
        <translation>Rabatt</translation>
    </message>
    <message>
        <location filename="../qml/components/BillListSectionDelegate.qml" line="92"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
</context>
<context>
    <name>BillListSummaryItem</name>
    <message>
        <location filename="../qml/components/BillListSummaryItem.qml" line="16"/>
        <source>Daily total</source>
        <translation>Tagestotal</translation>
    </message>
</context>
<context>
    <name>ButtonWithBorder</name>
    <message>
        <location filename="../qml/components/ButtonWithBorder.qml" line="9"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
</context>
<context>
    <name>CashRegisterPopUp</name>
    <message>
        <location filename="../qml/screens/CashRegisterPopUp.qml" line="99"/>
        <source>Insert Username</source>
        <translation>Benutzer angeben</translation>
    </message>
    <message>
        <location filename="../qml/screens/CashRegisterPopUp.qml" line="221"/>
        <source>User Login</source>
        <translation>Benutzer anmelden</translation>
    </message>
    <message>
        <location filename="../qml/screens/CashRegisterPopUp.qml" line="225"/>
        <source>Login</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <location filename="../qml/screens/CashRegisterPopUp.qml" line="240"/>
        <source>User Logout</source>
        <translation>Benutzer abmelden</translation>
    </message>
    <message>
        <location filename="../qml/screens/CashRegisterPopUp.qml" line="244"/>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
</context>
<context>
    <name>CashierScreen</name>
    <message>
        <location filename="../qml/screens/CashierScreen.qml" line="13"/>
        <source>&lt;Cashier Screen Placeholder&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangePageBar</name>
    <message>
        <location filename="../qml/components/ChangePageBar.qml" line="37"/>
        <source>Cash Register</source>
        <translation>Kasse</translation>
    </message>
    <message>
        <location filename="../qml/components/ChangePageBar.qml" line="48"/>
        <source>History</source>
        <translation>Verlauf</translation>
    </message>
    <message>
        <source>Transactions</source>
        <translation type="vanished">Verlauf</translation>
    </message>
</context>
<context>
    <name>ChangePageButton</name>
    <message>
        <location filename="../qml/components/ChangePageButton.qml" line="10"/>
        <source>Placeholder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CouponListHeader</name>
    <message>
        <location filename="../qml/components/CouponListHeader.qml" line="21"/>
        <source>COUPONS</source>
        <translation>Coupon</translation>
    </message>
    <message>
        <location filename="../qml/components/CouponListHeader.qml" line="41"/>
        <source>Enter amount</source>
        <translation>Betrag eingeben</translation>
    </message>
    <message>
        <source>Entrez I&apos;escompte</source>
        <translation type="vanished">Geben Sie den Rabatt</translation>
    </message>
</context>
<context>
    <name>CurrentOrderItem</name>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="77"/>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="313"/>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="319"/>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="322"/>
        <source>The payment is terminated</source>
        <translation>Die Zahlung wurde abgebrochen</translation>
    </message>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="166"/>
        <source>Internal error!</source>
        <translation>Technischer Fehler</translation>
    </message>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="175"/>
        <source>The transaction was successfully reverted</source>
        <translation>Die Zahlung wurde erfolgreich storniert</translation>
    </message>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="177"/>
        <source>Validation error</source>
        <translation>Prüfung fehlgeschlagen</translation>
    </message>
    <message>
        <source>The payment was collected successfully </source>
        <translation type="vanished">Die Zahlung wurde erfolgreich einkassiert </translation>
    </message>
    <message>
        <source>The payment of CHF </source>
        <translation type="vanished">Bezahlung von CHF</translation>
    </message>
    <message>
        <source> is collected successfully</source>
        <translation type="vanished">erfolgreich</translation>
    </message>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="301"/>
        <source>The payment is collected successfully</source>
        <translation>Die Zahlung wurde erfolgreich einkassiert</translation>
    </message>
</context>
<context>
    <name>CustomComboBox</name>
    <message>
        <location filename="../qml/components/CustomComboBox.qml" line="44"/>
        <source>Beacon Type</source>
        <translation>Beacon-Typ</translation>
    </message>
</context>
<context>
    <name>DarkOverlayScreen</name>
    <message>
        <location filename="../qml/screens/DarkOverlayScreen.qml" line="16"/>
        <source>This version is no longer supported. You need to install the new version before you can use TWINT again.</source>
        <translation>Diese Version wird nicht mehr unterstützt. Bevor Sie TWINT wieder nutzen können, muss die neue Version installiert werden.</translation>
    </message>
</context>
<context>
    <name>GearSettingsScreen</name>
    <message>
        <location filename="../qml/screens/GearSettingsScreen.qml" line="69"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Default size</source>
        <translation type="vanished">Standardgrösse</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Maximieren</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Minimieren</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation type="vanished">Vollbild</translation>
    </message>
    <message>
        <location filename="../qml/screens/GearSettingsScreen.qml" line="84"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>InputComponent</name>
    <message>
        <location filename="../qml/components/InputComponent.qml" line="18"/>
        <source>Place holder Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Passwort</translation>
    </message>
    <message>
        <location filename="../qml/components/InputComponent.qml" line="43"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InputWithLabel</name>
    <message>
        <location filename="../qml/components/InputWithLabel.qml" line="14"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">Browse</translation>
    </message>
</context>
<context>
    <name>KeyboardScreen</name>
    <message>
        <source>Payment only with code</source>
        <translation type="vanished">Bezahlung nur per Code möglich</translation>
    </message>
    <message>
        <location filename="../qml/screens/KeyboardScreen.qml" line="61"/>
        <source>Beacon not connected</source>
        <translation>Beacon nicht verbunden</translation>
    </message>
    <message>
        <source>The login data is incorrect</source>
        <translation type="vanished">Die Login-Daten sind nicht korrekt.</translation>
    </message>
    <message>
        <location filename="../qml/screens/KeyboardScreen.qml" line="34"/>
        <source>Please make sure your system is connected to the internet</source>
        <translation>Bitte stellen Sie sicher, dass Sie eine Verbindung zum Internet haben</translation>
    </message>
    <message>
        <location filename="../qml/screens/KeyboardScreen.qml" line="40"/>
        <location filename="../qml/screens/KeyboardScreen.qml" line="45"/>
        <source>First configure the app. To do so, click on the settings symbol.</source>
        <translation>Konfigurieren Sie zunächst die App. Klicken Sie hierzu auf das Einstellungssymbol.</translation>
    </message>
    <message>
        <location filename="../qml/screens/KeyboardScreen.qml" line="57"/>
        <source>Beacon connecting...</source>
        <translation>Beacon verbindet...</translation>
    </message>
    <message>
        <location filename="../qml/screens/KeyboardScreen.qml" line="171"/>
        <source>Collect</source>
        <translation>Kassieren</translation>
    </message>
    <message>
        <source>Einkassieren</source>
        <translation type="vanished">Einkassieren</translation>
    </message>
</context>
<context>
    <name>LoginComboBox</name>
    <message>
        <source>User Login</source>
        <translation type="obsolete">Benutzer anmelden</translation>
    </message>
    <message>
        <location filename="../qml/components/LoginComboBox.qml" line="38"/>
        <source>Insert Username</source>
        <translation>Benutzer angeben</translation>
    </message>
</context>
<context>
    <name>LoginScreen</name>
    <message>
        <location filename="../qml/screens/LoginScreen.qml" line="79"/>
        <source>TWINT Handlerportal</source>
        <translation>TWINT Handlerportal</translation>
    </message>
</context>
<context>
    <name>NewBillListDelegate</name>
    <message>
        <location filename="../qml/components/NewBillListDelegate.qml" line="127"/>
        <source>Daily total</source>
        <translation>Tagestotal</translation>
    </message>
</context>
<context>
    <name>NewBillSectionDelegate</name>
    <message>
        <location filename="../qml/components/NewBillSectionDelegate.qml" line="51"/>
        <source>Discount</source>
        <translation>Rabatt</translation>
    </message>
    <message>
        <location filename="../qml/components/NewBillSectionDelegate.qml" line="68"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
</context>
<context>
    <name>NumpadButton</name>
    <message>
        <location filename="../qml/components/NumpadButton.qml" line="9"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/NumpadButton.qml" line="10"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NumpadScreen</name>
    <message>
        <source>Einkassieren</source>
        <translation type="vanished">Einkassieren</translation>
    </message>
    <message>
        <location filename="../qml/screens/NumpadScreen.qml" line="67"/>
        <source>Collect</source>
        <translation>Kassieren</translation>
    </message>
    <message>
        <location filename="../qml/screens/NumpadScreen.qml" line="85"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/screens/NumpadScreen.qml" line="99"/>
        <source>The payment is terminated</source>
        <translation>Die Zahlung wurde abgebrochen</translation>
    </message>
</context>
<context>
    <name>Payment</name>
    <message>
        <source>ZWISCHENTOTAL</source>
        <translation type="vanished">ZWISCHENTOTAL</translation>
    </message>
    <message>
        <location filename="../qml/components/Payment.qml" line="93"/>
        <source>Zwischentotal</source>
        <translation>Zwischentotal</translation>
    </message>
    <message>
        <location filename="../qml/components/Payment.qml" line="111"/>
        <source>Discount</source>
        <translation>Rabatt</translation>
    </message>
    <message>
        <location filename="../qml/components/Payment.qml" line="129"/>
        <location filename="../qml/components/Payment.qml" line="187"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/Payment.qml" line="168"/>
        <source>TOTAL</source>
        <translation>TOTAL</translation>
    </message>
</context>
<context>
    <name>PopUpScreen</name>
    <message>
        <location filename="../qml/screens/PopUpScreen.qml" line="14"/>
        <source>text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/PopUpScreen.qml" line="58"/>
        <source>Schliessen</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/Communication/Messages/messageinterface.cpp" line="407"/>
        <location filename="../src/Communication/Messages/messageinterface.cpp" line="416"/>
        <location filename="../src/Communication/Messages/messageinterface.cpp" line="454"/>
        <location filename="../src/Communication/Messages/messageinterface.cpp" line="460"/>
        <source>Internal error!</source>
        <translation>Technischer Fehler</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation type="vanished">Vollbild</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Beenden</translation>
    </message>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="246"/>
        <source>Payment timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DataManagers/Models/currentorderitem.cpp" line="282"/>
        <source>The payment is terminated</source>
        <translation>Die Zahlung wurde abgebrochen</translation>
    </message>
</context>
<context>
    <name>QrCodeScreen</name>
    <message>
        <source>Payment only with code</source>
        <translation type="obsolete">Bezahlung nur per Code möglich</translation>
    </message>
    <message>
        <location filename="../qml/screens/QrCodeScreen.qml" line="66"/>
        <source>Payment only possible using QR code</source>
        <translation>Bezahlung nur per QR-Code möglich</translation>
    </message>
    <message>
        <location filename="../qml/screens/QrCodeScreen.qml" line="135"/>
        <source>Abbrechen</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/screens/QrCodeScreen.qml" line="147"/>
        <source>The payment is terminated</source>
        <translation>Die Zahlung wurde abgebrochen</translation>
    </message>
</context>
<context>
    <name>RectangleButton</name>
    <message>
        <location filename="../qml/components/RectangleButton.qml" line="9"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
</context>
<context>
    <name>ReverseConfirmationPopUp</name>
    <message>
        <location filename="../qml/screens/ReverseConfirmationPopUp.qml" line="30"/>
        <source>Reversal</source>
        <translation>Stornierung</translation>
    </message>
    <message>
        <location filename="../qml/screens/ReverseConfirmationPopUp.qml" line="50"/>
        <source>Are you sure?</source>
        <translation>Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../qml/screens/ReverseConfirmationPopUp.qml" line="68"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/screens/ReverseConfirmationPopUp.qml" line="81"/>
        <source>Ok</source>
        <translation>Ja</translation>
    </message>
</context>
<context>
    <name>SettingsScreen</name>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="158"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="245"/>
        <source>en</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="160"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="247"/>
        <source>de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="162"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="249"/>
        <source>fr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="164"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="251"/>
        <source>it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="59"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
    <message>
        <source>Merchant Alias</source>
        <translation type="vanished">Handler Alias</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="120"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="453"/>
        <source>TWINT Beacon</source>
        <translation>TWINT Beacon</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="121"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="457"/>
        <source>No TWINT Beacon</source>
        <translation>Kein TWINT Beacon</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="183"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="255"/>
        <source>Numeric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="185"/>
        <location filename="../qml/screens/SettingsScreen.qml" line="257"/>
        <source>Alphanumeric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="322"/>
        <source>TWINT MERCHANT</source>
        <translation>TWINT Händler</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="348"/>
        <source>Setup</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="365"/>
        <source>Cash Register Id</source>
        <translation>Kassen-ID</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="370"/>
        <source>Merchant UUID</source>
        <translation>Merchant-UUID</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="376"/>
        <source>Certificate</source>
        <translation>Zertifikat</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="392"/>
        <source>Browse Certificate</source>
        <translation>Zertifikat suchen</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="488"/>
        <source>Connect TWINT Beacon</source>
        <translation>TWINT Beacon verbinden</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="522"/>
        <source>Let TWINT solution run 
 actively in the background</source>
        <translation>TWINT Lösung aktiv im 
Hintergrund laufen lassen</translation>
    </message>
    <message>
        <source>Let TWINT solution run  
actively in the background</source>
        <translation type="vanished">TWINT Lösung aktiv im 
Hintergrund laufen lassen</translation>
    </message>
    <message>
        <source>Certificate Password</source>
        <translation type="vanished">Zertifikat Passwort</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="410"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>USB Port</source>
        <translation type="vanished">USB-Anschluss</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="464"/>
        <source>USB port</source>
        <translation>USB-Anschluss</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="476"/>
        <source>URL Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Run on Startup</source>
        <translation type="vanished">Bei Systemstart ausführen?</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="470"/>
        <source>Beacon URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connect USB</source>
        <translation type="vanished">USB-Anschluss verbinden</translation>
    </message>
    <message>
        <source>Let Merchant app run actively in the background</source>
        <translation type="vanished">Merchant-App aktiv im Hintergrund laufen lassen</translation>
    </message>
    <message>
        <source>Let Merchant app run 
actively in the background</source>
        <translation type="vanished">Merchant-App aktiv im 
Hintergrund laufen lassen</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="590"/>
        <source>Select User</source>
        <translation>Benutzer wählen</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="22"/>
        <source>Numerical code</source>
        <translation>Zahlen-Code</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="23"/>
        <source>Alphanumerical code</source>
        <translation>Zahlen- und Buchstaben-Code</translation>
    </message>
    <message>
        <source>No Beacon</source>
        <translation type="vanished">Kein Beacon</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="556"/>
        <source>Show keyboard</source>
        <translation>Tastatur einblenden</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="630"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="654"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="660"/>
        <source>Deutsch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="666"/>
        <source>Français</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="672"/>
        <source>Italiano</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="683"/>
        <source>Connect with Beacon</source>
        <translation>Einstellungen speichern</translation>
    </message>
    <message>
        <location filename="../qml/screens/SettingsScreen.qml" line="705"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Language:</source>
        <translation type="vanished">Sprache:</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Verbinden</translation>
    </message>
</context>
<context>
    <name>SoapManager</name>
    <message>
        <source>The password is not correct.</source>
        <translation type="vanished">Das Passwort ist nicht korrekt.</translation>
    </message>
</context>
<context>
    <name>ToogleButton</name>
    <message>
        <location filename="../qml/components/ToogleButton.qml" line="79"/>
        <source>Keacon type: USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ToogleButton.qml" line="87"/>
        <source>Keacon type: WIFI</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransactionsDetailsScreen</name>
    <message>
        <source>ZWISCHENTOTAL</source>
        <translation type="vanished">ZWISCHENTOTAL</translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="109"/>
        <source>Zwischentotal</source>
        <translation>Zwischentotal</translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="159"/>
        <source>Discount</source>
        <translation>Rabatt</translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="213"/>
        <source>TOTAL</source>
        <translation>TOTAL</translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="296"/>
        <source>TRX-ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="231"/>
        <source>Reversal</source>
        <translation>Storno</translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="337"/>
        <source>Stornierung</source>
        <translation>Stornierung</translation>
    </message>
    <message>
        <location filename="../qml/screens/TransactionsDetailsScreen.qml" line="355"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>TransparentTextButton</name>
    <message>
        <location filename="../qml/components/TransparentTextButton.qml" line="9"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TwoLevelInput</name>
    <message>
        <location filename="../qml/components/TwoLevelInput.qml" line="14"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/TwoLevelInput.qml" line="74"/>
        <source>The password is not correct.</source>
        <translation>Das Passwort ist nicht korrekt.</translation>
    </message>
    <message>
        <location filename="../qml/components/TwoLevelInput.qml" line="91"/>
        <source>USB connection to Beacon not found </source>
        <translation>USB-Verbindung zum Beacon nicht gefunden </translation>
    </message>
    <message>
        <location filename="../qml/components/TwoLevelInput.qml" line="108"/>
        <source>The access code or password is not correct.</source>
        <translation>Der Zugangscode oder das Passwort ist nicht korrekt.</translation>
    </message>
    <message>
        <location filename="../qml/components/TwoLevelInput.qml" line="258"/>
        <source>Browse</source>
        <translation>Blättern</translation>
    </message>
</context>
<context>
    <name>UsbManager</name>
    <message>
        <source>USB Keacon not found on Port </source>
        <translation type="vanished">USB Keacon nicht gefunden an Port </translation>
    </message>
    <message>
        <location filename="../src/Communication/usbmanager.cpp" line="142"/>
        <source>USB connection to Beacon not found </source>
        <translation>USB-Verbindung zum Beacon nicht gefunden </translation>
    </message>
    <message>
        <location filename="../src/Communication/usbmanager.cpp" line="236"/>
        <source>Empty device port.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Communication/usbmanager.cpp" line="251"/>
        <source>Device is busy. usually means that it&apos;s used by other application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Communication/usbmanager.cpp" line="327"/>
        <source>Error: port info is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Communication/usbmanager.cpp" line="343"/>
        <source>Error: unable to open device </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>TWINT Merchant</source>
        <translation type="vanished">TWINT Händler</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="122"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="124"/>
        <source>Preferences</source>
        <translation>Voreinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="12"/>
        <source>TWINT Windows solution</source>
        <translation>TWINT Windows-Lösung</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="12"/>
        <source>TWINT OS X solution</source>
        <translation>TWINT OS X-Lösung</translation>
    </message>
</context>
</TS>
