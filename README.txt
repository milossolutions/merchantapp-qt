TOC:
1. OsX changing system OpenSSL version


1. By default OsX comes with older version of OpenSSL, with is something around
   0.9.8zh.

   To upgrade to newer version do fallowing:

    a. install homebrew - http://brew.sh/
    b. brew update
    c. brew install openssl
    // determine location of openssl it's usually in /usr/bin
    // when running 10.11++ see apending a
    d. sudo mv /usr/bin/openssl ~/
    e. sudo ln -s /usr/local/Cellar/openssl/[OpenSSL current version]/bin/openssl /usr/bin
    f. sudo rm ~/openssl - delete old version if all is correct

    Appendinx a:
    OsX El Captain (10.11)++
        From 10.11 new protection mechanism is introduced, "rootless" mode
        (System Integrity Protection) with will prevent change to /usr/bin.
        In such case:

            aa. reboot into recovery mode, restart and hold COMMAND+R
            ab. open terminal
            ac. issue command > csrutil disable
            ad. issue command > reboot
         Fallow from point d to finish change
         To verify current status of SIP issue, in recovery terminal, csrutil status
            ae. after change re-enable SIP > csrutil enable and reboot
