#!/usr/bin/env python
from twint import ServiceAgent

agent = ServiceAgent(
    cert=('./certs/kibix-merchant-int.crt', './certs/kibix-merchant-int.key'),
    environ='int', agent_uuid=None,
    merchant_uuid='E5A7D9C1-52F4-420D-B611-328E8F8BA899',
    cash_register_id='twint-api-test',
    debug=False
)

agent.enroll_cash_register()
agent.cancel_checkin()

order = agent.start_order(0.1)
print order

status = agent.monitor_order(order.OrderUuid, blocking=True)
print status.Order.Status

loops = 5
while loops > 1 and status.Order.Status.Status.value == 'IN_PROGRESS':
    loops -= 1
    status = agent.monitor_order(order.OrderUuid, blocking=True)
    print status.Order.Status

agent.cancel_order(order.OrderUuid)
