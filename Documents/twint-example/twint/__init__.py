from suds.transport.http import HttpAuthenticated
from suds.transport import Reply, TransportError
from suds.client import Client
from os import path
import requests
import uuid
from suds.plugin import MessagePlugin
import xml.dom.minidom


TWINT_URLS = {
    'int': 'https://service-int.twint.ch/merchant/service/TWINTMerchantServiceV2',
    'pat': 'https://service-pat.twint.ch/merchant/service/TWINTMerchantServiceV2',
    'live': 'https://service.twint.ch/merchant/service/TWINTMerchantServiceV2',
}
WSDL_FILE = path.join(path.dirname(path.realpath(__file__)), 'schema',
                      'TWINTMerchantService_v2_0.wsdl')


class LogPlugin(MessagePlugin):
    def sending(self, context):
        tree = xml.dom.minidom.parseString(str(context.envelope))
        print('TX' + tree.toprettyxml() + '\n')

    def received(self, context):
        tree = xml.dom.minidom.parseString(str(context.reply))
        print('RX' + tree.toprettyxml() + '\n')


class RequestsTransport(HttpAuthenticated):
    def __init__(self, **kwargs):
        self.cert = kwargs.pop('cert', None)
        # super won't work because not using new style class
        HttpAuthenticated.__init__(self, **kwargs)

    def send(self, request):
        self.addcredentials(request)
        resp = requests.post(request.url, data=request.message,
                             headers=request.headers, cert=self.cert)
        result = Reply(resp.status_code, resp.headers, resp.content)
        return result


class ServiceAgent(object):
    def __init__(self, cert, agent_uuid, merchant_uuid, cash_register_id,
                 environ='int', debug=False):
        plugins = []
        if debug:
            plugins.append(LogPlugin())

        self._client = Client(
            url='file://' + WSDL_FILE,
            location=TWINT_URLS[environ],
            transport=RequestsTransport(cert=cert),
            plugins=plugins)

        header = self._client.factory.create('ns2:RequestHeaderType')
        header.MessageId = str(uuid.uuid4())
        header.ClientSoftwareName = 'fastlane'
        header.ClientSoftwareVersion = '0.1.0'
        self._client.set_options(soapheaders=header)

        self._merchant = self._client.factory.create('ns0:MerchantInformationType')
        self._merchant.CashRegisterId = cash_register_id
        self._merchant.ServiceAgentUuid = agent_uuid
        self._merchant.MerchantUuid = merchant_uuid

    def _create(self, soap_type):
        return self._client.factory.create(soap_type)

    def check_system_status(self):
        return self._client.service.CheckSystemStatus(self._merchant)

    def cancel_checkin(self):
        abort_reason = self._create('ns3:CancelCheckinReason')
        return self._client.service.CancelCheckIn(self._merchant,
                abort_reason.PAYMENT_ABORT)

    def enroll_cash_register(self):
        cash_register_type = self._create('ns3:CashRegisterType')
        return self._client.service.EnrollCashRegister(
            self._merchant, cash_register_type.EPOS)

    def start_credit_order(self, amount, qrcode=False):
        posting_type = self._create('ns3:PostingType')
        currency_amount = self._create('ns0:CurrencyAmountType')
        currency_amount.Amount = amount
        currency_amount.Currency = 'CHF'

        payment_type = self._create('ns3:OrderKindType')

        order = self._create('ns3:OrderRequestType')
        order.PostingType = posting_type.GOODS
        order.RequestedAmount = currency_amount
        order._type = payment_type.CREDIT

        # StartOrder(ns0:MerchantInformationType MerchantInformation,
        #             ns3:OrderRequestType Order,
        #            ns3:CouponListType Coupons,
        #            ns0:Token250Type OfflineAuthorization,
        #            ns0:UuidType CustomerRelationUuid,
        #            ns0:UuidType PairingUuid,
        #            xs:boolean UnidentifiedCustomer,
        #            xs:boolean QRCodeRendering)
        return self._client.service.StartOrder(
            self._merchant,
            order,
            None,  # ns3:CouponListType Coupons
            None,  # ns0:Token250Type OfflineAuthorization
            None,  # ns0:UuidType CustomerRelationUuid
            None,  # ns0:UuidType PairingUuid
            True,  # xs:boolean UnidentifiedCustomer
            qrcode # xs:boolean QRCodeRendering
        )

    def start_order(self, amount, qrcode=False):
        posting_type = self._create('ns3:PostingType')
        currency_amount = self._create('ns0:CurrencyAmountType')
        currency_amount.Amount = amount
        currency_amount.Currency = 'CHF'

        payment_type = self._create('ns3:OrderKindType')

        order = self._create('ns3:OrderRequestType')
        order.PostingType = posting_type.GOODS
        order.RequestedAmount = currency_amount
        order._type = payment_type.PAYMENT_IMMEDIATE

        return self._client.service.StartOrder(
            self._merchant, # ns0:MerchantInformationType MerchantInformation
            order,          # ns3:OrderRequestType Order
            None,           # ns3:CouponListType Coupons
            None,           # ns0:Token250Type OfflineAuthorization
            None,           # ns0:UuidType CustomerRelationUuid
            None,           # ns0:UuidType PairingUuid
            True,           # xs:boolean UnidentifiedCustomer
            qrcode          # xs:boolean QRCodeRendering
        )


    def monitor_order(self, order_uuid, blocking=False):
        return self._client.service.MonitorOrder(
            self._merchant, # ns0:MerchantInformationType MerchantInformation
            order_uuid,     # ns0:UuidType OrderUuid
            None,           # ns0:MerchantTransactionReferenceType MerchantTransactionReference
            blocking        # xs:boolean WaitForResponse
        )

    def cancel_order(self, order_uuid):
        return self._client.service.CancelOrder(
            self._merchant, # ns0:MerchantInformationType MerchantInformation
            order_uuid,     # ns0:UuidType OrderUuid
            None            # ns0:MerchantTransactionReferenceType MerchantTransactionReference
        )
