#!/bin/bash
# @(#) user4	Compare version numbers of form a.b.c.
# Adapted from post # 4.
# http://www.unix.com/shell-programming-and-scripting/186383-how-compare-version-values-shell-script.html

pe() { for _i;do printf "%s" "$_i";done; printf "\n"; }
pl() { pe;pe "-----" ;pe "$*"; }
db() { ( printf " db, ";for _i;do printf "%s" "$_i";done;printf "\n" ) >&2 ; }
db() { : ; }
C=$HOME/bin/context && [ -f $C ] && $C

# compare version numbers
# usage: vercmp <versionnr1> <versionnr2>
#         with format for versions xxx.xxx.xxx
# returns: 0 if versionnr1 equal or greater
#          1 if versionnr1 lower

vercmp()
{
local a1 b1 c1 a2 b2 c2
# echo|read succeeds in ksh, but fails in bash.
# bash alternative is "set --"
db "input 1 \"$1\", 2 \"$2\" "
v1=$1
v2=$2
db "v1 $v1, v2 $v2"
set -- $( echo "$v1" | sed 's/\./ /g' )
a1=$1 b1=$2 c1=$3
set -- $( echo "$v2" | sed 's/\./ /g' )
a2=$1 b2=$2 c2=$3
db "a1,b1,c1 $a1,$b1,$c1 ; a2,b2,c2 $a2,$b2,$c2"
ret=$(( (a1-a2)*1000000+(b1-b2)*1000+c1-c2 ))
db "ret is $ret"
if [ $ret -lt 0 ] ; then
v=-1
elif [ $ret -eq 0 ] ; then
v=0
else
v=1
fi
printf "%d" $v
return
}

currentVersion=$(sw_vers -productVersion)

# install beacon drivers based on mac os version
v=$( vercmp $currentVersion 10.9.0 )
if [ $v -lt 0 ]
then
# " $a is earlier than $b"
installer -store -pkg /Users/PL_151.pkg -target /
else
# " $a is later or equal than $b"
installer -store -pkg /Users/PL_161.pkg -target /
fi

rm /Users/PL_151.pkg
rm /Users/PL_161.pkg

exit 0