#include "messagelogger.h"

#include <QDir>
#include <QFile>
#include <QStandardPaths>
#include <QApplication>
#include <QDateTime>
#include <QWriteLocker>
#include <QReadLocker>
#include <QTextStream>
#include <QDateTime>

MessageLogger* MessageLogger::m_instance = Q_NULLPTR;

/*!
 * \class MessageLogger
 * \brief Class used to create file with request and responses received in communication with TWINT server api
 *
 * Message will be stored if ApplicationStatus::loggerStatus is set to true
 */
MessageLogger::MessageLogger(QObject *parent) :
    QObject(parent)
{
    init();
}

/*!
 * \brief Creates new file on application start
 */
void MessageLogger::init()
{
    m_path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/TwintMerchant/Logs";
    QDir appDataDir;
    appDataDir.mkpath(m_path);

    m_path.append("/LogFile " + QDateTime::currentDateTime().toString("dd-MM-yyyy hh_mm") + ".txt");
}

MessageLogger *MessageLogger::instance()
{
    if (!m_instance)
        m_instance = new MessageLogger(QCoreApplication::instance());

    return m_instance;
}

MessageLogger::~MessageLogger()
{

}

/*!
 * \brief Writes message in file
 * \param messageType GlobalSettings::Enums::LogMessageType
 * \param dataArray Message data
 */
void MessageLogger::writeMessageToFile(GlobalSettings::Enums::LogMessageType messageType, QByteArray dataArray)
{
    QFile file(m_path);

    if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
        return;

    if (!file.isSequential())
    {
        file.seek(file.size());
    }

    QTextStream stream(&file);

    if(messageType == GlobalSettings::Enums::LogRequestMessage){
        stream << "Request message: Timestamp: " << QDateTime::currentDateTime().toTime_t() << endl;
    }else if(messageType == GlobalSettings::Enums::LogResponseMessage){
        stream << "Response message: Timestamp: " << QDateTime::currentDateTime().toTime_t() << endl;
    }else if(messageType == GlobalSettings::Enums::LogErrorMessage){
        stream << "Result: Timestamp: " << QDateTime::currentDateTime().toTime_t() << endl;
    }
    stream << dataArray << endl << endl;

    file.close();
}

