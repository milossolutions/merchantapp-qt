/*
  Twintmerchant main.cpp by Milo Solutions. Copyright 2016
*/

#include "qtsingleapplication.h"

#include "globalsettings.h"
#include "settings.h"
#include "applicationstatus.h"
#include "uicontroller.h"
#include "applicationcontroller.h"
#include "datamanager.h"
#include "communicationmanager.h"

#include "neworderlistmodel.h"
#include "currentorderitem.h"
#include "qrcodeimageprovider.h"
#include "couponlistmodel.h"

#include "merchantinformationtype.h"
#include "customerinformationtype.h"
#include "currencyamounttype.h"
#include "orderlinktype.h"
#include "orderstatustype.h"
#include "couponrejectionreason.h"
#include "couponlisttype.h"
#include "findorderbydate.h"
#include "findorderbyreference.h"
#include "orderrequesttype.h"
#include "ordertype.h"
#include "beaconsecuritytype.h"
#include "checkinnotificationtype.h"
#include "loyaltytype.h"
#include "coupontype.h"
#include "rejectedcoupontype.h"
#include "paymentamounttype.h"
#include "crashhandler.h"

#include <QCoreApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QApplication>
#include <QScreen>
#include <QtQml>
#include <QMessageBox>

/*!
  Main routine. Remember to update the application name and initialise logger
  class, if present.
  */
int main(int argc, char *argv[]) {

#ifdef USE_HIGHDPI
    // enable Qt 5.6 High DPI support
    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling);
#endif
    QtSingleApplication application(argc, argv);

    CrashHandler::instance();
    ApplicationStatus::instance();
    Settings::instance();

    if (application.sendMessage("The TWINT merchant app is already open.")){
        return 0;
    }

    application.setApplicationVersion(APP_VERSION);
    application.setOrganizationName(QStringLiteral("TWINT AG"));
    application.setOrganizationDomain(QStringLiteral("www.twint.ch"));

    QCommandLineParser parser;

    QCommandLineOption checkServer("server");
    parser.addOption(checkServer);

    parser.process(application);

    QString option;
    if (parser.isSet(checkServer)) {
        if(parser.positionalArguments().count()>0){
            option = parser.positionalArguments().first();
            if(option==QLatin1Literal("int")){
                GlobalSettings::Paths::instance()->setTwintServerUrl(GlobalSettings::Paths::instance()->TwintIntUrl());
                GlobalSettings::Paths::instance()->setTwintServerId(int(GlobalSettings::Enums::TwintIntServer));
            }else if(option==QLatin1Literal("pat")){
                GlobalSettings::Paths::instance()->setTwintServerUrl(GlobalSettings::Paths::instance()->TwintPatUrl());
                GlobalSettings::Paths::instance()->setTwintServerId(int(GlobalSettings::Enums::TwintPatServer));
            }else if(option==QLatin1Literal("live")){
                GlobalSettings::Paths::instance()->setTwintServerUrl(GlobalSettings::Paths::instance()->TwintLiveUrl());
                GlobalSettings::Paths::instance()->setTwintServerId(int(GlobalSettings::Enums::TwintLiveServer));
            }else if(option==QLatin1Literal("ref")){
                GlobalSettings::Paths::instance()->setTwintServerUrl(GlobalSettings::Paths::instance()->TwintRefUrl());
                GlobalSettings::Paths::instance()->setTwintServerId(int(GlobalSettings::Enums::TwintRefServer));
            }else if(option==QLatin1Literal("major")){
                GlobalSettings::Paths::instance()->setTwintServerUrl(GlobalSettings::Paths::instance()->TwintMajorUrl());
                GlobalSettings::Paths::instance()->setTwintServerId(int(GlobalSettings::Enums::TwintMajorServer));
            }
        }
    }

    Settings::instance()->setApplicationName("TWINT Merchant");
    Settings::instance()->setApplicationVersion(application.applicationVersion());

    application.setApplicationName(Settings::instance()->applicationName());

    ApplicationController *applicationController = new ApplicationController();
    QObject::connect (&application, &QApplication::aboutToQuit,
                      applicationController, &ApplicationController::deleteLater);

    qRegisterMetaType<MerchantInformationType*>("MerchantInformationType*");
    qRegisterMetaType<CustomerInformationType*>("CustomerInformationType*");
    qRegisterMetaType<CurrencyAmountType*>("CurrencyAmountType*");
    qRegisterMetaType<OrderLinkType*>("OrderLinkType*");
    qRegisterMetaType<OrderStatusType*>("OrderStatusType*");
    qRegisterMetaType<CouponRejectionReason*>("CouponRejectionReason*");
    qRegisterMetaType<CouponListType*>("CouponListType*");
    qRegisterMetaType<FindOrderByDate*>("FindOrderByDate*");
    qRegisterMetaType<FindOrderByReference*>("FindOrderByReference*");
    qRegisterMetaType<LoyaltyType*>("LoyaltyType*");
    qRegisterMetaType<OrderRequestType*>("OrderRequestType*");
    qRegisterMetaType<OrderType*>("OrderType*");
    qRegisterMetaType<BeaconSecurityType*>("BeaconSecurityType*");
    qRegisterMetaType<CheckInNotificationType*>("CheckInNotificationType*");

    qRegisterMetaType<QList<CouponType*>>("QList<CouponType*>");
    qRegisterMetaType<QList<RejectedCouponType*>>("QList<RejectedCouponType*>");
    qRegisterMetaType<QList<OrderType*>>("QList<OrderType*>");
    qRegisterMetaType<QList<LoyaltyType*>>("QList<LoyaltyType*>");
    qRegisterMetaType<QList<PaymentAmountType*>>("QList<PaymentAmountType*>");

    qRegisterMetaType<QList<CouponData>>("QList<CouponData>");

    //place for register enums
    qRegisterMetaType<GlobalSettings::Enums::SoapMessageType>("GlobalSettings::Enums::SoapMessageType");
    qRegisterMetaType<GlobalSettings::Enums::QmlScreens>("GlobalSettings::Enums::QmlScreens");
    qRegisterMetaType<GlobalSettings::Enums::OrderStatus>("GlobalSettings::Enums::OrderStatus");
    qRegisterMetaType<GlobalSettings::Enums::PopUpType>("GlobalSettings::Enums::PopUpType");
    qRegisterMetaType<GlobalSettings::Enums::OrderType>("GlobalSettings::Enums::OrderType");
    qRegisterMetaType<GlobalSettings::Enums::Language>("GlobalSettings::Enums::Language");
    qRegisterMetaType<GlobalSettings::Enums::ErrorCheck>("GlobalSettings::Enums::ErrorCheck");
    qRegisterMetaType<GlobalSettings::Enums::LogMessageType>("GlobalSettings::Enums::LogMessageType");
    qRegisterMetaType<GlobalSettings::Enums::TwintServerAddress>("GlobalSettings::Enums::TwintServerAddress");
    qRegisterMetaType<GlobalSettings::Enums::CertificateCheckResult>("GlobalSettings::Enums::CertificateCheckResult");
    qRegisterMetaType<GlobalSettings::Enums::VersionCheckResult>("GlobalSettings::Enums::VersionCheckResult");
    qRegisterMetaType<GlobalSettings::Enums::VersionCheckResult>("GlobalSettings::Enums::BeaconConnectionStatus");

    qmlRegisterType<NewOrderListModel>("QMLEnums", 1, 0, "ModelRole");
    qmlRegisterType<CouponListModel>("QMLEnums", 1, 0, "CouponModelRole");

    //place for models registration
    qRegisterMetaType<NewOrderListModel*>("NewOrderListModel*");
    qRegisterMetaType<CouponListModel*>("CouponListModel*");
    qRegisterMetaType<CurrentOrderItem*>("CurrentOrderItem*");

    qmlRegisterType<GlobalSettings::Enums>("milosolutions.com", 1, 0, "Enums");

    qreal refDpi = 96;
    qreal refWidth = 320.;
    qreal refHeight = 548.;

    QRect screenGeometry = application.primaryScreen()->geometry();

    qreal screenWidth = screenGeometry.width();

    qreal screenHeight = screenGeometry.height();

    LOGD()<<application.platformName();

    qreal screenDpi = application.primaryScreen()->logicalDotsPerInch();

    qreal fontScale = qMin(screenHeight * refDpi / (screenDpi * refHeight), screenWidth * refDpi / (screenDpi * refWidth));
    qreal dpiScale = screenDpi / refDpi;

    LOGD()<<dpiScale<<application.primaryScreen()->logicalDotsPerInch();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("QApp", qApp);
    engine.rootContext()->setContextProperty("UiController", applicationController->uiController());
    engine.rootContext()->setContextProperty("Settings", Settings::instance());
    engine.rootContext()->setContextProperty("ApplicationStatus", ApplicationStatus::instance());
    engine.rootContext()->setContextProperty("GlobalSettings", GlobalSettings::Paths::instance());

    engine.rootContext()->setContextProperty("dpiScale", dpiScale);
    engine.rootContext()->setContextProperty("screenHeight", screenHeight);
    engine.rootContext()->setContextProperty("screenWidth", screenWidth);
    engine.rootContext()->setContextProperty("dataManager", applicationController->dataManager());
    engine.rootContext()->setContextProperty("newOrderListModel", applicationController->dataManager()->newOrderListModel());
    engine.rootContext()->setContextProperty("currentOrderItem", applicationController->dataManager()->currentOrderItem());
    engine.rootContext()->setContextProperty("couponListModel", applicationController->dataManager()->couponListModel());
    engine.rootContext()->setContextProperty("communicationManager", applicationController->communicationManager());

    engine.addImageProvider("qrCodeImage", applicationController->dataManager()->currentOrderItem()->imageProvider());

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return application.exec();
}
