#ifndef ABSTRACTDEVICEMANAGER_H
#define ABSTRACTDEVICEMANAGER_H

#include <QObject>

#include "globalsettings.h"

class AbstractDeviceManager : public QObject
{
    Q_OBJECT
public:
    explicit AbstractDeviceManager(QObject *parent = 0);

signals:
    void clientConnected( const QString& clientId) const;
    void clientUnConnected() const;

    void showMessage(GlobalSettings::Enums::PopUpType popUpType, QString message);
    void offlineCodeReceived(QByteArray data);

public slots:
    virtual void closeDevice() = 0;

    /*!
     * \brief find beacon
     * Perform search for keacon device.
     *
     * \param initString
     */
    virtual void findBeacon( const QString& initString) = 0;

protected:
    void parseKeaconData(QByteArray& data);
    void error(const QString& msg,
               const bool showErrorMessage = true);
};

#endif // ABSTRACTDEVICEMANAGER_H
