#include "abstractdevicemanager.h"

#include "abstractdevicedata.h"
#include "globalsettings.h"
#include "settings.h"
#include "applicationstatus.h"

AbstractDeviceManager::AbstractDeviceManager(QObject *parent) :
    QObject(parent)
{

}

/*!
 * \brief parse received data from keacon
 * Specification is available in document
 * "Keacon - Serial Protocol Specification.pdf" at page 4.
 *
 * General message format is:
 *  messageType, message body, carriage return
 *
 * Parser is common for WiFi and USB.
 */
void AbstractDeviceManager::parseKeaconData(QByteArray& data)
{
    if (data.isEmpty()) {
        LOGD() << "Warning, unable to parse empty message";
        return;
    }
    LOGD()<<"Cala data"<<data;

    const QByteArray msgType = data.left( 1);
    data.remove( 0, 2);

    if (msgType == "p") {
        // check message data
        const QByteArray mesgData = data.left( 4);
        data = data.right( data.size() - 5);

        if (mesgData != controllData) {
            LOGD() << "Error, keacon communication issues, controll data differs"
                   << "received:" << mesgData
                   << "send:" << controllData;


        }

        // check keacon status
        // 1 byte in hex,
        // 00:standby state, 01:running, 02:connected

        const QByteArray status = data.left( 2);
        data = data.right( data.size() - 3);

        if (status == "00") {

        } else if (status == "01") {

        } else if (status == "02") {

        } else {
            LOGD() << "Warning, received unknown status"
                   << status;
        }

        // extended keacon information, for logging only up to 128 chars
        data = data.simplified();
    }
    else if (msgType == "d") {
        LOGD() << "-- data message " << data;
        if(data.count()>100){
            data.chop( 1);
            emit offlineCodeReceived(data);
        }
    }
    else if (msgType == "g") {
        // general data messages should be ignored

        LOGD() << "Received general data from keacon device"
               << data;
    }
    else if (msgType == "a") {
        // general data messages should be ignored

        LOGD() << "Received data ack from keacon device"
               << data;
    }
    else if (msgType == "x") {

        const QByteArray connStatus = data.left( 1);
        data.remove( 0, 5);
        data.chop( 1);

        // connection status 1 byte in Hex (00:not connected, 01:connected)
        LOGD()<<"Connection status"<<connStatus;
        if (connStatus == "0") {

            emit clientUnConnected();

        } else if (connStatus == "1") {

            emit clientConnected( data);

        } else {
            LOGD() << "Error, unable to determine connection status from:"
                   << connStatus;
        }

    }
    else if (msgType == "l") {
        LOGD() << "Keacon log message:" << data;
    }
    else if (msgType == "v") {
        LOGD() << "Keacon version message:" << data;
        QString versionNumber = data.replace("\r","");
        ApplicationStatus::instance()->setFirmwareVersion(versionNumber.split(",").last());
    }

    data.clear();
}

/*!
 * \brief error handler
 * Logg and display, if showErrorMessage is set, error message to user
 */

void AbstractDeviceManager::error(const QString &msg,
                                  const bool showErrorMessage)
{
    LOGD() << msg;

    if (showErrorMessage) {
        //emit showMessage(GlobalSettings::Enums::ErrorPopUp, msg);
    }

}
