#ifndef FINDORDERMESSAGE_H
#define FINDORDERMESSAGE_H

#include "messageinterface.h"

/**
 * \class FindOrderMessage
 * \brief CancelCheckInMessage is responsible for creating structure of FindOrder soap message that is send to the server.
 * After receiving response it parses data and fill FindOrderResponseItem object.
*/
class FindOrderMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit FindOrderMessage(QObject *parent = 0);
    ~FindOrderMessage();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // FINDORDERMESSAGE_H
