#ifndef MONITORCHECKINMESSAGE_H
#define MONITORCHECKINMESSAGE_H

#include "messageinterface.h"

/**
 * \class MonitorCheckInMessage
 * \brief CancelCheckInMessage is responsible for creating structure of CancelCheckIn soap message that is send to the server.
 * After receiving response it parses data and fill MonitorCheckInResponseItem object.
*/

class MonitorCheckInMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit MonitorCheckInMessage(QObject *parent = 0);
    ~MonitorCheckInMessage();

    ContainerItemInterface *getItemContainer();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // MONITORCHECKINMESSAGE_H
