#ifndef REQUESTCHECKINMESSAGE_H
#define REQUESTCHECKINMESSAGE_H

#include "messageinterface.h"

/**
 * \class RequestCheckInMessage
 * \brief CancelCheckInMessage is responsible for creating structure of RequestCheckIn soap message that is send to the server.
 * After receiving response it parses data and fill RequestCheckInResponseItem object.
*/

class RequestCheckInMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit RequestCheckInMessage(QObject *parent = 0);
    ~RequestCheckInMessage();

    ContainerItemInterface *getItemContainer();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // REQUESTCHECKINMESSAGE_H
