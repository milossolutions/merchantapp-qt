#ifndef MONITORORDERSTATUSMESSAGE_H
#define MONITORORDERSTATUSMESSAGE_H

#include "messageinterface.h"

/**
 * \class MonitorOrderMessage
 * \brief CancelCheckInMessage is responsible for creating structure of MonitorOrder soap message that is send to the server.
 * After receiving response it parses data and fill MonitorOrderResponseItem object.
*/

class MonitorOrderMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit MonitorOrderMessage(QObject *parent = 0);
    ~MonitorOrderMessage();

    ContainerItemInterface *getItemContainer();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // MONITORORDERSTATUSMESSAGE_H
