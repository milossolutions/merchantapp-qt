#include "confirmordermessage.h"

#include "confirmorderresponseitem.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

ConfirmOrderMessage::ConfirmOrderMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::ConfirmOrder);
}

ConfirmOrderMessage::~ConfirmOrderMessage()
{

}

ResponseItemInterface *ConfirmOrderMessage::returnItem()
{
    return new ConfirmOrderResponseItem(this);
}
