#include "monitorcheckinmessage.h"

#include "monitorcheckinresponseitem.h"
#include "monitorcheckincontainer.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

MonitorCheckInMessage::MonitorCheckInMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::MonitorCheckIn);
}

MonitorCheckInMessage::~MonitorCheckInMessage()
{

}

ContainerItemInterface *MonitorCheckInMessage::getItemContainer()
{
    return new MonitorCheckInContainer();
}

ResponseItemInterface *MonitorCheckInMessage::returnItem()
{
    return new MonitorCheckInResponseItem(this);
}
