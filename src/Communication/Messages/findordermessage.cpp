#include "findordermessage.h"

#include "findorderresponseitem.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

FindOrderMessage::FindOrderMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::FindOrder);
}

FindOrderMessage::~FindOrderMessage()
{

}

ResponseItemInterface *FindOrderMessage::returnItem()
{
    return new FindOrderResponseItem(this);
}
