#ifndef CANCELORDERMESSAGE_H
#define CANCELORDERMESSAGE_H

#include "messageinterface.h"

/**
 * \class CancelOrderMessage
 * \brief CancelCheckInMessage is responsible for creating structure of CancelOrder soap message that is send to the server.
 * After receiving response it parses data and fill CancelOrderResponseItem object.
*/
class CancelOrderMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit CancelOrderMessage(QObject *parent = 0);
    ~CancelOrderMessage();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // CANCELORDERMESSAGE_H
