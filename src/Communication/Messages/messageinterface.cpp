#include "messageinterface.h"

#include "messagelogger.h"
#include "settings.h"
#include "applicationstatus.h"

#include "findorderresponseitem.h"

#include "merchantinformationtype.h"
#include "ordertype.h"
#include "paymentamounttype.h"
#include "loyaltytype.h"
#include "coupontype.h"
#include "customerinformationtype.h"
#include "orderstatustype.h"
#include "couponlisttype.h"
#include "rejectedcoupontype.h"

#include "requestiteminterface.h"
#include "typeinterface.h"
#include "containeriteminterface.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QMetaProperty>
#include <QVariant>
#include <QApplication>
#include <QStack>
#include <QSharedPointer>
#include <QUuid>

#include <QSslKey>
#include <QSslError>
#include <QString>
#include <QDateTime>
#include <QNetworkConfigurationManager>
#include <QTimer>

#include "requestcheckinresponseitem.h"
#include "checkinnotificationtype.h"
#include "keconinformation.h"
#include "checksystemstatusresponseitem.h"

#include "requestcheckincontainer.h"
#include "monitorcheckinresponseitem.h"

MessageInterface::MessageInterface(QObject *parent) :
    QObject(parent),
    m_networkAccessManager(Q_NULLPTR),
    m_timeout(new QTimer(this))
{
    m_timeout->setInterval(5000);
    connect(m_timeout, &QTimer::timeout, this, &MessageInterface::messageTimeoutHandler, Qt::QueuedConnection);
}

MessageInterface::~MessageInterface()
{
    if(m_requestItem){
        m_requestItem->deleteLater();
    }
}

GlobalSettings::Enums::SoapMessageType MessageInterface::getType() const
{
    return m_type;
}

void MessageInterface::setType(const GlobalSettings::Enums::SoapMessageType &type)
{
    m_type = type;
}

ContainerItemInterface *MessageInterface::getItemContainer()
{
    return Q_NULLPTR;
}

void MessageInterface::createRequest(QNetworkAccessManager *qNetworkAccessManager, QSslConfiguration sslConfiguration, RequestItemInterface *requestItem)
{
    m_requestItem = requestItem;
    m_networkAccessManager = qNetworkAccessManager;
    QNetworkConfigurationManager mgr;

    if(mgr.isOnline()){
        m_networkAccessManager->setNetworkAccessible(QNetworkAccessManager::Accessible);
    }

    connect(m_networkAccessManager, &QNetworkAccessManager::finished, this, &MessageInterface::getReply);
    connect ( m_networkAccessManager,
              SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),
              this,
              SLOT(onAuthenticationRequestSlot(QNetworkReply*,QAuthenticator*)) );

    QByteArray byteArray;

    //creating namespace
    byteArray.append(createMessageNamespaces());
    //creating header
    byteArray.append(createMessageHeader());
    //creating body depending on provided data
    QString xmlString;
    QXmlStreamWriter xmlWriter(&xmlString);
    xmlWriter.writeStartElement("ns2:Body");
    createMessageBody(xmlWriter, requestItem);
    xmlWriter.writeEndElement();
    byteArray.append(xmlString);


    byteArray.append("</SOAP-ENV:Envelope>");

    QNetworkRequest *request = new QNetworkRequest(QUrl(GlobalSettings::Paths::instance()->TwintServerUrl()));
    if(ApplicationStatus::instance()->loggerStatus()){
        MessageLogger::instance()->writeMessageToFile(GlobalSettings::Enums::LogRequestMessage, {"Request server " +GlobalSettings::Paths::instance()->TwintServerUrl().toLocal8Bit()});
    }
    request->setOriginatingObject(this);
    request->setSslConfiguration(sslConfiguration);
    request->setHeader( QNetworkRequest::ContentTypeHeader, QVariant( QString("text/xml;charset=utf-8")));
    request->setHeader(QNetworkRequest::ContentLengthHeader, QVariant( qulonglong(byteArray.size()) ));
    request->setAttribute(QNetworkRequest::CacheLoadControlAttribute, QVariant( int(QNetworkRequest::AlwaysNetwork) ));

    //Required for checking ssl error
    connect( m_networkAccessManager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)),
             this, SLOT( sslErrorOccured(QNetworkReply*,QList<QSslError>)));

    if(ApplicationStatus::instance()->loggerStatus()){
        MessageLogger::instance()->writeMessageToFile(GlobalSettings::Enums::LogRequestMessage, byteArray);
    }

    LOGD()<<"REQUEST"<<byteArray;

    //Start timeout signal for catching lost messages
    m_timeout->start();

    m_networkAccessManager->post(*request,byteArray);
}

QString MessageInterface::createMessageNamespaces()
{
    QString msgNamespace = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                           "<SOAP-ENV:Envelope xmlns:tns=\"http://service.twint.ch/header/types/v2\""
                           " xmlns:ns0=\"http://service.twint.ch/merchant/types/v2\""
                           " xmlns:ns1=\"http://service.twint.ch/base/types/v2\""
                           " xmlns:ns2=\"http://schemas.xmlsoap.org/soap/envelope/\""
                           " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                           " xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    return msgNamespace;
}

QString MessageInterface::createMessageHeader()
{
    QString msgHeader = "<SOAP-ENV:Header>"
                        "<tns:RequestHeaderElement>"
                        "<tns:MessageId>" + createUuid() + "</tns:MessageId>"
                                                           "<tns:ClientSoftwareName>" + QApplication::instance()->applicationName() + "</tns:ClientSoftwareName>"
                                                                                                                                      "<tns:ClientSoftwareVersion>" + QApplication::instance()->applicationVersion() + "</tns:ClientSoftwareVersion>"
                                                                                                                                                                                                                       "</tns:RequestHeaderElement>"
                                                                                                                                                                                                                       "</SOAP-ENV:Header>";
    return msgHeader;
}

QString MessageInterface::createUuid()
{
    QUuid uuid = QUuid::createUuid();
    QString uuidStr = uuid.toString().remove('{').remove('}');
    return uuidStr;
}

void MessageInterface::createMessageBody(QXmlStreamWriter &xmlWriter, QObject *object)
{
    QObjectList out;
    QObjectList in;
    ItemInterface *item = qobject_cast<ItemInterface*>(object);
    if (!item)
        return;

    in.append(item);
    CouponListType *couponListType = qobject_cast<CouponListType*>(item);
    if (couponListType) {
        QList<CouponType*> couponType = couponListType->processedCouponListType();
        QList<RejectedCouponType*> rejectedCouponType = couponListType->rejectedCouponListType();
        for (int i = 0; i < couponType.size(); ++i) {
            in.append(couponType.at(i));
        }
        for (int i = 0; i < rejectedCouponType.size(); ++i) {
            in.append(rejectedCouponType.at(i));
        }
    }
    item->getAllChildrensOfObject(in, out);

    bool found = item->isAnyPropertySet(out);
    if (!found)
        return;

    QString itemNamespace = "ns0:";
    xmlWriter.writeStartElement(itemNamespace + object->objectName());
    for (int i = object->metaObject()->propertyOffset(); i < object->metaObject()->propertyCount() ; ++i) {
        QMetaProperty property = object->metaObject()->property(i);
        QString propertyName = property.name();
        if (propertyName.contains("Attribute")) {
            QString tmp = propertyName;
            tmp.replace("Attribute", "");
            xmlWriter.writeAttribute(tmp, property.read(object).toString());
        }
    }

    for (int i = object->metaObject()->propertyOffset(); i < object->metaObject()->propertyCount(); ++i) {
        QObject *typeObject = qvariant_cast<QObject*>(object->metaObject()->property(i).read(object));
        if (typeObject && typeObject->objectName() == "Coupons") {
            CouponListType *couponListType = qobject_cast<CouponListType*>(typeObject);
            if (couponListType) {
                QList<CouponType*> couponType = couponListType->processedCouponListType();
                QList<RejectedCouponType*> rejectedCouponType = couponListType->rejectedCouponListType();
                if (couponType.size() > 0 || rejectedCouponType.size() > 0)
                    xmlWriter.writeStartElement("ns0:Coupons");
                for (int i = 0; i < couponType.size(); ++i) {
                    createMessageBody(xmlWriter, couponType.at(i));
                }
                for (int i = 0; i < rejectedCouponType.size(); ++i) {
                    createMessageBody(xmlWriter, rejectedCouponType.at(i));
                }
                if (couponType.size() > 0 || rejectedCouponType.size() > 0)
                    xmlWriter.writeEndElement();
            }
        } else if (typeObject) {
            createMessageBody(xmlWriter, typeObject);
        } else {
            QMetaProperty property = object->metaObject()->property(i);
            QString propertyName = property.name();
            if (property.read(object).toString() == "") {
                continue;
            }
            if (propertyName.contains("Attribute")) {
                continue;
            }
            if (propertyName.isEmpty())
                continue;
            propertyName.replace(0,1,propertyName.at(0).toUpper());
            QString elementNamespace;
            if (qobject_cast<CurrencyAmountType*>(object) || qobject_cast<MerchantInformationType*>(object)) {
                elementNamespace = "ns1:";
            } else {
                elementNamespace = "ns0:";
            }
            xmlWriter.writeStartElement(elementNamespace + propertyName);
            if (property.read(object).toString() != " ") {
                xmlWriter.writeCharacters(property.read(object).toString());
            }
            xmlWriter.writeEndElement();
        }
    }
    xmlWriter.writeEndElement();
}

/**
 * @brief MessageInterface::readData
 * @param msgData
 * Class responsible for parsing received data and creating item with informations based on response
 * @return
 */

ResponseItemInterface *MessageInterface::readData(QByteArray msgData)
{
    QXmlStreamReader xml(msgData);
    xml.setNamespaceProcessing(true);

    ResponseItemInterface *item = returnItem();

    if(!item){
        return Q_NULLPTR;
    }

    if(ApplicationStatus::instance()->loggerStatus()){
        MessageLogger::instance()->writeMessageToFile(GlobalSettings::Enums::LogResponseMessage, msgData);
    }

    item->parse(msgData);

    QObjectList childrens;
    if (!item->children().isEmpty())
        item->getAllChildrensOfObject(item->children(), childrens);
    childrens.append(item);

    //temporary for tests
    //    foreach (QObject *object, childrens) {
    //        for (int i = object->metaObject()->propertyOffset(); i < object->metaObject()->propertyCount() ; ++i) {
    //            QMetaProperty property = object->metaObject()->property(i);
    //            LOGD() << property.name() << property.read(object).toString() << object->objectName();
    //        }
    //    }
    //    foreach (QObject *object, childrens) {
    //        for (int i = object->metaObject()->superClass()->propertyOffset(); i < object->metaObject()->superClass()->propertyCount() ; ++i) {
    //            QMetaProperty property = object->metaObject()->superClass()->property(i);
    //            LOGD() << property.name() << property.read(object).toString() << object->objectName();
    //        }
    //    }

    return item;
}

void MessageInterface::getReply(QNetworkReply *reply)
{
    if(reply->request().originatingObject() != this){
        return;
    }

    m_timeout->stop();

    if(reply->error() == QNetworkReply::NoError){
        ApplicationStatus::instance()->setHostConnection(true);
        QByteArray dataResult = reply->readAll();
        LOGD()<<"REPLY"<<dataResult;
        if(dataResult.contains("<ns2:Addendum key=\"customer.hasTWINTCoupons\">false</ns2:Addendum>")){
            dataResult.replace("<ns2:Addendum key=\"customer.hasTWINTCoupons\">false</ns2:Addendum>","");
        }

        ResponseItemInterface *cashRegister = readData(dataResult);

        if(ApplicationStatus::instance()->loggerStatus()){
            MessageLogger::instance()->writeMessageToFile(GlobalSettings::Enums::LogErrorMessage, {"Success - No error"});
        }

        if (m_type == GlobalSettings::Enums::StartOrder) {
            //            ApplicationStatus::instance()->setPaymentOngoingStatus(false);
            ApplicationStatus::instance()->setRequestCheckInStatus(false);
            ContainerItemInterface *containerInterface = this->getItemContainer();
            containerInterface->fillData(m_requestItem, cashRegister);
            emit sendingOrderContainer(containerInterface);
        } else if (m_type == GlobalSettings::Enums::MonitorOrder) {
            ContainerItemInterface *containerInterface = this->getItemContainer();
            containerInterface->fillData(m_requestItem, cashRegister);
            emit sendingMonitorContainer(containerInterface);
        } else if (m_type == GlobalSettings::Enums::EnrollCashRegister) {
            ContainerItemInterface *containerInterface = this->getItemContainer();
            containerInterface->fillData(m_requestItem, cashRegister);
            KeconInformation *tmpContainer = qobject_cast<KeconInformation*>(containerInterface);
            LOGD()<<"entered here enrol cash register";
            if(!tmpContainer->isDataValid()){
                LOGD()<<"Invalid certificate";
                ApplicationStatus::instance()->setCredentialsStatus(false);
                ApplicationStatus::instance()->setOfflineCode("");
            }else{
                ApplicationStatus::instance()->setCredentialsStatus(true);
                emit sendingKeaconInfoContainer(containerInterface);
            }
        } else if (m_type == GlobalSettings::Enums::RequestCheckIn){
            LOGD()<<"Receiving client Informations";
            RequestCheckInResponseItem *responseItem = qobject_cast<RequestCheckInResponseItem*>(cashRegister);
            LOGD()<<responseItem->checkInNotificationType()->pairingUuid()<<responseItem->checkInNotificationType()->pairingStatus();

            ContainerItemInterface *containerInterface = this->getItemContainer();
            containerInterface->fillData(m_requestItem, cashRegister);
            emit sendingRequestCheckInContainer(containerInterface);
        } else if (m_type == GlobalSettings::Enums::CancelCheckIn) {
            ApplicationStatus::instance()->setRequestCheckInStatus(false);
            ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        } else if (m_type == GlobalSettings::Enums::CheckSystemStatus) {
            LOGD()<<"entered here system check ";
            CheckSystemStatusResponseItem *checkSystemResponse = qobject_cast<CheckSystemStatusResponseItem*>(cashRegister);
            if(checkSystemResponse->status() == QString::null){
                LOGD()<<"Invalid status";
                ApplicationStatus::instance()->setCredentialsStatus(false);
                emit checkSystemStatusResult(GlobalSettings::Enums::SystemStatusCheck, false);
            }else{
                ApplicationStatus::instance()->setCredentialsStatus(true);
                emit checkSystemStatusResult(GlobalSettings::Enums::SystemStatusCheck, true);
            }
        } else if(m_type == GlobalSettings::Enums::MonitorCheckIn){
            ContainerItemInterface *containerInterface = this->getItemContainer();
            containerInterface->fillData(m_requestItem, cashRegister);
            emit sendingMonitorCheckInContainer(containerInterface);
        }

    }else{
        LOGD()<<"Error occured"<<reply->error()<<reply->errorString();

        if(ApplicationStatus::instance()->loggerStatus()){
            MessageLogger::instance()->writeMessageToFile(GlobalSettings::Enums::LogErrorMessage, {"Failed - " + reply->errorString().toLocal8Bit()});
        }

        if(reply->error() == QNetworkReply::HostNotFoundError
                || reply->error() == QNetworkReply::TimeoutError
                || reply->error() == QNetworkReply::RemoteHostClosedError
                || reply->error() == QNetworkReply::TemporaryNetworkFailureError
                || reply->error() == QNetworkReply::NetworkSessionFailedError){
            ApplicationStatus::instance()->setHostConnection(false);
        }

        if (m_type == GlobalSettings::Enums::CheckSystemStatus) {
            emit checkSystemStatusResult(GlobalSettings::Enums::SystemStatusCheck, false);
        }

        if(reply->error() == QNetworkReply::UnknownNetworkError){
            if(reply->errorString().contains("ssl handshake failure")){
                ApplicationStatus::instance()->setCredentialsStatus(false);
                ApplicationStatus::instance()->setOfflineCode("");
            }
        }

        if(reply->error() == QNetworkReply::SslHandshakeFailedError){
            //ToDo
            emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QString(reply->errorString()));
        }

        if (m_type == GlobalSettings::Enums::StartOrder) {
            ApplicationStatus::instance()->setPaymentOngoingStatus(false);
            ApplicationStatus::instance()->setOfflineCode("");
            emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("Internal error!"));
        }

        if(m_type == GlobalSettings::Enums::RequestCheckIn
                || m_type == GlobalSettings::Enums::MonitorCheckIn
                || m_type == GlobalSettings::Enums::MonitorOrder){
            ApplicationStatus::instance()->setRequestCheckInStatus(false);
            ApplicationStatus::instance()->setPaymentOngoingStatus(false);
            ApplicationStatus::instance()->setOfflineCode("");
            emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("Internal error!"));
        }

        if(m_type == GlobalSettings::Enums::MonitorOrder){
            emit abortOrder();
        }

    }
    emit requestFinished();

    reply->deleteLater();
}

void MessageInterface::onAuthenticationRequestSlot(QNetworkReply *aReply, QAuthenticator *aAuthenticator)
{
    LOGD()<<"Authentication required";
}

void MessageInterface::sslErrorOccured(QNetworkReply *reply, QList<QSslError> errorList)
{
    Q_UNUSED(reply)
    Q_UNUSED(errorList)

    //    if(Settings::instance()->loggerStatus()){
    //        foreach (QSslError error, errorList) {
    //            MessageLogger::instance()->writeMessageToFile(GlobalSettings::Enums::LogErrorMessage, {"SSL Error - " + error.errorString().toLocal8Bit()});
    //        }
    //    }
}

void MessageInterface::messageTimeoutHandler()
{
    if (m_type == GlobalSettings::Enums::CheckSystemStatus) {
        emit checkSystemStatusResult(GlobalSettings::Enums::SystemStatusCheck, false);
    }

    if (m_type == GlobalSettings::Enums::StartOrder) {
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("Internal error!"));
    }

    if(m_type == GlobalSettings::Enums::RequestCheckIn
            || m_type == GlobalSettings::Enums::MonitorCheckIn
            || m_type == GlobalSettings::Enums::MonitorOrder){
        emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("Internal error!"));
        ApplicationStatus::instance()->setRequestCheckInStatus(false);
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
    }

    if(m_type == GlobalSettings::Enums::MonitorOrder){
        emit abortOrder();
    }

    emit requestFinished();
}
