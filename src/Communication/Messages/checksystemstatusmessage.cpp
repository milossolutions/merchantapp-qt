#include "checksystemstatusmessage.h"

#include "checksystemstatusresponseitem.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

CheckSystemStatusMessage::CheckSystemStatusMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::CheckSystemStatus);
}

CheckSystemStatusMessage::~CheckSystemStatusMessage()
{

}

ResponseItemInterface *CheckSystemStatusMessage::returnItem()
{
    return new CheckSystemStatusResponseItem(this);
}
