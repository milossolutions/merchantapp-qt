#ifndef CONFIRMORDERMESSAGE_H
#define CONFIRMORDERMESSAGE_H

#include "messageinterface.h"

/**
 * \class ConfirmOrderMessage
 * \brief CancelCheckInMessage is responsible for creating structure of ConfirmOrder soap message that is send to the server.
 * After receiving response it parses data and fill ConfirmOrderResponseItem object.
*/

class ConfirmOrderMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit ConfirmOrderMessage(QObject *parent = 0);
    ~ConfirmOrderMessage();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // CONFIRMORDERMESSAGE_H
