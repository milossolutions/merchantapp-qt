#include "monitorordermessage.h"

#include "monitororderresponseitem.h"

#include "monitorordercontainer.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

MonitorOrderMessage::MonitorOrderMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::MonitorOrder);
}

MonitorOrderMessage::~MonitorOrderMessage()
{

}

ContainerItemInterface *MonitorOrderMessage::getItemContainer()
{
    return new MonitorOrderContainer();
}

ResponseItemInterface *MonitorOrderMessage::returnItem()
{
    return new MonitorOrderResponseItem(this);
}
