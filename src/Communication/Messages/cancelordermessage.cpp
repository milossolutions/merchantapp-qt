#include "cancelordermessage.h"

#include "cancelorderresponseitem.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

CancelOrderMessage::CancelOrderMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::CancelOrder);
}

CancelOrderMessage::~CancelOrderMessage()
{

}

ResponseItemInterface *CancelOrderMessage::returnItem()
{
    return new CancelOrderResponseItem(this);
}
