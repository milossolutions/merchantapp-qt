#ifndef CHECKSYSTEMSTATUSMESSAGE_H
#define CHECKSYSTEMSTATUSMESSAGE_H

#include "messageinterface.h"

/**
 * \class
 * \brief CancelCheckInMessage is responsible for creating structure of CheckSystemStatus soap message that is send to the server.
 * After receiving response it parses data and fill CheckSystemStatusResponseItem object.
*/

class CheckSystemStatusMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit CheckSystemStatusMessage(QObject *parent = 0);
    ~CheckSystemStatusMessage();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // CHECKSYSTEMSTATUSMESSAGE_H
