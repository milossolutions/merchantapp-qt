#ifndef MESSAGEINTERFACE_H
#define MESSAGEINTERFACE_H

#include "globalsettings.h"

#include <QObject>
#include <QNetworkReply>

class QNetworkAccessManager;
class QSslConfiguration;
class ResponseItemInterface;
class RequestItemInterface;
class QXmlStreamWriter;
class QXmlStreamReader;
class ContainerItemInterface;
class MonitorOrderResponseItem;
class MonitorOrderContainer;
class QTimer;

/**
 * \class MessageInterface
 * \brief MessageInterface is responsible for creating structure of soap message that is send to the server.
 * After receiving response it parses it to corresponding ResponseItemInterface class and fills data.
 * Response data is then sent from here to proper places.
 */
class MessageInterface : public QObject
{
    Q_OBJECT
public:
    explicit MessageInterface(QObject *parent = 0);
    ~MessageInterface();

    GlobalSettings::Enums::SoapMessageType getType() const;
    void setType(const GlobalSettings::Enums::SoapMessageType &type);

    /**
     * \brief Pointer to QNetworkAccessManager with set certificates
     */
    QNetworkAccessManager *m_networkAccessManager;

    /**
     * \brief Object that is used in various functions.
     * It contains data from requests and response.
     * Isn't deleted upon removing message object
     * \return
     */
    virtual ContainerItemInterface *getItemContainer();

signals:
    void makeRequest(const GlobalSettings::Enums::SoapMessageType type) const;
    void requestFinished() const;
    void sendingOrderContainer(ContainerItemInterface *containerItem) const;
    void sendingMonitorContainer(ContainerItemInterface *containerItem) const;
    void sendingKeaconInfoContainer(ContainerItemInterface *containerItem) const;
    void sendingRequestCheckInContainer(ContainerItemInterface *containerItem) const;
    void sendingMonitorCheckInContainer(ContainerItemInterface *containerItem) const;

    void showMessage(GlobalSettings::Enums::PopUpType popUpType, QString message);

    void checkSystemStatusResult(GlobalSettings::Enums::ErrorCheck errorCheckEnum, bool value) const;

    void abortOrder() const;

public slots:
    /**
     * \brief Returns response object containing required members.
     * Each Message returns it's own response item.
     * \return
     */
    virtual ResponseItemInterface *returnItem() = 0;
    /**
     * \brief Creates soap message structure.
     * \param qNetworkAccessManager
     * \param sslConfiguration
     * \param requestItem - unique object structure to each message type
     */
    virtual void createRequest(QNetworkAccessManager *qNetworkAccessManager, QSslConfiguration sslConfiguration, RequestItemInterface *requestItem);
    /**
     * \brief Contains message namespaces
     * \return
     */
    QString createMessageNamespaces();
    /**
     * \brief Creates message header
     * \return
     */
    QString createMessageHeader();
    /**
     * \brief Generates random uuid of message
     * \return
     */
    QString createUuid();
    /**
     * \brief Creates soap message body by using members from corresponding RequestItemInterface classes
     * \param xmlWriter
     * \param object
     */
    virtual void createMessageBody(QXmlStreamWriter &xmlWriter, QObject *object);
    /**
     * \brief Parses received data from Twint server and fills ResponseItemInterface object
     * \param msgData
     * \return
     */
    ResponseItemInterface *readData(QByteArray msgData);
    /**
     * \brief Slot where we receive reply from QNetworkAccessManager
     * \param reply
     */
    void getReply(QNetworkReply *reply);

    void onAuthenticationRequestSlot(QNetworkReply *aReply, QAuthenticator *aAuthenticator);

    void sslErrorOccured(QNetworkReply *reply, QList<QSslError> errorList);

private slots:
    void messageTimeoutHandler();

protected:
    /**
     * \brief Soap message type
     */
    GlobalSettings::Enums::SoapMessageType m_type;
    RequestItemInterface *m_requestItem;
    QTimer *m_timeout;
};

#endif // MESSAGEINTERFACE_H
