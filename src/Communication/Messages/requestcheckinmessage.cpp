#include "requestcheckinmessage.h"

#include "requestcheckinresponseitem.h"

#include "requestcheckincontainer.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>

RequestCheckInMessage::RequestCheckInMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::RequestCheckIn);
}

RequestCheckInMessage::~RequestCheckInMessage()
{

}

ContainerItemInterface *RequestCheckInMessage::getItemContainer()
{
    return new RequestCheckInContainer();
}

ResponseItemInterface *RequestCheckInMessage::returnItem()
{
    return new RequestCheckInResponseItem(this);
}
