#ifndef STARTORDERMESSAGE_H
#define STARTORDERMESSAGE_H

#include "messageinterface.h"

/**
 * \class StartOrderMessage
 * \brief CancelCheckInMessage is responsible for creating structure of StartOrder soap message that is send to the server.
 * After receiving response it parses data and fill StartOrderResponseItem object.
*/
class StartOrderMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit StartOrderMessage(QObject *parent = 0);
    ~StartOrderMessage();

    ContainerItemInterface *getItemContainer();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // STARTORDERMESSAGE_H
