#include "startordermessage.h"

#include "startorderresponseitem.h"
#include "couponlisttype.h"
#include "coupontype.h"
#include "rejectedcoupontype.h"
#include "merchantinformationtype.h"
#include "currencyamounttype.h"
#include "startordercontainer.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>
#include <QXmlStreamWriter>
#include <QMetaProperty>

StartOrderMessage::StartOrderMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::StartOrder);
}

StartOrderMessage::~StartOrderMessage()
{

}

ContainerItemInterface *StartOrderMessage::getItemContainer()
{
    return new StartOrderContainer();
}

ResponseItemInterface *StartOrderMessage::returnItem()
{
    return new StartOrderResponseItem(this);
}
