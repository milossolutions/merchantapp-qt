#ifndef CANCELCHECKINMESSAGE_H
#define CANCELCHECKINMESSAGE_H

#include "messageinterface.h"

/**
 * \class CancelCheckInMessage
 * \brief CancelCheckInMessage is responsible for creating structure of CancelCheckIn soap message that is send to the server.
 * After receiving response it parses data and fill CancelCheckInResponseItem object.
*/

class CancelCheckInMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit CancelCheckInMessage(QObject *parent = 0);
    ~CancelCheckInMessage();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:
};

#endif // CANCELCHECKINMESSAGE_H
