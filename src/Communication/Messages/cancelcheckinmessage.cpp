#include "cancelcheckinmessage.h"

#include "cancelcheckinresponseitem.h"
#include "couponlisttype.h"
#include "coupontype.h"
#include "rejectedcoupontype.h"
#include "merchantinformationtype.h"
#include "currencyamounttype.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>
#include <QXmlStreamWriter>
#include <QMetaProperty>

CancelCheckInMessage::CancelCheckInMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::CancelCheckIn);
}

CancelCheckInMessage::~CancelCheckInMessage()
{

}

ResponseItemInterface *CancelCheckInMessage::returnItem()
{
    return new CancelCheckInResponseItem(this);
}
