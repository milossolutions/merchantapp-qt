#include "enrollcashregistermessage.h"

#include "enrollcashregisterresponseitem.h"

#include "keconinformation.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QSslKey>



EnrollCashRegisterMessage::EnrollCashRegisterMessage(QObject *parent) :
    MessageInterface(parent)
{
    setType(GlobalSettings::Enums::EnrollCashRegister);
}

EnrollCashRegisterMessage::~EnrollCashRegisterMessage()
{

}

ContainerItemInterface *EnrollCashRegisterMessage::getItemContainer()
{
    return new KeconInformation();
}

ResponseItemInterface *EnrollCashRegisterMessage::returnItem()
{
    return new EnrollCashRegisterResponseItem(this);
}
