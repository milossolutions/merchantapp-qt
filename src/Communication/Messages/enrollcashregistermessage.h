#ifndef ENROLLCASHREGISTERMESSAGE_H
#define ENROLLCASHREGISTERMESSAGE_H

#include "messageinterface.h"

/**
 * \class EnrollCashRegisterMessage
 * \brief CancelCheckInMessage is responsible for creating structure of EnrollCashRegister soap message that is send to the server.
 * After receiving response it parses data and fill EnrollCashRegisterResponseItem object.
*/

class EnrollCashRegisterMessage : public MessageInterface
{
    Q_OBJECT
public:
    explicit EnrollCashRegisterMessage(QObject *parent = 0);
    ~EnrollCashRegisterMessage();

    ContainerItemInterface *getItemContainer();

signals:

public slots:
    ResponseItemInterface *returnItem();

private:

};

#endif // ENROLLCASHREGISTERMESSAGE_H
