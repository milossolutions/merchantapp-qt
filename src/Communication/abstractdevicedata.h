#ifndef ABSTRACTDEVICEDATA_H
#define ABSTRACTDEVICEDATA_H

#include <QByteArray>

/*!
 * \brief controllData
 * Control data used to verify if communication is correct. Used in send /
 * received "POLL" and "REPLY-ON-POLL" messages.
 *
 * In current implementation not used. Using QIODevice means to detect whether
 * device is online or no, reducing traffic data.
 */

const QByteArray controllData   = "A1B1";   // 2 bytes, in hex
const int poolDelay = 3000; // in ms

#endif // ABSTRACTDEVICEDATA_H
