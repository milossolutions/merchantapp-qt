#include "versionchecker.h"

#include <QFileInfo>
#include <QFile>
#include <QSettings>
#include <QTimer>

#include "httpdownload.h"

/*!
 * \class VersionChecker
 * \brief Class controls time when request to download new version.ini file is sent.
 * Contains methods for parsing version file.
 */
VersionChecker::VersionChecker(QObject *parent) :
    QObject(parent),
    m_httpDownload(Q_NULLPTR)
{
    initWidgets();
    makeConnections();
    start();
}

VersionChecker::~VersionChecker()
{
    m_httpDownload->deleteLater();
}

/*!
 * \brief Starts proccess of downloading new file.
 * Creates temporary file that will be filled with data.
 */
void VersionChecker::start()
{
    LOGD()<<"Start download process";
    const QString tmp = QStandardPaths::locate(
                QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory);
    QFile *file = new QFile(tmp + "version.ini",this);
    LOGD()<<"Temporary file path"<<tmp + "version.ini";
    m_httpDownload->setFile(file);
    m_httpDownload->process();
}

void VersionChecker::initWidgets()
{
    m_httpDownload = new HTTPDownload(GlobalSettings::Paths::instance()->TwintVersionUrl(), this);
    m_dailyTimer = new QTimer(this);
    m_dailyTimer->setInterval(86400000);
    m_dailyTimer->start();
}

void VersionChecker::makeConnections()
{
    connect(m_httpDownload, &HTTPDownload::errorOccurred, this, &VersionChecker::errorHandler, Qt::QueuedConnection);
    connect(m_httpDownload, &HTTPDownload::finished, this, &VersionChecker::checkVersion, Qt::QueuedConnection);
    connect(m_dailyTimer, &QTimer::timeout, this, &VersionChecker::start, Qt::QueuedConnection);
}

void VersionChecker::errorHandler(QString errorDescription)
{
    LOGD()<<"ErrorOccured"<<errorDescription;
}

/*!
 * \brief Function takes values from download file and compares it with current version of TWINT Merchant application
 *
 * If MA version number will be lower or equal to MANDATORY_VERSION field value,
 * signal that will block application for user and display pop up will be emited
 * If MA version version will be lower than value inside VERSION field, signal that will show pop up will be emited
 */
void VersionChecker::checkVersion()
{
    LOGD()<<"Download completed, starting to parse file";
    QSettings versionFile(m_httpDownload->file()->fileName(), QSettings::IniFormat);

    QString versionNumber = versionFile.value("VERSION","").toString();
    QString mandatoryVersionNumber = versionFile.value("MANDATORY_VERSION","").toString();

    LOGD()<<"Version number"<<versionNumber<<"Mandatory Version Number"<<mandatoryVersionNumber;

    if (versionNumber == "" || mandatoryVersionNumber == ""){
        LOGD() << "Cannot read file or its content has worng format";
        emit versionCheckResult(GlobalSettings::Enums::UpdateError);
        return;
    }

    QStringList currentVersionNumberList = versionNumber.split(".");
    QStringList applicationVersionNumberList = qApp->applicationVersion().split(".");
    QStringList mandatoryVersionNumberList = mandatoryVersionNumber.split(".");

    if(currentVersionNumberList.size() != applicationVersionNumberList.size()
            || currentVersionNumberList.size() != mandatoryVersionNumberList.size()){
        LOGD()<<"Version structure doesn't match";
        emit versionCheckResult(GlobalSettings::Enums::UpdateError);
        return;
    }

    if((mandatoryVersionNumberList == applicationVersionNumberList)
            || compareVersion(mandatoryVersionNumberList, applicationVersionNumberList)){
        emit versionCheckResult(GlobalSettings::Enums::MandatoryUpdateAvalible);
        return;
    }

    if(compareVersion(currentVersionNumberList, applicationVersionNumberList)){
        emit versionCheckResult(GlobalSettings::Enums::StandardUpdateAvalible);
        return;
    }

    emit versionCheckResult(GlobalSettings::Enums::ApplicationUpToDate);
}

/*!
 * \brief Function compares two version strings and returns true if first string is higher than second
 * Function can compare different size of version string as long as they are split by the same amount of dots.
 * For example 1.2.1.12 and 1.2.1.122 can be compared properly.
 * \param newerVersion Version string that could be higher
 * \param olderVersion Canditate for older version
 * \return true if newerVersion is higher
 */
bool VersionChecker::compareVersion(const QStringList newerVersion, const QStringList olderVersion)
{
    bool isNewVersionHigher = false;

    for(int i = 0; i < newerVersion.size(); i++){
        if(newerVersion.at(i) > olderVersion.at(i)){
            isNewVersionHigher = true;
            break;
        }else if(olderVersion.at(i) > newerVersion.at(i)){
            break;
        }
    }

    return isNewVersionHigher;
}

