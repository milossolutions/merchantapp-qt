#include "soapmanager.h"

#include "globalsettings.h"
#include "settings.h"
#include "applicationstatus.h"

#include "messagelogger.h"

#include "requestcheckinmessage.h"
#include "monitorcheckinmessage.h"
#include "cancelcheckinmessage.h"
#include "startordermessage.h"
#include "monitorordermessage.h"
#include "confirmordermessage.h"
#include "cancelordermessage.h"
#include "findordermessage.h"
#include "enrollcashregistermessage.h"
#include "checksystemstatusmessage.h"

#include "requestiteminterface.h"

//#include <QNetworkAccessManager>
#include <QSslConfiguration>
#include <QFile>
#include <QSslKey>
#include <QTimer>

#ifdef __APPLE__
#include "customsslcertificatereader.h"
#endif

//temp
#include "containeriteminterface.h"
#include "monitororderresponseitem.h"
#include "enrollcashregisterrequestitem.h"
#include "checksystemstatusrequestitem.h"

#include "requestcheckincontainer.h"
#include "monitorcheckinrequestitem.h"
#include "monitorcheckincontainer.h"


/**
 * \class SoapManager
 * \brief Class responsible for communication with soap service
 * It creates messages and holds them until request is completed.
 */
SoapManager::SoapManager(QObject *parent) :
    QObject(parent),
    m_networkAccessManager(new QNetworkAccessManager(this)),
    m_enrollCashRegisterTimer(new QTimer(this))
{
    init();
    sendSystemCheckStatusRequest();
    prepareConnections();
}

SoapManager::~SoapManager()
{
    if(m_networkAccessManager){
        m_networkAccessManager->deleteLater();
    }
}

/**
 * \brief Function create messages with proper format and data, then sends them to twint server using QNetworkAcessManager
 * Message is then added to the list and removed upon receiving response.
 * \param type - message type
 * \param requestItem - data that will be used in preparing message
 */
void SoapManager::createRequest(const GlobalSettings::Enums::SoapMessageType type, RequestItemInterface *requestItem)
{
    if(!m_networkAccessManager){
        qWarning()<<"QNetworkAccessManager doesn't exist";
        return;
    }

    MessageInterface *message = Q_NULLPTR;

    switch(type){
        case GlobalSettings::Enums::RequestCheckIn:
        {
            ApplicationStatus::instance()->setRequestCheckInStatus(true);
            ApplicationStatus::instance()->setPairingOnGoingStatus(false);
            message = new RequestCheckInMessage;
            connect(message, &MessageInterface::sendingRequestCheckInContainer, this, &SoapManager::sendingRequestCheckInContainer, Qt::QueuedConnection);
            break;
        }
        case GlobalSettings::Enums::MonitorCheckIn:
        {
            message = new MonitorCheckInMessage;
            connect(message, &MessageInterface::sendingMonitorCheckInContainer, this, &SoapManager::sendingMonitorCheckInContainer, Qt::QueuedConnection);
            break;
        }
        case GlobalSettings::Enums::CancelCheckIn:
        {
            message = new CancelCheckInMessage;
            break;
        }
        case GlobalSettings::Enums::StartOrder:
        {
            ApplicationStatus::instance()->setPaymentOngoingStatus(true);
            message = new StartOrderMessage;
            connect(message, &MessageInterface::sendingOrderContainer, this, &SoapManager::sendingOrderContainer, Qt::QueuedConnection);
            break;
        }
        case GlobalSettings::Enums::MonitorOrder:
        {
            message = new MonitorOrderMessage;
            connect(message, &MessageInterface::sendingMonitorContainer, this, &SoapManager::sendingMonitorContainer, Qt::QueuedConnection);
            connect(message, &MessageInterface::abortOrder, this, &SoapManager::abortOrder, Qt::QueuedConnection);
            break;
        }
        case GlobalSettings::Enums::ConfirmOrder:
        {
            message = new ConfirmOrderMessage;
            break;
        }
        case GlobalSettings::Enums::CancelOrder:
        {
            message = new CancelOrderMessage;
            break;
        }
        case GlobalSettings::Enums::FindOrder:
        {
            message = new FindOrderMessage;
            break;
        }
        case GlobalSettings::Enums::EnrollCashRegister:
        {
            message = new EnrollCashRegisterMessage;
            connect(message, &EnrollCashRegisterMessage::sendingKeaconInfoContainer, this, &SoapManager::onKeaconInfoContainer, Qt::QueuedConnection);
            break;
        }
        case GlobalSettings::Enums::CheckSystemStatus:
        {
            message = new CheckSystemStatusMessage;
            connect(message, &MessageInterface::checkSystemStatusResult, this, &SoapManager::checkSystemStatusResult, Qt::QueuedConnection);
            break;
        }
        default:
        {
            LOGD()<<"Unused message type";
            return;
        }
    }

    if(!message){
        qWarning()<<"Couldn't create request";
        return;
    }
    connect(message, &MessageInterface::showMessage, this, &SoapManager::showMessage);
    connect(message, &MessageInterface::requestFinished, this, &SoapManager::removeFinishedRequest, Qt::QueuedConnection);

    message->createRequest(m_networkAccessManager, m_sslConfiguration, requestItem);

    m_messageList << message;
}

/**
 * \brief Removing sent message upon receiving response from the server
 */
void SoapManager::removeFinishedRequest()
{
    MessageInterface *request = dynamic_cast<MessageInterface*>(sender());

    if(!request){
        return;
    }

    if(m_messageList.contains(request)){
        m_messageList.removeOne(request);
    }

    request->deleteLater();
}

//Load certificate will use certificate saved in system registry
/*!
 * \brief Function passes certificate data stored inside system registry to NetworkAccessManager.
 */
void SoapManager::loadCertificate()
{
    m_networkAccessManager->clearAccessCache();

    m_sslConfiguration.setDefaultConfiguration(QSslConfiguration::defaultConfiguration());

    QSslCertificate certificate(Settings::instance()->certificateData());
    QSslKey key(Settings::instance()->certificateKeyData(), QSsl::Rsa);

    QList<QSslCertificate> caCertificateList;

    caCertificateList.append(m_sslConfiguration.defaultConfiguration().caCertificates());

    QFile cacertificateFile(":/certificate/CaCertificates");
    bool r = cacertificateFile.open(QFile::ReadOnly);

    //Add additional ca certificates from file
    caCertificateList.append(QSslCertificate::fromDevice(&cacertificateFile, QSsl::Pem));

    m_sslConfiguration.setCaCertificates(caCertificateList);
    m_sslConfiguration.setLocalCertificate(certificate);
    m_sslConfiguration.setPrivateKey(key);

    m_sslConfiguration.setPeerVerifyMode( QSslSocket::VerifyPeer);
}

/*!
 * \brief Function tries to read certificate file set in settings screen.
 *
 * File can be removed from hard drive once it was parsed in the past.
 * Signal with result will be emited and proper message displayed in settings screen.
 */
void SoapManager::performCertificatePasswordCheck()
{
    m_networkAccessManager->clearAccessCache();

#ifdef __APPLE__
    QFile certificateFile("/" + Settings::instance()->cerfiticatePath());
#else
    QFile certificateFile(Settings::instance()->cerfiticatePath());
#endif

    if(certificateFile.exists()){
        LOGD()<<"File exist";
        QSslCertificate certificate;
        QSslKey key;
        QByteArray certificatePassword = QString(Settings::instance()->certificatePassword()).toLatin1();
        QList<QSslCertificate> caCertificateList;

        certificateFile.open(QFile::ReadOnly);
        bool importResult = QSslCertificate::importPkcs12(&certificateFile, &key, &certificate, &caCertificateList, certificatePassword);
        certificateFile.close();

        LOGD()<<"IMPORT RESULT"<<importResult;

        if(importResult){
            m_networkAccessManager->clearAccessCache();

            m_sslConfiguration.setDefaultConfiguration(QSslConfiguration::defaultConfiguration());

            caCertificateList.append(m_sslConfiguration.defaultConfiguration().caCertificates());

            QFile cacertificateFile(":/certificate/CaCertificates");
            cacertificateFile.open(QFile::ReadOnly);

            //Add additional ca certificates from file
            caCertificateList.append(QSslCertificate::fromDevice(&cacertificateFile, QSsl::Pem));

            Settings::instance()->setCertificateData(certificate.toPem());
            Settings::instance()->setCertificateKeyData(key.toPem());
            Settings::instance()->storeCertificateData();

            m_sslConfiguration.setCaCertificates(caCertificateList);
            m_sslConfiguration.setLocalCertificate(certificate);
            m_sslConfiguration.setPrivateKey(key);

            m_sslConfiguration.setPeerVerifyMode( QSslSocket::VerifyPeer);
        }
        emit cerfitificatePasswordCheckResult(GlobalSettings::Enums::PasswordCheck, importResult);
    } else {
        emit cerfitificatePasswordCheckResult(GlobalSettings::Enums::PasswordCheck, GlobalSettings::Enums::FileNotExist);
    }
}

/*!
 * \brief Function sends SystemCheckRequest to Twint server
 */
void SoapManager::sendSystemCheckStatusRequest()
{
    if ( Settings::instance()->merchantUuid() == QString::null
         || Settings::instance()->cerfiticatePath() == QString::null
         || Settings::instance()->certificatePassword() == QString::null)
        return;
    CheckSystemStatusRequestItem *checkSystemRequest = new CheckSystemStatusRequestItem(this);
    createRequest(GlobalSettings::Enums::CheckSystemStatus, checkSystemRequest);
}

/*!
 * \brief Function sends EnrollCashRegisterRequest  to Twint server
 */
void SoapManager::sendEnrollCashRegisterRequest()
{
    if ( Settings::instance()->cashRegisterId() == QString::null
         || Settings::instance()->merchantUuid() == QString::null
         || Settings::instance()->isSelectUser())
        return;
    EnrollCashRegisterRequestItem *enrollCashRegisterRequestItem = new EnrollCashRegisterRequestItem(this);
    enrollCashRegisterRequestItem->setCashRegisterType("EPOS");
    createRequest(GlobalSettings::Enums::EnrollCashRegister, enrollCashRegisterRequestItem);
}

/*!
 * \brief Function handles network accesebility changes and set hostConnection setting to false if it was lost.
 * \param accessible
 */
void SoapManager::networkAccesebilityChanged(QNetworkAccessManager::NetworkAccessibility accessible)
{
    if(QNetworkAccessManager::NotAccessible == accessible){
        ApplicationStatus::instance()->setHostConnection(false);
    }
}

/**
 * \brief Function initializes cash register timer and load default certificate data on start
 */
void SoapManager::init()
{
    m_enrollCashRegisterTimer->start(14400 * 1000);
    loadCertificate();
}

void SoapManager::prepareConnections()
{
    connect(m_enrollCashRegisterTimer, &QTimer::timeout, this, &SoapManager::sendEnrollCashRegisterRequest);
    connect(m_networkAccessManager, &QNetworkAccessManager::networkAccessibleChanged, this, &SoapManager::networkAccesebilityChanged);
}
