#include "customsslcertificatereader.h"

#include "globalsettings.h"
#include "settings.h"

#include <stdio.h>
#include <stdlib.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/pkcs12.h>

#include <QFile>
#include <QSslKey>
#include <QSslCertificate>

CustomSslCertificateReader::CustomSslCertificateReader(QObject *parent) :
    QObject(parent)
{

}

CustomSslCertificateReader::~CustomSslCertificateReader()
{

}

bool CustomSslCertificateReader::importPkcs12(QString filePath, QSslKey *key, QSslCertificate *certificate, QList<QSslCertificate> *caCertificates, const QByteArray &passPhrase)
{
    FILE *fp;
    EVP_PKEY *pkey;
    X509 *cert;
    STACK_OF(X509) *ca = NULL;
    PKCS12 *p12;

    QByteArray certByteArray;
    QByteArray keyByterArray;

    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();
    if (!(fp = fopen(filePath.toLatin1().data(), "rb"))) {
        LOGD()<<"Error opening file "<<filePath;
        return false;
    }
    p12 = d2i_PKCS12_fp(fp, NULL);
    fclose (fp);
    if (!p12) {
        fprintf(stderr, "Error reading PKCS#12 file\n");
        ERR_print_errors_fp(stderr);
        return false;
    }
    if (!PKCS12_parse(p12, passPhrase, &pkey, &cert, &ca)) {
        LOGD()<<"Error parsing PKCS#12 file";
        ERR_print_errors_fp(stderr);
        return false;
    }

    PKCS12_free(p12);

    if (pkey) {
        BIO *bioKey = NULL;
        char *pemKey = NULL;

        bioKey = BIO_new(BIO_s_mem());
        if(bioKey == NULL){
            X509_free(cert);
            EVP_PKEY_free(pkey);
            BIO_free(bioKey);
            return false;
        }


        if(PEM_write_bio_PrivateKey(bioKey, pkey, NULL, NULL, 0, NULL, NULL) == 0){
            X509_free(cert);
            EVP_PKEY_free(pkey);
            BIO_free(bioKey);
            return false;
        }

        pemKey = (char*)malloc(bioKey->num_write + 1);

        BIO_read(bioKey, pemKey, bioKey->num_write);

        keyByterArray = QByteArray::fromRawData(pemKey, bioKey->num_write);

        //free(pemKey);

    }

    if (cert) {
        BIO *bioCertificate = NULL;
        char *pem = NULL;

        bioCertificate = BIO_new(BIO_s_mem());
        if(bioCertificate == NULL){
            X509_free(cert);
            EVP_PKEY_free(pkey);
            BIO_free(bioCertificate);
            return false;
        }

        if(PEM_write_bio_X509(bioCertificate, cert) == 0){
            X509_free(cert);
            EVP_PKEY_free(pkey);
            BIO_free(bioCertificate);
            return false;
        }

        pem = (char*)malloc(bioCertificate->num_write + 1);

        BIO_read(bioCertificate, pem, bioCertificate->num_write);

        certByteArray = QByteArray::fromRawData(pem,bioCertificate->num_write);

        //free(pem);
    }

//    if (ca && sk_X509_num(ca)) {
//        fprintf(fp, "***Other Certificates***\n");
//        for (int i = 0; i < sk_X509_num(ca); ++i)
//            (fp, sk_X509_value(ca, i));
//    }

    *key = QSslKey(keyByterArray, QSsl::Rsa, QSsl::Pem,QSsl::PrivateKey);

    *certificate = QSslCertificate(certByteArray, QSsl::Pem);;

    //clear memory
    sk_X509_pop_free(ca, X509_free);
    X509_free(cert);
    EVP_PKEY_free(pkey);

    return true;
}
