#ifndef USBMANAGER_H
#define USBMANAGER_H

#include "abstractdevicemanager.h"
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QAbstractNativeEventFilter>
#include <QTimer>

/**
 * @brief The UsbManager
 * Class responsible for communication with beacon.
 */
class UsbManager : public AbstractDeviceManager//, public QAbstractNativeEventFilter
{
    Q_OBJECT

public:
    explicit UsbManager(QObject *parent = 0);
    ~UsbManager();

    //bool nativeEventFilter(const QByteArray &eventType, void *message, long *result) Q_DECL_OVERRIDE;

public slots:
    void findBeacon(const QString &initString);
    void closeDevice();
    void findDevicePort();
    void performUsbPortCheck();

protected:
    void timerEvent(QTimerEvent *) Q_DECL_FINAL;

signals:
    void usbPortCheckResult(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value) const;
    void usbEventOccured() const;
    void reconnectUsbDevice() const;

private slots:
    void readyRead();
    void onBytesWritten(qint64 bytes);
    void serialError(QSerialPort::SerialPortError portErr);
    void pullData();
    void scanUsbPorts();

private:
    void init();
    void initConnections();

    bool findDevice(const QString &deviceLocation);
    void dumpDeviceInfo(const QSerialPortInfo& device);
    void openDevice(const QString &initString);
    void writeData(const QByteArray& data);

    int m_ioWriteTimerId;
    int m_ioReadTimerId;

    bool m_singleReconnect;

    qint64 m_totalToWrite;
    qint64 m_written;

    QSerialPortInfo m_portInfo;
    QSerialPort* m_serialPort;

    QByteArray m_ioBuffer;

    QTimer *m_scanPortTimer;
};

#endif // USBMANAGER_H
