#ifndef CUSTOMSSLCERTIFICATEREADER_H
#define CUSTOMSSLCERTIFICATEREADER_H

#include <QObject>

class QSslKey;
class QSslCertificate;

class CustomSslCertificateReader : public QObject
{
    Q_OBJECT
public:
    explicit CustomSslCertificateReader(QObject *parent = 0);
    ~CustomSslCertificateReader();

signals:

public slots:
    bool importPkcs12(QString filePath, QSslKey *key, QSslCertificate *certificate, QList<QSslCertificate> *caCertificates, const QByteArray &passPhrase = QByteArray());

};

#endif // CUSTOMSSLCERTIFICATEREADER_H
