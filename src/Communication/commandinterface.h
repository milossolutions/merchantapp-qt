#ifndef COMMANDINTERFACE_H
#define COMMANDINTERFACE_H

#include <QObject>

class CommandInterface : public QObject {
    Q_OBJECT
    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY progressChanged)
    int m_progress;

public:
    CommandInterface(QObject *parent = 0) : QObject(parent) {}
    virtual int progress() const
    {
        return m_progress;
    }

signals:
    void finished();
    void errorOccurred(QString errorDescription);
    void retry();
    void timeoutReached();

    void progressChanged(int arg);

public slots:
    virtual void process() = 0;
    virtual void setProgress(int arg)
    {
        if (m_progress == arg)
            return;

        m_progress = arg;
        emit progressChanged(arg);
    }

private slots:
    virtual void createConnections(){}
};

#endif // COMMANDINTERFACE_H
