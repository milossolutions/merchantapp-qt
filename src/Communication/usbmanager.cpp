#include "usbmanager.h"

#include "abstractdevicedata.h"
#include "globalsettings.h"
#include "settings.h"
#include "applicationstatus.h"
#include "crashhandler.h"

#include <QSerialPort>
#include <QTimer>
#include <QFile>

#ifdef Q_OS_WIN
#include <windows.h>
#endif
/*!
    - Linux:
        Drivers:
            No driver is required, newer kernels (2.4.31 and up) are prebuild
            with module enabled (PL-2303 VID_067B&PID_2303). TA model in keacon.

        Usage:
            On linux OS to be able to connect to ttyUSBX user, whom launch this
            application, must be in group dialout.

            To add user to group use:
            \verbatim
            sudo usermod -a -G dialout $USER
            \endverbatim
            relogin and relaunch application

    - Windows:
        Drivers are required, available to download at:
        http://www.prolific.com.tw/US/ShowProduct.aspx?p_id=225&pcid=41

    - OsX:
        Drivers are required, available to download at:
        http://www.prolific.com.tw/US/ShowProduct.aspx?p_id=229&pcid=41

  */

/*!< expeced baud rate by keacon device */
const quint32 baudRate = 115200;

/*!< expeced data bits by keacon device */
const QSerialPort::DataBits dataBits = QSerialPort::Data8;

/*!< expeced parity by keacon device */
const QSerialPort::Parity parity = QSerialPort::NoParity;

/*!< expeced stop bits by keacon device, can be either one or two */
const QSerialPort::StopBits stopBits = QSerialPort::OneStop;

/*!< software flow control */
const QSerialPort::FlowControl flowControl = QSerialPort::SoftwareControl;

const quint16 vendorId  =   1659;   // hex 067B
const quint16 productId =   8963;   // hex 2303

const qint32 timeoutInterval    = 5000;    // timeout in ms, 5 sec default

UsbManager::UsbManager(QObject *parent) :
    AbstractDeviceManager(parent)
{
    init();
    initConnections();
}

UsbManager::~UsbManager()
{
    closeDevice();
}

void UsbManager::init()
{
    m_totalToWrite = -1;
    m_written = 0;
    m_ioWriteTimerId = -1;
    m_ioReadTimerId = -1;

    m_serialPort = new QSerialPort( this);

    m_scanPortTimer = new QTimer(this);
    m_scanPortTimer->setInterval(5000);
    m_scanPortTimer->setSingleShot(false);

#ifdef Q_OS_MAC
    if(Settings::instance()->deviceType() == "USB"){
        m_scanPortTimer->start();
    }
#endif
}

void UsbManager::initConnections()
{
    connect (CrashHandler::instance(), &CrashHandler::crashDetected, this, &UsbManager::closeDevice);
    connect (m_serialPort, &QSerialPort::readyRead, this, &UsbManager::readyRead);
    connect (m_serialPort, &QSerialPort::bytesWritten, this, &UsbManager::onBytesWritten);
    connect (m_serialPort, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
             this, &UsbManager::serialError);
#ifdef Q_OS_MAC
    connect(m_scanPortTimer, &QTimer::timeout, this, &UsbManager::scanUsbPorts);
#endif
}

void UsbManager::timerEvent(QTimerEvent *e)
{
    QObject::timerEvent( e);

    if (e->timerId() == m_ioWriteTimerId) {
        const QString errMsg = QString( "Error: write timeout for port %1, error %2")
                .arg( m_serialPort->portName())
                .arg( m_serialPort->errorString());

        if(m_serialPort->error() != QSerialPort::NoError){
            error( errMsg);
        }

        killTimer( m_ioWriteTimerId);
        m_ioWriteTimerId = -1;
    }
    else if (e->timerId() == m_ioReadTimerId) {

        LOGD() << "--- io buffer" << m_ioBuffer;

        if (m_ioBuffer.endsWith( "\r")) {
            LOGD() << "------ Whole data received, logic error!";
        }


        const QString errMsg = QString( "Error: read timeout for port %1, error %2")
                .arg( m_serialPort->portName())
                .arg( m_serialPort->errorString());
        if(m_serialPort->error() != QSerialPort::NoError){
            error( errMsg);
        }

        killTimer( m_ioReadTimerId);
        m_ioReadTimerId = -1;
    }
}

void UsbManager::findBeacon(const QString& initString)
{

    if (findDevice( Settings::instance()->usbPort())) {
        openDevice( initString);
        //ApplicationStatus::instance()->setCurrentEnrollStatus(true);
        ApplicationStatus::instance()->setBeaconConnectionStatus(GlobalSettings::Enums::Connected);
    } else {
        QString keaconError = tr( "USB connection to Beacon not found ");
        error (QString(keaconError + Settings::instance()->usbPort()));
        ApplicationStatus::instance()->setSingleReconnectRequest(true);
#ifdef Q_OS_MAC
        if(!m_scanPortTimer->isActive())
            m_scanPortTimer->start();
#endif
    }
}

/*!
 * \brief ready read serial data
 */

void UsbManager::readyRead()
{
#ifdef QT_DEBUG
    //LOGD() << "Info: readyRead";
#endif

    m_ioBuffer.append( m_serialPort->readAll());

    //LOGD()<<"Reading Data"<<m_ioBuffer;

    //LOGD()<<"ready read"<<m_ioBuffer;

    // check if whole message is available, each message froms erial ends with
    // cariage return "<CR>" string
    // empty message is not an error and will return "<CR>"
    // keacon do not apply any timeouts

    if (m_ioBuffer.endsWith( "\r")) {

        parseKeaconData( m_ioBuffer);

        if (m_ioReadTimerId != -1) {
            killTimer( m_ioReadTimerId);
            m_ioReadTimerId = -1;
        }
        return;
    }

    if (m_ioReadTimerId == -1) {
        m_ioReadTimerId = startTimer( timeoutInterval);
    }
}

/*!
 * \brief on bytes written compare and stop timeout timer is data transmitted
 * match
 */

void UsbManager::onBytesWritten(qint64 bytes)
{
    m_written += bytes;
    if (m_written == m_totalToWrite) {

#ifdef QT_DEBUG
        LOGD() << "Send" << m_written << "data to port:" << m_serialPort->portName();
#endif

        m_totalToWrite = -1;
        m_written = 0;

        killTimer( m_ioWriteTimerId);
        m_ioWriteTimerId = -1;
    }
}

/*!
 * \brief serial poert error
 */

void UsbManager::serialError(QSerialPort::SerialPortError portErr)
{
    if (portErr == QSerialPort::NoError) {
        return;
    }

    if (portErr == QSerialPort::ResourceError || portErr == QSerialPort::TimeoutError) {
        ApplicationStatus::instance()->setBeaconConnectionStatus(GlobalSettings::Enums::NotConnected);
        ApplicationStatus::instance()->setSingleReconnectRequest(true);

#ifdef Q_OS_MAC
        if(Settings::instance()->deviceType() == "USB"){
            m_scanPortTimer->start();
        }
#endif
        return;
    }

    //    error( m_serialPort->errorString());
    closeDevice();
}

/*!
 * \brief find device
 * Perform search for local usb / serial device on provided location
 */

bool UsbManager::findDevice(const QString& deviceLocation)
{
    if (deviceLocation.isEmpty()) {
        error( tr("Empty device port."));
        return false;
    }

    QList<QSerialPortInfo> portInfoList = QSerialPortInfo::availablePorts();

    foreach (const QSerialPortInfo &portInfo, portInfoList) {

        dumpDeviceInfo( portInfo);
        LOGD()<<vendorId<<portInfo.vendorIdentifier()<<productId<<portInfo.productIdentifier()<<deviceLocation<<portInfo.systemLocation();

        if (   vendorId == portInfo.vendorIdentifier()
               && productId == portInfo.productIdentifier()
               && portInfo.systemLocation().split("\\").last() == deviceLocation.split("\\").last()){
            if (portInfo.isBusy()) {
                error( tr("Device is busy. usually means that it's used by other application."));
                return false;
            } else {
                m_portInfo = portInfo;
                return true;
            }
        }
    }
    return false;
}

/*!
 * \brief Function scans usb devices and sets discovered port if device matches product identifier description.
 */
void UsbManager::findDevicePort()
{
    QList<QSerialPortInfo> portInfoList = QSerialPortInfo::availablePorts();

    foreach (const QSerialPortInfo &portInfo, portInfoList) {
        dumpDeviceInfo( portInfo);
        if (vendorId == portInfo.vendorIdentifier() &&
                productId == portInfo.productIdentifier()){
            ApplicationStatus::instance()->setDiscoverdPort(portInfo.systemLocation().split("\\").last());
        }
    }
}

/*!
 * \brief Function checks if keacon is connected to set usb port while saving settings and emites result.
 */
void UsbManager::performUsbPortCheck()
{
    QList<QSerialPortInfo> portInfoList = QSerialPortInfo::availablePorts();

    foreach (const QSerialPortInfo &portInfo, portInfoList) {
        dumpDeviceInfo( portInfo);
        if (vendorId == portInfo.vendorIdentifier() &&
                productId == portInfo.productIdentifier()){
            if(Settings::instance()->usbPort() == portInfo.systemLocation().split("\\").last()){
                emit usbPortCheckResult(GlobalSettings::Enums::UsbCheck, true);
                return;
            }
        }
    }
    emit usbPortCheckResult(GlobalSettings::Enums::UsbCheck, false);
}

/*!
 * \brief dump serial port device info
 * Helper method allowing easier pinpointing location of connected keacon
 * device to system
 */

void UsbManager::dumpDeviceInfo(const QSerialPortInfo &device)
{
#ifdef QT_DEBUG
    const QString productId = device.hasProductIdentifier() ?
                QString::number( device.productIdentifier()) : "N/A";

    const QString vendorId = device.hasVendorIdentifier() ?
                QString::number( device.vendorIdentifier()) : "N/A";

    //    LOGD() << "\nDevice:" << device.description()
    //           << "\nmanufacturer:" << device.manufacturer()
    //           << "\nproductId:" << productId
    //           << "\npvendorId:" << vendorId
    //           << "\nport:" << device.portName()
    //           << "\nlocation:" << device.systemLocation()
    //           << "\nserial:" << device.serialNumber()
    //              ;
#endif
}

void UsbManager::openDevice(const QString& initString)
{
    if (!m_portInfo.isValid()) {
        error( tr( "Error: port info is invalid"));
        return;
    }

    if(ApplicationStatus::instance()->beaconConnectionStatus() != GlobalSettings::Enums::Connected){
        ApplicationStatus::instance()->setBeaconConnectionStatus(GlobalSettings::Enums::Connecting);
    }

    m_serialPort->setPort( m_portInfo);
    m_serialPort->setBaudRate( baudRate);
    m_serialPort->setDataBits( dataBits);
    m_serialPort->setParity( parity);
    m_serialPort->setStopBits( stopBits);
    m_serialPort->setFlowControl( flowControl);

    if (!m_serialPort->open( QIODevice::ReadWrite)) {
        error( tr( "Error: unable to open device ")
               .arg( m_serialPort->error())
               .arg( m_serialPort->errorString()));
        return;
    }

#ifdef Q_OS_MAC
    if(m_scanPortTimer->isActive()){
        m_scanPortTimer->stop();
    }
#endif

#ifdef QT_DEBUG
    LOGD() << "Start device communication";
#endif
    //Enable long transaction support
    writeData("Y,01\r");

    writeData( "S," + initString.toUtf8() + "\r");
    QTimer::singleShot(1000, [this]() { writeData("V\r"); } );
}

/*!
 * \brief write data
 * Write and compare written data.
 *
 * Timeout mechanism is also put in place to keep track of data. This is not
 * really necessary as serial port will emit timeout error message.
 */

void UsbManager::writeData(const QByteArray& data)
{
    LOGD()<<"Writing Data"<<data;
    m_totalToWrite = data.size();
    const qint64 written = m_serialPort->write( data);

    if (written == -1) {
        const QString errMsg = QString( "Error: failed to write data to port %1, error %2")
                .arg( m_serialPort->portName())
                .arg( m_serialPort->errorString());
        error( errMsg);

        killTimer( m_ioWriteTimerId);
        m_ioWriteTimerId = -1;

    } else if (written != data.size()) {
        const QString errMsg = QString( "Error: failed to write all data to port %1, error %2")
                .arg( m_serialPort->portName())
                .arg( m_serialPort->errorString());
        error( errMsg);

        killTimer( m_ioWriteTimerId);
        m_ioWriteTimerId = -1;
    }

    if (m_ioWriteTimerId == -1) {
        m_ioWriteTimerId = startTimer( timeoutInterval);
    } else {
        LOGD() << "Writting data while previouse reply not processed.";

        killTimer( m_ioWriteTimerId);
        m_ioWriteTimerId = startTimer( timeoutInterval);
    }
}

/*!
 * \brief close Device
 * End communication with keacon and send termination message.
 * Invoked also when crash is detected, close attempt, depending on crash
 * situation, may not work properly.
 */

void UsbManager::closeDevice()
{
    if (m_serialPort->isOpen() && m_serialPort->isWritable()) {
        LOGD() << "---- usb close device";

        const qint64 written = m_serialPort->write( "T\r");
        m_serialPort->flush();

        //Code was causing loop on MacOS when beacon was connected and we closed application
#ifdef Q_OS_WIN
        if (written != -1) {
            LOGD() << "Info: clocing device on" << m_serialPort->portName();
            m_serialPort->waitForBytesWritten( timeoutInterval);
        } else {
            LOGD() << "Info: unable close keacon on crash.";
        }
#endif
        m_serialPort->close();
        ApplicationStatus::instance()->setSingleReconnectRequest(true);
    }
}

/*!
 * \brief pull data request
 * Keacon supports pulling status of device, example received information
 * \verbatim
 *  p,A1B1,01,#running\r
 * \endverbatim
 * Where p is reply-on-pool message type, A1B1 is pool data, 2 Bytes in Hex,
 * it's set by application for comparison purposes, 01 is status, witch can be
 * 00:standby state, 01:running, 02:connected and lastly logging information,
 * string up to 128 characters with extended information.
 *
 * This is not necessary, because QIODevice has mechanism that will inform
 * when device is unconnected, for QSerialPort it's IO error, for QSslSocket
 * when device is unplugged connection will close immediately, socket also has
 * keep alive flag set so when i.e. serve do power off connection will
 * automatically close.
 */

void UsbManager::pullData()
{
    writeData( "P,"
               + controllData + ","
                                "\r"
               );

    QTimer::singleShot( poolDelay, this, SLOT(pullData()));
}

void UsbManager::scanUsbPorts()
{
#ifdef Q_OS_MAC
    if((Settings::instance()->deviceType() != "USB") || (ApplicationStatus::instance()->beaconConnectionStatus() == GlobalSettings::Enums::Connected)){
        if(m_scanPortTimer->isActive())
            m_scanPortTimer->stop();
    }
    QList<QSerialPortInfo> portInfoList = QSerialPortInfo::availablePorts();
    foreach (const QSerialPortInfo &portInfo, portInfoList) {
        if (vendorId == portInfo.vendorIdentifier() &&
                productId == portInfo.productIdentifier()){
            if(Settings::instance()->usbPort() == portInfo.systemLocation().split("\\").last()){
                emit reconnectUsbDevice();
                return;
            }
        }
    }
#endif
}
