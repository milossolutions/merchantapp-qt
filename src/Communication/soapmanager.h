#ifndef SOAPMANAGER_H
#define SOAPMANAGER_H

#include "globalsettings.h"

#include <QObject>
#include <QSslConfiguration>
#include <QNetworkAccessManager>

//class QNetworkAccessManager;
class MessageInterface;
class RequestItemInterface;
class ResponseItemInterface;
class ContainerItemInterface;
class EnrollCashRegisterMessage;
class QTimer;

class SoapManager : public QObject
{
    Q_OBJECT
public:
    explicit SoapManager(QObject *parent = 0);
    ~SoapManager();

    void createRequest(const GlobalSettings::Enums::SoapMessageType type, RequestItemInterface *requestItem);

public slots:
    void removeFinishedRequest();
    void loadCertificate();
    void performCertificatePasswordCheck();

private slots:
    void sendSystemCheckStatusRequest();
    void sendEnrollCashRegisterRequest();
    void networkAccesebilityChanged(QNetworkAccessManager::NetworkAccessibility accessible);

signals:
    void sendingOrderContainer(ContainerItemInterface *containerItem) const;
    void sendingMonitorContainer(ContainerItemInterface *containerItem) const;
    void onEnrollCashRegister(ContainerItemInterface *containerItem) const;
    void onKeaconInfoContainer(ContainerItemInterface *containerItem) const;
    void sendingRequestCheckInContainer(ContainerItemInterface *containerItem) const;
    void sendingMonitorCheckInContainer(ContainerItemInterface *containerItem) const;

    void showMessage(GlobalSettings::Enums::PopUpType popUpType, QString message);

    void cerfitificatePasswordCheckResult(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value) const;
    void checkSystemStatusResult(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value) const;

    void abortOrder() const;

private:
    void init();
    void prepareConnections();
    QNetworkAccessManager *m_networkAccessManager;
    QList<MessageInterface*> m_messageList;
    QSslConfiguration m_sslConfiguration;

    /**
     * @brief m_enrollCashRegisterTimer
     * Timer that sends is related with EncrollCashRegisterMessage.
     * Sends new requests every 4 hours.
     */
    QTimer *m_enrollCashRegisterTimer;
};

#endif // SOAPMANAGER_H
