#include "httpdownload.h"

#include <QFileInfo>
#include <QDebug>

#include "globalsettings.h"

/*!
 * \class HTTPDownload
 * \brief Class responsible for downloading file from set address and store it on drive.
 * \param url Adress from which file will be downloaded from
 */
HTTPDownload::HTTPDownload(const QString &url, QObject *parent) :
    CommandInterface(parent),
    m_timeoutInterval(5000),
    m_url(url),
    m_file(Q_NULLPTR),
    m_currentRequest(Q_NULLPTR),
    m_qNam(Q_NULLPTR),
    m_tryFunc(Q_NULLPTR)
{
    m_timer.setInterval(5000);
    m_timer.setSingleShot(true);
    setDState(HTTPDownloadState::Ready);
    createConnections();
}

HTTPDownload::HTTPDownload(QObject *parent) :
    CommandInterface(parent)
{

}

HTTPDownload::~HTTPDownload()
{

}

/*!
 * \brief File with data that is being downloaded
 * \return
 */
QFile *HTTPDownload::file() const
{
    return m_file;
}

void HTTPDownload::setFile(QFile *file)
{
    m_file = file;
    if (file)
        m_originalName = file->fileName();
}

/*!
 * \brief Represent current download state of proccess
 * \return
 */
HTTPDownload::HTTPDownloadState HTTPDownload::dState() const
{
    return m_dState;
}

/*!
 * \brief Time after which request will be discarded
 * \return
 */
int HTTPDownload::timeoutInterval() const
{
    return m_timeoutInterval;
}

void HTTPDownload::setTimeoutInterval(int timeoutInterval)
{
    m_timeoutInterval = timeoutInterval;
    m_timer.setInterval(timeoutInterval);
}

/*!
 * \brief Adress from which file will be downloaded from
 * \return
 */
QString HTTPDownload::url() const
{
    return m_url;
}

void HTTPDownload::setUrl(const QString &url)
{
    m_url = url;
}

/*!
 * \brief Function tries to restart process if it was terminated by some error
 * \param lastState
 */
void HTTPDownload::setTryAgainFunction(HTTPDownloadState lastState)
{
    if ( lastState == HTTPDownloadState::DownloadingHeader )
        m_tryFunc = &HTTPDownload::process;
    else if ( lastState == HTTPDownloadState::DownloadingFile )
        m_tryFunc = &HTTPDownload::download;
    else
        m_tryFunc = Q_NULLPTR;
}

/*!
 * \brief Function starts download process
 */
void HTTPDownload::process()
{
    LOGD() << "GET HEADER";
    m_timer.stop();
    setDState(HTTPDownloadState::DownloadingHeader);
    //Reset progress.
    setProgress(0);
    if (!m_file)
        qFatal("File is null.");

    if (m_file->exists()) {
        bool fileRemoved = false;
        fileRemoved = m_file->remove();
        if (!fileRemoved) {
            emit errorOccurred( QString(tr("Cannot remove old downloaded file: %1.")).arg(m_file->fileName()) );
            return;
        }
    }

    m_file->rename(m_originalName + partSuffix); // Rename to something.part

    if (m_file->exists()) {
        bool fileRemoved = false;
        fileRemoved = m_file->remove();
        if (!fileRemoved) {
            emit errorOccurred( QString(tr("Cannot remove old downloaded file: %1.")).arg(m_file->fileName()) );
            return;
        }
    }

    if (!m_qNam)
        m_qNam = new QNetworkAccessManager(this);

    m_currentRequest = new QNetworkRequest(m_url);

    m_currentReply = m_qNam->head(*m_currentRequest);

    connect(m_currentReply, &QNetworkReply::finished, this,  &HTTPDownload::receivedHead);
    connect(m_currentReply, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(error(QNetworkReply::NetworkError)), Qt::UniqueConnection);
    emit downloadStarted();
    m_timer.start();
}

void HTTPDownload::setDState(HTTPDownload::HTTPDownloadState arg)
{
    if (m_dState == arg)
        return;

    m_dState = arg;
    emit dStateChanged(arg);
}

void HTTPDownload::createConnections()
{
    connect(this, &HTTPDownload::headReceived, this, &HTTPDownload::download);
    connect(&m_timer, &QTimer::timeout, this, &HTTPDownload::timeout);
    connect(this, &HTTPDownload::retry, this, &HTTPDownload::tryAgain);
}

/*!
 * \brief Handler function called when request head was received
 */
void HTTPDownload::receivedHead()
{
    m_timer.stop();
    setDState(HTTPDownloadState::HeaderDownloadComplete);
    m_acceptRanges = false;

    if (m_currentReply->hasRawHeader("Accept-Ranges")) {
        QString qstrAcceptRanges = m_currentReply->rawHeader("Accept-Ranges");
        m_acceptRanges = (qstrAcceptRanges.compare("bytes", Qt::CaseInsensitive) == 0);
        LOGD() << "Accept-Ranges = " << qstrAcceptRanges << m_acceptRanges;
    }

    m_totalSize = m_currentReply->header(QNetworkRequest::ContentLengthHeader).toInt();
    m_currentRequest->setRawHeader("Connection", "Keep-Alive");
    m_currentRequest->setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);

    if ( !m_file->isOpen() ) {
        if ( !m_file->open(QIODevice::ReadWrite | QIODevice::Append) )
            qFatal("Cannot open file");
    }

    LOGD() << "Size: " << m_totalSize;
    LOGD() << "Path: " << m_file->fileName();

    emit headReceived();
    m_timer.start();
}

/*!
 * \brief Handler function starts file download process
 */
void HTTPDownload::download()
{
    LOGD() << "GET BODY";
    m_timer.stop();
    setDState(HTTPDownloadState::DownloadingFile);

    if ( m_acceptRanges ) {
        QByteArray rangeHeaderValue = "bytes=" + QByteArray::number(m_file->size()) + "-";
        if (m_totalSize > 0) {
            rangeHeaderValue += QByteArray::number(m_totalSize);
            LOGD() << "R_VALUE: " << rangeHeaderValue;
        }
        m_currentRequest->setRawHeader("Range", rangeHeaderValue);
    }

    m_currentReply->deleteLater();
    m_currentReply = Q_NULLPTR;
    m_currentReply = m_qNam->get(*m_currentRequest);

    connect(m_currentReply, &QNetworkReply::downloadProgress, this, &HTTPDownload::downloadProgress);
    connect(m_currentReply, &QNetworkReply::finished, this, &HTTPDownload::downloadComplete);
    connect(m_currentReply, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(error(QNetworkReply::NetworkError)), Qt::UniqueConnection);

    m_timer.start();
}

/*!
 * \brief Handler function, monitors download progress
 * \param received
 * \param total
 */
void HTTPDownload::downloadProgress(quint64 received, quint64 total)
{
    m_timer.stop();
    LOGD() << received << "bytes / " << total << "bytes";
    if ( !m_file->isOpen())
        if ( !m_file->open(QIODevice::ReadWrite | QIODevice::Append) )
            qFatal("Cannot open file");

    if ( m_file->write(m_currentReply->readAll()) == -1 )
        qFatal("Cannot write to file.");
    if (total > 0)
        setProgress(static_cast<int>((received / total) * 100));

    m_timer.start();
}

/*!
 * \brief Handler function called after download finish.
 * Emits signal that is catched in VersionCheck to let it know that file is ready to be read.
 */
void HTTPDownload::downloadComplete()
{
    LOGD() << "HERE FINISH";
    m_timer.stop();
    setDState(HTTPDownloadState::FileDownloadComplete);
    m_file->close();
    m_file->rename(m_originalName);
    emit finished();
}

/*!
 * \brief Function called on m_timer timeout. That means request took to long to be processed.
 * Try again function is called to restart download process.
 */
void HTTPDownload::timeout()
{
    LOGD() << "TIMEOUT";
    if (dState() == HTTPDownloadState::DownloadingHeader) {
        disconnect(m_currentReply, &QNetworkReply::finished, this,  &HTTPDownload::receivedHead);
    } else if (dState() == HTTPDownloadState::DownloadingFile){
        disconnect(m_currentReply, &QNetworkReply::downloadProgress, this, &HTTPDownload::downloadProgress);
        disconnect(m_currentReply, &QNetworkReply::finished, this, &HTTPDownload::downloadComplete);
    }
    disconnect(m_currentReply, SIGNAL(error(QNetworkReply::NetworkError)),
               this, SLOT(error(QNetworkReply::NetworkError)));
    setTryAgainFunction(dState());
    setDState(HTTPDownloadState::Timeout);
    emit timeoutReached();
}

/*!
 * \brief Slot called on network error. Request is resend to service.
 * \param code
 */
void HTTPDownload::error(QNetworkReply::NetworkError code)
{
    Q_UNUSED(code);
    m_timer.stop();
    if (dState() == HTTPDownloadState::DownloadingHeader) {
        disconnect(m_currentReply, &QNetworkReply::finished, this,  &HTTPDownload::receivedHead);
    } else if (dState() == HTTPDownloadState::DownloadingFile){
        disconnect(m_currentReply, &QNetworkReply::downloadProgress, this, &HTTPDownload::downloadProgress);
        disconnect(m_currentReply, &QNetworkReply::finished, this, &HTTPDownload::downloadComplete);
    }
    disconnect(m_currentReply, SIGNAL(error(QNetworkReply::NetworkError)),
               this, SLOT(error(QNetworkReply::NetworkError)));
    setTryAgainFunction(dState());
    setDState(HTTPDownloadState::Error);
    emit errorOccurred( QString(tr("Network error: %1")).arg( static_cast<int>(code) ) );
}

void HTTPDownload::tryAgain()
{
    if ( m_tryFunc )
        (this->*m_tryFunc)();
}

