#ifndef HTTPDOWNLOAD_H
#define HTTPDOWNLOAD_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QTimer>

#include "commandinterface.h"

const QString partSuffix = ".part";
class HTTPDownload;
typedef void (HTTPDownload::*TryAgainFunction)();

class HTTPDownload : public CommandInterface
{
    Q_OBJECT
    Q_PROPERTY(HTTPDownloadState dState READ dState WRITE setDState NOTIFY dStateChanged)

public:
    enum HTTPDownloadState {
        Ready,
        DownloadingHeader,
        HeaderDownloadComplete,
        DownloadingFile,
        FileDownloadComplete,
        Error,
        Timeout
    };

    explicit HTTPDownload(const QString &url, QObject *parent = 0);
    ~HTTPDownload();
    QFile *file() const;
    void setFile(QFile *file);
    HTTPDownloadState dState() const;
    int timeoutInterval() const;
    void setTimeoutInterval(int timeoutInterval = 5000);
    QString url() const;
    void setUrl(const QString &url);

private:
    explicit HTTPDownload(QObject *parent);
    int m_timeoutInterval;
    int m_totalSize;
    bool m_acceptRanges;
    QString m_url;
    QFile *m_file;
    QString m_originalName;
    QNetworkRequest *m_currentRequest;
    QNetworkReply *m_currentReply;
    QNetworkAccessManager *m_qNam;
    QTimer m_timer;
    TryAgainFunction m_tryFunc;
    void setTryAgainFunction(HTTPDownloadState lastState);

    HTTPDownloadState m_dState;

signals:
    void downloadStarted() const;
    void headReceived() const;
    void dStateChanged(HTTPDownloadState arg) const;

public slots:
    void process();
    void setDState(HTTPDownloadState arg);
    void timeout();
    void error(QNetworkReply::NetworkError code);
    void tryAgain();
    void downloadComplete();

private slots:
    void createConnections();
    void receivedHead();
    void download();
    void downloadProgress(quint64 received, quint64 total);
};

#endif // HTTPDOWNLOAD_H
