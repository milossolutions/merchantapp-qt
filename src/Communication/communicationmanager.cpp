#include "communicationmanager.h"

#include "soapmanager.h"
#include "usbmanager.h"
#include "wifimanager.h"
#include "versionchecker.h"

#include "enrollcashregisterrequestitem.h"
#include "merchantinformationtype.h"
#include "startorderrequestitem.h"
#include "requestcheckinrequestitem.h"
#include "orderrequesttype.h"
#include "currencyamounttype.h"
#include "monitorcheckinrequestitem.h"
#include "cancelcheckinrequestitem.h"
#include "monitorcheckinrequestitem.h"
#include "confirmorderrequestitem.h"
#include "cancelorderrequestitem.h"
#include "findorderrequestitem.h"
#include "checksystemstatusrequestitem.h"
#include "monitororderrequestitem.h"
#include "couponlisttype.h"
#include "coupontype.h"
#include "currencyamounttype.h"
#include "rejectedcoupontype.h"
#include "couponrejectionreason.h"
#include "ordertype.h"
#include "orderlinktype.h"

#include "requestcheckinresponseitem.h"
#include "monitorcheckinresponseitem.h"
#include "cancelcheckinresponseitem.h"
#include "startorderresponseitem.h"
#include "monitororderresponseitem.h"
#include "confirmorderresponseitem.h"
#include "cancelorderresponseitem.h"
#include "findorderresponseitem.h"
#include "enrollcashregisterresponseitem.h"
#include "checksystemstatusresponseitem.h"

#include "containeriteminterface.h"
#include "keconinformation.h"
#include "requestcheckincontainer.h"
#include "monitorcheckincontainer.h"

#include "settings.h"
#include "applicationstatus.h"

#include <QMetaProperty>
#include <QPointer>
#include <QTimer>
#include <QNetworkConfigurationManager>
#include <QDateTime>

/*! \file communicationmanager.h */

/*! \var KeconInformation* m_keaconInformation
    \brief Keacon information received from TWINT .

    This object is stored for feature usage, i.e. for situation when Merchant
    change keacon type or certificate.
*/

/**
 * \class CommunicationManager
 * @brief Class responsible managing communication between soap service, usb connection, wifi connection.
 * It receive and sends data to proper places.
 */
CommunicationManager::CommunicationManager(QObject *parent) :
    QObject(parent),
    m_keaconInformation( Q_NULLPTR),
    m_monitorMessagesCounter(0),
    m_monitorPairingCounter(0)
{
}

CommunicationManager::~CommunicationManager()
{

}

void CommunicationManager::init()
{
    {
        m_versionChecker = new VersionChecker(this);
        m_soapManager = new SoapManager(this);
        m_usbManager = new UsbManager(this);
        m_wifiManager = new WifiManager(this);
        m_reconnectTimer = new QTimer(this);
        m_reconnectTimer->setInterval(5000);
        m_offlineTimer = new QTimer(this);
        m_offlineTimer->setSingleShot(true);
        m_offlineTimer->setInterval(60000);

        prepareConnections();
    }
}

/*!
 * \brief Function creates request objects, fills them with received data,
 *  then passes them to SoapManger to be further send to TWINT server.
 * \param messageType Type of message request that will be send
 * \param list List with parameters, based on its values request will cause different actions.
 */
void CommunicationManager::makeRequestHandler(GlobalSettings::Enums::SoapMessageType messageType, QStringList list)
{
    bool isListNonEmpty = !list.isEmpty();
    switch(messageType){
        case GlobalSettings::Enums::RequestCheckIn:
        {
            RequestCheckInRequestItem *requestCheckInRequestItem = new RequestCheckInRequestItem(this);
            if(list.contains("renderQr")){
                requestCheckInRequestItem->setQRCodeRendering("true");
            }else{
                requestCheckInRequestItem->setQRCodeRendering("false");
            }

            if(ApplicationStatus::instance()->offlineCode() != ""){
                qDebug()<<"OfflineCode "<<ApplicationStatus::instance()->offlineCode();
                requestCheckInRequestItem->setOfflineAuthorization(ApplicationStatus::instance()->offlineCode());
                m_offlineTimer->stop();
            }else{
                requestCheckInRequestItem->setUnidentifiedCustomer("false");
            }
            emit makeSoapRequest(GlobalSettings::Enums::RequestCheckIn, requestCheckInRequestItem);
            break;
        }
        case GlobalSettings::Enums::MonitorCheckIn:
        {
            MonitorCheckInRequestItem *monitorcheckInRequestItem = new MonitorCheckInRequestItem(this);
            if (isListNonEmpty)
                monitorcheckInRequestItem->setPairingUuid(list.at(0));

            emit makeSoapRequest(GlobalSettings::Enums::MonitorCheckIn, monitorcheckInRequestItem);
            break;
        }
        case GlobalSettings::Enums::CancelCheckIn:
        {
            CancelCheckInRequestItem *cancelCheckInRequestItem = new CancelCheckInRequestItem(this);
            if (isListNonEmpty)
                cancelCheckInRequestItem->setReason(list.at(0));

            if(list.count() > 1){
                cancelCheckInRequestItem->setPairingUuid(list.at(1));
            }

            emit makeSoapRequest(GlobalSettings::Enums::CancelCheckIn, cancelCheckInRequestItem);
            break;
        }
        case GlobalSettings::Enums::StartOrder:
        {
            StartOrderRequestItem *startOrderRequestItem = new StartOrderRequestItem(this);

            startOrderRequestItem->orderType()->setPostingType("GOODS");

            if (isListNonEmpty)
                startOrderRequestItem->orderType()->requestedAmountType()->setAmount(list.at(0));
            startOrderRequestItem->orderType()->requestedAmountType()->setCurrency("CHF");

            startOrderRequestItem->orderType()->setMerchantTransactionReference(" ");

            if (list.size() > 1) {
                startOrderRequestItem->orderType()->setTypeAttribute(list.at(1));
            }
            if (list.at(1) == "REVERSAL" && list.size() > 2) {
                startOrderRequestItem->orderType()->linkType()->setOrderUuid(list.at(2));
            } else {
                for (int i = 2; i < list.size(); i+=2) {
                    CouponType *coupon = new CouponType(this);
                    coupon->setCouponId(list.at(i));
                    coupon->couponValueType()->setAmount(list.at(i + 1));
                    coupon->couponValueType()->setCurrency("CHF");
                    startOrderRequestItem->couponsType()->appendProcessedCoupon(coupon);
                }
            }

            startOrderRequestItem->orderType()->setConfirmationNeededAttribute("false");


            if(ApplicationStatus::instance()->offlineCode() != ""){
                qDebug()<<"OfflineCode "<<ApplicationStatus::instance()->offlineCode();
                startOrderRequestItem->setOfflineAuthorization(ApplicationStatus::instance()->offlineCode());
            }else{
                startOrderRequestItem->setUnidentifiedCustomer("false");
            }

            if (list.at(1) == "REVERSAL"){
                startOrderRequestItem->setQRCodeRendering("false");
            }else{
                startOrderRequestItem->setQRCodeRendering("true");
            }

            emit makeSoapRequest(GlobalSettings::Enums::StartOrder, startOrderRequestItem);
            break;
        }
        case GlobalSettings::Enums::EnrollCashRegister:
        {
            EnrollCashRegisterRequestItem *enrollCashRegisterRequestItem = new EnrollCashRegisterRequestItem(this);
            if (isListNonEmpty)
                enrollCashRegisterRequestItem->setCashRegisterType(list.at(0));
            emit makeSoapRequest(GlobalSettings::Enums::EnrollCashRegister, enrollCashRegisterRequestItem);
            break;
        }
        case GlobalSettings::Enums::CancelOrder:
        {
            CancelOrderRequestItem *cancelOrderRequestItem = new CancelOrderRequestItem(this);
            if (isListNonEmpty)
                cancelOrderRequestItem->setOrderUuid(list.at(0));
            emit makeSoapRequest(GlobalSettings::Enums::CancelOrder, cancelOrderRequestItem);
            break;
        }
        case GlobalSettings::Enums::ConfirmOrder:
        {
            ConfirmOrderRequestItem *confirmOrderRequestItem = new ConfirmOrderRequestItem(this);


            emit makeSoapRequest(GlobalSettings::Enums::ConfirmOrder, confirmOrderRequestItem);
            break;
        }
        case GlobalSettings::Enums::MonitorOrder:
        {
            MonitorOrderRequestItem *monitorOrderRequestItem = new MonitorOrderRequestItem(this);
            if (isListNonEmpty)
                monitorOrderRequestItem->setOrderUuid(list.at(0));
            emit makeSoapRequest(GlobalSettings::Enums::MonitorOrder, monitorOrderRequestItem);
            break;
        }
        case GlobalSettings::Enums::CheckSystemStatus:
        {
            CheckSystemStatusRequestItem *checkSystemStatusItem = new CheckSystemStatusRequestItem(this);
            emit makeSoapRequest(GlobalSettings::Enums::CheckSystemStatus, checkSystemStatusItem);
            break;
        }
        default:
        {
            LOGD()<<"Unknown message type";
        }
    }
}

/*!
 * \brief Slot catches received response from RequestCheckInRequest for further handling.
 *
 * If pairing cannot be established we drop transaction process.
 * PAIRING_IN_PROGRESS means that pairing yet has to be established
 * If pairing is active that means TWINT servers successfully paired customer with merchant,
 * we additionally check here if coupons were received in response.
 * If they weren't we send additional MonitorCheckInRequest because delay exist between TWINT server and coupons service,
 * so it's not clear at the start if customer has avalible coupons or not.
 * If we receive qr code data we send signal to display it on screen.
 * \param containerItem
 */
void CommunicationManager::onRequestCheckInHandler(ContainerItemInterface *containerItem)
{
    LOGD()<<"Received request check in container";
    RequestCheckInContainer *requestCheckInContainer = qobject_cast<RequestCheckInContainer*>(containerItem);

    if(!requestCheckInContainer){
        return;
    }

    m_monitorMessagesCounter = 0;
    m_monitorPairingCounter = 0;

    if(requestCheckInContainer->qrCode() == QString::null){
        //Null qr code means customer requested payment by beacon
        if(requestCheckInContainer->pairingStatus() == "NO_PAIRING"){
            this->makeRequestHandler(GlobalSettings::Enums::CancelCheckIn, QStringList{"INVALID_PAIRING",requestCheckInContainer->pairingUuid()});
            containerItem->deleteLater();
            ApplicationStatus::instance()->setRequestCheckInStatus(false);
            ApplicationStatus::instance()->setPairingOnGoingStatus(false);
            showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("The payment is terminated"));
        }else if(requestCheckInContainer->pairingStatus() == "PAIRING_IN_PROGRESS"){
            //Call delayed monitor check in message. To download coupons pairing must be active
            QString pairingUuid = requestCheckInContainer->pairingUuid();
            QTimer::singleShot(2000, [=]() {
                makeRequestHandler(GlobalSettings::Enums::MonitorCheckIn, QStringList{pairingUuid});
            } );
            containerItem->deleteLater();
        }else if(requestCheckInContainer->pairingStatus() == "PAIRING_ACTIVE"){
            if(requestCheckInContainer->couponListType().count() == 0){
                QString pairingUuid = requestCheckInContainer->pairingUuid();
                QTimer::singleShot(2000, [=]() {
                    makeRequestHandler(GlobalSettings::Enums::MonitorCheckIn, QStringList{pairingUuid});
                } );
                containerItem->deleteLater();
            }else {
                if(!ApplicationStatus::instance()->pairingOnGoingStatus()){
                    ApplicationStatus::instance()->setPairingOnGoingStatus(true);
                    emit receivedCouponsDataFromRequest(containerItem);
                }

                if(!ApplicationStatus::instance()->paymentOngoingStatus()){
                    QString pairingUuid = requestCheckInContainer->pairingUuid();
                    QTimer::singleShot(2000, [=]() {
                        makeRequestHandler(GlobalSettings::Enums::MonitorCheckIn, QStringList{pairingUuid});
                    } );
                }
            }
        }
    } else {
        //Display pairing qrCode
        emit receivedRequestCheckInData(requestCheckInContainer);
    }

}

/*!
 * \brief Function receives response from MonitorCheckInRequest
 * It functionality works in similar way to onRequestCheckInHandler.
 * Additional timeout process was created to drop transaction process if establishing pairing is taking too long.
 * Additional MonitorCheckInRequest is send if we haven't received coupons on establishing pairing.
 * \param containerItem
 */
void CommunicationManager::onMonitorCheckInHandler(ContainerItemInterface *containerItem)
{
    MonitorCheckInContainer *monitorCheckInContainer = qobject_cast<MonitorCheckInContainer*>(containerItem);

    if(!monitorCheckInContainer){
        return;
    }

    m_monitorMessagesCounter++;

    qDebug()<<"Monitor check in";
    if(monitorCheckInContainer->pairingStatus() == "NO_PAIRING"){
        qDebug()<<"Monitor check in no pairing";
        qDebug()<<"Request check in"<<ApplicationStatus::instance()->requestCheckInStatus();
        qDebug()<<"paymentOngoingStatus"<<ApplicationStatus::instance()->paymentOngoingStatus();
        qDebug()<<"pairingOnGoingStatus"<<ApplicationStatus::instance()->pairingOnGoingStatus();
        if(ApplicationStatus::instance()->requestCheckInStatus() || ApplicationStatus::instance()->couponScreen()){
            this->makeRequestHandler(GlobalSettings::Enums::CancelCheckIn, QStringList{"INVALID_PAIRING",monitorCheckInContainer->pairingUuid()});
            ApplicationStatus::instance()->setRequestCheckInStatus(false);
            ApplicationStatus::instance()->setPairingOnGoingStatus(false);
            showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("The payment is terminated"));
        }
        containerItem->deleteLater();
    }else if(monitorCheckInContainer->pairingStatus() == "PAIRING_IN_PROGRESS"){
        //Sending new request check in if customer tries to pair with offline code
        LOGD()<<"MONITOR PAIRING"<<ApplicationStatus::instance()->offlineCode();
        if(ApplicationStatus::instance()->offlineCode() != ""){
            QTimer::singleShot(2000, [=]() {
                makeRequestHandler(GlobalSettings::Enums::RequestCheckIn, QStringList{"renderQr"});
            } );
            containerItem->deleteLater();
            return;
        }
        //If customer cannot pair with merchant for 90 seconds cancel pairing
        if(m_monitorMessagesCounter > 45){
            this->makeRequestHandler(GlobalSettings::Enums::CancelCheckIn, QStringList{"INVALID_PAIRING",monitorCheckInContainer->pairingUuid()});
            containerItem->deleteLater();
            showMessage(GlobalSettings::Enums::ErrorPopUp, QObject::tr("Payment timeout"));
            ApplicationStatus::instance()->paymentTimeout();
            return;
        }

        QString pairingUuid = monitorCheckInContainer->pairingUuid();
        //Call delayed monitor check in message. To download coupons pairing must be active
        QTimer::singleShot(2000, [=]() {
            makeRequestHandler(GlobalSettings::Enums::MonitorCheckIn, QStringList{pairingUuid});
        } );
        containerItem->deleteLater();
    }else if(monitorCheckInContainer->pairingStatus() == "PAIRING_ACTIVE"){
        //It could occur that coupons aren't properly downloaded after active pairing occured so we are delaying starting order
        if((m_monitorPairingCounter < 1) && (monitorCheckInContainer->couponListType().count() == 0)){
            m_monitorPairingCounter++;

            QString pairingUuid = monitorCheckInContainer->pairingUuid();
            QTimer::singleShot(1000, [=]() {
                makeRequestHandler(GlobalSettings::Enums::MonitorCheckIn, QStringList{pairingUuid});
            } );
            containerItem->deleteLater();
        } else {
            if(!ApplicationStatus::instance()->pairingOnGoingStatus()){
                ApplicationStatus::instance()->setPairingOnGoingStatus(true);
                emit receivedCouponsDataFromMonitor(containerItem);
            }

            if(m_monitorMessagesCounter > 45){
                this->makeRequestHandler(GlobalSettings::Enums::CancelCheckIn, QStringList{"INVALID_PAIRING",monitorCheckInContainer->pairingUuid()});
                containerItem->deleteLater();
                showMessage(GlobalSettings::Enums::ErrorPopUp, QObject::tr("Payment timeout"));
                ApplicationStatus::instance()->paymentTimeout();
                return;
            }

            if(!ApplicationStatus::instance()->paymentOngoingStatus()){
                QString pairingUuid = monitorCheckInContainer->pairingUuid();
                QTimer::singleShot(2000, [=]() {
                    makeRequestHandler(GlobalSettings::Enums::MonitorCheckIn, QStringList{pairingUuid});
                } );
            }
        }
    }
}

/*!
 * \brief set keacon information
 * Setter for keacon device initialization information, obtained after registering
 * cash resister
 */
void CommunicationManager::setKeaconInformation(ContainerItemInterface *keaconInfo)
{
    if (m_keaconInformation) {
        delete m_keaconInformation;
        m_keaconInformation = Q_NULLPTR;
    }

    m_keaconInformation = qobject_cast<KeconInformation*>( keaconInfo);

    startKeaconCommunication();
}

/*!
 * \brief start keacon communication
 * Start communication with keacon device base on selected settings use USB or
 * WiFi keacon.
 *
 * Slot is connected to Settings::instance()->settingsChanged();
 */
void CommunicationManager::startKeaconCommunication()
{
    if(Settings::instance()->isSelectUser() && ApplicationStatus::instance()->currentCashRegister().isEmpty()){
        return;
    }

    if (!m_keaconInformation) {
        LOGD() << "Error, invalid keacon infromation object.";
        return;
    }

    // close connection, user may changed settings
    m_usbManager->closeDevice();
    m_wifiManager->closeDevice();

    ApplicationStatus::instance()->setCurrentEnrollStatus(false);

    // start communication

    if (Settings::instance()->deviceType() == "USB") {
        m_usbManager->findBeacon( m_keaconInformation->m_beaconInitString);
    } else if (Settings::instance()->deviceType() == "WIFI") {
        m_wifiManager->findBeacon( m_keaconInformation->m_beaconInitString);
    } else if (Settings::instance()->deviceType() == "NONE") {
    }else {
        LOGD() << "Error, invalid device type";
    }

    //Received enroll cash register message
    ApplicationStatus::instance()->setCurrentEnrollStatus(true);
}

/*!
 * \brief Function closses connection with beacon.
 * Used when cash register is set to "select user" mode. When cash register changes it's ids.
 */
void CommunicationManager::closeKeaconCommunication()
{
    ApplicationStatus::instance()->setCurrentEnrollStatus(false);
    m_usbManager->closeDevice();
    m_wifiManager->closeDevice();

    ApplicationStatus::instance()->setBeaconConnectionStatus(GlobalSettings::Enums::NotConnected);
}

/*!
 * \brief connectionStatus changed handler. Stop sending reconnect request if connection was restored
 * \param connectionStatus connectivity value
 */
void CommunicationManager::connectionChangedHandler(bool connectionStatus)
{
    LOGD()<< "Connection changed"<<connectionStatus;
    if(connectionStatus){
        if(m_reconnectTimer->isActive()){
            m_reconnectTimer->stop();
        }
    }else{
        m_reconnectTimer->start();
    }
}

/*!
 * \brief timeout handler related with reconnect. Sends CheckSystemStatus on timeout
 */
void CommunicationManager::reconnectTimerTimeout()
{
    makeRequestHandler(GlobalSettings::Enums::CheckSystemStatus, QStringList{});
}

/*!
 * \brief Function catches emited offline code generated by customer and saves it in ApplicationStatus settings.
 * Timer is started to drop this code if transaction proccess hasn't been started in 60 seconds.
 * \param array
 */
void CommunicationManager::offlineAuthorization(QByteArray array)
{
    //Received offline code here
    ApplicationStatus::instance()->setOfflineCode(array);
    m_offlineTimer->start();
}

/*!
 * \brief offline timer handler. Drops offline code if transactions wasn't started in 60 seconds from receiving it.
 */
void CommunicationManager::offlineHandlerTimeout()
{
    ApplicationStatus::instance()->setOfflineCode("");
}

/*!
 * \brief Emites signal with various system check values for them to be handled by UI
 * \param errorCheckEnum
 * \param value
 */
void CommunicationManager::checkSystemStatusHandler(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value)
{
    if((errorCheckEnum == GlobalSettings::Enums::SystemStatusCheck) && (value == 1)){
        makeRequestHandler(GlobalSettings::Enums::CancelCheckIn, QStringList{"INVALID_PAIRING"});
    }
    emit checkResult(errorCheckEnum, value);
}

/*!
 * \brief customer connected to keacon handler. Currently unused
 */
void CommunicationManager::customerConnected(const QString& customerId)
{
    // start order
    LOGD() << "Customer connected:" << customerId;
    if(!ApplicationStatus::instance()->requestCheckInStatus()){
        //makeRequestHandler(GlobalSettings::Enums::RequestCheckIn, QStringList{customerId});
    }
}

/*!
 * \brief customer disconnected handler
 */
void CommunicationManager::userDisconnected()
{
    LOGD() << "Customer disconnected.";
    ApplicationStatus::instance()->setRequestCheckInStatus(false);
}

/*!
 * \brief Handler for checkVersionResult signal
 *
 * If update is mandatory request for showing message pop up will be sent.
 * If standard update is avalible we first check if 48 has elapsed since last pop up.
 * \param checkResult
 */
void CommunicationManager::checkVersionHandler(GlobalSettings::Enums::VersionCheckResult checkResult)
{
    switch(checkResult)
    {
        case GlobalSettings::Enums::UpdateError:
        {
            break;
        }
        case GlobalSettings::Enums::ApplicationUpToDate:
        {
            break;
        }
        case GlobalSettings::Enums::StandardUpdateAvalible:
        {
            if(QDateTime::currentDateTime().toTime_t() > Settings::instance()->nextVersionCheckDate()){
                emit showUpdatePopUp(checkResult);
            }
            break;
        }
        case GlobalSettings::Enums::MandatoryUpdateAvalible:
        {
            emit showUpdatePopUp(checkResult);
            break;
        }
        default:
        {
            LOGD()<<"Unknown action occured while downloading version file";
        }
    }
}

void CommunicationManager::prepareConnections()
{
    connect (Settings::instance(), &Settings::settingsChanged, m_soapManager, &SoapManager::loadCertificate, Qt::QueuedConnection);
    connect (Settings::instance(), &Settings::settingsChanged, this, &CommunicationManager::startKeaconCommunication, Qt::QueuedConnection);
    connect(this, &CommunicationManager::makeSoapRequest, m_soapManager, &SoapManager::createRequest, Qt::QueuedConnection);
    connect(m_soapManager, &SoapManager::sendingOrderContainer, this, &CommunicationManager::sendingOrderContainer);
    connect(m_soapManager, &SoapManager::sendingMonitorContainer, this, &CommunicationManager::sendingMonitorContainer);
    connect(m_soapManager, &SoapManager::onKeaconInfoContainer, this, &CommunicationManager::setKeaconInformation);
    connect(m_soapManager, &SoapManager::sendingRequestCheckInContainer, this, &CommunicationManager::onRequestCheckInHandler, Qt::QueuedConnection);
    connect(m_soapManager, &SoapManager::sendingMonitorCheckInContainer, this, &CommunicationManager::onMonitorCheckInHandler, Qt::QueuedConnection);
    connect (m_usbManager, &UsbManager::clientConnected, this, &CommunicationManager::customerConnected, Qt::QueuedConnection);
    connect (m_wifiManager, &UsbManager::clientConnected, this, &CommunicationManager::customerConnected, Qt::QueuedConnection);
    connect (m_usbManager, &UsbManager::clientUnConnected, this, &CommunicationManager::userDisconnected, Qt::QueuedConnection);
    connect (m_wifiManager, &UsbManager::clientUnConnected, this, &CommunicationManager::userDisconnected, Qt::QueuedConnection);
    connect(m_usbManager, &UsbManager::showMessage, this, &CommunicationManager::showMessage);
    connect(m_wifiManager, &WifiManager::showMessage, this, &CommunicationManager::showMessage);
    connect(m_soapManager, &SoapManager::showMessage, this, &CommunicationManager::showMessage);
    connect(this, &CommunicationManager::discoverUsbPort, m_usbManager, &UsbManager::findDevicePort, Qt::QueuedConnection);
    connect(m_usbManager, &UsbManager::clientConnected, this, &CommunicationManager::raiseWindow, Qt::QueuedConnection);
    connect(m_wifiManager, &WifiManager::clientConnected, this, &CommunicationManager::raiseWindow, Qt::QueuedConnection);
    connect(m_usbManager, &UsbManager::offlineCodeReceived, this, &CommunicationManager::offlineAuthorization, Qt::QueuedConnection);

    connect(this, &CommunicationManager::performPasswordCheck, m_soapManager, &SoapManager::performCertificatePasswordCheck, Qt::QueuedConnection);
    connect(this, &CommunicationManager::performUsbCheck, m_usbManager, &UsbManager::performUsbPortCheck, Qt::QueuedConnection);
    connect(m_usbManager, &UsbManager::usbPortCheckResult, this, &CommunicationManager::checkResult, Qt::QueuedConnection);
    connect(m_soapManager, &SoapManager::cerfitificatePasswordCheckResult, this, &CommunicationManager::checkResult, Qt::QueuedConnection);
    connect(m_soapManager, &SoapManager::checkSystemStatusResult, this, &CommunicationManager::checkSystemStatusHandler, Qt::QueuedConnection);

    connect(m_soapManager, &SoapManager::abortOrder, this, &CommunicationManager::abortOrder, Qt::QueuedConnection);

    connect(this, &CommunicationManager::usbEventOccured, this, &CommunicationManager::startKeaconCommunication, Qt::QueuedConnection);

    connect(ApplicationStatus::instance(), &ApplicationStatus::hostConnectionChanged, this, &CommunicationManager::connectionChangedHandler, Qt::QueuedConnection);
    connect(m_reconnectTimer, &QTimer::timeout, this, &CommunicationManager::reconnectTimerTimeout, Qt::QueuedConnection);
    connect(m_versionChecker, &VersionChecker::versionCheckResult, this, &CommunicationManager::checkVersionHandler, Qt::QueuedConnection);
    connect(m_offlineTimer, &QTimer::timeout, this, &CommunicationManager::offlineHandlerTimeout, Qt::QueuedConnection);

    connect(m_usbManager, &UsbManager::reconnectUsbDevice, this, &CommunicationManager::startKeaconCommunication);
}
