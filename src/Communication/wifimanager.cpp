#include "wifimanager.h"

#include "globalsettings.h"
#include "settings.h"
#include "applicationstatus.h"

#include <QHostInfo>
#include <QHostAddress>
#include <QSslKey>
#include <QUdpSocket>
#include <QFile>
#include <QMetaEnum>
#include <QTimer>

#ifdef Q_OS_WIN32
#include <WinSock2.h>   // include before windows
#include <Windows.h>
//#include <tlhelp32.h>
#include <ws2tcpip.h>
#include "mstcpip.h"
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#endif

const int discoveryTimeout  = 5000;

WifiManager::WifiManager(QObject *parent) :
    AbstractDeviceManager(parent),
    m_udpSocket(new QUdpSocket(this)),
    m_sslSocket(Q_NULLPTR),
    m_timer(new QTimer(this))
{
    init();
    initConnections();
}

WifiManager::~WifiManager()
{
    closeDevice();
}

void WifiManager::init()
{
    qRegisterMetaType<QAbstractSocket::SocketError>( "QAbstractSocket::SocketError");
    qRegisterMetaType<QSslSocket::SslMode>( "QSslSocket::SslMode");
    qRegisterMetaType<QSslError>( "QSslError");

    m_udpPort = 0;
    m_tcpPort = 0;

    m_timer->setSingleShot(true);
}

void WifiManager::initConnections()
{
    connect(m_udpSocket, &QUdpSocket::readyRead, this, &WifiManager::udpReadyRead, Qt::QueuedConnection);
    connect(m_udpSocket, static_cast<void (QUdpSocket::*)(QUdpSocket::SocketError)>(&QUdpSocket::error),
            this, &WifiManager::udpError, Qt::QueuedConnection);
    connect(m_timer, &QTimer::timeout, this, &WifiManager::udpTimeOut, Qt::QueuedConnection);
    connect(this, &WifiManager::keaconDiscovered, this, &WifiManager::onKeaconDiscovered, Qt::QueuedConnection);
}

/*!
 * \brief find beacon
 * Start searching for keacon deamon on local network and connect using initial
 * string obtained from serwer
 */

void WifiManager::findBeacon(const QString& keaconInitString)
{
    LOGD() << "WiFi find beacon";

    m_initString = keaconInitString.toUtf8();

    bool ok = false;
    const quint16 udpPort = Settings::instance()->urlPort().toUShort( &ok);

    if (!ok) {
        QString errMsg = Settings::instance()->urlPort().isEmpty() ?
                                    tr("Error, wifi port is empty") :
                                    tr("Error, wifi port conversion failed");
        error( errMsg);
        return;
    }

    //ApplicationStatus::instance()->setCurrentEnrollStatus(true);

    m_udpPort = udpPort;

    if (m_udpSocket->isOpen()) {
        m_udpSocket->abort();
    }

    m_udpSocket->bind( m_udpPort, QUdpSocket::ShareAddress);
    m_timer->start( discoveryTimeout);

    const QByteArray datagram = "KTCPD";
    m_udpSocket->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, m_udpPort);

    ApplicationStatus::instance()->setBeaconConnectionStatus(GlobalSettings::Enums::Connected);
}

/*!
 * \brief udp error
 */

void WifiManager::udpError(QAbstractSocket::SocketError)
{
    if (m_udpSocket) {
        error( m_udpSocket->errorString());
    }
}

/*!
 * \brief udp time out
 * Keacon deamon discovery did time outed
 */

void WifiManager::udpTimeOut()
{
    error( tr("keacon search timeout."));

    if (m_udpSocket->isOpen()) {
        m_udpSocket->abort();
    }
}

/*!
 * \brief udp ready read
 * read incoming udp data
 */

void WifiManager::udpReadyRead()
{
    while (m_udpSocket->hasPendingDatagrams())
    {
        QHostAddress remote;
        QByteArray datagram;

        datagram.resize(m_udpSocket->pendingDatagramSize());
        m_udpSocket->readDatagram( datagram.data(), datagram.size(), &remote);

#ifdef QT_DEBUG
        LOGD() << "Satagram data:" << datagram;
#endif

        if (datagram.startsWith("KTCPD OK")) {

            m_timer->stop();

            bool ok = false;
            m_tcpPort = datagram.mid(9).toShort( &ok);

            if (!ok) {
                error( tr( "Error converting tcp port."));
                m_udpSocket->abort();
                return;
            }

            // retrieve version information

            QByteArray verba = "VERSION";
            m_udpSocket->writeDatagram( verba.data(), verba.size(), QHostAddress::Broadcast, m_udpPort);

            m_timer->start( discoveryTimeout);
        }
        else if (datagram.startsWith("VERSION OK")) {
            m_timer->stop();
            remote = QHostAddress(remote.toIPv4Address());

            LOGD() << "Beacon Discovered" << remote.toString();
            emit keaconDiscovered( remote.toString());
        }
        else if (datagram.startsWith("KTCPD ERR")) {
            m_timer->stop();

            datagram.chop( 1);
            datagram.remove( 0, 10);

            error (datagram);
        }
    }
}

/*!
 * \brief on keacon discovered
 * Keacon deamon discovered on ip, connect to deamon and start communication with
 * keacon device
 * \param ip
 */

void WifiManager::onKeaconDiscovered(const QString& ip)
{
    if (m_sslSocket && m_sslSocket->state() != QAbstractSocket::UnconnectedState) {
        m_sslSocket->abort();
    }

    m_udpSocket->abort();

    /**
        Compare discovered and provided host.
        TODO: make this more universal. Problem with Ip vs domain names that
        would require domain look up implementation, not just warn.
      */

    if (QHostAddress( ip).toString() !=
        QHostAddress( Settings::instance()->keackonUrl()).toString())
    {
        LOGD() << "Discovered host and provided by user do not match up, discovered:"
               << QHostAddress( ip).toString()
               << "provided:" << QHostAddress( Settings::instance()->keackonUrl()).toString();
    }

    if (!m_sslSocket) {
        m_sslSocket = new QSslSocket( this);

        connect (m_sslSocket, &QSslSocket::readyRead, this, &WifiManager::tcpReadyRead, Qt::QueuedConnection);
        connect (m_sslSocket, &QSslSocket::encrypted, this, &WifiManager::socketEncrypted, Qt::QueuedConnection);
        connect (m_sslSocket, &QSslSocket::stateChanged, this, &WifiManager::tcpStateChanged, Qt::QueuedConnection);
        connect (m_sslSocket, static_cast<void (QSslSocket::*)(QSslSocket::SocketError)>(&QSslSocket::error),
                 this, &WifiManager::tcpError, Qt::QueuedConnection);
        connect (m_sslSocket, SIGNAL(sslErrors(QList<QSslError>)), this,
                 SLOT(sslErrors(QList<QSslError>)), Qt::QueuedConnection);
        connect (m_sslSocket, &QSslSocket::peerVerifyError, this, &WifiManager::peerVerifyError, Qt::QueuedConnection);
    }

    if (!loadCertificate()) {
        return;
    }

    /**
        Verify SNI (Server Name Indication) on other side.
        Certificates provided are bound to "service-int.twint.ch" and obviously
        won't work correctly in local env.

        To bypass this define "USE_DEBUG_CERT" is used and this shouldn't be
        used when releasing application
      */

#ifdef USE_DEBUG_CERT
    m_sslSocket->setPeerVerifyMode( QSslSocket::VerifyNone);
    LOGD() << "Warning: peer verification is disabled, use only for development";
#else
    m_sslSocket->setPeerVerifyMode( QSslSocket::VerifyPeer);
#endif

    QHostAddress remoteAdr = QHostAddress( ip);

    LOGD() << "Connecting to:" << remoteAdr << ":" << m_tcpPort;

    m_sslSocket->connectToHostEncrypted( remoteAdr.toString(),
                                         m_tcpPort, "");
}

/*!
 * \brief load user selected certificate
 */

bool WifiManager::loadCertificate()
{
    QString certPathNme = Settings::instance()->cerfiticatePath();

    /**
        Using pem certificate file
      */

    QList<QSslCertificate> certs = QSslCertificate::fromPath( certPathNme);
    if (certs.isEmpty()) {
        LOGD() << "Error: no trusted cert found.";
    }
    m_sslSocket->addCaCertificates( certs);

    /**
        Dev test certificate code using provided certs and keys.
      */

    /**
        // dev. for testings
        certPathNme = "keacon-000002-aa123";
        certPathNme = "ca";

        QFile certFile( certPathNme + ".crt");
        if (!certFile.open(QFile::ReadOnly)) {
            error( tr( "Error: unable to open certificate file %1 error: %2")
                   .arg( certFile.fileName())
                   .arg( certFile.errorString()));
            return false;
        }
        QSslCertificate cert(&certFile, QSsl::Pem);
        certFile.close();

        QFile keyFile( certPathNme + ".key");
        if (!keyFile.open(QFile::ReadOnly)) {
            error( tr( "Error: unable to open certificate key file %1")
                   .arg( certFile.errorString()));
            return false;
        }

        // dev. for testings
        QByteArray pass = "02222";

        QSslKey key(&keyFile, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey, pass);
        keyFile.close();

        QList<QSslCertificate> trustCert = QSslCertificate::fromPath( certPathNme + ".crt");
        if (trustCert.isEmpty()) {
            LOGD() << "Error: no trusted cert found.";
        }
        m_sslSocket->setCaCertificates( trustCert);

        m_sslSocket->setLocalCertificate( cert);
        m_sslSocket->setPrivateKey( key);
    // */


    return true;
}

/*!
 * \brief tcp ready read
 * Read SSlSocket data and parse received messages
 */

void WifiManager::tcpReadyRead()
{
    QByteArray data = m_sslSocket->readAll();
    parseKeaconData( data);
}

/*!
 * \brief socket enters in encrypted state
 * All transmision past this point are encrypted, start actuall communication
 * with keacon deamon
 */

void WifiManager::socketEncrypted()
{
#ifdef QT_DEBUG
    LOGD() << "Socket entered encrypted state, start communication with wifi keacon";
#endif
    QByteArray data = "S," + m_initString + "\r";
    m_sslSocket->write( data);
}

/*!
 * \brief tcp socket state changed
 * In current implementation used to set keep alive on socket, socket must be
 * in connected state.
 * Used also for debugging purpose.
 */

void WifiManager::tcpStateChanged(QAbstractSocket::SocketState state)
{
#ifdef QT_DEBUG
    LOGD() << "SSl Socket state changed:" << state;
#endif

    if (state == QAbstractSocket::ConnectedState) {
        setKeepAliveForSocket( m_sslSocket);
    }
}

/*!
 * \brief tcp error
 * Transmission error for ssl socket
 */

void WifiManager::tcpError(QAbstractSocket::SocketError)
{
    if (m_sslSocket) {
        error( m_sslSocket->errorString());
    }
}

/*!
 * \brief ssl errors
 */

void WifiManager::sslErrors(const QList<QSslError> &errors)
{
#ifdef USE_DEBUG_CERT
    m_sslSocket->ignoreSslErrors( errors);
#else
    QString errStr = tr( "SSL Error:");
    for (auto err: errors) {
        errStr.append( "\n");
        errStr.append( err.errorString());
    }
    error( errStr);
#endif
}

/*!
 * \brief peer verify error
 * Error happening on invalid peer verification, see Server Name Indication
 */

void WifiManager::peerVerifyError(const QSslError &sslError)
{
#ifdef USE_DEBUG_CERT
    // do not display verify errors in dev. build
    Q_UNUSED( sslError)
#else
    error( sslError.errorString(), false);
#endif
}

/*!
 * \brief close device
 * Attempt to send close message to peer
 */

void WifiManager::closeDevice()
{
    if (   m_sslSocket && m_sslSocket->isOpen()
        && m_sslSocket->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray data = "T\r";
        m_sslSocket->write( data);

        m_sslSocket->waitForBytesWritten( discoveryTimeout);
        m_sslSocket->abort();
    }
}

/*!
 * \brief set keep alive flag for socket
 * Using native ppraoch due to issues with Qt
 */

void WifiManager::setKeepAliveForSocket(QAbstractSocket *socket)
{
    if (!socket) {
        LOGD() << "Error, keep alive socket ptr is null";
        return;
    }

    if (socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->setSocketOption( QAbstractSocket::KeepAliveOption, 1);

#ifdef Q_OS_WIN32
        struct tcp_keepalive   alive;
        DWORD         dwBytesRet = 0;
        alive.onoff = TRUE;
        alive.keepalivetime = 8000;
        alive.keepaliveinterval = 800;

        qintptr s = socket->socketDescriptor();

        if (s == -1)
            LOGD() << "KeepAlive: error invalid socket descriptor";
        else {
            if (WSAIoctl(s, SIO_KEEPALIVE_VALS, &alive, sizeof(alive),
                         NULL, 0, &dwBytesRet, NULL, NULL) == SOCKET_ERROR)
            {
                LOGD() << QString( "KeepAlive: set keep alive failed: %1")
                          .arg( WSAGetLastError());
            } else {
#ifdef QT_DEBUG
                LOGD() << "KeepAlive: set keep alive ok";
#endif
            }
        }
#elif defined(Q_OS_ANROID) || defined(Q_OS_LINUX)
        int enableKeepAlive = 1;
        int fd = socket->socketDescriptor();
        setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &enableKeepAlive, sizeof(enableKeepAlive));

        int maxIdle = 8; /* seconds */
        setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE, &maxIdle, sizeof(maxIdle));

        int count = 4;  // send up to 4 keepalive packets out, then disconnect if no response
        setsockopt(fd, SOL_TCP, TCP_KEEPCNT, &count, sizeof(count));

        int interval = 2;   // send a keepalive packet out every 2 seconds (after the 8 second idle period)
        setsockopt(fd, SOL_TCP, TCP_KEEPINTVL, &interval, sizeof(interval));

#elif defined(Q_OS_MAC)
        // apply to MacOSX and iOS

        int on = 1, secs = 10;
        int fd = socket->socketDescriptor();

        setsockopt(fd, IPPROTO_TCP,  SO_KEEPALIVE, &on, sizeof( on));

        setsockopt(fd, IPPROTO_TCP, TCP_KEEPALIVE, &secs, sizeof( secs));

// On 10.8.5 those socket options are not available,
// TODO: detect OsX version.

        int count = 4;  // send up to 4 keepalive packets out, then disconnect if no response
        setsockopt(fd, IPPROTO_TCP, TCP_KEEPCNT, &count, sizeof(count));

        int interval = 2;   // send a keepalive packet out every 2 seconds (after the 8 second idle period)
        setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(interval));

#endif
        LOGD() << "KeepAlive:" << socket->socketOption(QAbstractSocket::KeepAliveOption) << "\n"
               << "low delay" << socket->socketOption(QAbstractSocket::LowDelayOption) << "\n";
    } else {
        LOGD() << "KeepAlive: error setting keep alive on socket.";
    }
}
