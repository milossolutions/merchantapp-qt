#ifndef VERSIONCHECKER_H
#define VERSIONCHECKER_H

#include <QObject>

#include "globalsettings.h"

class HTTPDownload;
class QTimer;

class VersionChecker : public QObject
{
    Q_OBJECT
public:
    explicit VersionChecker(QObject* parent = 0);
    ~VersionChecker();

public slots:
    void start();

signals:
    void versionCheckResult(GlobalSettings::Enums::VersionCheckResult checkResult) const;

private slots:
    void initWidgets();
    void makeConnections();

    void errorHandler(QString errorDescription);
    void checkVersion();
    bool compareVersion(const QStringList newerVersion, const QStringList olderVersion);

private:
    HTTPDownload *m_httpDownload;
    QTimer *m_dailyTimer;
};

#endif // VERSIONCHECKER_H
