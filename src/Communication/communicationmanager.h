#ifndef COMMUNICATIONMANAGER_H
#define COMMUNICATIONMANAGER_H

#include <QObject>

#include "globalsettings.h"
#include "requestiteminterface.h"

class SoapManager;
class UsbManager;
class WifiManager;
class ResponseItemInterface;
class QTimer;
class ContainerItemInterface;
class KeconInformation;
class VersionChecker;

class CommunicationManager : public QObject
{
    Q_OBJECT
public:
    explicit CommunicationManager(QObject *parent = 0);
    ~CommunicationManager();

signals:
    void makeSoapRequest(const GlobalSettings::Enums::SoapMessageType, RequestItemInterface *requestItem) const;
    void sendingOrderContainer(ContainerItemInterface *itemContainer) const;
    void sendingMonitorContainer(ContainerItemInterface *containerItem) const;
    void receivedCouponsDataFromRequest(ContainerItemInterface *containerItem) const;
    void receivedCouponsDataFromMonitor(ContainerItemInterface *containerItem) const;
    void receivedRequestCheckInData(ContainerItemInterface *containerItem) const;

    void showMessage(GlobalSettings::Enums::PopUpType popUpType, QString message);
    void discoverUsbPort() const;
    void raiseWindow() const;

    void performPasswordCheck() const;
    void performUsbCheck() const;
    void checkResult(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value) const;
    void abortOrder() const;
    void usbEventOccured() const;
    void showUpdatePopUp(GlobalSettings::Enums::VersionCheckResult checkResult) const;

public slots:
    void init();

    void makeRequestHandler(GlobalSettings::Enums::SoapMessageType messageType, QStringList list);
    void onRequestCheckInHandler(ContainerItemInterface *containerItem);
    void onMonitorCheckInHandler(ContainerItemInterface *containerItem);
    void closeKeaconCommunication();
    void connectionChangedHandler(bool connectionStatus);
    void reconnectTimerTimeout();
    void offlineAuthorization(QByteArray array);
    void offlineHandlerTimeout();

private slots:
    void checkSystemStatusHandler(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value);

private:
    void prepareConnections();

    SoapManager *m_soapManager;
    UsbManager *m_usbManager;
    WifiManager *m_wifiManager;
    KeconInformation* m_keaconInformation;
    VersionChecker *m_versionChecker;

    int m_monitorMessagesCounter;
    int m_monitorPairingCounter;

    QTimer *m_reconnectTimer;
    QTimer *m_offlineTimer;

private slots:
    void startKeaconCommunication();
    void setKeaconInformation(ContainerItemInterface *keaconInfo);
    void customerConnected(const QString& customerId);
    void userDisconnected();
    void checkVersionHandler(GlobalSettings::Enums::VersionCheckResult checkResult);

};

#endif // COMMUNICATIONMANAGER_H
