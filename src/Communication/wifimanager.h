#ifndef WIFIMANAGER_H
#define WIFIMANAGER_H

#include "abstractdevicemanager.h"
#include <QSslSocket>

class QUdpSocket;
class QTimer;

/**
 * \brief The WifiManager class is responsible for communication with wifi keacon model
 */
class WifiManager : public AbstractDeviceManager
{
    Q_OBJECT
public:
    explicit WifiManager(QObject *parent = 0);
    ~WifiManager();

signals:
    void keaconDiscovered(const QString &ip) const;

public slots:
    void findBeacon(const QString &keaconInitString);
    void closeDevice();

private slots:
    void udpTimeOut();
    void udpReadyRead();
    void onKeaconDiscovered(const QString &ip);
    void tcpReadyRead();
    void socketEncrypted();
    void tcpStateChanged(QAbstractSocket::SocketState state);
    void udpError(QAbstractSocket::SocketError);
    void tcpError(QAbstractSocket::SocketError);
    void sslErrors(const QList<QSslError> &errors);
    void peerVerifyError(const QSslError &sslError);

private:
    void init();
    void initConnections();
    bool loadCertificate();
    void setKeepAliveForSocket(QAbstractSocket *socket);

    QUdpSocket *m_udpSocket;
    QSslSocket *m_sslSocket;

    QByteArray m_initString;

    quint16 m_udpPort;
    quint16 m_tcpPort;

    QTimer *m_timer;
};

#endif // WIFIMANAGER_H
