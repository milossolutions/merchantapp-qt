#include "uicontroller.h"

#include <QUrl>

#include "settings.h"

/**
 * @brief The UiController class
 * Controller responsible for handling functions called in qml.
 * Emites signals that are handled in ApplicationController.
 */
UiController::UiController(QObject *parent) :
    QObject(parent)
{

}

UiController::~UiController()
{

}

/**
 * @brief Removes unnecessary string from file path
 * @return filePath
 */
QString UiController::getFilePath(QString uncutFilePath) const
{
    return uncutFilePath.remove("file:///");
}

/**
 * @brief Calls StartOrderFunction receives order value and choosen coupons list.
 * @param Order value
 * @param Chosen coupons
 */
void UiController::startOrder(QString value, QList<CouponData> coupons) const
{
    LOGD() << "Using coupons: " << coupons.size();
    QStringList couponsInformation;
    for (int i = 0; i < coupons.size(); ++i) {
        couponsInformation.append(coupons.at(i).m_couponId);
        couponsInformation.append(QString::number(coupons.at(i).m_currentAmount));
        LOGD() << "Id: " << coupons.at(i).m_couponId << ", Amount: " << coupons.at(i).m_amount <<  ", Current Amount: " << coupons.at(i).m_currentAmount;
    }
    QStringList requestList{value, "PAYMENT_IMMEDIATE"};
    requestList.append(couponsInformation);
    emit makeRequest(GlobalSettings::Enums::StartOrder, requestList);
}

/*!
 * \brief Calls StartOrderFunction with inputed value
 * \param value Order value
 */
void UiController::startOrderWithoutCupons(QString value) const
{
    QStringList requestList{value, "PAYMENT_IMMEDIATE"};
    emit makeRequest(GlobalSettings::Enums::StartOrder, requestList);
}

/**
 * \brief Creates request for reversal order
 * \param value - transaction value
 * \param orderUUID - transaction UUID
 */
void UiController::reverseOrder(QString value, QString orderUUID) const
{
    emit makeRequest(GlobalSettings::Enums::StartOrder, QStringList{value, "REVERSAL", orderUUID});
}

/**
 * \brief Creates Enroll Cash Register request
 * \param value Type of register
 */
void UiController::enrollCashRegister(QString value) const
{
    emit makeRequest(GlobalSettings::Enums::EnrollCashRegister, QStringList{value});
}

/**
 * \brief Creates Cancel Check In request
 * \param reason Reason code of cancel
 */
void UiController::cancelCheckIn(QString reason) const
{
    emit makeRequest(GlobalSettings::Enums::CancelCheckIn, QStringList{reason});
}

/**
 * \brief Calls Confirm order function
 */
void UiController::confirmOrder() const
{
    emit makeRequest(GlobalSettings::Enums::ConfirmOrder, QStringList{});
}

/**
 * \brief Calls cancel order function
 * \param orderUuid Uuid of canceled order
 */
void UiController::cancelOrder(QString orderUuid) const
{
    emit makeRequest(GlobalSettings::Enums::CancelOrder, QStringList{orderUuid});
}

/*!
 * \brief Creates check system status message.
 * Used for checking status of twint server
 */
void UiController::checkSystemStatus() const
{
    emit makeRequest(GlobalSettings::Enums::CheckSystemStatus, QStringList{});
}

/*!
 * \brief Creates RequestCheckIn request
 */
void UiController::performRegisterCheckIn() const
{
    emit makeRequest(GlobalSettings::Enums::RequestCheckIn, QStringList{"renderQr"});
}

/*!
 * \brief Slot called when input field gains focus.
 * Emits signal to display on screen keyboard if proper setting is chosen
 */
void UiController::showSystemKeyboard()
{
    if(Settings::instance()->showKeyboard()){
        emit showSystemKeyboardClicked();
    }
}
