#include "applicationcontroller.h"

#include "datamanager.h"
#include "databasemanager.h"
#include "communicationmanager.h"
#include "uicontroller.h"

#include "containeriteminterface.h"

#include "currentorderitem.h"

#include "monitororderresponseitem.h"

#include "settings.h"
#include "applicationstatus.h"

#include <QSystemTrayIcon>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QWidget>
#include <QPushButton>
#include <QProcess>
#include <QSysInfo>
#include <QTimer>
#include <QDesktopServices>

#ifdef Q_OS_WIN
#include <windows.h>
#endif

/**
 * \class ApplicationController
 * \brief The ApplicationController class
 * is main backend class of the application.
 * Holds objects and connections between main components.
 */
ApplicationController::ApplicationController(QObject *parent) :
    QObject(parent)
{
    init();
    configureSystemTray();
    prepareConnections();
    startCommunicationManager();
}

ApplicationController::~ApplicationController()
{
    if (m_commLibThread.isRunning()) {
        m_commLibThread.quit();
        m_commLibThread.wait();
    }
}

void ApplicationController::init()
{
    m_databaseManager = new DatabaseManager;
    m_dataManager = new DataManager(m_databaseManager->connectionName(), this);
    m_communicationManager = new CommunicationManager;
    m_uiController = new UiController(this);
    m_systemTrayIcon = new QSystemTrayIcon(this);
    m_keyboardProcess = new QProcess();

#ifdef Q_OS_WIN
    QString program = "explorer.exe";
    QString folder = "C:\\Program Files\\Common Files\\microsoft shared\\ink\\TabTip.exe";
    m_keyboardProcess->setProgram(program);
    m_keyboardProcess->setArguments(QStringList() << folder);
#else

#endif

#ifdef Q_OS_WIN
    qApp->installNativeEventFilter(this);
#elif defined(Q_OS_MAC)
    //qApp->installNativeEventFilter(this);
    qApp->installEventFilter(this);
#endif
}

void ApplicationController::prepareConnections()
{
    //TODO THIS WAS CHANGED
    connect(&m_commLibThread, &QThread::started, m_communicationManager, &CommunicationManager::init, Qt::QueuedConnection);
    //connect(&m_commLibThread, &QThread::started, m_communicationManager, &CommunicationManager::initializeVersionChecker, Qt::QueuedConnection);
    connect(m_uiController, &UiController::makeRequest, m_communicationManager, &CommunicationManager::makeRequestHandler, Qt::QueuedConnection);
    connect(m_uiController, &UiController::discoverUsbPort, m_communicationManager, &CommunicationManager::discoverUsbPort, Qt::QueuedConnection);
    connect(m_uiController, &UiController::quitClicked, this, &ApplicationController::quitHandler, Qt::QueuedConnection);
    connect(m_uiController, &UiController::exitButtonClicked, this, &ApplicationController::showQuitMessageBox, Qt::QueuedConnection);
    connect(m_uiController, &UiController::appIsAlreadyRunning, this, &ApplicationController::appRunningHandler, Qt::QueuedConnection);
    connect(m_uiController, &UiController::logoutUser, m_communicationManager, &CommunicationManager::closeKeaconCommunication, Qt::QueuedConnection);
    connect(m_uiController, &UiController::updateSystemTray, this, [this](){updateSystemTray();});
    connect(m_uiController, &UiController::performPasswordCheck, m_communicationManager, &CommunicationManager::performPasswordCheck, Qt::QueuedConnection);
    connect(m_uiController, &UiController::performUsbCheck, m_communicationManager, &CommunicationManager::performUsbCheck, Qt::QueuedConnection);

    connect(m_databaseManager, &DatabaseManager::refreshTransactionsModel, m_dataManager, &DataManager::refreshTransactionsModel, Qt::QueuedConnection);

    connect(m_communicationManager, &CommunicationManager::sendingOrderContainer, m_databaseManager, &DatabaseManager::insertOrder, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::sendingOrderContainer, m_dataManager->currentOrderItem(), &CurrentOrderItem::setCurrentOrderItemData, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::sendingMonitorContainer, m_dataManager->currentOrderItem(), &CurrentOrderItem::monitorOrderSlot, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::receivedRequestCheckInData, m_dataManager->currentOrderItem(), &CurrentOrderItem::requestCheckInSlot, Qt::QueuedConnection);

    connect(m_dataManager->currentOrderItem(), &CurrentOrderItem::sendRequest, m_communicationManager, &CommunicationManager::makeRequestHandler, Qt::QueuedConnection);
    connect(m_dataManager->currentOrderItem(), &CurrentOrderItem::updateDatabaseOrderStatus, m_databaseManager, &DatabaseManager::updateOrderStatus, Qt::QueuedConnection);
    connect(m_dataManager->currentOrderItem(), &CurrentOrderItem::showMessage, m_uiController, &UiController::showMessage);
    connect(m_dataManager, &DataManager::makeRequest, m_communicationManager, &CommunicationManager::makeRequestHandler, Qt::QueuedConnection);
    connect(m_dataManager, &DataManager::showCouponsList, m_uiController, &UiController::showCouponsList, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::showMessage, m_uiController, &UiController::showMessage);
    connect(m_communicationManager, &CommunicationManager::raiseWindow, m_uiController, &UiController::raiseWindow, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::checkResult, m_uiController, &UiController::errorCheckCallback, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::receivedCouponsDataFromRequest, m_dataManager, &DataManager::receivedCouponsDataFromRequest, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::receivedCouponsDataFromMonitor, m_dataManager, &DataManager::receivedCouponsDataFromMonitor, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::abortOrder, m_dataManager->currentOrderItem(), &CurrentOrderItem::abortOrder, Qt::QueuedConnection);
    connect(m_communicationManager, &CommunicationManager::abortOrder, m_dataManager->currentOrderItem(), &CurrentOrderItem::abortOrder, Qt::QueuedConnection);

    connect(m_uiController, &UiController::showSystemKeyboardClicked, this, &ApplicationController::showSystemKeyboard, Qt::QueuedConnection);

    connect(this, &ApplicationController::usbEventOccured, m_communicationManager, &CommunicationManager::usbEventOccured, Qt::QueuedConnection);

    connect(m_communicationManager, &CommunicationManager::showUpdatePopUp, this, &ApplicationController::showUpdatePopUp, Qt::QueuedConnection);
}

/*!
 * \brief Initializes communication thread for CommunicationController and DatabaseManager
 */
void ApplicationController::startCommunicationManager()
{
    if(m_databaseManager){
        m_databaseManager->moveToThread(&m_commLibThread);
        connect(&m_commLibThread, &QThread::finished,
                m_databaseManager, &DatabaseManager::deleteLater);
    }

    if(m_communicationManager){
        m_communicationManager->moveToThread(&m_commLibThread);
        connect(&m_commLibThread, &QThread::finished,
                m_communicationManager, &CommunicationManager::deleteLater);
    }

    m_commLibThread.start();
}

/*!
 * \brief Initializes SystemTray and it's actions
 */
void ApplicationController::configureSystemTray()
{
    m_systemTrayIcon->contextMenu()->deleteLater();

    QAction *settingsAction = new QAction(QString(tr("Settings") + ""), this);
    connect(settingsAction, &QAction::triggered, m_uiController, &UiController::settingsClicked, Qt::QueuedConnection);
    QAction *exitAction = new QAction(QString(tr("Exit") + ""), this);
    connect(exitAction, &QAction::triggered, this, &ApplicationController::showQuitMessageBox, Qt::QueuedConnection);

    QMenu *trayIconMenu = new QMenu();
    trayIconMenu->addAction(settingsAction);
    trayIconMenu->addAction(exitAction);

    m_systemTrayIcon->setContextMenu(trayIconMenu);
    m_systemTrayIcon->setIcon(QIcon(":/images/HeaderLogoImage"));
    m_systemTrayIcon->show();
}

UiController *ApplicationController::uiController() const
{
    return m_uiController;
}

void ApplicationController::setUiController(UiController *uiController)
{
    m_uiController = uiController;
}

/*!
 * \brief Function required for catching natives usb event. Used in reconnection of Beacon device and input.
 */
bool ApplicationController::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(result)
#ifdef Q_OS_WIN
    MSG *msg = static_cast<MSG*>(message);

    if(eventType == "windows_generic_MSG" && msg->message == WM_DEVICECHANGE){
        LOGD()<<"Native usb occured";
        if(!ApplicationStatus::instance()->beaconConnectionStatus() && ApplicationStatus::instance()->singleReconnectRequest()){
            ApplicationStatus::instance()->setSingleReconnectRequest(false);
            QTimer::singleShot(500, [=]() {
                emit usbEventOccured();
            });
        }
    }
#endif

    return false;
}

/*!
 * \brief eventFilter used for catching click event on icon inside DockWidget on MacOS
 */
bool ApplicationController::eventFilter(QObject *object, QEvent *event)
{
    Q_UNUSED(object)
#ifdef Q_OS_MAC
    if(event->type() == QEvent::ApplicationActivate){
        m_uiController->showClicked();
    }
#endif

    return false;
}

void ApplicationController::setDataManager(DataManager *dataManager)
{
    if (m_dataManager == dataManager)
        return;

    m_dataManager = dataManager;
    emit dataManagerChanged(dataManager);
}

/*!
 * \brief Custom quit handler that takes different actions based on used system
 *
 * On Mac aplication is minimalized.
 * On Windows Quit pop up is displayed
 */
void ApplicationController::quitHandler()
{
#ifdef Q_OS_WIN
    showQuitMessageBox();
#else
    m_uiController->minimalizeClicked();
#endif
}

/*!
 * \brief Function displays pop up when second instance of application starts
 *
 * Function related with QtSingleApplication.
 * Handles signal received from in when user wants to start application second time.
 */
void ApplicationController::appRunningHandler()
{
    QPushButton cancelButton;
    cancelButton.setText(tr("Cancel"));

    QPushButton openButton;
    //openButton.setText(tr("Go to merchant app"));

    QMessageBox *messageBox = new QMessageBox();
    messageBox->setWindowFlags(Qt::WindowStaysOnTopHint);
    messageBox->setModal(true);
    messageBox->setIconPixmap(QPixmap::fromImage(QImage(":/images/HeaderLogoImage")));

#ifdef Q_OS_WIN
    messageBox->setWindowTitle(tr("TWINT Windows solution"));
    messageBox->setText(tr("The TWINT Windows solution is already open."));
    openButton.setText(tr("Go to TWINT Windows solution"));
#elif defined(Q_OS_MAC)
    messageBox->setWindowTitle(tr("TWINT OS X solution"));
    messageBox->setText(tr("The TWINT OS X solution is already open."));
    openButton.setText(tr("Go to TWINT OS X solution"));
#endif

    messageBox->addButton(&cancelButton, QMessageBox::AcceptRole);
    messageBox->addButton(&openButton, QMessageBox::RejectRole);

    int result = messageBox->exec();

    if (result == QMessageBox::RejectRole){
        m_uiController->raiseWindow();
    }

    messageBox->deleteLater();
}

/*!
 * \brief Function required for changing language of labels inside SystemTray
 */
void ApplicationController::updateSystemTray()
{
    configureSystemTray();
}

/*!
 * \brief Displays on screen keyboard based on used system
 */
void ApplicationController::showSystemKeyboard()
{
#ifdef Q_OS_WIN
    //HWND hwnd = FindWindow(L"IPTip_Main_Window", NULL);
    // if(hwnd == NULL){
    m_keyboardProcess->start(QProcess::ReadWrite);
    // }
#else
    m_keyboardProcess->start("/bin/bash",QStringList{"-c", "osascript -e 'quit app \"KeyboardViewer\"'"});
    m_keyboardProcess->waitForFinished(500);
    m_keyboardProcess->start("open -a KeyboardViewer");
#endif
}

/*!
 * \brief Function displays different pop-up message based of received value from versionCheckResult signal
 * \param checkResult GlobalSettings::Enums::VersionCheckResult
 *
 * If mandatory update is avalible application will be block by displaying DarkOverlayScreen on top of MainWindow
 * Pop-up with standard update avalible will be only displayed if 48 hours elapsed since last question asked
 */
void ApplicationController::showUpdatePopUp(GlobalSettings::Enums::VersionCheckResult checkResult)
{
    QMessageBox *messageBox = new QMessageBox();
    messageBox->setIconPixmap(QPixmap::fromImage(QImage(":/images/HeaderLogoImage")));
    messageBox->setWindowTitle(tr("New Version"));

    if(checkResult == GlobalSettings::Enums::StandardUpdateAvalible){
#ifdef Q_OS_WIN
    messageBox->setText(tr("A new version of TWINT Windows Solution is available"));
#elif defined(Q_OS_MAC)
    messageBox->setText(tr("A new version of TWINT OS X Solution is available"));
#endif

        QPushButton askButton;
        askButton.setText(tr("Ask later"));

        QPushButton downloadButton;
        downloadButton.setText(tr("Download update"));

        messageBox->addButton(&askButton, QMessageBox::RejectRole);
        messageBox->addButton(&downloadButton, QMessageBox::AcceptRole);

        int result = messageBox->exec();

        if(result == 0){
            QDateTime date = QDateTime::currentDateTime().addDays(2);
            Settings::instance()->setNextVersionCheckDate(date.toTime_t());
            Settings::instance()->updateNextVersionCheck();
        } else {
            QDesktopServices::openUrl(QUrl(GlobalSettings::Paths::instance()->TwintApplicationUrl()));
        }

    }else if(checkResult == GlobalSettings::Enums::MandatoryUpdateAvalible){
        ApplicationStatus::instance()->setMandatoryUpdateRequired(true);

#ifdef Q_OS_WIN
    messageBox->setText(tr("A new version of TWINT Windows Solution is available. You need to install the new version before you can use TWINT again."));
#elif defined(Q_OS_MAC)
    messageBox->setText(tr("A new version of TWINT OS X Solution is available. You need to install the new version before you can use TWINT again."));
#endif

        QPushButton closeButton;
        closeButton.setText(tr("Close app"));

        QPushButton downloadButton;
        downloadButton.setText(tr("Download update"));

        messageBox->addButton(&closeButton, QMessageBox::RejectRole);
        messageBox->addButton(&downloadButton, QMessageBox::AcceptRole);

        int result = messageBox->exec();

        if(result == 0){
            qApp->quit();
        } else {
            QDesktopServices::openUrl(QUrl(GlobalSettings::Paths::instance()->TwintApplicationUrl()));
        }

    }

    messageBox->deleteLater();
}

/*!
 * \brief Pop up displayed on quit action to check if user really wants to quit application
 */
void ApplicationController::showQuitMessageBox()
{
    QPushButton okButton;
    okButton.setText(tr("Ok"));

    QPushButton cancelButton;
    cancelButton.setText(tr("Cancel"));

    QMessageBox *messageBox = new QMessageBox();
    messageBox->setIconPixmap(QPixmap::fromImage(QImage(":/images/HeaderLogoImage")));
    messageBox->setWindowTitle(tr("Application exit"));
    messageBox->setText(tr("Are you sure to log out?"));
    messageBox->addButton(&okButton, QMessageBox::AcceptRole);
    messageBox->addButton(&cancelButton, QMessageBox::RejectRole);

    int result = messageBox->exec();

    if (result == 0){
        qApp->quit();
    }

    messageBox->deleteLater();
}

DataManager *ApplicationController::dataManager() const
{
    return m_dataManager;
}

CommunicationManager *ApplicationController::communicationManager() const
{
    return m_communicationManager;
}

void ApplicationController::setCommunicationManager(CommunicationManager *communicationManager)
{
    m_communicationManager = communicationManager;
}

DatabaseManager *ApplicationController::databaseManager() const
{
    return m_databaseManager;
}

void ApplicationController::setDatabaseManager(DatabaseManager *databaseManager)
{
    m_databaseManager = databaseManager;
}
