#ifndef UICONTROLLER_H
#define UICONTROLLER_H

#include <QObject>
#include <QUrl>

#include "globalsettings.h"
#include "coupondata.h"

class UiController : public QObject
{
    Q_OBJECT
public:
    explicit UiController(QObject *parent = 0);
    ~UiController();

    Q_INVOKABLE QString getFilePath(QString uncutFilePath) const;

signals:
    void makeRequest(GlobalSettings::Enums::SoapMessageType messageType, QStringList list) const;

    void showMessage(GlobalSettings::Enums::PopUpType popUpType, QString message) const;

    Q_INVOKABLE void discoverUsbPort() const;

    void raiseWindow() const;
    void minimalizeClicked() const;
    void showClicked() const;
    void settingsClicked() const;
    void showSystemKeyboardClicked() const;
    Q_INVOKABLE void appIsAlreadyRunning() const;
    Q_INVOKABLE void quitClicked() const;
    Q_INVOKABLE void exitButtonClicked() const;
    Q_INVOKABLE void logoutUser() const;
    Q_INVOKABLE void updateSystemTray() const;
    Q_INVOKABLE void performPasswordCheck() const;
    Q_INVOKABLE void performUsbCheck() const;
    Q_INVOKABLE void errorCheckCallback(GlobalSettings::Enums::ErrorCheck errorCheckEnum, int value) const;

    void showCouponsList() const;

public slots:
    Q_INVOKABLE void startOrder(QString value, QList<CouponData> coupons) const;
    Q_INVOKABLE void startOrderWithoutCupons(QString value) const;
    Q_INVOKABLE void reverseOrder(QString value, QString orderUUID) const;
    Q_INVOKABLE void enrollCashRegister(QString value) const;
    Q_INVOKABLE void cancelCheckIn(QString reason) const;
    Q_INVOKABLE void confirmOrder() const;
    Q_INVOKABLE void cancelOrder(QString orderUuid) const;
    Q_INVOKABLE void checkSystemStatus() const;
    Q_INVOKABLE void performRegisterCheckIn() const;

    Q_INVOKABLE void showSystemKeyboard();

private:

};

#endif // UICONTROLLER_H
