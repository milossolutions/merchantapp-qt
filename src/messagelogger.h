#ifndef MESSAGELOGGER_H
#define MESSAGELOGGER_H

#include <QObject>
#include "globalsettings.h"

class MessageLogger : public QObject
{
public:
    static MessageLogger* instance();
    ~MessageLogger();

    void writeMessageToFile(GlobalSettings::Enums::LogMessageType messageType, QByteArray dataArray);

private:
    static MessageLogger *m_instance;
    MessageLogger(QObject *parent = 0);

    void init();

    QString m_path;
};

#endif // MESSAGELOGGER_H
