#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>

class QSqlQuery;
class ContainerItemInterface;
class StartOrderContainer;

class DatabaseManager : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseManager(QObject *parent = 0);
    ~DatabaseManager();

    bool open() const;
    bool close() const;

    QString connectionName() const;
    void setConnectionName(const QString &connectionName);
    QString driver() const;
    void setDriver(const QString &driver);
    QString name() const;
    void setName(const QString &name);
    QString databasePath() const;
    void setDatabasePath(const QString &databasePath);

signals:
    /**
     * \brief refreshTransactionsModel
     * Signal used to call refreshModel function in NewOrderListModel
     */
    void refreshTransactionsModel() const;

public slots:
    void initDatabase();
    bool insertOrder(ContainerItemInterface *orderRequestItem) const;
    bool insertBillQuery(StartOrderContainer *orderRequestItem, QSqlQuery &query) const;
    int insertBillAmountQuery(StartOrderContainer *orderRequestItem, QSqlQuery &query) const;
    bool updateOrderStatus(QString order_id, int reasonCode) const;
    int getCurrencyId(StartOrderContainer *orderRequestItem) const;
    int getCashierId(StartOrderContainer *orderRequestItem) const;

private:
    void init();
    bool createDatabase();
    bool prepareToQuery() const;
    void performUpdateOfOldDatabase();
    QString getCurrentTimestamp() const;

    QString m_connectionName;
    QString m_driver;
    QString m_name;
    QString m_databasePath;

};

#endif // DATABASEMANAGER_H
