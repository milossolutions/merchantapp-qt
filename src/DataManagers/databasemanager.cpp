#include "databasemanager.h"

#include "globalsettings.h"
#include "settings.h"

#include "startorderrequestitem.h"
#include "orderrequesttype.h"
#include "currencyamounttype.h"
#include "merchantinformationtype.h"

#include "containeriteminterface.h"
#include "startordercontainer.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include <QSqlError>
#include <QDir>
#include <QFile>

/**
 * \class DatabaseManager
 * \brief Local sql database to store data
 */
DatabaseManager::DatabaseManager(QObject *parent) :
    QObject(parent)
{
    init();
    initDatabase();
    createDatabase();
}

DatabaseManager::~DatabaseManager()
{
    if (!m_connectionName.isEmpty()) {
        QSqlDatabase db = QSqlDatabase::database(m_connectionName);
        if (db.isValid() && db.isOpen()) {
            db.close();
        } else {
            LOGD() << "Warning: unable to close database.";
        }
    }
}

bool DatabaseManager::open() const
{
    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    if (!db.isValid())
        return false;
    return db.open();
}

bool DatabaseManager::close() const
{
    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    if (!db.isValid())
        return false;
    db.close();
    return !db.isOpen();
}

void DatabaseManager::initDatabase()
{
    QSqlDatabase db = QSqlDatabase::addDatabase(m_driver, m_connectionName);
    if (!m_name.isEmpty() && !m_databasePath.isEmpty()){
        db.setDatabaseName(QString(m_databasePath+m_name));
    }
}

/**
 * \brief Function used to insert transaction data into database
 * \param orderRequestItem
 * @return
 */
bool DatabaseManager::insertOrder(ContainerItemInterface *orderRequestItem) const
{
    if (!prepareToQuery()) {
        return false;
    }

    StartOrderContainer *orderContainer = qobject_cast<StartOrderContainer*>(orderRequestItem);

    if(!orderContainer){
        return false;
    }

    if (orderContainer->orderType() == GlobalSettings::Enums::OrderType::REVERSAL) {
        return updateOrderStatus(orderContainer->linkOrder(), GlobalSettings::Enums::OrderStatus::ORDER_REVERSAL);
    }

    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    QSqlQuery query(db);

    bool result = insertBillQuery(orderContainer, query);

    if(result){
        LOGD()<<"Record inserted successfully";
        emit refreshTransactionsModel();
    }else{
        LOGD()<<"Error occured"<<query.lastError();
    }
    return result;
    return false;
}

/**
 * \brief Function returns id of currency used in order.
 * Inserts new record if it didn't already exist
 * \param orderRequestItem
 * \return
 */
//Get currency id if it doesn't exist create record
int DatabaseManager::getCurrencyId(StartOrderContainer *orderRequestItem) const
{
    if (!prepareToQuery()) {
        return false;
    }

    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    QSqlQuery query(db);

    query.prepare("SELECT id FROM currency where name=:name;");
    query.bindValue(":name", orderRequestItem->currency());
    query.exec();

    if(query.next()){
        return query.value("id").toInt();
    }else{
        query.clear();

        query.prepare("INSERT INTO currency (name) VALUES(:name)");
        query.bindValue(":name", orderRequestItem->currency());

        if(query.exec()){
            return query.lastInsertId().toInt();
        }else{
            return 0;
        }
    }
}

/**
 * \brief Retuns id of cashier depending on merchant uuid
 * Insert new record if it didn't already exist.
 * \param orderRequestItem
 * \return
 */
//Get cashier id if it doesn't exist create record
int DatabaseManager::getCashierId(StartOrderContainer *orderRequestItem) const
{
    if (!prepareToQuery()) {
        return false;
    }

    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    QSqlQuery query(db);

    LOGD()<<"Cashier uuid"<<orderRequestItem->cashierUuid();

    query.prepare("SELECT id FROM cashier where uuid=:uuid;");
    query.bindValue(":uuid", orderRequestItem->cashierUuid());
    query.exec();

    if(query.next()){
        return query.value("id").toInt();
    }else{
        query.prepare("INSERT INTO cashier (name, uuid, alias_id, cash_register_id, service_agent_id) "
                      "VALUES(:name, :uuid, :alias_id, :cash_register_id, :service_agent_id)");

        //ToDo change cashier name
        query.bindValue(":name", orderRequestItem->cashierAlias());
        query.bindValue(":uuid", orderRequestItem->cashierUuid());
        query.bindValue(":alias_id", orderRequestItem->cashierAlias());
        query.bindValue(":cash_register_id", orderRequestItem->cashRegisterId());
        query.bindValue(":service_agent_id", orderRequestItem->serviceAgentId());

        if(query.exec()){
            return query.lastInsertId().toInt();
        }else{
            return 0;
        }
    }
}

/**
 * \brief Statement to insert data into bill table based on values from StartOrderContainer
 * \param orderRequestItem
 * \param query
 * \return
 */
//Insert new bill record with data from request
bool DatabaseManager::insertBillQuery(StartOrderContainer *orderRequestItem, QSqlQuery &query) const
{
    int billAmountId = insertBillAmountQuery(orderRequestItem, query);
    int cashierId = getCashierId(orderRequestItem);

    if((billAmountId == 0) || (cashierId == 0)){
        LOGD()<<"Bill amount id invalid"<<billAmountId<<cashierId;
        return false;
    }

    query.prepare("INSERT INTO bill (date, pk_id, trx_id, "
                  "order_id, bill_status, cashier_id, bill_amount_id, twint_server_id) "
                  "VALUES(:date, :pk_id, :trx_id, "
                  ":order_id, :bill_status, :cashier_id, :bill_amount_id, :twint_server_id)");
    query.bindValue(":date", getCurrentTimestamp());
    query.bindValue(":pk_id","");
    query.bindValue(":trx_id","");
    query.bindValue(":order_id", orderRequestItem->orderId());
    query.bindValue(":bill_status", GlobalSettings::Enums::ORDER_RECEIVED);
    query.bindValue(":cashier_id", cashierId);
    query.bindValue(":bill_amount_id", billAmountId);
    query.bindValue(":twint_server_id", GlobalSettings::Paths::instance()->twintServerId());

    return query.exec();
}

/**
 * \brief Inserts data into bill_amount table and returns id of that record
 * \param orderRequestItem
 * \param query
 * \return
 */
//Insert new bill amount record that will be assigned to order
int DatabaseManager::insertBillAmountQuery(StartOrderContainer *orderRequestItem, QSqlQuery &query) const
{
    int currencyId = getCurrencyId(orderRequestItem);

    if(currencyId == 0){
        LOGD()<<"Currency id invalid";
        return 0;
    }

    query.prepare("INSERT INTO bill_amount (value, discount, currency_id) "
                  "VALUES (:value, :discount, :currency_id)");
    query.bindValue(":value", orderRequestItem->value());
    query.bindValue(":discount", orderRequestItem->discount());
    query.bindValue(":currency_id", currencyId);
    query.exec();

    return query.lastInsertId().toInt();
}

/**
 * \brief Statement which updates status of existing order
 * \param order_id
 * \param reasonCode
 * \return
 */
bool DatabaseManager::updateOrderStatus(QString order_id, int reasonCode) const
{
    if (!prepareToQuery()) {
        return false;
    }

    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    QSqlQuery query(db);

    query.prepare("UPDATE bill SET bill_status=:bill_status "
                  "WHERE order_id=:order_id");

    query.bindValue(":bill_status", reasonCode);
    query.bindValue(":order_id", order_id);

    if(query.exec()){
        LOGD()<<"Order status updated";
        emit refreshTransactionsModel();
        return true;
    }else{
        LOGD()<<"Error occured"<<query.lastError();
        return false;
    }
}

void DatabaseManager::init()
{
    setDriver("QSQLITE");
    setConnectionName("default");
    setDatabasePath(GlobalSettings::Paths::instance()->DatabasePath);
    setName("local.db");
}

/*!
 * \brief create database
 * Creates database function
 */
bool DatabaseManager::createDatabase()
{
    // create db file, if not exists
    if (!QFileInfo(GlobalSettings::Paths::instance()->DatabasePath) .exists()) {
        if (!QDir().mkpath( GlobalSettings::Paths::instance()->DatabasePath)) {
            LOGD() << "Error, unable to create path"
                   << GlobalSettings::Paths::instance()->DatabasePath;
            LOGD()<<"Tworzy baze danych";
            return false;
        }
    }

    const QString dbFilePath = GlobalSettings::Paths::instance()->DatabasePath + "local.db";
    if (!QFileInfo(dbFilePath).exists()) {
        QFile dbeFile( dbFilePath);
        if (!dbeFile.open( QIODevice::WriteOnly)) {
            LOGD() << "Error, unable to create db file" << dbeFile.errorString();
            return false;
        }
        dbeFile.close();
    }

    if (!prepareToQuery()) {
        LOGD() << "Db error, unable to prepare query";
        return false;
    }

    QSqlDatabase db = QSqlDatabase::database(m_connectionName);

    // check db tables and verify if all tables are present
    QStringList tables = db.tables();

    //check column structure and delete database if it has old structure
    if (tables.contains( "bill")){
        QSqlQuery checkColumns(db);
        checkColumns.prepare("SELECT twint_server_id from bill");
        if (!checkColumns.exec()) {
            LOGD() << "Error executing query" << checkColumns.lastError();
            db.close();
            bool removeSuccess = QFile::remove(dbFilePath);
            LOGD()<<"Remove success"<<removeSuccess<<dbFilePath;
            if(removeSuccess){
                return createDatabase();
            }
        }
    }

    // double check tables, this is done to minimize query to db
    QStringList queriesList;

    if (!tables.contains( "currency")) {
        queriesList.append("CREATE TABLE IF NOT EXISTS currency (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR (45));");
    }

    if (!tables.contains( "cashier")) {
        queriesList.append("CREATE TABLE IF NOT EXISTS cashier (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR (45), "
                           "uuid VARCHAR (45), alias_id VARCHAR (45), cash_register_id VARCHAR (45), "
                           "service_agent_id VARCHAR (45));");
    }

    if (!tables.contains( "bill_amount")) {
        queriesList.append("CREATE TABLE IF NOT EXISTS bill_amount (id INTEGER PRIMARY KEY NOT NULL, "
                           "value VARCHAR (45), discount VARCHAR (45), "
                           "currency_id INTEGER REFERENCES currency (id) NOT NULL);");
    }

    if (!tables.contains( "twint_server")) {
        queriesList.append("CREATE TABLE IF NOT EXISTS twint_server (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR (45));");
    }

    if (!tables.contains( "bill")) {
        queriesList.append("CREATE TABLE IF NOT EXISTS bill (id INTEGER NOT NULL PRIMARY KEY, "
                           "date INTEGER, pk_id VARCHAR (45), trx_id VARCHAR (45), "
                           "order_id VARCHAR (45), bill_status INTEGER, "
                           "cashier_id INTEGER REFERENCES cashier (id) NOT NULL, "
                           "bill_amount_id INTEGER REFERENCES bill_amount (id) NOT NULL, "
                           "twint_server_id INTEGER REFERENCES twint_server (id) NOT NULL);");
    }

    if (!tables.contains( "twint_server")) {
        queriesList.append("INSERT INTO twint_server (id, name) VALUES (1, 'TwintLiveServer');");
        queriesList.append("INSERT INTO twint_server (id, name) VALUES (2, 'TwintIntServer');");
        queriesList.append("INSERT INTO twint_server (id, name) VALUES (3, 'TwintPatServer');");
        queriesList.append("INSERT INTO twint_server (id, name) VALUES (4, 'TwintRefServer');");
        queriesList.append("INSERT INTO twint_server (id, name) VALUES (5, 'TwintMajorServer');");
    }

    QSqlQuery query(db);

    for (auto queryStr : queriesList) {
        query.prepare( queryStr);
        if (!query.exec()) {
            LOGD() << "Error executing query" << query.lastError();
            return false;
        }
    }

    performUpdateOfOldDatabase();

    return true;
}

/**
 * \brief Checks if database connection is valid
 * \return
 */
bool DatabaseManager::prepareToQuery() const
{
    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    if (!db.isValid()){
        LOGD()<<"Db invalid";
        return false;
    }
    if (!db.isOpen()){
        LOGD()<<"Db not open";
        return  db.open();
    } else {
        return true;
    }
}

/*!
 * \brief Function perform update of server records on old database. Can be deleted after version 1.5.0.1 becomes mandatory
 */
void DatabaseManager::performUpdateOfOldDatabase()
{
    QSqlDatabase db = QSqlDatabase::database(m_connectionName);
    QSqlQuery query(db);
    query.prepare("SELECT COUNT(*) FROM twint_server;");
    query.exec();

    if (!query.next())
        return;

    //If number of servers is lower than 5 that means user is using old db version
    if(query.value(0).toInt() < 5){
        query.prepare("INSERT INTO twint_server (id, name) VALUES (5, 'TwintMajorServer');");
        query.exec();
    }
}

/**
 * \brief Returns current date time timestamp
 * \return
 */
QString DatabaseManager::getCurrentTimestamp() const
{
    return QString::number(QDateTime::currentDateTime().toTime_t());
}

QString DatabaseManager::databasePath() const
{
    return m_databasePath;
}

void DatabaseManager::setDatabasePath(const QString &databasePath)
{
    m_databasePath = databasePath;
}

QString DatabaseManager::name() const
{
    return m_name;
}

void DatabaseManager::setName(const QString &name)
{
    m_name = name;
}

QString DatabaseManager::driver() const
{
    return m_driver;
}

void DatabaseManager::setDriver(const QString &driver)
{
    m_driver = driver;
}

QString DatabaseManager::connectionName() const
{
    return m_connectionName;
}

void DatabaseManager::setConnectionName(const QString &connectionName)
{
    m_connectionName = connectionName;
}
