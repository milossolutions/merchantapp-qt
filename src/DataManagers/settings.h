#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QMutex>
#include <QVariantMap>
#include <QReadWriteLock>
#include <QTranslator>
#include <QDateTime>

#include "globalsettings.h"

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY languageChanged)
    Q_PROPERTY(QString emptyString READ emptyString NOTIFY languageChanged)
    Q_PROPERTY(QString applicationName READ applicationName WRITE setApplicationName NOTIFY applicationNameChanged)
    Q_PROPERTY(QString applicationVersion READ applicationVersion WRITE setApplicationVersion NOTIFY applicationVersionChanged)
    Q_PROPERTY(QString merchantAlias READ merchantAlias WRITE setMerchantAlias NOTIFY merchantAliasChanged)
    Q_PROPERTY(QString merchantUuid READ merchantUuid WRITE setMerchantUuid NOTIFY merchantUuidChanged)
    Q_PROPERTY(QString cashRegisterId READ cashRegisterId WRITE setCashRegisterId NOTIFY cashRegisterIdChanged)
    Q_PROPERTY(QString cerfiticatePath READ cerfiticatePath WRITE setCerfiticatePath NOTIFY cerfiticatePathChanged)
    Q_PROPERTY(QString certificatePassword READ certificatePassword WRITE setCertificatePassword NOTIFY certificatePasswordChanged)
    Q_PROPERTY(QString deviceType READ deviceType WRITE setDeviceType NOTIFY deviceTypeChanged)
    Q_PROPERTY(QString usbPort READ usbPort WRITE setUsbPort NOTIFY usbPortChanged)
    Q_PROPERTY(QString keackonUrl READ keackonUrl WRITE setKeackonUrl NOTIFY keackonUrlChanged)
    Q_PROPERTY(QString urlPort READ urlPort WRITE setUrlPort NOTIFY urlPortChanged)
    Q_PROPERTY(bool runOnStartup READ runOnStartup WRITE setRunOnStartup NOTIFY runOnStartupChanged)
    Q_PROPERTY(bool isDataFilled READ isDataFilled WRITE setIsDataFilled NOTIFY isDataFilledChanged)
    Q_PROPERTY(bool isSelectUser READ isSelectUser WRITE setIsSelectUser NOTIFY isSelectUserChanged)
    Q_PROPERTY(QString cashRegisterIdType READ cashRegisterIdType WRITE setCashRegisterIdType NOTIFY cashRegisterIdTypeChanged)
    Q_PROPERTY(bool showKeyboard READ showKeyboard WRITE setShowKeyboard NOTIFY showKeyboardChanged)
    Q_PROPERTY(QByteArray certificateData READ certificateData WRITE setCertificateData NOTIFY certificateDataChanged)
    Q_PROPERTY(QByteArray certificateKeyData READ certificateKeyData WRITE setCertificateKeyData NOTIFY certificateKeyDataChanged)
    Q_PROPERTY(int nextVersionCheckDate READ nextVersionCheckDate WRITE setNextVersionCheckDate NOTIFY nextVersionCheckDateChanged)

public:
    ~Settings();

    static Settings* instance();

    Q_INVOKABLE void loadSettings();
    Q_INVOKABLE void storeSettings();
    void storeCertificateData();
    void updateNextVersionCheck();

    QString language();
    QString emptyString() const;

    QString applicationName();
    QString applicationVersion(); 
    QString merchantAlias();
    QString merchantUuid();
    QString cashRegisterId();
    QString cerfiticatePath();
    QString certificatePassword();
    QString deviceType();
    QString usbPort();
    QString keackonUrl();
    QString urlPort();
    bool runOnStartup();
    bool isSelectUser();
    QString cashRegisterIdType();
    bool showKeyboard();
    QByteArray certificateData();
    QByteArray certificateKeyData();
    int nextVersionCheckDate();

    Q_INVOKABLE void createAutorunRegistry(bool runOnStartup);

    bool isDataFilled();

signals:
    void languageChanged(QString language) const;
    void applicationNameChanged(QString applicationName) const;
    void applicationVersionChanged(QString applicationVersion) const;
    void merchantAliasChanged(QString merchantAlias) const;
    void merchantUuidChanged(QString merchantUuid) const;
    void cashRegisterIdChanged(QString cashRegisterId) const;
    void cerfiticatePathChanged(QString cerfiticatePath) const;
    void certificatePasswordChanged(QString certificatePassword) const;
    void deviceTypeChanged(QString deviceType) const;
    void usbPortChanged(QString usbPort) const;
    void keackonUrlChanged(QString keackonUrl) const;
    void urlPortChanged(QString urlPort) const;
    void runOnStartupChanged(bool runOnStartup) const;
    void isSelectUserChanged(bool isSelectUser) const;
    void cashRegisterIdTypeChanged(QString cashRegisterIdType) const;
    void showKeyboardChanged(bool showKeyboard) const;
    void certificateDataChanged(QByteArray certificateData);
    void certificateKeyDataChanged(QByteArray certificateKeyData);
    void nextVersionCheckDateChanged(int nextVersionCheckDate);

    void settingsChanged() const;

    void isDataFilledChanged(bool isDataFilled);

public slots:
    void setDefaults();
    void setLanguage(QString language);
    void setApplicationName(QString applicationName);
    void setApplicationVersion(QString applicationVersion);

    Q_INVOKABLE void logIn();
    Q_INVOKABLE int timestampForLastLogin();

    void setMerchantAlias(QString merchantAlias);
    void setMerchantUuid(QString merchantUuid);
    void setCashRegisterId(QString cashRegisterId);
    void setCerfiticatePath(QString cerfiticatePath);
    void setCertificatePassword(QString certificatePassword);
    void setDeviceType(QString deviceType);
    void setUsbPort(QString usbPort);
    void setKeackonUrl(QString keackonUrl);
    void setUrlPort(QString urlPort);
    void setRunOnStartup(bool runOnStartup);
    void setShowKeyboard(bool showKeyboard);
    void setIsSelectUser(bool isSelectUser);
    void setCashRegisterIdType(QString cashRegisterIdType);
    void setIsDataFilled(bool isDataFilled);
    void setNextVersionCheckDate(int nextVersionCheckDate);
    void setCertificateData(QByteArray certificateData);
    void setCertificateKeyData(QByteArray certificateKeyData);

private:
    static Settings *m_instance;
    Settings(QObject *parent = 0);

    void init();
    void checkIfDataIsFilled();

    bool installTranslator(const QString &lang);
    QTranslator *m_translator = nullptr;
    QString m_language;
    QVariantMap m_settingsVariantMap;
    QReadWriteLock m_readWriteLocker;
    QString m_applicationName;
    QString m_applicationVersion;
    QDateTime m_lastLoginDateTime;
    QDateTime m_lastEnrollCashRegisterDateTime;
    QString m_merchantAlias;
    QString m_merchantUuid;
    QString m_cashRegisterId;
    QString m_cerfiticatePath;
    QString m_certificatePassword;
    QString m_deviceType;
    QString m_usbPort;
    QString m_keackonUrl;
    QString m_urlPort;
    bool m_runOnStartup;
    bool m_isDataFilled;
    bool m_isSelectUser;
    QByteArray m_certificateData;
    int m_nextVersionCheckDate;

    QString m_cashRegisterIdType;
    bool m_showKeyboard;
    QByteArray m_certificateKeyData;
};

#endif // SETTINGS_H
