#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>

#include "globalsettings.h"

class CurrentOrderItem;
class ContainerItemInterface;
class CouponListModel;
class NewOrderListModel;

class DataManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(CurrentOrderItem* currentOrderItem READ currentOrderItem WRITE setCurrentOrderItem NOTIFY currentOrderItemChanged)
    Q_PROPERTY(CouponListModel* couponListModel READ couponListModel WRITE setCouponListModel NOTIFY couponListModelChanged)
    Q_PROPERTY(NewOrderListModel* newOrderListModel READ newOrderListModel WRITE setNewOrderListModel NOTIFY newOrderListModelChanged)
public:
    explicit DataManager(QString connectionName, QObject *parent = 0);
    ~DataManager();

    CurrentOrderItem* currentOrderItem() const;
    CouponListModel* couponListModel() const;
    NewOrderListModel* newOrderListModel() const;

signals:
    void refreshTransactionsModel() const;
    void currentOrderItemChanged(CurrentOrderItem* currentOrderItem) const;
    void couponListModelChanged(CouponListModel* couponListModel) const;
    void newOrderListModelChanged(NewOrderListModel* newOrderListModel) const;
    void showCouponsList() const;
    void makeRequest(GlobalSettings::Enums::SoapMessageType messageType, QStringList list) const;

public slots:
    void setCurrentOrderItem(CurrentOrderItem* currentOrderItem);
    void setCouponListModel(CouponListModel* couponListModel);
    void receivedCouponsDataFromRequest(ContainerItemInterface *containerItemInterface);
    void receivedCouponsDataFromMonitor(ContainerItemInterface *containerItemInterface);
    void setNewOrderListModel(NewOrderListModel* newOrderListModel);

private:
    void prepareConnections();

    CurrentOrderItem* m_currentOrderItem;
    CouponListModel* m_couponListModel;
    NewOrderListModel* m_newOrderListModel;
};

#endif // DATAMANAGER_H
