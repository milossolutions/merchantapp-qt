#ifndef OPERATIONRESULTTYPE_H
#define OPERATIONRESULTTYPE_H

#include "typeinterface.h"

/**
 * \class OperationResultType
 * \brief Class represents OperationResultType
 */
class OperationResultType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)

public:
    explicit OperationResultType(QObject *parent = 0);

    QString status() const;

signals:
    void statusChanged(const QString status) const;

public slots:
    void setStatus(const QString status);

private:
    QString m_status;
};

#endif // OPERATIONRESULTTYPE_H
