#ifndef CUSTOMERINFORMATIONTYPE_H
#define CUSTOMERINFORMATIONTYPE_H

#include "typeinterface.h"

class LoyaltyType;
class CouponType;

/**
 * \class CustomerInformationType
 * \brief Class represents CustomerInformationType
 */

class CustomerInformationType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QList<LoyaltyType *> loyaltyListType READ loyaltyListType WRITE setLoyaltyListType NOTIFY loyaltyListTypeChanged)
    Q_PROPERTY(QList<CouponType *> couponListType READ couponListType WRITE setCouponListType NOTIFY couponListTypeChanged)
    Q_PROPERTY(QString customerRelationUuid READ customerRelationUuid WRITE setCustomerRelationUuid NOTIFY customerRelationUuidChanged)
    Q_PROPERTY(QStringList addendum READ addendum WRITE setAddendum NOTIFY addendumChanged)

    Q_PROPERTY(QStringList keyAttribute READ keyAttribute WRITE setKeyAttribute NOTIFY keyAttributeChanged)

public:
    explicit CustomerInformationType(QObject *parent = 0);

    QList<LoyaltyType *> loyaltyListType() const;
    QList<CouponType *> couponListType() const;
    QString customerRelationUuid() const;
    QStringList addendum() const;

    void appendLoyalty(LoyaltyType *loyalty);
    void appendCoupon(CouponType *coupon);

    QStringList keyAttribute() const;

signals:
    void loyaltyListTypeChanged(QList<LoyaltyType *> loyaltyListType) const;
    void couponListTypeChanged(QList<CouponType *> couponListType) const;
    void customerRelationUuidChanged(const QString customerRelationUuid) const;
    void addendumChanged(const QStringList addendum) const;

    void keyAttributeChanged(const QStringList keyAttribute) const;

public slots:
    void setLoyaltyListType(QList<LoyaltyType *> loyaltyListType);
    void setCouponListType(QList<CouponType *> couponListType);
    void setCustomerRelationUuid(const QString customerRelationUuid);
    void setAddendum(const QStringList addendum);

    void setKeyAttribute(const QStringList keyAttribute);

public:
    void parseElementAttributes(QXmlStreamReader &xmlReader);

protected:
    void parse(QByteArray msgData);

private:
    QString m_customerRelationUuid;
    QStringList m_addendum;

    QList<LoyaltyType *> m_loyaltyList;
    QList<CouponType *> m_couponList;

    QStringList m_keyAttribute;
};

#endif // CUSTOMERINFORMATIONTYPE_H
