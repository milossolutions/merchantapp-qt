#include "loyaltytype.h"

LoyaltyType::LoyaltyType(QObject *parent) :
    TypeInterface(parent)
{

}

QString LoyaltyType::program() const
{
    return m_program;
}

QString LoyaltyType::reference() const
{
    return m_reference;
}

void LoyaltyType::setProgram(const QString program)
{
    if (m_program == program)
        return;

    m_program = program;
    emit programChanged(program);
}

void LoyaltyType::setReference(const QString reference)
{
    if (m_reference == reference)
        return;

    m_reference = reference;
    emit referenceChanged(reference);
}
