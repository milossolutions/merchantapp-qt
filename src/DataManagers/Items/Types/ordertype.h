#ifndef ORDERTYPE_H
#define ORDERTYPE_H

#include "orderrequesttype.h"

class OrderStatusType;
class CurrencyAmountType;
class PaymentAmountType;

/**
 * \class OrderType
 * \brief Class represents OrderType
 */
class OrderType : public OrderRequestType
{
    Q_OBJECT
    Q_PROPERTY(QString UUID READ UUID WRITE setUUID NOTIFY UUIDChanged)
    Q_PROPERTY(OrderStatusType* orderStatusType READ orderStatusType WRITE setOrderStatusType NOTIFY orderStatusTypeChanged)
    Q_PROPERTY(QString creationTimestamp READ creationTimestamp WRITE setCreationTimestamp NOTIFY creationTimestampChanged)
    Q_PROPERTY(CurrencyAmountType* authorizedAmountType READ authorizedAmountType WRITE setAuthorizedAmountType NOTIFY authorizedAmountTypeChanged)
    Q_PROPERTY(CurrencyAmountType* feeType READ feeType WRITE setFeeType NOTIFY feeTypeChanged)
    Q_PROPERTY(QString processingTimestamp READ processingTimestamp WRITE setProcessingTimestamp NOTIFY processingTimestampChanged)
    Q_PROPERTY(QList<PaymentAmountType *> paymentAmountListType READ paymentAmountListType WRITE setPaymentAmountListType NOTIFY paymentAmountListTypeChanged)

public:
    explicit OrderType(QObject *parent = 0);

    QString UUID() const;
    OrderStatusType *orderStatusType() const;
    QString creationTimestamp() const;
    CurrencyAmountType *authorizedAmountType() const;
    CurrencyAmountType *feeType() const;
    QString processingTimestamp() const;
    QList<PaymentAmountType *> paymentAmountListType() const;

    void appendPaymentAmount(PaymentAmountType *paymentAmount);

signals:
    void UUIDChanged(const QString UUID) const;
    void orderStatusTypeChanged(OrderStatusType* orderStatusType) const;
    void creationTimestampChanged(const QString creationTimestamp) const;
    void authorizedAmountTypeChanged(CurrencyAmountType* authorizedAmountType) const;
    void feeTypeChanged(CurrencyAmountType* feeType) const;
    void processingTimestampChanged(const QString processingTimestamp) const;
    void paymentAmountListTypeChanged(QList<PaymentAmountType *> paymentAmountListType) const;

public slots:
    void setUUID(const QString UUID);
    void setOrderStatusType(OrderStatusType* orderStatusType);
    void setCreationTimestamp(const QString creationTimestamp);
    void setAuthorizedAmountType(CurrencyAmountType* authorizedAmountType);
    void setFeeType(CurrencyAmountType* feeType);
    void setProcessingTimestamp(const QString processingTimestamp);
    void setPaymentAmountListType(QList<PaymentAmountType *> paymentAmountListType);

protected:
    void parse(QByteArray msgData);

private:
    QString m_UUID;
    QString m_creationTimestamp;
    QString m_processingTimestamp;

    OrderStatusType *m_orderStatus;
    CurrencyAmountType *m_fee;
    CurrencyAmountType *m_authorizedAmount;

    QList<PaymentAmountType *> m_paymentAmountList;
};

#endif // ORDERTYPE_H
