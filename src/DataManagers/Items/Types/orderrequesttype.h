#ifndef ORDERREQUESTTYPE_H
#define ORDERREQUESTTYPE_H

#include "typeinterface.h"

class CurrencyAmountType;
class OrderLinkType;

/**
 * \class OrderRequestType
 * \brief Class represents OrderRequestType
 */
class OrderRequestType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString postingType READ postingType WRITE setPostingType NOTIFY postingTypeChanged)
    Q_PROPERTY(CurrencyAmountType* requestedAmountType READ requestedAmountType WRITE setRequestedAmountType NOTIFY requestedAmountTypeChanged)
    Q_PROPERTY(QString merchantTransactionReference READ merchantTransactionReference WRITE setMerchantTransactionReference NOTIFY merchantTransactionReferenceChanged)
    Q_PROPERTY(CurrencyAmountType* customerBenefitType READ customerBenefitType WRITE setCustomerBenefitType NOTIFY customerBenefitTypeChanged)
    Q_PROPERTY(QString eReceiptURL READ eReceiptURL WRITE setEReceiptURL NOTIFY eReceiptURLChanged)
    Q_PROPERTY(OrderLinkType* linkType READ linkType WRITE setLinkType NOTIFY linkTypeChanged)

    Q_PROPERTY(QString confirmationNeededAttribute READ confirmationNeededAttribute WRITE setConfirmationNeededAttribute NOTIFY confirmationNeededAttributeChanged)
    Q_PROPERTY(QString typeAttribute READ typeAttribute WRITE setTypeAttribute NOTIFY typeAttributeChanged)

public:
    explicit OrderRequestType(QObject *parent = 0);

    QString postingType() const;
    CurrencyAmountType *requestedAmountType() const;
    QString merchantTransactionReference() const;
    CurrencyAmountType *customerBenefitType() const;
    QString eReceiptURL() const;
    OrderLinkType *linkType() const;

    QString typeAttribute() const;
    QString confirmationNeededAttribute() const;

signals:
    void postingTypeChanged(const QString postingType) const;
    void requestedAmountTypeChanged(CurrencyAmountType* requestedAmountType) const;
    void merchantTransactionReferenceChanged(const QString merchantTransactionReference) const;
    void customerBenefitTypeChanged(CurrencyAmountType* customerBenefitType) const;
    void eReceiptURLChanged(const QString eReceiptURL) const;
    void linkTypeChanged(OrderLinkType* linkTypee) const;

    void typeAttributeChanged(const QString typeAttribute) const;
    void confirmationNeededAttributeChanged(const QString confirmationNeededAttribute) const;

public slots:
    void setPostingType(const QString postingType);
    void setRequestedAmountType(CurrencyAmountType* requestedAmountType);
    void setMerchantTransactionReference(const QString merchantTransactionReference);
    void setCustomerBenefitType(CurrencyAmountType* customerBenefitType);
    void setEReceiptURL(const QString eReceiptURL);
    void setLinkType(OrderLinkType* linkType);

    void setTypeAttribute(const QString typeAttribute);
    void setConfirmationNeededAttribute(const QString confirmationNeededAttribute);

private:
    QString m_postingType;
    QString m_merchantTransactionReference;
    QString m_eReceiptURL;

    QString m_typeAttribute;
    QString m_confirmationNeededAttribute;

    CurrencyAmountType *m_requestedAmount;
    CurrencyAmountType *m_customerBenefit;
    OrderLinkType *m_link;
};

#endif // ORDERREQUESTTYPE_H
