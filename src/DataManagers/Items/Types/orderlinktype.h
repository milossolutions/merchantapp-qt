#ifndef ORDERLINKTYPE_H
#define ORDERLINKTYPE_H

#include "typeinterface.h"

/**
 * \class OrderLinkType
 * \brief Class represents OrderLinkType
 */
class OrderLinkType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString merchantTransactionReference READ merchantTransactionReference WRITE setMerchantTransactionReference NOTIFY merchantTransactionReferenceChanged)
    Q_PROPERTY(QString orderUuid READ orderUuid WRITE setOrderUuid NOTIFY orderUuidChanged)

public:
    explicit OrderLinkType(QObject *parent = 0);

    QString merchantTransactionReference() const;
    QString orderUuid() const;

signals:
    void merchantTransactionReferenceChanged(const QString merchantTransactionReference) const;
    void orderUuidChanged(const QString orderUuid) const;

public slots:
    void setMerchantTransactionReference(const QString merchantTransactionReference);
    void setOrderUuid(const QString orderUuid);

private:
    QString m_merchantTransactionReference;
    QString m_orderUuid;
};

#endif // ORDERLINKTYPE_H
