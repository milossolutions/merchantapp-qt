#ifndef BEACONSECURITYTYPE_H
#define BEACONSECURITYTYPE_H

#include "typeinterface.h"

/**
 * \class BeaconSecurityType
 * \brief Class represents BeaconSecurityType
 */
class BeaconSecurityType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString beaconUuid READ beaconUuid WRITE setBeaconUuid NOTIFY beaconUuidChanged)
    Q_PROPERTY(QString majorId READ majorId WRITE setMajorId NOTIFY majorIdChanged)
    Q_PROPERTY(QString minorId READ minorId WRITE setMinorId NOTIFY minorIdChanged)
    Q_PROPERTY(QString beaconInitString READ beaconInitString WRITE setBeaconInitString NOTIFY beaconInitStringChanged)
    Q_PROPERTY(QString beaconSecret READ beaconSecret WRITE setBeaconSecret NOTIFY beaconSecretChanged)

public:
    explicit BeaconSecurityType(QObject *parent = 0);

    QString beaconUuid() const;
    QString majorId() const;
    QString minorId() const;
    QString beaconInitString() const;
    QString beaconSecret() const;

signals:
    void beaconUuidChanged(const QString beaconUuid) const;
    void majorIdChanged(const QString majorId) const;
    void minorIdChanged(const QString minorId)const ;
    void beaconInitStringChanged(const QString beaconInitString)const ;
    void beaconSecretChanged(const QString beaconSecret)const ;

public slots:
    void setBeaconUuid(const QString beaconUuid);
    void setMajorId(const QString majorId);
    void setMinorId(const QString minorId);
    void setBeaconInitString(const QString beaconInitString);
    void setBeaconSecret(const QString beaconSecret);

private:
    QString m_beaconUuid;
    QString m_majorId;
    QString m_minorId;
    QString m_beaconInitString;
    QString m_beaconSecret;
};

#endif // BEACONSECURITYTYPE_H
