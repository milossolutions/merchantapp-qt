#include "currencyamounttype.h"

CurrencyAmountType::CurrencyAmountType(QObject *parent) :
    TypeInterface(parent)
{

}

QString CurrencyAmountType::amount() const
{
    return m_amount;
}

QString CurrencyAmountType::currency() const
{
    return m_currency;
}

void CurrencyAmountType::setAmount(const QString amount)
{
    if (m_amount == amount)
        return;

    m_amount = amount;
    emit amountChanged(amount);
}

void CurrencyAmountType::setCurrency(const QString currency)
{
    if (m_currency == currency)
        return;

    m_currency = currency;
    emit currencyChanged(currency);
}
