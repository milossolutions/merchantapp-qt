#ifndef CURRENCYAMOUNTTYPE_H
#define CURRENCYAMOUNTTYPE_H

#include "typeinterface.h"

/**
 * \class CurrencyAmountType
 * \brief Class represents CurrencyAmountType
 */
class CurrencyAmountType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString amount READ amount WRITE setAmount NOTIFY amountChanged)
    Q_PROPERTY(QString currency READ currency WRITE setCurrency NOTIFY currencyChanged)

public:
    explicit CurrencyAmountType(QObject *parent = 0);

    QString amount() const;
    QString currency() const;

signals:
    void amountChanged(const QString amount) const;
    void currencyChanged(const QString currency) const;

public slots:
    void setAmount(const QString amount);
    void setCurrency(const QString currency);

private:
    QString m_amount;
    QString m_currency;
};

#endif // CURRENCYAMOUNTTYPE_H
