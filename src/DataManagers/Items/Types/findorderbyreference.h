#ifndef FINDORDERBYREFERENCE_H
#define FINDORDERBYREFERENCE_H

#include "typeinterface.h"

/**
 * \class FindOrderByReference
 * \brief Class represents FindOrderByReference
 */
class FindOrderByReference : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString orderUuid READ orderUuid WRITE setOrderUuid NOTIFY orderUuidChanged)
    Q_PROPERTY(QString merchantTransactionReference READ merchantTransactionReference WRITE setMerchantTransactionReference NOTIFY merchantTransactionReferenceChanged)

public:
    explicit FindOrderByReference(QObject *parent = 0);

    QString orderUuid() const;
    QString merchantTransactionReference() const;

signals:
    void orderUuidChanged(const QString orderUuid) const;
    void merchantTransactionReferenceChanged(const QString merchantTransactionReference) const;

public slots:
    void setOrderUuid(const QString orderUuid);
    void setMerchantTransactionReference(const QString merchantTransactionReference);

private:
    QString m_orderUuid;
    QString m_merchantTransactionReference;
};

#endif // FINDORDERBYREFERENCE_H
