#include "paymentamounttype.h"

PaymentAmountType::PaymentAmountType(QObject *parent) :
    TypeInterface(parent),
    m_amount(new CurrencyAmountType(this))
{
    m_amount->setObjectName("Amount");
}

CurrencyAmountType *PaymentAmountType::amountType() const
{
    return m_amount;
}

QString PaymentAmountType::paymentMethod() const
{
    return m_paymentMethod;
}

void PaymentAmountType::setAmountType(CurrencyAmountType *amountType)
{
    if (m_amount == amountType)
        return;

    m_amount = amountType;
    emit amountTypeChanged(amountType);
}

void PaymentAmountType::setPaymentMethod(const QString paymentMethod)
{
    if (m_paymentMethod == paymentMethod)
        return;

    m_paymentMethod = paymentMethod;
    emit paymentMethodChanged(paymentMethod);
}
