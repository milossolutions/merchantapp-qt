#ifndef MERCHANTINFORMATIONTYPE_H
#define MERCHANTINFORMATIONTYPE_H

#include "typeinterface.h"

#include <QReadWriteLock>

/**
 * \class MerchantInformationType
 * \brief Class represents MerchantInformationType
 */
class MerchantInformationType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString merchantUuid READ merchantUuid WRITE setMerchantUuid NOTIFY merchantUuidChanged)
    Q_PROPERTY(QString merchantAliasId READ merchantAliasId WRITE setMerchantAliasId NOTIFY merchantAliasIdChanged)
    Q_PROPERTY(QString cashRegisterId READ cashRegisterId WRITE setCashRegisterId NOTIFY cashRegisterIdChanged)
    Q_PROPERTY(QString serviceAgentUuid READ serviceAgentUuid WRITE setServiceAgentUuid NOTIFY serviceAgentUuidChanged)

public:
    explicit MerchantInformationType(QObject *parent = 0);
    ~MerchantInformationType();
    void loadDefaults();

    QString merchantUuid();
    QString merchantAliasId();
    QString cashRegisterId();
    QString serviceAgentUuid();

signals:
    void merchantUuidChanged(const QString merchantUuid) const;
    void merchantAliasIdChanged(const QString merchantAliasId) const;
    void cashRegisterIdChanged(const QString cashRegisterId) const;
    void serviceAgentUuidChanged(const QString serviceAgentUuid) const;

public slots:
    void setMerchantUuid(const QString merchantUuid);
    void setMerchantAliasId(const QString merchantAliasId);
    void setCashRegisterId(const QString cashRegisterId);
    void setServiceAgentUuid(const QString serviceAgentUuid);

private:
    QString m_merchantUuid;
    QString m_merchantAliasId;
    QString m_cashRegisterId;
    QString m_serviceAgentUuid;
};

#endif // MERCHANTINFORMATIONTYPE_H
