#include "orderstatustype.h"

#include <QXmlStreamReader>
#include <QMetaProperty>

#include <QDebug>

OrderStatusType::OrderStatusType(QObject *parent) :
    TypeInterface(parent)
{

}

QString OrderStatusType::status() const
{
    return m_status;
}

QString OrderStatusType::reason() const
{
    return m_reason;
}

QString OrderStatusType::codeStatusAttribute() const
{
    return m_codeStatusAttribute;
}

QString OrderStatusType::codeReasonAttribute() const
{
    return m_codeReasonAttribute;
}

void OrderStatusType::setStatus(const QString status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(status);
}

void OrderStatusType::setReason(const QString reason)
{
    if (m_reason == reason)
        return;

    m_reason = reason;
    emit reasonChanged(reason);
}

void OrderStatusType::setCodeStatusAttribute(const QString codeStatusAttribute)
{
    if (m_codeStatusAttribute == codeStatusAttribute)
        return;

    m_codeStatusAttribute = codeStatusAttribute;
    emit codeStatusAttributeChanged(codeStatusAttribute);
}

void OrderStatusType::setCodeReasonAttribute(const QString codeReasonAttribute)
{
    if (m_codeReasonAttribute == codeReasonAttribute)
        return;

    m_codeReasonAttribute = codeReasonAttribute;
    emit codeReasonAttributeChanged(codeReasonAttribute);
}

void OrderStatusType::parseElementAttributes(QXmlStreamReader &xmlReader)
{
    if (!xmlReader.isStartElement()) {
        return;
    }
    foreach(const QXmlStreamAttribute &attr, xmlReader.attributes()) {
        foreach (QMetaProperty property, m_metaPropertyList) {
            QString propertyName(property.name());
            if (!propertyName.contains("Attribute")) {
                continue;
            }
            QString tmp = propertyName;
            tmp.replace("Attribute", "");
            QString node = xmlReader.name().toString();
            QString attributeValue = attr.value().toString();
            if (tmp == "codeReason" && node == "Reason") {
                property.write(this, attributeValue);
            } else if (tmp == "codeStatus" && node == "Status") {
                property.write(this, attributeValue);
            }

        }
    }
}
