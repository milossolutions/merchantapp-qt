#include "couponrejectionreason.h"

CouponRejectionReason::CouponRejectionReason(QObject *parent) :
    TypeInterface(parent)
{

}

QString CouponRejectionReason::rejectionReason() const
{
    return m_rejectionReason;
}

QString CouponRejectionReason::details() const
{
    return m_details;
}

void CouponRejectionReason::setRejectionReason(const QString rejectionReason)
{
    if (m_rejectionReason == rejectionReason)
        return;

    m_rejectionReason = rejectionReason;
    emit rejectionReasonChanged(rejectionReason);
}

void CouponRejectionReason::setDetails(const QString details)
{
    if (m_details == details)
        return;

    m_details = details;
    emit detailsChanged(details);
}
