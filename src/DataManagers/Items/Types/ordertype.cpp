#include "ordertype.h"

#include "orderstatustype.h"
#include "currencyamounttype.h"
#include "paymentamounttype.h"

#include <QXmlStreamReader>
#include <QStack>
#include <QMetaProperty>
#include <QDebug>

OrderType::OrderType(QObject *parent) :
    OrderRequestType(parent),
    m_orderStatus(new OrderStatusType(this)),
    m_fee(new CurrencyAmountType(this)),
    m_authorizedAmount(new CurrencyAmountType(this))
{
    m_orderStatus->setObjectName("Status");
    m_fee->setObjectName("Fee");
    m_authorizedAmount->setObjectName("AuthorizedAmount");
}

QString OrderType::UUID() const
{
    return m_UUID;
}

OrderStatusType *OrderType::orderStatusType() const
{
    return m_orderStatus;
}

QString OrderType::creationTimestamp() const
{
    return m_creationTimestamp;
}

CurrencyAmountType *OrderType::authorizedAmountType() const
{
    return m_authorizedAmount;
}

CurrencyAmountType *OrderType::feeType() const
{
    return m_fee;
}

QString OrderType::processingTimestamp() const
{
    return m_processingTimestamp;
}

QList<PaymentAmountType *> OrderType::paymentAmountListType() const
{
    return m_paymentAmountList;
}

void OrderType::appendPaymentAmount(PaymentAmountType *paymentAmount)
{
    paymentAmount->setObjectName("PaymentAmount");
    m_paymentAmountList.append(paymentAmount);
}

void OrderType::setUUID(const QString UUID)
{
    if (m_UUID == UUID)
        return;

    m_UUID = UUID;
    emit UUIDChanged(UUID);
}

void OrderType::setOrderStatusType(OrderStatusType *orderStatusType)
{
    if (m_orderStatus == orderStatusType)
        return;

    m_orderStatus = orderStatusType;
    emit orderStatusTypeChanged(orderStatusType);
}

void OrderType::setCreationTimestamp(const QString creationTimestamp)
{
    if (m_creationTimestamp == creationTimestamp)
        return;

    m_creationTimestamp = creationTimestamp;
    emit creationTimestampChanged(creationTimestamp);
}

void OrderType::setAuthorizedAmountType(CurrencyAmountType *authorizedAmountType)
{
    if (m_authorizedAmount == authorizedAmountType)
        return;

    m_authorizedAmount = authorizedAmountType;
    emit authorizedAmountTypeChanged(authorizedAmountType);
}

void OrderType::setFeeType(CurrencyAmountType *feeType)
{
    if (m_fee == feeType)
        return;

    m_fee = feeType;
    emit feeTypeChanged(feeType);
}

void OrderType::setProcessingTimestamp(const QString processingTimestamp)
{
    if (m_processingTimestamp == processingTimestamp)
        return;

    m_processingTimestamp = processingTimestamp;
    emit processingTimestampChanged(processingTimestamp);
}

void OrderType::setPaymentAmountListType(QList<PaymentAmountType *> paymentAmountListType)
{
    if (m_paymentAmountList == paymentAmountListType)
        return;

    m_paymentAmountList = paymentAmountListType;
    emit paymentAmountListTypeChanged(paymentAmountListType);
}

void OrderType::parse(QByteArray msgData)
{
    QXmlStreamReader xml(msgData);
    xml.setNamespaceProcessing(true);
    QObjectList childrens;
    if (!this->children().isEmpty())
        getAllChildrensOfObject(this->children(), childrens);
    childrens.append(this);

    const QObjectList objectList = childrens;

    QStack<QString> stack;
    bool isStartElement = false;
    while (!xml.atEnd()) {
        QString name = xml.name().toString();
        QString parent = "";
        isStartElement = false;
        if (xml.isStartElement()) {
            isStartElement = true;
            if (stack.count() != 0) {
                parent = stack.top();
            }
            stack.push(name);
        } else if (xml.isEndElement()) {
            stack.pop();
        }

        xml.readNext();
        foreach (QObject *object, childrens) {
            ItemInterface *itemInterface = qobject_cast<ItemInterface*>(object);
            if (itemInterface) {
                itemInterface->createPropertyList();
                itemInterface->parseElementAttributes(xml);
            }
        }
        if(xml.name().isEmpty() && !xml.text().isEmpty()){
            foreach (QObject *object, childrens) {
                QList<QMetaProperty> metaPropertyList = getPropertyListOfObject(object);
                foreach (QMetaProperty property, metaPropertyList) {
                    QString propertyName(property.name());
                    if (!(property.type() == QVariant::String && propertyName.toLower() == name.toLower()))
                        continue;
                    if (parent.toLower() != object->objectName().toLower()) {
                        continue;
                    }
                    QVariant variant = QVariant::fromValue(xml.text().toString());
                    property.write(object, variant);
                    break;
                }
            }
        } else if (name.toLower() == "paymentamount" && isStartElement) {
            childrens = objectList;
            PaymentAmountType *amountType = new PaymentAmountType(this);
            appendPaymentAmount(amountType);

            QObjectList list;
            if (!amountType->children().isEmpty())
                getAllChildrensOfObject(amountType->children(), list);
            list.append(amountType);

            childrens.append(list);
        }
    }
}
