#ifndef CHECKINNOTIFICATIONTYPE_H
#define CHECKINNOTIFICATIONTYPE_H

#include "typeinterface.h"

class MerchantInformationType;
class CustomerInformationType;

/**
 * \class CheckInNotificationType
 * \brief Class represents CheckInNotificationType
 */
class CheckInNotificationType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(CustomerInformationType* customerInformationType READ customerInformationType WRITE setCustomerInformationType NOTIFY customerInformationTypeChanged)
    Q_PROPERTY(QString pairingUuid READ pairingUuid WRITE setPairingUuid NOTIFY pairingUuidChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)

public:
    explicit CheckInNotificationType(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    CustomerInformationType *customerInformationType() const;
    QString pairingUuid() const;
    QString pairingStatus() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void customerInformationTypeChanged(CustomerInformationType* customerInformationType) const;
    void pairingUuidChanged(const QString pairingUuid) const;
    void pairingStatusChanged(const QString pairingStatus) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setCustomerInformationType(CustomerInformationType* customerInformationType);
    void setPairingUuid(const QString pairingUuid);
    void setPairingStatus(const QString pairingStatus);

private:
    QString m_pairingUuid;
    QString m_pairingStatus;

    MerchantInformationType *m_merchantInformation;
    CustomerInformationType *m_customerInformation;
};

#endif // CHECKINNOTIFICATIONTYPE_H
