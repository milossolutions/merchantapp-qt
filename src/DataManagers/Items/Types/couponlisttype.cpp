#include "couponlisttype.h"

#include "coupontype.h"
#include "rejectedcoupontype.h"

CouponListType::CouponListType(QObject *parent) :
    TypeInterface(parent)
{

}

QList<CouponType *> CouponListType::processedCouponListType() const
{
    return m_processedCouponList;
}

QList<RejectedCouponType *> CouponListType::rejectedCouponListType() const
{
    return m_rejectedCouponList;
}

void CouponListType::appendProcessedCoupon(CouponType *coupon)
{
    coupon->setObjectName("ProcessedCoupon");
    m_processedCouponList.append(coupon);
}

void CouponListType::appendRejectedCoupon(RejectedCouponType *rejectedCoupon)
{
    rejectedCoupon->setObjectName("RejectedCoupon");
    m_rejectedCouponList.append(rejectedCoupon);
}

void CouponListType::setProcessedCouponListType(QList<CouponType *> processedCouponListType)
{
    if (m_processedCouponList == processedCouponListType)
        return;

    m_processedCouponList = processedCouponListType;
    emit processedCouponListTypeChanged(processedCouponListType);
}

void CouponListType::setRejectedCouponListType(QList<RejectedCouponType *> rejectedCouponListType)
{
    if (m_rejectedCouponList == rejectedCouponListType)
        return;

    m_rejectedCouponList = rejectedCouponListType;
    emit rejectedCouponListTypeChanged(rejectedCouponListType);
}
