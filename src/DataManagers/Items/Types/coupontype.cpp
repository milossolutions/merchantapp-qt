#include "coupontype.h"

#include "currencyamounttype.h"

CouponType::CouponType(QObject *parent) :
    TypeInterface(parent),
    m_couponValue(new CurrencyAmountType(this))
{
    m_couponValue->setObjectName("CouponValue");
}

QString CouponType::couponId() const
{
    return m_couponId;
}

CurrencyAmountType *CouponType::couponValueType() const
{
    return m_couponValue;
}

void CouponType::setCouponId(const QString couponId)
{
    if (m_couponId == couponId)
        return;

    m_couponId = couponId;
    emit couponIdChanged(couponId);
}

void CouponType::setCouponValueType(CurrencyAmountType *couponValueType)
{
    if (m_couponValue == couponValueType)
        return;

    m_couponValue = couponValueType;
    emit couponValueTypeChanged(couponValueType);
}
