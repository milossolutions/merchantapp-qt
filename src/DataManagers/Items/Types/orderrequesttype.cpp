#include "orderrequesttype.h"

#include "currencyamounttype.h"
#include "orderlinktype.h"

#include <QDebug>
#include <QXmlStreamReader>

OrderRequestType::OrderRequestType(QObject *parent) :
    TypeInterface(parent),
    m_requestedAmount(new CurrencyAmountType(this)),
    m_customerBenefit(new CurrencyAmountType(this)),
    m_link(new OrderLinkType(this))
{
    m_requestedAmount->setObjectName("RequestedAmount");
    m_customerBenefit->setObjectName("CustomerBenefit");
    m_link->setObjectName("Link");
}

QString OrderRequestType::postingType() const
{
    return m_postingType;
}

CurrencyAmountType *OrderRequestType::requestedAmountType() const
{
    return m_requestedAmount;
}

QString OrderRequestType::merchantTransactionReference() const
{
    return m_merchantTransactionReference;
}

CurrencyAmountType *OrderRequestType::customerBenefitType() const
{
    return m_customerBenefit;
}

QString OrderRequestType::eReceiptURL() const
{
    return m_eReceiptURL;
}

OrderLinkType *OrderRequestType::linkType() const
{
    return m_link;
}

QString OrderRequestType::typeAttribute() const
{
    return m_typeAttribute;
}

QString OrderRequestType::confirmationNeededAttribute() const
{
    return m_confirmationNeededAttribute;
}

void OrderRequestType::setPostingType(const QString postingType)
{
    if (m_postingType == postingType)
        return;

    m_postingType = postingType;
    emit postingTypeChanged(postingType);
}

void OrderRequestType::setRequestedAmountType(CurrencyAmountType *requestedAmountType)
{
    if (m_requestedAmount== requestedAmountType)
        return;

    m_requestedAmount = requestedAmountType;
    emit requestedAmountTypeChanged(requestedAmountType);
}

void OrderRequestType::setMerchantTransactionReference(const QString merchantTransactionReference)
{
    if (m_merchantTransactionReference == merchantTransactionReference)
        return;

    m_merchantTransactionReference = merchantTransactionReference;
    emit merchantTransactionReferenceChanged(merchantTransactionReference);
}

void OrderRequestType::setCustomerBenefitType(CurrencyAmountType *customerBenefitType)
{
    if (m_customerBenefit == customerBenefitType)
        return;

    m_customerBenefit = customerBenefitType;
    emit customerBenefitTypeChanged(customerBenefitType);
}

void OrderRequestType::setEReceiptURL(const QString eReceiptURL)
{
    if (m_eReceiptURL == eReceiptURL)
        return;

    m_eReceiptURL = eReceiptURL;
    emit eReceiptURLChanged(eReceiptURL);
}

void OrderRequestType::setLinkType(OrderLinkType *linkType)
{
    if (m_link == linkType)
        return;

    m_link = linkType;
    emit linkTypeChanged(linkType);
}

void OrderRequestType::setTypeAttribute(const QString typeAttribute)
{
    if (m_typeAttribute == typeAttribute)
        return;

    m_typeAttribute = typeAttribute;
    emit typeAttributeChanged(typeAttribute);
}

void OrderRequestType::setConfirmationNeededAttribute(const QString confirmationNeededAttribute)
{
    if (m_confirmationNeededAttribute == confirmationNeededAttribute)
        return;

    m_confirmationNeededAttribute = confirmationNeededAttribute;
    emit confirmationNeededAttributeChanged(confirmationNeededAttribute);
}
