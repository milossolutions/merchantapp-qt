#ifndef REJECTEDCOUPONTYPE_H
#define REJECTEDCOUPONTYPE_H

#include "typeinterface.h"

class CouponRejectionReason;

/**
 * \class RejectedCouponType
 * \brief Class represents RejectedCouponType
 */
class RejectedCouponType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString couponId READ couponId WRITE setCouponId NOTIFY couponIdChanged)
    Q_PROPERTY(CouponRejectionReason* rejectionReasonType READ rejectionReasonType WRITE setRejectionReasonType NOTIFY rejectionReasonTypeChanged)

public:
    explicit RejectedCouponType(QObject *parent = 0);

    QString couponId() const;
    CouponRejectionReason *rejectionReasonType() const;

signals:
    void couponIdChanged(const QString couponId) const;
    void rejectionReasonTypeChanged(CouponRejectionReason* rejectionReasonType) const;

public slots:
    void setCouponId(const QString couponId);
    void setRejectionReasonType(CouponRejectionReason* rejectionReasonType);

private:
    QString m_couponId;

    CouponRejectionReason *m_rejectionReason;
};

#endif // REJECTEDCOUPONTYPE_H
