#ifndef COUPONTYPE_H
#define COUPONTYPE_H

#include "typeinterface.h"

class CurrencyAmountType;

/**
 * \class CouponType
 * \brief Class represents CouponType
 */
class CouponType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString couponId READ couponId WRITE setCouponId NOTIFY couponIdChanged)
    Q_PROPERTY(CurrencyAmountType* couponValueType READ couponValueType WRITE setCouponValueType NOTIFY couponValueTypeChanged)

public:
    explicit CouponType(QObject *parent = 0);

    QString couponId() const;
    CurrencyAmountType *couponValueType() const;

signals:
    void couponIdChanged(const QString couponId) const;
    void couponValueTypeChanged(CurrencyAmountType* couponValueType);

public slots:
    void setCouponId(const QString couponId);
    void setCouponValueType(CurrencyAmountType* couponValueType);

private:
    QString m_couponId;

    CurrencyAmountType *m_couponValue;
};

#endif // COUPONTYPE_H
