#ifndef COUPONREJECTIONREASON_H
#define COUPONREJECTIONREASON_H

#include "typeinterface.h"

/**
 * \class CouponRejectionReason
 * \brief Class represents CouponRejectionReason
 */
class CouponRejectionReason : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString rejectionReason READ rejectionReason WRITE setRejectionReason NOTIFY rejectionReasonChanged)
    Q_PROPERTY(QString details READ details WRITE setDetails NOTIFY detailsChanged)

public:
    explicit CouponRejectionReason(QObject *parent = 0);

    QString rejectionReason() const;
    QString details() const;

signals:
    void rejectionReasonChanged(const QString rejectionReason) const;
    void detailsChanged(const QString details) const;

public slots:
    void setRejectionReason(const QString rejectionReason);
    void setDetails(const QString details);

private:
    QString m_rejectionReason;
    QString m_details;
};

#endif // COUPONREJECTIONREASON_H
