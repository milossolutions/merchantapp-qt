#ifndef FINDORDERBYDATE_H
#define FINDORDERBYDATE_H

#include "typeinterface.h"

/**
 * \class FindOrderByDate
 * \brief Class represents FindOrderByDate
 */
class FindOrderByDate : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString searchStartDate READ searchStartDate WRITE setSearchStartDate NOTIFY searchStartDateChanged)
    Q_PROPERTY(QString searchEndDate READ searchEndDate WRITE setSearchEndDate NOTIFY searchEndDateChanged)

public:
    explicit FindOrderByDate(QObject *parent = 0);

    QString searchStartDate();
    QString searchEndDate();

signals:
    void searchStartDateChanged(const QString searchStartDate) const;
    void searchEndDateChanged(const QString searchEndDate);

public slots:
    void setSearchStartDate(const QString searchStartDate);
    void setSearchEndDate(const QString searchEndDate);

private:
    QString m_searchStartDate;
    QString m_searchEndDate;
};

#endif // FINDORDERBYDATE_H
