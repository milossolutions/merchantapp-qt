#include "checkinnotificationtype.h"

#include "merchantinformationtype.h"
#include "customerinformationtype.h"

CheckInNotificationType::CheckInNotificationType(QObject *parent) :
    TypeInterface(parent),
    m_merchantInformation(new MerchantInformationType(this)),
    m_customerInformation(new CustomerInformationType(this))
{
    m_merchantInformation->setObjectName("MerchantInformation");
    m_customerInformation->setObjectName("CustomerInformation");
}

MerchantInformationType *CheckInNotificationType::merchantInformationType() const
{
    return m_merchantInformation;
}

CustomerInformationType *CheckInNotificationType::customerInformationType() const
{
    return m_customerInformation;
}

QString CheckInNotificationType::pairingUuid() const
{
    return m_pairingUuid;
}

QString CheckInNotificationType::pairingStatus() const
{
    return m_pairingStatus;
}

void CheckInNotificationType::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void CheckInNotificationType::setCustomerInformationType(CustomerInformationType *customerInformationType)
{
    if (m_customerInformation == customerInformationType)
        return;

    m_customerInformation = customerInformationType;
    emit customerInformationTypeChanged(customerInformationType);
}

void CheckInNotificationType::setPairingUuid(const QString pairingUuid)
{
    if (m_pairingUuid == pairingUuid)
        return;

    m_pairingUuid = pairingUuid;
    emit pairingUuidChanged(pairingUuid);
}

void CheckInNotificationType::setPairingStatus(const QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}
