#include "rejectedcoupontype.h"

#include "couponrejectionreason.h"

RejectedCouponType::RejectedCouponType(QObject *parent) :
    TypeInterface(parent),
    m_rejectionReason(new CouponRejectionReason(this))
{
    m_rejectionReason->setObjectName("RejectionReason");
}

QString RejectedCouponType::couponId() const
{
    return m_couponId;
}

CouponRejectionReason *RejectedCouponType::rejectionReasonType() const
{
    return m_rejectionReason;
}

void RejectedCouponType::setCouponId(const QString couponId)
{
    if (m_couponId == couponId)
        return;

    m_couponId = couponId;
    emit couponIdChanged(couponId);
}

void RejectedCouponType::setRejectionReasonType(CouponRejectionReason *rejectionReasonType)
{
    if (m_rejectionReason == rejectionReasonType)
        return;

    m_rejectionReason = rejectionReasonType;
    emit rejectionReasonTypeChanged(rejectionReasonType);
}
