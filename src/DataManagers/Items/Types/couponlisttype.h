#ifndef COUPONLISTTYPE_H
#define COUPONLISTTYPE_H

#include "typeinterface.h"

class CouponType;
class RejectedCouponType;

/**
 * \class CouponListType
 * @brief Class represents CouponListType
 */
class CouponListType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QList<CouponType *> processedCouponListType READ processedCouponListType WRITE setProcessedCouponListType NOTIFY processedCouponListTypeChanged)
    Q_PROPERTY(QList<RejectedCouponType *> rejectedCouponListType READ rejectedCouponListType WRITE setRejectedCouponListType NOTIFY rejectedCouponListTypeChanged)

public:
    explicit CouponListType(QObject *parent = 0);

    QList<CouponType *> processedCouponListType() const;
    QList<RejectedCouponType *> rejectedCouponListType() const;
    void appendProcessedCoupon(CouponType *coupon);
    void appendRejectedCoupon(RejectedCouponType *rejectedCoupon);

signals:
    void processedCouponListTypeChanged(QList<CouponType *> processedCouponListType) const;
    void rejectedCouponListTypeChanged(QList<RejectedCouponType *> rejectedCouponListType) const;

public slots:
    void setProcessedCouponListType(QList<CouponType *> processedCouponListType);
    void setRejectedCouponListType(QList<RejectedCouponType *> rejectedCouponListType);

private:
    QList<CouponType *> m_processedCouponList;
    QList<RejectedCouponType *> m_rejectedCouponList;
};

#endif // COUPONLISTTYPE_H
