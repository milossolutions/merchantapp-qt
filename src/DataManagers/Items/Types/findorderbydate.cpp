#include "findorderbydate.h"

FindOrderByDate::FindOrderByDate(QObject *parent) :
    TypeInterface(parent)
{

}

QString FindOrderByDate::searchStartDate()
{
    return m_searchStartDate;
}

QString FindOrderByDate::searchEndDate()
{
    return m_searchEndDate;
}

void FindOrderByDate::setSearchStartDate(const QString searchStartDate)
{
    if (m_searchStartDate == searchStartDate)
        return;

    m_searchStartDate = searchStartDate;
    emit searchStartDateChanged(searchStartDate);
}

void FindOrderByDate::setSearchEndDate(const QString searchEndDate)
{
    if (m_searchEndDate == searchEndDate)
        return;

    m_searchEndDate = searchEndDate;
    emit searchEndDateChanged(searchEndDate);
}
