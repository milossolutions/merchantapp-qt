#include "beaconsecuritytype.h"

/*! \var QString m_beaconInitString
    \brief Convenient string that is used to initialize keacon device .

    This string contains initialization string that is send to keacon and is
    correctly formatted, i.e. major and minor versions are already in hex notation.
*/

BeaconSecurityType::BeaconSecurityType(QObject *parent) :
    TypeInterface(parent)
{

}

QString BeaconSecurityType::beaconUuid() const
{
    return m_beaconUuid;
}

QString BeaconSecurityType::majorId() const
{
    return m_majorId;
}

QString BeaconSecurityType::minorId() const
{
    return m_minorId;
}

QString BeaconSecurityType::beaconInitString() const
{
    return m_beaconInitString;
}

QString BeaconSecurityType::beaconSecret() const
{
    return m_beaconSecret;
}

void BeaconSecurityType::setBeaconUuid(const QString beaconUuid)
{
    if (m_beaconUuid == beaconUuid)
        return;

    m_beaconUuid = beaconUuid;
    emit beaconUuidChanged(beaconUuid);
}

void BeaconSecurityType::setMajorId(const QString majorId)
{
    if (m_majorId == majorId)
        return;

    m_majorId = majorId;
    emit majorIdChanged(majorId);
}

void BeaconSecurityType::setMinorId(const QString minorId)
{
    if (m_minorId == minorId)
        return;

    m_minorId = minorId;
    emit minorIdChanged(minorId);
}

void BeaconSecurityType::setBeaconInitString(const QString beaconInitString)
{
    if (m_beaconInitString == beaconInitString)
        return;

    m_beaconInitString = beaconInitString;
    emit beaconInitStringChanged(beaconInitString);
}

void BeaconSecurityType::setBeaconSecret(const QString beaconSecret)
{
    if (m_beaconSecret == beaconSecret)
        return;

    m_beaconSecret = beaconSecret;
    emit beaconSecretChanged(beaconSecret);
}
