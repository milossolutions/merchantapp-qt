#ifndef TYPEINTERFACE_H
#define TYPEINTERFACE_H

#include "iteminterface.h"

/**
 * \class TypeInterface
 * \brief Objects inheriting after TypeInterface represents different data types,
 * which are used in RequestItemInterface and ResponseItemInterface objects.
 */
class TypeInterface : public ItemInterface
{
    Q_OBJECT
public:
    explicit TypeInterface(QObject *parent = 0);

signals:

public slots:
};

#endif // TYPEINTERFACE_H
