#include "customerinformationtype.h"

#include "loyaltytype.h"
#include "coupontype.h"
#include "currencyamounttype.h"

#include <QXmlStreamReader>
#include <QStack>
#include <QMetaProperty>


CustomerInformationType::CustomerInformationType(QObject *parent) :
    TypeInterface(parent)
{

}

QList<LoyaltyType *> CustomerInformationType::loyaltyListType() const
{
    return m_loyaltyList;
}

QList<CouponType *> CustomerInformationType::couponListType() const
{
    return m_couponList;
}

QString CustomerInformationType::customerRelationUuid() const
{
    return m_customerRelationUuid;
}

QStringList CustomerInformationType::addendum() const
{
    return m_addendum;
}

QStringList CustomerInformationType::keyAttribute() const
{
    return m_keyAttribute;
}

void CustomerInformationType::setLoyaltyListType(QList<LoyaltyType *> loyaltyListType)
{
    if (m_loyaltyList == loyaltyListType)
        return;

    m_loyaltyList = loyaltyListType;
    emit loyaltyListTypeChanged(loyaltyListType);
}

void CustomerInformationType::setCouponListType(QList<CouponType *> couponListType)
{
    if (m_couponList == couponListType)
        return;

    m_couponList = couponListType;
    emit couponListTypeChanged(couponListType);
}

void CustomerInformationType::appendLoyalty(LoyaltyType *loyalty)
{
    loyalty->setObjectName("Loyalty");
    m_loyaltyList.append(loyalty);
}

void CustomerInformationType::appendCoupon(CouponType *coupon)
{
    coupon->setObjectName("Coupon");
    m_couponList.append(coupon);
}

void CustomerInformationType::setCustomerRelationUuid(const QString customerRelationUuid)
{
    if (m_customerRelationUuid == customerRelationUuid)
        return;

    m_customerRelationUuid = customerRelationUuid;
    emit customerRelationUuidChanged(customerRelationUuid);
}

void CustomerInformationType::setAddendum(const QStringList addendum)
{
    if (m_addendum == addendum)
        return;

    m_addendum = addendum;
    emit addendumChanged(addendum);
}

void CustomerInformationType::setKeyAttribute(const QStringList keyAttribute)
{
    if (m_keyAttribute == keyAttribute)
        return;

    m_keyAttribute = keyAttribute;
    emit keyAttributeChanged(keyAttribute);
}

void CustomerInformationType::parseElementAttributes(QXmlStreamReader &xmlReader)
{
    if (!xmlReader.isStartElement()) {
        return;
    }
    foreach(const QXmlStreamAttribute &attr, xmlReader.attributes()) {
        foreach (QMetaProperty property, m_metaPropertyList) {
            QString propertyName(property.name());
            if (!propertyName.contains("Attribute")) {
                continue;
            }
            QString tmp = propertyName;
            tmp.replace("Attribute", "");
            QString attributeValue = attr.value().toString();
            m_keyAttribute.append(attributeValue);
        }
    }
}

void CustomerInformationType::parse(QByteArray msgData)
{
    QXmlStreamReader xml(msgData);
    xml.setNamespaceProcessing(true);
    QObjectList childrens;
    childrens.append(this);
    const QObjectList objectList = childrens;

    QStack<QString> stack;
    bool isStartElement = false;
    while (!xml.atEnd()) {
        QString name = xml.name().toString();
        QString parent = "";
        isStartElement = false;
        if (xml.isStartElement()) {
            isStartElement = true;
            if (stack.count() != 0) {
                parent = stack.top();
            }
            stack.push(name);
        } else if (xml.isEndElement()) {
            stack.pop();
        }

        xml.readNext();
        foreach (QObject *object, childrens) {
            ItemInterface *itemInterface = qobject_cast<ItemInterface*>(object);
            if (itemInterface) {
                itemInterface->createPropertyList();
                itemInterface->parseElementAttributes(xml);
            }
        }
        if(xml.name().isEmpty() && !xml.text().isEmpty()){
            foreach (QObject *object, childrens) {
                QList<QMetaProperty> metaPropertyList = getPropertyListOfObject(object);
                foreach (QMetaProperty property, metaPropertyList) {
                    QString propertyName(property.name());
                    if (!(property.type() == QVariant::String && propertyName.toLower() == name.toLower())) {
                        continue;
                    }
                    if (parent.toLower() != object->objectName().toLower()) {
                        continue;
                    }
                    QVariant variant = QVariant::fromValue(xml.text().toString());
                    property.write(object, variant);
                    break;
                }
            }
        } else if (name.toLower() == "loyalty" && isStartElement) {
            childrens = objectList;
            LoyaltyType *loyaltyType = new LoyaltyType(this);
            appendLoyalty(loyaltyType);

            QObjectList list;
            if (!loyaltyType->children().isEmpty())
                getAllChildrensOfObject(loyaltyType->children(), list);
            list.append(loyaltyType);

        } else if (name.toLower() == "coupon" && isStartElement) {
            //Parses object type until it ends
            CouponType *couponType = new CouponType(this);
            appendCoupon(couponType);

            QString xmlPropertyName = name.toLower();

            stack.pop();

            bool  couoponObject = true;

            while(couoponObject){
                if(!xml.name().toString().isEmpty()){
                    xmlPropertyName = xml.name().toString().toLower();
                }else{
                    if(xmlPropertyName == "couponid"){
                        couponType->setCouponId(xml.text().toString());
                    }else if(xmlPropertyName == "amount"){
                        couponType->couponValueType()->setAmount(xml.text().toString());
                    }else if((xmlPropertyName == "currency")){
                        couponType->couponValueType()->setCurrency(xml.text().toString());
                    }
                }
                xml.readNext();
                couoponObject = !((xml.name().toString().toLower() == "coupon") && (xml.isEndElement() == true));
            }
            xml.readNext();
        }
    }
}
