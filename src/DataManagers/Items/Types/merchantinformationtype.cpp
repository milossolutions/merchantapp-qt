#include "merchantinformationtype.h"

#include <QWriteLocker>
#include <QReadLocker>
#include <QCoreApplication>

#include "settings.h"
#include "applicationstatus.h"

MerchantInformationType::MerchantInformationType(QObject *parent) :
    TypeInterface(parent)
{
    loadDefaults();
}

MerchantInformationType::~MerchantInformationType()
{

}

void MerchantInformationType::loadDefaults()
{
    setMerchantUuid(Settings::instance()->merchantUuid());

    //new implementation for different type of cash register id depending on select user option
    if(Settings::instance()->isSelectUser()){
        if(ApplicationStatus::instance()->currentCashRegister().isEmpty()){
            if(Settings::instance()->cashRegisterId().isEmpty()){
                setCashRegisterId("twint-api-test");
            }else{
                setCashRegisterId(Settings::instance()->cashRegisterId());
            }
        }else{
            setCashRegisterId(ApplicationStatus::instance()->currentCashRegister());
        }
    }else{
        setCashRegisterId(Settings::instance()->cashRegisterId());
    }
}

QString MerchantInformationType::merchantUuid()
{
    return m_merchantUuid;
}

QString MerchantInformationType::merchantAliasId()
{
    return m_merchantAliasId;
}

QString MerchantInformationType::cashRegisterId()
{
    return m_cashRegisterId;
}

QString MerchantInformationType::serviceAgentUuid()
{
    return m_serviceAgentUuid;
}

void MerchantInformationType::setMerchantUuid(const QString merchantUuid)
{
    if (m_merchantUuid == merchantUuid)
        return;

    m_merchantUuid = merchantUuid;
    emit merchantUuidChanged(merchantUuid);
}

void MerchantInformationType::setMerchantAliasId(const QString merchantAliasId)
{
    if (m_merchantAliasId == merchantAliasId)
        return;

    m_merchantAliasId = merchantAliasId;
    emit merchantAliasIdChanged(merchantAliasId);
}

void MerchantInformationType::setCashRegisterId(const QString cashRegisterId)
{
    if (m_cashRegisterId == cashRegisterId)
        return;

    m_cashRegisterId = cashRegisterId;
    emit cashRegisterIdChanged(cashRegisterId);
}

void MerchantInformationType::setServiceAgentUuid(const QString serviceAgentUuid)
{
    if (m_serviceAgentUuid == serviceAgentUuid)
        return;

    m_serviceAgentUuid = serviceAgentUuid;
    emit serviceAgentUuidChanged(serviceAgentUuid);
}
