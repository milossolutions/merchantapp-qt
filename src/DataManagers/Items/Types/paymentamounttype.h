#ifndef PAYMENTAMOUNTTYPE_H
#define PAYMENTAMOUNTTYPE_H

#include "typeinterface.h"

#include "currencyamounttype.h"

/**
 * \class PaymentAmountType
 * \brief Class represents PaymentAmountType
 */
class PaymentAmountType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(CurrencyAmountType* amountType READ amountType WRITE setAmountType NOTIFY amountTypeChanged)
    Q_PROPERTY(QString paymentMethod READ paymentMethod WRITE setPaymentMethod NOTIFY paymentMethodChanged)

public:
    explicit PaymentAmountType(QObject *parent = 0);

    CurrencyAmountType *amountType() const;
    QString paymentMethod() const;

signals:
    void amountTypeChanged(CurrencyAmountType* amountType);
    void paymentMethodChanged(const QString paymentMethod);

public slots:
    void setAmountType(CurrencyAmountType* amountType);
    void setPaymentMethod(const QString paymentMethod);

private:
    QString m_paymentMethod;
    CurrencyAmountType *m_amount;
};

#endif // PAYMENTAMOUNTTYPE_H
