#include "operationresulttype.h"

OperationResultType::OperationResultType(QObject *parent) :
    TypeInterface(parent)
{

}

QString OperationResultType::status() const
{
    return m_status;
}

void OperationResultType::setStatus(const QString status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(status);
}
