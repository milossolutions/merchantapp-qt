#ifndef LOYALTYTYPE_H
#define LOYALTYTYPE_H

#include "typeinterface.h"

/**
 * \class LoyaltyType
 * \brief Class represents LoyaltyType
 */
class LoyaltyType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString program READ program WRITE setProgram NOTIFY programChanged)
    Q_PROPERTY(QString reference READ reference WRITE setReference NOTIFY referenceChanged)

public:
    explicit LoyaltyType(QObject *parent = 0);

    QString program() const;
    QString reference() const;

signals:
    void programChanged(const QString program) const;
    void referenceChanged(const QString reference) const;

public slots:
    void setProgram(const QString program);
    void setReference(const QString reference);

private:
    QString m_program;
    QString m_reference;
};

#endif // LOYALTYTYPE_H
