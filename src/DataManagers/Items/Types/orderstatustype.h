#ifndef ORDERSTATUSTYPE_H
#define ORDERSTATUSTYPE_H

#include "typeinterface.h"

/**
 * \class OrderStatusType
 * \brief Class represents OrderStatusType
 */
class OrderStatusType : public TypeInterface
{
    Q_OBJECT
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(QString reason READ reason WRITE setReason NOTIFY reasonChanged)

    Q_PROPERTY(QString codeStatusAttribute READ codeStatusAttribute WRITE setCodeStatusAttribute NOTIFY codeStatusAttributeChanged)
    Q_PROPERTY(QString codeReasonAttribute READ codeReasonAttribute WRITE setCodeReasonAttribute NOTIFY codeReasonAttributeChanged)

public:
    explicit OrderStatusType(QObject *parent = 0);

    QString status() const;
    QString reason() const;

    QString codeStatusAttribute() const;
    QString codeReasonAttribute() const;

signals:
    void statusChanged(const QString status) const;
    void reasonChanged(const QString reason) const;

    void codeStatusAttributeChanged(const QString codeStatusAttribute) const;
    void codeReasonAttributeChanged(const QString codeReasonAttribute) const;

public slots:
    void setStatus(const QString status);
    void setReason(const QString reason);

    void setCodeStatusAttribute(const QString codeStatusAttribute);
    void setCodeReasonAttribute(const QString codeReasonAttribute);

public:
    void parseElementAttributes(QXmlStreamReader &xmlReader);

private:
    QString m_status;
    QString m_reason;

    QString m_codeStatusAttribute;
    QString m_codeReasonAttribute;
};

#endif // ORDERSTATUSTYPE_H
