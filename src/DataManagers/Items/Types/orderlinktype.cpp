#include "orderlinktype.h"

OrderLinkType::OrderLinkType(QObject *parent) :
    TypeInterface(parent)
{

}

QString OrderLinkType::merchantTransactionReference() const
{
    return m_merchantTransactionReference;
}

QString OrderLinkType::orderUuid() const
{
    return m_orderUuid;
}

void OrderLinkType::setMerchantTransactionReference(const QString merchantTransactionReference)
{
    if (m_merchantTransactionReference == merchantTransactionReference)
        return;

    m_merchantTransactionReference = merchantTransactionReference;
    emit merchantTransactionReferenceChanged(merchantTransactionReference);
}

void OrderLinkType::setOrderUuid(const QString orderUuid)
{
    if (m_orderUuid == orderUuid)
        return;

    m_orderUuid = orderUuid;
    emit orderUuidChanged(orderUuid);
}
