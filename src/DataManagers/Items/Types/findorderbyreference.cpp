#include "findorderbyreference.h"

FindOrderByReference::FindOrderByReference(QObject *parent) :
    TypeInterface(parent)
{

}

QString FindOrderByReference::orderUuid() const
{
    return m_orderUuid;
}

QString FindOrderByReference::merchantTransactionReference() const
{
    return m_merchantTransactionReference;
}

void FindOrderByReference::setOrderUuid(const QString orderUuid)
{
    if (m_orderUuid == orderUuid)
        return;

    m_orderUuid = orderUuid;
    emit orderUuidChanged(orderUuid);
}

void FindOrderByReference::setMerchantTransactionReference(const QString merchantTransactionReference)
{
    if (m_merchantTransactionReference == merchantTransactionReference)
        return;

    m_merchantTransactionReference = merchantTransactionReference;
    emit merchantTransactionReferenceChanged(merchantTransactionReference);
}
