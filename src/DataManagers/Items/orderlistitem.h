#ifndef ORDERLISTITEM_H
#define ORDERLISTITEM_H

#include <QObject>

class OrderListItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString orderDate READ orderDate WRITE setOrderDate NOTIFY orderDateChanged)
    Q_PROPERTY(QString pkId READ pkId WRITE setPkId NOTIFY pkIdChanged)
    Q_PROPERTY(QString trxId READ trxId WRITE setTrxId NOTIFY trxIdChanged)
    Q_PROPERTY(QString orderId READ orderId WRITE setOrderId NOTIFY orderIdChanged)
    Q_PROPERTY(QString billStatus READ billStatus WRITE setBillStatus NOTIFY billStatusChanged)
    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString discount READ discount WRITE setDiscount NOTIFY discountChanged)
    Q_PROPERTY(QString currencyName READ currencyName WRITE setCurrencyName NOTIFY currencyNameChanged)
    Q_PROPERTY(QString cashierName READ cashierName WRITE setCashierName NOTIFY cashierNameChanged)
    Q_PROPERTY(QString orderHour READ orderHour WRITE setOrderHour NOTIFY orderHourChanged)
    Q_PROPERTY(QString summaryValue READ summaryValue WRITE setSummaryValue NOTIFY summaryValueChanged)
    Q_PROPERTY(QString summaryDiscount READ summaryDiscount WRITE setSummaryDiscount NOTIFY summaryDiscountChanged)
    Q_PROPERTY(int twintServerId READ twintServerId WRITE setTwintServerId NOTIFY twintServerIdChanged)
    Q_PROPERTY(bool isSummaryItem READ isSummaryItem WRITE setIsSummaryItem NOTIFY isSummaryItemChanged)

public:
    OrderListItem(QObject *parent = 0);
    OrderListItem(QString orderDate, QString pkId, QString trxId, QString orderId,
                  QString billStatus, QString value, QString discount, QString currencyName,
                  QString cashierName, QString orderHour, int twintServerId, QObject *parent = 0);
    ~OrderListItem();


    QString orderDate() const;
    QString pkId() const;
    QString trxId() const;
    QString billStatus() const;
    QString value() const;
    QString discount() const;
    QString currencyName() const;
    QString cashierName() const;
    QString orderHour() const;
    QString summaryValue() const;
    QString orderId() const;
    bool isSummaryItem() const;
    QString summaryDiscount() const;
    int twintServerId() const;

signals:
    void orderDateChanged(QString orderDate);
    void pkIdChanged(QString pkId);
    void trxIdChanged(QString trxId);
    void billStatusChanged(QString billStatus);
    void valueChanged(QString value);
    void discountChanged(QString discount);
    void currencyNameChanged(QString currencyName);
    void cashierNameChanged(QString cashierName);
    void orderHourChanged(QString orderHour);
    void summaryValueChanged(QString summaryValue);
    void orderIdChanged(QString orderId);
    void isSummaryItemChanged(bool isSummaryItem);
    void summaryDiscountChanged(QString summaryDiscount);
    void twintServerIdChanged(int twintServerId);

public slots:
    void setOrderDate(QString orderDate);
    void setPkId(QString pkId);
    void setTrxId(QString trxId);
    void setBillStatus(QString billStatus);
    void setValue(QString value);
    void setDiscount(QString discount);
    void setCurrencyName(QString currencyName);
    void setCashierName(QString cashierName);
    void setOrderHour(QString orderHour);
    void setSummaryValue(QString summaryValue);
    void setOrderId(QString orderId);
    void setIsSummaryItem(bool isSummaryItem);
    void setSummaryDiscount(QString summaryDiscount);
    void setTwintServerId(int twintServerId);

private:
    QString m_orderDate;
    QString m_pkId;
    QString m_trxId;
    QString m_billStatus;
    QString m_value;
    QString m_discount;
    QString m_currencyName;
    QString m_cashierName;
    QString m_orderHour;
    QString m_summaryValue;
    QString m_orderId;
    bool m_isSummaryItem;
    QString m_summaryDiscount;
    int m_twintServerId;
};

#endif // ORDERLISTITEM_H
