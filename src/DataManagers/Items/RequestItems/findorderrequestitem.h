#ifndef FINDORDERREQUESTITEM_H
#define FINDORDERREQUESTITEM_H

#include "requestiteminterface.h"

class FindOrderByDate;
class FindOrderByReference;

/**
 * \class FindOrderRequestItem
 * \brief Class represents FindOrderRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class FindOrderRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString merchantUuid READ merchantUuid WRITE setMerchantUuid NOTIFY merchantUuidChanged)
    Q_PROPERTY(QString merchantAliasId READ merchantAliasId WRITE setMerchantAliasId NOTIFY merchantAliasIdChanged)
    Q_PROPERTY(QString cashRegisterId READ cashRegisterId WRITE setCashRegisterId NOTIFY cashRegisterIdChanged)
    Q_PROPERTY(FindOrderByDate* searchByDateType READ searchByDateType WRITE setSearchByDateType NOTIFY searchByDateTypeChanged)
    Q_PROPERTY(FindOrderByReference* searchByReferenceType READ searchByReferenceType WRITE setSearchByReferenceType NOTIFY searchByReferenceTypeChanged)

public:
    explicit FindOrderRequestItem(QObject *parent = 0);

    QString merchantUuid() const;
    QString merchantAliasId() const;
    QString cashRegisterId() const;
    FindOrderByDate *searchByDateType() const;
    FindOrderByReference *searchByReferenceType() const;

signals:
    void merchantUuidChanged(const QString merchantUuid) const;
    void merchantAliasIdChanged(const QString merchantAliasId) const;
    void cashRegisterIdChanged(const QString cashRegisterId) const;
    void searchByDateTypeChanged(FindOrderByDate* searchByDateType) const;
    void searchByReferenceTypeChanged(FindOrderByReference* searchByReferenceType) const;

public slots:
    void setMerchantUuid(const QString merchantUuid);
    void setMerchantAliasId(const QString merchantAliasId);
    void setCashRegisterId(const QString cashRegisterId);
    void setSearchByDateType(FindOrderByDate* searchByDateType);
    void setSearchByReferenceType(FindOrderByReference* searchByReferenceType);

private:
    QString m_merchantUuid;
    QString m_merchantAliasId;
    QString m_cashRegisterId;

    FindOrderByDate *m_searchByDate;
    FindOrderByReference *m_searchByReference;
};

#endif // FINDORDERREQUESTITEM_H
