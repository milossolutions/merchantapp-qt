#ifndef REQUESTCHECKINREQUESTITEM_H
#define REQUESTCHECKINREQUESTITEM_H

#include "requestiteminterface.h"

class LoyaltyType;
class MerchantInformationType;

/**
 * \class RequestCheckInRequestItem
 * \brief Class represents RequestCheckInRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class RequestCheckInRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString offlineAuthorization READ offlineAuthorization WRITE setOfflineAuthorization NOTIFY offlineAuthorizationChanged)
    Q_PROPERTY(QString couponCode READ couponCode WRITE setCouponCode NOTIFY couponCodeChanged)
    Q_PROPERTY(QString customerRelationUuid READ customerRelationUuid WRITE setCustomerRelationUuid NOTIFY customerRelationUuidChanged)
    Q_PROPERTY(QString unidentifiedCustomer READ unidentifiedCustomer WRITE setUnidentifiedCustomer NOTIFY unidentifiedCustomerChanged)
    Q_PROPERTY(LoyaltyType* loyaltyInformationType READ loyaltyInformationType WRITE setLoyaltyInformationType NOTIFY loyaltyInformationTypeChanged)
    Q_PROPERTY(QString requestCustomerRelationAlias READ requestCustomerRelationAlias WRITE setRequestCustomerRelationAlias NOTIFY requestCustomerRelationAliasChanged)
    Q_PROPERTY(QString QRCodeRendering READ QRCodeRendering WRITE setQRCodeRendering NOTIFY QRCodeRenderingChanged)

public:
    explicit RequestCheckInRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    QString offlineAuthorization() const;
    QString couponCode() const;
    QString customerRelationUuid() const;
    QString unidentifiedCustomer() const;
    LoyaltyType *loyaltyInformationType() const;
    QString requestCustomerRelationAlias() const;
    QString QRCodeRendering() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void offlineAuthorizationChanged(const QString offlineAuthorization) const;
    void couponCodeChanged(const QString couponCode) const;
    void customerRelationUuidChanged(const QString customerRelationUuid) const;
    void unidentifiedCustomerChanged(const QString unidentifiedCustomer) const;
    void loyaltyInformationTypeChanged(LoyaltyType* loyaltyInformationType) const;
    void requestCustomerRelationAliasChanged(const QString requestCustomerRelationAlias) const;
    void QRCodeRenderingChanged(const QString QRCodeRendering) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setOfflineAuthorization(const QString offlineAuthorization);
    void setCouponCode(const QString couponCode);
    void setCustomerRelationUuid(const QString customerRelationUuid);
    void setUnidentifiedCustomer(const QString unidentifiedCustomer);
    void setLoyaltyInformationType(LoyaltyType* loyaltyInformationType);
    void setRequestCustomerRelationAlias(const QString requestCustomerRelationAlias);
    void setQRCodeRendering(const QString QRCodeRendering);

private:
    QString m_offlineAuthorization;
    QString m_couponCode;
    QString m_customerRelationUuid;
    QString m_unidentifiedCustomer;
    QString m_requestCustomerRelationAlias;
    QString m_QRCodeRendering;

    LoyaltyType *m_loyaltyInformation;
    MerchantInformationType *m_merchantInformation;
};

#endif // REQUESTCHECKINREQUESTITEM_H
