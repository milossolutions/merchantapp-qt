#ifndef STARTORDERREQUESTITEM_H
#define STARTORDERREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;
class CouponListType;
class OrderRequestType;

/**
 * \class StartOrderRequestItem
 * \brief Class represents StartOrderRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class StartOrderRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(OrderRequestType* orderType READ orderType WRITE setOrderType NOTIFY orderTypeChanged)
    Q_PROPERTY(CouponListType* couponsType READ couponsType WRITE setCouponsType NOTIFY couponsTypeChanged)
    Q_PROPERTY(QString offlineAuthorization READ offlineAuthorization WRITE setOfflineAuthorization NOTIFY offlineAuthorizationChanged)
    Q_PROPERTY(QString pairingUuid READ pairingUuid WRITE setPairingUuid NOTIFY pairingUuidChanged)
    Q_PROPERTY(QString customerRelationUuid READ customerRelationUuid WRITE setCustomerRelationUuid NOTIFY customerRelationUuidChanged)
    Q_PROPERTY(QString unidentifiedCustomer READ unidentifiedCustomer WRITE setUnidentifiedCustomer NOTIFY unidentifiedCustomerChanged)
    Q_PROPERTY(QString QRCodeRendering READ QRCodeRendering WRITE setQRCodeRendering NOTIFY QRCodeRenderingChanged)

public:
    explicit StartOrderRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    OrderRequestType *orderType() const;
    CouponListType *couponsType() const;
    QString offlineAuthorization() const;
    QString pairingUuid() const;
    QString customerRelationUuid() const;
    QString unidentifiedCustomer() const;
    QString QRCodeRendering() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void orderTypeChanged(OrderRequestType *orderType) const;
    void couponsTypeChanged(CouponListType *couponsType) const;
    void offlineAuthorizationChanged(const QString offlineAuthorization) const;
    void pairingUuidChanged(const QString pairingUuid) const;
    void customerRelationUuidChanged(const QString customerRelationUuid) const;
    void unidentifiedCustomerChanged(const QString unidentifiedCustomer) const;
    void QRCodeRenderingChanged(const QString QRCodeRendering) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setOrderType(OrderRequestType *orderType);
    void setCouponsType(CouponListType *couponsType);
    void setOfflineAuthorization(const QString offlineAuthorization);
    void setPairingUuid(const QString pairingUuid);
    void setCustomerRelationUuid(const QString customerRelationUuid);
    void setUnidentifiedCustomer(const QString unidentifiedCustomer);
    void setQRCodeRendering(const QString QRCodeRendering);

private:
    QString m_offlineAuthorization;
    QString m_pairingUuid;
    QString m_customerRelationUuid;
    QString m_unidentifiedCustomer;
    QString m_QRCodeRendering;

    MerchantInformationType *m_merchantInformation;
    OrderRequestType *m_order;
    CouponListType *m_coupons;
};

#endif // STARTORDERREQUESTITEM_H
