#include "findorderrequestitem.h"

#include "findorderbydate.h"
#include "findorderbyreference.h"

FindOrderRequestItem::FindOrderRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_searchByDate(new FindOrderByDate(this)),
    m_searchByReference(new FindOrderByReference(this))
{
    setObjectName("FindOrderRequestElement");
    m_searchByDate->setObjectName("SearchByDate");
    m_searchByReference->setObjectName("SearchByReference");
}

QString FindOrderRequestItem::merchantUuid() const
{
    return m_merchantUuid;
}

QString FindOrderRequestItem::merchantAliasId() const
{
    return m_merchantAliasId;
}

QString FindOrderRequestItem::cashRegisterId() const
{
    return m_cashRegisterId;
}

FindOrderByDate *FindOrderRequestItem::searchByDateType() const
{
    return m_searchByDate;
}

FindOrderByReference *FindOrderRequestItem::searchByReferenceType() const
{
    return m_searchByReference;
}

void FindOrderRequestItem::setMerchantUuid(const QString merchantUuid)
{
    if (m_merchantUuid == merchantUuid)
        return;

    m_merchantUuid = merchantUuid;
    emit merchantUuidChanged(merchantUuid);
}

void FindOrderRequestItem::setMerchantAliasId(const QString merchantAliasId)
{
    if (m_merchantAliasId == merchantAliasId)
        return;

    m_merchantAliasId = merchantAliasId;
    emit merchantAliasIdChanged(merchantAliasId);
}

void FindOrderRequestItem::setCashRegisterId(const QString cashRegisterId)
{
    if (m_cashRegisterId == cashRegisterId)
        return;

    m_cashRegisterId = cashRegisterId;
    emit cashRegisterIdChanged(cashRegisterId);
}

void FindOrderRequestItem::setSearchByDateType(FindOrderByDate *searchByDateType)
{
    if (m_searchByDate == searchByDateType)
        return;

    m_searchByDate = searchByDateType;
    emit searchByDateTypeChanged(searchByDateType);
}

void FindOrderRequestItem::setSearchByReferenceType(FindOrderByReference *searchByReferenceType)
{
    if (m_searchByReference == searchByReferenceType)
        return;

    m_searchByReference = searchByReferenceType;
    emit searchByReferenceTypeChanged(searchByReferenceType);
}
