#ifndef ENROLLCASHREGISTERREQUESTITEM_H
#define ENROLLCASHREGISTERREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;

/**
 * \class EnrollCashRegisterRequestItem
 * @brief Class represents EnrollCashRegisterRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class EnrollCashRegisterRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString cashRegisterType READ cashRegisterType WRITE setCashRegisterType NOTIFY cashRegisterTypeChanged)
    Q_PROPERTY(QString formerCashRegisterId READ formerCashRegisterId WRITE setFormerCashRegisterId NOTIFY formerCashRegisterIdChanged)
    Q_PROPERTY(QString beaconInventoryNumber READ beaconInventoryNumber WRITE setBeaconInventoryNumber NOTIFY beaconInventoryNumberChanged)
    Q_PROPERTY(QString beaconDaemonVersion READ beaconDaemonVersion WRITE setBeaconDaemonVersion NOTIFY beaconDaemonVersionChanged)

public:
    explicit EnrollCashRegisterRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    QString cashRegisterType() const;
    QString formerCashRegisterId() const;
    QString beaconInventoryNumber() const;
    QString beaconDaemonVersion() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void cashRegisterTypeChanged(const QString cashRegisterType) const;
    void formerCashRegisterIdChanged(const QString formerCashRegisterId) const;
    void beaconInventoryNumberChanged(const QString beaconInventoryNumber) const;
    void beaconDaemonVersionChanged(const QString beaconDaemonVersion) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setCashRegisterType(const QString cashRegisterType);
    void setFormerCashRegisterId(const QString formerCashRegisterId);
    void setBeaconInventoryNumber(const QString beaconInventoryNumber);
    void setBeaconDaemonVersion(const QString beaconDaemonVersion);

private:
    QString m_cashRegisterType;
    QString m_formerCashRegisterId;
    QString m_beaconInventoryNumber;
    QString m_beaconDaemonVersion;

    MerchantInformationType *m_merchantInformation;
};

#endif // ENROLLCASHREGISTERREQUESTITEM_H
