#ifndef CONFIRMORDERREQUESTITEM_H
#define CONFIRMORDERREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;
class CurrencyAmountType;

/**
 * \class CheckSystemStatusRequestItem
 * \brief Class represents CheckSystemStatusRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class ConfirmOrderRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString orderUuid READ orderUuid WRITE setOrderUuid NOTIFY orderUuidChanged)
    Q_PROPERTY(QString merchantTransactionReference READ merchantTransactionReference WRITE setMerchantTransactionReference NOTIFY merchantTransactionReferenceChanged)
    Q_PROPERTY(CurrencyAmountType* requestedAmountType READ requestedAmountType WRITE setRequestedAmountType NOTIFY requestedAmountTypeChanged)
    Q_PROPERTY(QString partialConfirmation READ partialConfirmation WRITE setPartialConfirmation NOTIFY partialConfirmationChanged)

public:
    explicit ConfirmOrderRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    QString orderUuid() const;
    QString merchantTransactionReference() const;
    CurrencyAmountType *requestedAmountType() const;
    QString partialConfirmation() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType *merchantInformationType) const;
    void orderUuidChanged(const QString orderUuid) const;
    void merchantTransactionReferenceChanged(const QString merchantTransactionReference) const;
    void requestedAmountTypeChanged(CurrencyAmountType* requestedAmountType) const;
    void partialConfirmationChanged(const QString partialConfirmation) const;

public slots:
    void setMerchantInformationType(MerchantInformationType *merchantInformationType);
    void setOrderUuid(const QString orderUuid);
    void setMerchantTransactionReference(const QString merchantTransactionReference);
    void setRequestedAmountType(CurrencyAmountType* requestedAmountType);
    void setPartialConfirmation(const QString partialConfirmation);

private:
    QString m_orderUuid;
    QString m_merchantTransactionReference;
    QString m_partialConfirmation;

    MerchantInformationType *m_merchantInformation;
    CurrencyAmountType *m_requestedAmount;
};

#endif // CONFIRMORDERREQUESTITEM_H
