#ifndef CANCELORDERREQUESTITEM_H
#define CANCELORDERREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;

/**
 * \class CancelOrderRequestItem
 * \brief Class represents CancelOrderRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class CancelOrderRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString orderUuid READ orderUuid WRITE setOrderUuid NOTIFY orderUuidChanged)
    Q_PROPERTY(QString merchantTransactionReference READ merchantTransactionReference WRITE setMerchantTransactionReference NOTIFY merchantTransactionReferenceChanged)

public:
    explicit CancelOrderRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    QString orderUuid() const;
    QString merchantTransactionReference() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void orderUuidChanged(const QString orderUuid) const;
    void merchantTransactionReferenceChanged(const QString merchantTransactionReference) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setOrderUuid(const QString orderUuid);
    void setMerchantTransactionReference(const QString merchantTransactionReference);

private:
    QString m_orderUuid;
    QString m_merchantTransactionReference;

    MerchantInformationType *m_merchantInformation;
};

#endif // CANCELORDERREQUESTITEM_H
