#ifndef REQUESTITEMINTERFACE_H
#define REQUESTITEMINTERFACE_H

#include "iteminterface.h"

/**
 * \class RequestItemInterface
 * @brief Objects inheriting after RequestItemInterface contains different members
 * which are then used in preparing soap message body structure.
 */

class RequestItemInterface : public ItemInterface
{
    Q_OBJECT
public:
    explicit RequestItemInterface(QObject *parent = 0);

signals:

public slots:
};

#endif // REQUESTITEMINTERFACE_H
