#include "requestcheckinrequestitem.h"

#include "loyaltytype.h"
#include "merchantinformationtype.h"

RequestCheckInRequestItem::RequestCheckInRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_loyaltyInformation(new LoyaltyType(this)),
    m_merchantInformation(new MerchantInformationType(this))
{
    setObjectName("RequestCheckInRequestElement");
    m_loyaltyInformation->setObjectName("LoyaltyInformation");
    m_merchantInformation->setObjectName("MerchantInformation");
}

QString RequestCheckInRequestItem::offlineAuthorization() const
{
    return m_offlineAuthorization;
}

QString RequestCheckInRequestItem::couponCode() const
{
    return m_couponCode;
}

QString RequestCheckInRequestItem::customerRelationUuid() const
{
    return m_customerRelationUuid;
}

QString RequestCheckInRequestItem::unidentifiedCustomer() const
{
    return m_unidentifiedCustomer;
}

QString RequestCheckInRequestItem::requestCustomerRelationAlias() const
{
    return m_requestCustomerRelationAlias;
}

QString RequestCheckInRequestItem::QRCodeRendering() const
{
    return m_QRCodeRendering;
}

void RequestCheckInRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

LoyaltyType *RequestCheckInRequestItem::loyaltyInformationType() const
{
    return m_loyaltyInformation;
}

MerchantInformationType *RequestCheckInRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

void RequestCheckInRequestItem::setOfflineAuthorization(const QString offlineAuthorization)
{
    if (m_offlineAuthorization == offlineAuthorization)
        return;

    m_offlineAuthorization = offlineAuthorization;
    emit offlineAuthorizationChanged(offlineAuthorization);
}

void RequestCheckInRequestItem::setCouponCode(const QString couponCode)
{
    if (m_couponCode == couponCode)
        return;

    m_couponCode = couponCode;
    emit couponCodeChanged(couponCode);
}

void RequestCheckInRequestItem::setCustomerRelationUuid(const QString customerRelationUuid)
{
    if (m_customerRelationUuid == customerRelationUuid)
        return;

    m_customerRelationUuid = customerRelationUuid;
    emit customerRelationUuidChanged(customerRelationUuid);
}

void RequestCheckInRequestItem::setUnidentifiedCustomer(const QString unidentifiedCustomer)
{
    if (m_unidentifiedCustomer == unidentifiedCustomer)
        return;

    m_unidentifiedCustomer = unidentifiedCustomer;
    emit unidentifiedCustomerChanged(unidentifiedCustomer);
}

void RequestCheckInRequestItem::setLoyaltyInformationType(LoyaltyType *loyaltyInformationType)
{
    if (m_loyaltyInformation == loyaltyInformationType)
        return;

    m_loyaltyInformation = loyaltyInformationType;
    emit loyaltyInformationTypeChanged(loyaltyInformationType);
}

void RequestCheckInRequestItem::setRequestCustomerRelationAlias(const QString requestCustomerRelationAlias)
{
    if (m_requestCustomerRelationAlias == requestCustomerRelationAlias)
        return;

    m_requestCustomerRelationAlias = requestCustomerRelationAlias;
    emit requestCustomerRelationAliasChanged(requestCustomerRelationAlias);
}

void RequestCheckInRequestItem::setQRCodeRendering(const QString QRCodeRendering)
{
    if (m_QRCodeRendering == QRCodeRendering)
        return;

    m_QRCodeRendering = QRCodeRendering;
    emit QRCodeRenderingChanged(QRCodeRendering);
}
