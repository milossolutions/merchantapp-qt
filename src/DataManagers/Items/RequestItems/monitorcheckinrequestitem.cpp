#include "monitorcheckinrequestitem.h"

#include "merchantinformationtype.h"

MonitorCheckInRequestItem::MonitorCheckInRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this))
{
    setObjectName("MonitorCheckInRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
}

MerchantInformationType *MonitorCheckInRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

QString MonitorCheckInRequestItem::customerRelationUuid() const
{
    return m_customerRelationUuid;
}

QString MonitorCheckInRequestItem::pairingUuid() const
{
    return m_pairingUuid;
}

QString MonitorCheckInRequestItem::waitForResponse() const
{
    return m_waitForResponse;
}

void MonitorCheckInRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void MonitorCheckInRequestItem::setCustomerRelationUuid(const QString customerRelationUuid)
{
    if (m_customerRelationUuid == customerRelationUuid)
        return;

    m_customerRelationUuid = customerRelationUuid;
    emit customerRelationUuidChanged(customerRelationUuid);
}

void MonitorCheckInRequestItem::setPairingUuid(const QString pairingUuid)
{
    if (m_pairingUuid == pairingUuid)
        return;

    m_pairingUuid = pairingUuid;
    emit pairingUuidChanged(pairingUuid);
}

void MonitorCheckInRequestItem::setWaitForResponse(const QString waitForResponse)
{
    if (m_waitForResponse == waitForResponse)
        return;

    m_waitForResponse = waitForResponse;
    emit waitForResponseChanged(waitForResponse);
}
