#include "checksystemstatusrequestitem.h"

CheckSystemStatusRequestItem::CheckSystemStatusRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this))
{
    setObjectName("CheckSystemStatusRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
}

MerchantInformationType *CheckSystemStatusRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

void CheckSystemStatusRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}
