#include "confirmorderrequestitem.h"

#include "merchantinformationtype.h"
#include "currencyamounttype.h"

ConfirmOrderRequestItem::ConfirmOrderRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this)),
    m_requestedAmount(new CurrencyAmountType(this))
{
    setObjectName("ConfirmOrderRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
    m_requestedAmount->setObjectName("RequestedAmount");
}

MerchantInformationType *ConfirmOrderRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

QString ConfirmOrderRequestItem::orderUuid() const
{
    return m_orderUuid;
}

QString ConfirmOrderRequestItem::merchantTransactionReference() const
{
    return m_merchantTransactionReference;
}

CurrencyAmountType *ConfirmOrderRequestItem::requestedAmountType() const
{
    return m_requestedAmount;
}

QString ConfirmOrderRequestItem::partialConfirmation() const
{
    return m_partialConfirmation;
}

void ConfirmOrderRequestItem::setMerchantInformationType(MerchantInformationType* merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void ConfirmOrderRequestItem::setOrderUuid(const QString orderUuid)
{
    if (m_orderUuid == orderUuid)
        return;

    m_orderUuid = orderUuid;
    emit orderUuidChanged(orderUuid);
}

void ConfirmOrderRequestItem::setMerchantTransactionReference(const QString merchantTransactionReference)
{
    if (m_merchantTransactionReference == merchantTransactionReference)
        return;

    m_merchantTransactionReference = merchantTransactionReference;
    emit merchantTransactionReferenceChanged(merchantTransactionReference);
}

void ConfirmOrderRequestItem::setRequestedAmountType(CurrencyAmountType *requestedAmountType)
{
    if (m_requestedAmount == requestedAmountType)
        return;

    m_requestedAmount = requestedAmountType;
    emit requestedAmountTypeChanged(requestedAmountType);
}

void ConfirmOrderRequestItem::setPartialConfirmation(const QString partialConfirmation)
{
    if (m_partialConfirmation == partialConfirmation)
        return;

    m_partialConfirmation = partialConfirmation;
    emit partialConfirmationChanged(partialConfirmation);
}
