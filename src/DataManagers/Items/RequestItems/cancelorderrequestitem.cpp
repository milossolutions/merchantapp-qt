#include "cancelorderrequestitem.h"

#include "merchantinformationtype.h"

CancelOrderRequestItem::CancelOrderRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this))
{
    setObjectName("CancelOrderRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
}

MerchantInformationType *CancelOrderRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

QString CancelOrderRequestItem::orderUuid() const
{
    return m_orderUuid;
}

QString CancelOrderRequestItem::merchantTransactionReference() const
{
    return m_merchantTransactionReference;
}

void CancelOrderRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void CancelOrderRequestItem::setOrderUuid(const QString orderUuid)
{
    if (m_orderUuid == orderUuid)
        return;

    m_orderUuid = orderUuid;
    emit orderUuidChanged(orderUuid);
}

void CancelOrderRequestItem::setMerchantTransactionReference(const QString merchantTransactionReference)
{
    if (m_merchantTransactionReference == merchantTransactionReference)
        return;

    m_merchantTransactionReference = merchantTransactionReference;
    emit merchantTransactionReferenceChanged(merchantTransactionReference);
}
