#include "monitororderrequestitem.h"

#include "merchantinformationtype.h"

MonitorOrderRequestItem::MonitorOrderRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this))
{
    setObjectName("MonitorOrderRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
}

MerchantInformationType *MonitorOrderRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

QString MonitorOrderRequestItem::orderUuid() const
{
    return m_orderUuid;
}

QString MonitorOrderRequestItem::merchantTransactionReference() const
{
    return m_merchantTransactionReference;
}

QString MonitorOrderRequestItem::waitForResponse() const
{
    return m_waitForResponse;
}

void MonitorOrderRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void MonitorOrderRequestItem::setOrderUuid(const QString orderUuid)
{
    if (m_orderUuid == orderUuid)
        return;

    m_orderUuid = orderUuid;
    emit orderUuidChanged(orderUuid);
}

void MonitorOrderRequestItem::setMerchantTransactionReference(const QString merchantTransactionReference)
{
    if (m_merchantTransactionReference == merchantTransactionReference)
        return;

    m_merchantTransactionReference = merchantTransactionReference;
    emit merchantTransactionReferenceChanged(merchantTransactionReference);
}

void MonitorOrderRequestItem::setWaitForResponse(const QString waitForResponse)
{
    if (m_waitForResponse == waitForResponse)
        return;

    m_waitForResponse = waitForResponse;
    emit waitForResponseChanged(waitForResponse);
}
