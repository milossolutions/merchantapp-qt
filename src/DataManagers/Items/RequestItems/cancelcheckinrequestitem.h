#ifndef CANCELCHECKINREQUESTITEM_H
#define CANCELCHECKINREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;
class CouponListType;

/**
 * \class CancelCheckInRequestItem
 * \brief Class represents CancelCheckInRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class CancelCheckInRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString reason READ reason WRITE setReason NOTIFY reasonChanged)
    Q_PROPERTY(QString customerRelationUuid READ customerRelationUuid WRITE setCustomerRelationUuid NOTIFY customerRelationUuidChanged)
    Q_PROPERTY(QString pairingUuid READ pairingUuid WRITE setPairingUuid NOTIFY pairingUuidChanged)
    Q_PROPERTY(CouponListType* couponsType READ couponsType WRITE setCouponsType NOTIFY couponsTypeChanged)

public:
    explicit CancelCheckInRequestItem(QObject *parent = 0);

    MerchantInformationType* merchantInformationType() const;
    QString reason() const;
    QString customerRelationUuid() const;
    QString pairingUuid() const;
    CouponListType *couponsType() const;

signals:
    void merchantInformationTypeChanged(const MerchantInformationType *merchantInformationType) const;
    void reasonChanged(const QString reason) const;
    void customerRelationUuidChanged(const QString customerRelationUuid) const;
    void pairingUuidChanged(const QString pairingUuid) const;
    void couponsTypeChanged(CouponListType* couponsType) const;

public slots:
    void setMerchantInformationType(MerchantInformationType *merchantInformation);
    void setReason(const QString reason);
    void setCustomerRelationUuid(const QString customerRelationUuid);
    void setPairingUuid(const QString pairingUuid);
    void setCouponsType(CouponListType* couponsType);

private:
    QString m_reason;
    QString m_customerRelationUuid;
    QString m_pairingUuid;

    MerchantInformationType *m_merchantInformation;
    CouponListType *m_coupons;
};

#endif // CANCELCHECKINREQUESTITEM_H
