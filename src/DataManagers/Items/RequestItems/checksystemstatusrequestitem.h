#ifndef CHECKSYSTEMSTATUSREQUESTITEM_H
#define CHECKSYSTEMSTATUSREQUESTITEM_H

#include "requestiteminterface.h"

#include "merchantinformationtype.h"
class MerchantInformationType;

/**
 * \class CheckSystemStatusRequestItem
 * @brief Class represents CheckSystemStatusRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class CheckSystemStatusRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)

public:
    explicit CheckSystemStatusRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);

private:
    MerchantInformationType *m_merchantInformation;
};

#endif // CHECKSYSTEMSTATUSREQUESTITEM_H
