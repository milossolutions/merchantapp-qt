#ifndef MONITORORDERREQUESTITEM_H
#define MONITORORDERREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;

/**
 * \class MonitorOrderRequestItem
 * \brief Class represents MonitorOrderRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class MonitorOrderRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString orderUuid READ orderUuid WRITE setOrderUuid NOTIFY orderUuidChanged)
    Q_PROPERTY(QString merchantTransactionReference READ merchantTransactionReference WRITE setMerchantTransactionReference NOTIFY merchantTransactionReferenceChanged)
    Q_PROPERTY(QString waitForResponse READ waitForResponse WRITE setWaitForResponse NOTIFY waitForResponseChanged)

public:
    explicit MonitorOrderRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    QString orderUuid() const;
    QString merchantTransactionReference() const;
    QString waitForResponse() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void orderUuidChanged(const QString orderUuid) const;
    void merchantTransactionReferenceChanged(const QString merchantTransactionReference) const;
    void waitForResponseChanged(const QString waitForResponse) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setOrderUuid(const QString orderUuid);
    void setMerchantTransactionReference(const QString merchantTransactionReference);
    void setWaitForResponse(const QString waitForResponse);

private:
    QString m_orderUuid;
    QString m_merchantTransactionReference;
    QString m_waitForResponse;

    MerchantInformationType *m_merchantInformation;
};

#endif // MONITORORDERREQUESTITEM_H
