#include "enrollcashregisterrequestitem.h"

#include "merchantinformationtype.h"

EnrollCashRegisterRequestItem::EnrollCashRegisterRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this))
{
    setObjectName("EnrollCashRegisterRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
}

MerchantInformationType *EnrollCashRegisterRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

QString EnrollCashRegisterRequestItem::cashRegisterType() const
{
    return m_cashRegisterType;
}

QString EnrollCashRegisterRequestItem::formerCashRegisterId() const
{
    return m_formerCashRegisterId;
}

QString EnrollCashRegisterRequestItem::beaconInventoryNumber() const
{
    return m_beaconInventoryNumber;
}

QString EnrollCashRegisterRequestItem::beaconDaemonVersion() const
{
    return m_beaconDaemonVersion;
}

void EnrollCashRegisterRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void EnrollCashRegisterRequestItem::setCashRegisterType(const QString cashRegisterType)
{
    if (m_cashRegisterType == cashRegisterType)
        return;

    m_cashRegisterType = cashRegisterType;
    emit cashRegisterTypeChanged(cashRegisterType);
}

void EnrollCashRegisterRequestItem::setFormerCashRegisterId(const QString formerCashRegisterId)
{
    if (m_formerCashRegisterId == formerCashRegisterId)
        return;

    m_formerCashRegisterId = formerCashRegisterId;
    emit formerCashRegisterIdChanged(formerCashRegisterId);
}

void EnrollCashRegisterRequestItem::setBeaconInventoryNumber(const QString beaconInventoryNumber)
{
    if (m_beaconInventoryNumber == beaconInventoryNumber)
        return;

    m_beaconInventoryNumber = beaconInventoryNumber;
    emit beaconInventoryNumberChanged(beaconInventoryNumber);
}

void EnrollCashRegisterRequestItem::setBeaconDaemonVersion(const QString beaconDaemonVersion)
{
    if (m_beaconDaemonVersion == beaconDaemonVersion)
        return;

    m_beaconDaemonVersion = beaconDaemonVersion;
    emit beaconDaemonVersionChanged(beaconDaemonVersion);
}
