#ifndef MONITORCHECKINREQUESTITEM_H
#define MONITORCHECKINREQUESTITEM_H

#include "requestiteminterface.h"

class MerchantInformationType;

/**
 * \class MonitorCheckInRequestItem
 * \brief Class represents MonitorCheckInRequestElement, which is used to create a structure of soap message that is send to the server.
 */

class MonitorCheckInRequestItem : public RequestItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(QString customerRelationUuid READ customerRelationUuid WRITE setCustomerRelationUuid NOTIFY customerRelationUuidChanged)
    Q_PROPERTY(QString pairingUuid READ pairingUuid WRITE setPairingUuid NOTIFY pairingUuidChanged)
    Q_PROPERTY(QString waitForResponse READ waitForResponse WRITE setWaitForResponse NOTIFY waitForResponseChanged)

public:
    explicit MonitorCheckInRequestItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    QString customerRelationUuid() const;
    QString pairingUuid() const;
    QString waitForResponse() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void customerRelationUuidChanged(const QString customerRelationUuid) const;
    void pairingUuidChanged(const QString pairingUuid) const;
    void waitForResponseChanged(const QString waitForResponse) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setCustomerRelationUuid(const QString customerRelationUuid);
    void setPairingUuid(const QString pairingUuid);
    void setWaitForResponse(const QString waitForResponse);

private:
    QString m_customerRelationUuid;
    QString m_pairingUuid;
    QString m_waitForResponse;

    MerchantInformationType *m_merchantInformation;
};

#endif // MONITORCHECKINREQUESTITEM_H
