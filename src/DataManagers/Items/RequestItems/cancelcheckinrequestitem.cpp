#include "cancelcheckinrequestitem.h"

#include "merchantinformationtype.h"
#include "couponlisttype.h"

CancelCheckInRequestItem::CancelCheckInRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this)),
    m_coupons(new CouponListType(this))
{
    setObjectName("CancelCheckInRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
    m_coupons->setObjectName("Coupons");
}

MerchantInformationType *CancelCheckInRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

QString CancelCheckInRequestItem::reason() const
{
    return m_reason;
}

QString CancelCheckInRequestItem::customerRelationUuid() const
{
    return m_customerRelationUuid;
}

QString CancelCheckInRequestItem::pairingUuid() const
{
    return m_pairingUuid;
}

CouponListType *CancelCheckInRequestItem::couponsType() const
{
    return m_coupons;
}

void CancelCheckInRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void CancelCheckInRequestItem::setReason(const QString reason)
{
    if (m_reason == reason)
        return;

    m_reason = reason;
    emit reasonChanged(reason);
}

void CancelCheckInRequestItem::setCustomerRelationUuid(const QString customerRelationUuid)
{
    if (m_customerRelationUuid == customerRelationUuid)
        return;

    m_customerRelationUuid = customerRelationUuid;
    emit customerRelationUuidChanged(customerRelationUuid);
}

void CancelCheckInRequestItem::setPairingUuid(const QString pairingUuid)
{
    if (m_pairingUuid == pairingUuid)
        return;

    m_pairingUuid = pairingUuid;
    emit pairingUuidChanged(pairingUuid);
}

void CancelCheckInRequestItem::setCouponsType(CouponListType *couponsType)
{
    if (m_coupons == couponsType)
        return;

    m_coupons = couponsType;
    emit couponsTypeChanged(couponsType);
}
