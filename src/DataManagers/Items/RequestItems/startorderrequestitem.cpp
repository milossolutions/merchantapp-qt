#include "startorderrequestitem.h"

#include "merchantinformationtype.h"
#include "couponlisttype.h"
#include "orderrequesttype.h"

StartOrderRequestItem::StartOrderRequestItem(QObject *parent) :
    RequestItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this)),
    m_order(new OrderRequestType(this)),
    m_coupons(new CouponListType(this))
{
    setObjectName("StartOrderRequestElement");
    m_merchantInformation->setObjectName("MerchantInformation");
    m_order->setObjectName("Order");
    m_coupons->setObjectName("Coupons");
}

MerchantInformationType *StartOrderRequestItem::merchantInformationType() const
{
    return m_merchantInformation;
}

OrderRequestType *StartOrderRequestItem::orderType() const
{
    return m_order;
}

CouponListType *StartOrderRequestItem::couponsType() const
{
    return m_coupons;
}

QString StartOrderRequestItem::offlineAuthorization() const
{
    return m_offlineAuthorization;
}

QString StartOrderRequestItem::pairingUuid() const
{
    return m_pairingUuid;
}

QString StartOrderRequestItem::customerRelationUuid() const
{
    return m_customerRelationUuid;
}

QString StartOrderRequestItem::unidentifiedCustomer() const
{
    return m_unidentifiedCustomer;
}

QString StartOrderRequestItem::QRCodeRendering() const
{
    return m_QRCodeRendering;
}


void StartOrderRequestItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void StartOrderRequestItem::setOrderType(OrderRequestType *orderType)
{
    if (m_order == orderType)
        return;

    m_order = orderType;
    emit orderTypeChanged(orderType);
}

void StartOrderRequestItem::setCouponsType(CouponListType *couponsType)
{
    if (m_coupons == couponsType)
        return;

    m_coupons = couponsType;
    emit couponsTypeChanged(couponsType);
}

void StartOrderRequestItem::setOfflineAuthorization(const QString offlineAuthorization)
{
    if (m_offlineAuthorization == offlineAuthorization)
        return;

    m_offlineAuthorization = offlineAuthorization;
    emit offlineAuthorizationChanged(offlineAuthorization);
}

void StartOrderRequestItem::setPairingUuid(const QString pairingUuid)
{
    if (m_pairingUuid == pairingUuid)
        return;

    m_pairingUuid = pairingUuid;
    emit pairingUuidChanged(pairingUuid);
}

void StartOrderRequestItem::setCustomerRelationUuid(const QString customerRelationUuid)
{
    if (m_customerRelationUuid == customerRelationUuid)
        return;

    m_customerRelationUuid = customerRelationUuid;
    emit customerRelationUuidChanged(customerRelationUuid);
}

void StartOrderRequestItem::setUnidentifiedCustomer(const QString unidentifiedCustomer)
{
    if (m_unidentifiedCustomer == unidentifiedCustomer)
        return;

    m_unidentifiedCustomer = unidentifiedCustomer;
    emit unidentifiedCustomerChanged(unidentifiedCustomer);
}

void StartOrderRequestItem::setQRCodeRendering(const QString QRCodeRendering)
{
    if (m_QRCodeRendering == QRCodeRendering)
        return;

    m_QRCodeRendering = QRCodeRendering;
    emit QRCodeRenderingChanged(QRCodeRendering);
}
