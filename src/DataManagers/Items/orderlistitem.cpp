#include "orderlistitem.h"

/*!
 * \class OrderListItem
 * \brief Object contains data about single transaction. Used in NewOrderListModel
 */
OrderListItem::OrderListItem(QObject *parent) :
    QObject(parent),
    m_orderDate(QString::null),
    m_pkId(QString::null),
    m_trxId(QString::null),
    m_orderId(QString::null),
    m_billStatus(QString::null),
    m_value(QString::null),
    m_discount(QString::null),
    m_currencyName(QString::null),
    m_cashierName(QString::null),
    m_orderHour(QString::null),
    m_summaryDiscount(QString::null),
    m_summaryValue(QString::null),
    m_isSummaryItem(false)
{

}

OrderListItem::OrderListItem(QString orderDate, QString pkId, QString trxId, QString orderId,
                             QString billStatus, QString value, QString discount, QString currencyName,
                             QString cashierName, QString orderHour, int twintServerId, QObject *parent) :
    QObject(parent),
    m_orderDate(orderDate),
    m_pkId(pkId),
    m_trxId(trxId),
    m_orderId(orderId),
    m_billStatus(billStatus),
    m_value(value),
    m_discount(discount),
    m_currencyName(currencyName),
    m_cashierName(cashierName),
    m_orderHour(orderHour),
    m_summaryDiscount(QString::number(0)),
    m_summaryValue(QString::number(0)),
    m_isSummaryItem(false),
    m_twintServerId(twintServerId)
{

}

OrderListItem::~OrderListItem()
{

}

/*!
 * \brief When order was completed
 * \return
 */
QString OrderListItem::orderDate() const
{
    return m_orderDate;
}

/*!
 * \brief Pk id value. Currently unused.
 * \return
 */
QString OrderListItem::pkId() const
{
    return m_pkId;
}

/*!
 * \brief TRX Id value. Currently unused.
 * \return
 */
QString OrderListItem::trxId() const
{
    return m_trxId;
}

/*!
 * \brief Value of GlobalSettings::Enums::OrderStatus
 * \return
 */
QString OrderListItem::billStatus() const
{
    return m_billStatus;
}

/*!
 * \brief Transaction value
 * \return
 */
QString OrderListItem::value() const
{
    return m_value;
}

/*!
 * \brief Sum of discount from used coupons
 * \return
 */
QString OrderListItem::discount() const
{
    return m_discount;
}

/*!
 * \brief Name of currency used in transaction
 * \return
 */
QString OrderListItem::currencyName() const
{
    return m_currencyName;
}

/*!
 * \brief Name of cashier that finalized transaction. Currently unused
 * \return
 */
QString OrderListItem::cashierName() const
{
    return m_cashierName;
}

/*!
 * \brief Hour of completed tranaction
 * \return
 */
QString OrderListItem::orderHour() const
{
    return m_orderHour;
}

/*!
 * \brief Summary of Orders Value from one day. Used in summary item delegate type.
 * \return
 */
QString OrderListItem::summaryValue() const
{
    return m_summaryValue;
}

/*!
 * \brief Order id
 * \return
 */
QString OrderListItem::orderId() const
{
    return m_orderId;
}

/*!
 * \brief Type of item.
 * Summary delegates are used to show sum of orders values on daily basis.
 * \return
 */
bool OrderListItem::isSummaryItem() const
{
    return m_isSummaryItem;
}

/*!
 * \brief Summary of orders discount. Used in Summary delegate.
 * \return
 */
QString OrderListItem::summaryDiscount() const
{
    return m_summaryDiscount;
}

/*!
 * \brief Server id. Points to which server tranaction was made on.
 * \return
 */
int OrderListItem::twintServerId() const
{
    return m_twintServerId;
}

void OrderListItem::setOrderDate(QString orderDate)
{
    if (m_orderDate == orderDate)
        return;

    m_orderDate = orderDate;
    emit orderDateChanged(orderDate);
}

void OrderListItem::setPkId(QString pkId)
{
    if (m_pkId == pkId)
        return;

    m_pkId = pkId;
    emit pkIdChanged(pkId);
}

void OrderListItem::setTrxId(QString trxId)
{
    if (m_trxId == trxId)
        return;

    m_trxId = trxId;
    emit trxIdChanged(trxId);
}

void OrderListItem::setBillStatus(QString billStatus)
{
    if (m_billStatus == billStatus)
        return;

    m_billStatus = billStatus;
    emit billStatusChanged(billStatus);
}

void OrderListItem::setValue(QString value)
{
    if (m_value == value)
        return;

    m_value = value;
    emit valueChanged(value);
}

void OrderListItem::setDiscount(QString discount)
{
    if (m_discount == discount)
        return;

    m_discount = discount;
    emit discountChanged(discount);
}

void OrderListItem::setCurrencyName(QString currencyName)
{
    if (m_currencyName == currencyName)
        return;

    m_currencyName = currencyName;
    emit currencyNameChanged(currencyName);
}

void OrderListItem::setCashierName(QString cashierName)
{
    if (m_cashierName == cashierName)
        return;

    m_cashierName = cashierName;
    emit cashierNameChanged(cashierName);
}

void OrderListItem::setOrderHour(QString orderHour)
{
    if (m_orderHour == orderHour)
        return;

    m_orderHour = orderHour;
    emit orderHourChanged(orderHour);
}

void OrderListItem::setSummaryValue(QString summaryValue)
{
    if (m_summaryValue == summaryValue)
        return;

    m_summaryValue = summaryValue;
    emit summaryValueChanged(summaryValue);
}

void OrderListItem::setOrderId(QString orderId)
{
    if (m_orderId == orderId)
        return;

    m_orderId = orderId;
    emit orderIdChanged(orderId);
}

void OrderListItem::setIsSummaryItem(bool isSummaryItem)
{
    if (m_isSummaryItem == isSummaryItem)
        return;

    m_isSummaryItem = isSummaryItem;
    emit isSummaryItemChanged(isSummaryItem);
}

void OrderListItem::setSummaryDiscount(QString summaryDiscount)
{
    if (m_summaryDiscount == summaryDiscount)
        return;

    m_summaryDiscount = summaryDiscount;
    emit summaryDiscountChanged(summaryDiscount);
}

void OrderListItem::setTwintServerId(int twintServerId)
{
    if (m_twintServerId == twintServerId)
        return;

    m_twintServerId = twintServerId;
    emit twintServerIdChanged(twintServerId);
}
