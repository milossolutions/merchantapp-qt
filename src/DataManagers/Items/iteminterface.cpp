#include "iteminterface.h"

#include <QDebug>
#include <QXmlStreamReader>
#include <QStack>
#include <QMetaProperty>

#include "globalsettings.h"

ItemInterface::ItemInterface(QObject *parent) :
    QObject(parent)
{

}

void ItemInterface::parse(QByteArray msgData)
{
    createPropertyList();
    QXmlStreamReader xml(msgData);
    xml.setNamespaceProcessing(true);
    QStack<QString> stack;
    while (!xml.atEnd()) {
        QString name = xml.name().toString(); //name of start/end element or empty string if it is element's content
        QString parent = "";
        if (xml.isStartElement()) {
            if (stack.count() != 0) {
                parent = stack.top(); //get parent of the current element
            }
            stack.push(name); //add start element to the stack
        } else if (xml.isEndElement()) {
            stack.pop(); //remove top element from the stack
        }

        xml.readNext();
        parseElementAttributes(xml);
        //name() should return an empty string and text() should return element's content
        if (!xml.name().isEmpty() && xml.text().isEmpty())
            continue;
        foreach (QMetaProperty property, m_metaPropertyList) {
            QString propertyName(property.name());
            //looking for a property that has the same name as the element in soap xml
            if (!(property.type() == QVariant::String && propertyName.toLower() == name.toLower())) {
                continue;
            }
            //because different types have the same property names
            //you must also check the names of the element's parent
            if (parent.toLower() != this->objectName().toLower()) {
                continue;
            }
            //save element's content in property
            QVariant variant = QVariant::fromValue(xml.text().toString());
            property.write(this, variant);
            break;
        }
    }
    //recursive function call for each type
    parseChildrens(msgData);
}

void ItemInterface::parseChildrens(QByteArray msgData)
{
    foreach (QObject *object, children()) {
        ItemInterface *item = qobject_cast<ItemInterface*>(object);
        if (item) {
            item->parse(msgData);
        }
    }
}

void ItemInterface::getAllChildrensOfObject(QObjectList childrens, QObjectList &output)
{
    foreach (QObject *object, childrens) {
        output.append(object);
        if (!object->children().isEmpty()) {
            getAllChildrensOfObject(object->children(), output);
        }
    }
}

QList<QMetaProperty> ItemInterface::getPropertyListOfObject(QObject *object)
{
    QList<QMetaProperty> metaPropertyList;
    for (int i = object->metaObject()->propertyOffset(); i < object->metaObject()->propertyCount() ; ++i) {
        QMetaProperty property = object->metaObject()->property(i);
        metaPropertyList.append(property);
    }
    if (object->metaObject()->superClass()) {
        for (int i = object->metaObject()->superClass()->propertyOffset(); i < object->metaObject()->superClass()->propertyCount() ; ++i) {
            QMetaProperty property = object->metaObject()->superClass()->property(i);
            metaPropertyList.append(property);
        }
    }
    return metaPropertyList;
}

void ItemInterface::createPropertyList()
{
    m_metaPropertyList.clear();
    m_metaPropertyList = getPropertyListOfObject(this);
}

void ItemInterface::parseElementAttributes(QXmlStreamReader &xmlReader)
{
    if (!xmlReader.isStartElement()) {
        return;
    }
    foreach(const QXmlStreamAttribute &attr, xmlReader.attributes()) {
        foreach (QMetaProperty property, m_metaPropertyList) {
            QString propertyName(property.name());
            if (!propertyName.contains("Attribute")) {
                continue;
            }
            QString tmp = propertyName;
            tmp.replace("Attribute", "");
            QString attributeValue = attr.value().toString();
            if (tmp == attr.name()) {
                property.write(this, attributeValue);
            }
        }
    }
}

bool ItemInterface::isAnyPropertySet(QObjectList objectList)
{
    foreach (QObject *obj, objectList) {
        for (int i = obj->metaObject()->propertyOffset(); i < obj->metaObject()->propertyCount() ; ++i) {
            QMetaProperty property = obj->metaObject()->property(i);
            if (property.read(obj).toString() != "") {
                return true;
            }
        }
    }
    return false;
}
