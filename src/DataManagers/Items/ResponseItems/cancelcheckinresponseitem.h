#ifndef CANCELCHECKINITEM_H
#define CANCELCHECKINITEM_H

#include "responseiteminterface.h"

/**
 * \class CancelCheckInResponseItem
 * \brief Class represents CancelCheckInResponseElement. Object of this class is properly filled
 * based on CancelCheckInResponseElement data that are received in the server response.
 */
class CancelCheckInResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)

public:
    explicit CancelCheckInResponseItem(QObject *parent = 0);

    QString status() const;

signals:
    void statusChanged(const QString status) const;

public slots:
    void setStatus(const QString status);

private:
    QString m_status;
};

#endif // CANCELCHECKINITEM_H
