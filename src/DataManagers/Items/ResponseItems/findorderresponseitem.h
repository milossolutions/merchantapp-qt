#ifndef FINDORDERITEM_H
#define FINDORDERITEM_H

#include "responseiteminterface.h"

class OrderType;

/**
 * \class FindOrderResponseItem
 * \brief Class represents FindOrderResponseElement. Object of this class is properly filled
 * based on FindOrderResponseElement data that are received in the server response.
 */

class FindOrderResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QList<OrderType *> orderListType READ orderListType WRITE setOrderListType NOTIFY orderListTypeChanged)

public:
    explicit FindOrderResponseItem(QObject *parent = 0);

    QList<OrderType *>orderListType() const;
    void appendOrder(OrderType *order);

signals:
    void orderListTypeChanged(QList<OrderType *> orderListType) const;

public slots:
    void setOrderListType(QList<OrderType *> orderListType);

protected:
    void parse(QByteArray msgData);

private:
    QList<OrderType *> m_orderList;
};

#endif // FINDORDERITEM_H
