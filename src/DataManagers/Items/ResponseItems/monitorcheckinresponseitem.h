#ifndef MONITORCHECKINITEM_H
#define MONITORCHECKINITEM_H

#include "responseiteminterface.h"

class CheckInNotificationType;

/**
 * \class MonitorCheckInResponseItem
 * \brief Class represents MonitorCheckInResponseElement. Object of this class is properly filled
 * based on MonitorCheckInResponseElement data that are received in the server response.
 */
class MonitorCheckInResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(CheckInNotificationType* checkInNotificationType READ checkInNotificationType WRITE setCheckInNotificationType NOTIFY checkInNotificationTypeChanged)

public:
    explicit MonitorCheckInResponseItem(QObject *parent = 0);

    CheckInNotificationType *checkInNotificationType();

signals:
    void checkInNotificationTypeChanged(CheckInNotificationType* checkInNotificationType) const;

public slots:
    void setCheckInNotificationType(CheckInNotificationType* checkInNotificationType);

private:
    CheckInNotificationType *m_checkInNotification;
};

#endif // MONITORCHECKINITEM_H
