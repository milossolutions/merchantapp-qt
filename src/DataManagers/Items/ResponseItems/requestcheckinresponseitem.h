#ifndef REQUESTCHECKINITEM_H
#define REQUESTCHECKINITEM_H

#include "responseiteminterface.h"

class CheckInNotificationType;

/**
 * \class RequestCheckInResponseItem
 * \brief Class represents RequestCheckInResponseElement. Object of this class is properly filled
 * based on RequestCheckInResponseElement data that are received in the server response.
 */

class RequestCheckInResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(CheckInNotificationType* checkInNotificationType READ checkInNotificationType WRITE setCheckInNotificationType NOTIFY checkInNotificationTypeChanged)
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)
    Q_PROPERTY(QString QRCode READ QRCode WRITE setQRCode NOTIFY QRCodeChanged)

public:
    explicit RequestCheckInResponseItem(QObject *parent = 0);

    CheckInNotificationType *checkInNotificationType() const;
    QString token() const;
    QString QRCode() const;

signals:
    void checkInNotificationTypeChanged(CheckInNotificationType* checkInNotificationType) const;
    void tokenChanged(const QString token) const;
    void QRCodeChanged(const QString QRCode) const;

public slots:
    void setCheckInNotificationType(CheckInNotificationType* checkInNotificationType);
    void setToken(QString token);
    void setQRCode(QString QRCode);

private:
    QString m_token;
    QString m_QRCode;

    CheckInNotificationType *m_checkInNotification ;
};

#endif // REQUESTCHECKINITEM_H
