#include "enrollcashregisterresponseitem.h"

#include "beaconsecuritytype.h"

EnrollCashRegisterResponseItem::EnrollCashRegisterResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_beaconSecurity(new BeaconSecurityType(this))
{
    setObjectName("EnrollCashRegisterResponseElement");
    m_beaconSecurity->setObjectName("BeaconSecurity");
}

BeaconSecurityType *EnrollCashRegisterResponseItem::beaconSecurityType() const
{
    return m_beaconSecurity;
}

void EnrollCashRegisterResponseItem::setBeaconSecurityType(BeaconSecurityType *beaconSecurityType)
{
    if (m_beaconSecurity == beaconSecurityType)
        return;

    m_beaconSecurity = beaconSecurityType;
    emit beaconSecurityTypeChanged(beaconSecurityType);
}
