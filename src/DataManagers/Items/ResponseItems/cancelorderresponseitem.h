#ifndef CANCELORDERITEM_H
#define CANCELORDERITEM_H

#include "responseiteminterface.h"

class OrderType;

/**
 * \class CancelOrderResponseItem
 * \brief Class represents CancelOrderResponseElement. Object of this class is properly filled
 * based on CancelOrderResponseElement data that are received in the server response.
 */
class CancelOrderResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(OrderType* orderType READ orderType WRITE setOrderType NOTIFY orderTypeChanged)

public:
    explicit CancelOrderResponseItem(QObject *parent = 0);

    OrderType *orderType() const;

signals:
    void orderTypeChanged(OrderType* orderType) const;

public slots:
    void setOrderType(OrderType* orderType);

private:
    OrderType *m_order;
};

#endif // CANCELORDERITEM_H
