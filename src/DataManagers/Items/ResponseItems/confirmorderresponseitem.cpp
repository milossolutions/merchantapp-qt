#include "confirmorderresponseitem.h"

#include "merchantinformationtype.h"
#include "ordertype.h"

ConfirmOrderResponseItem::ConfirmOrderResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this)),
    m_order(new OrderType(this))
{
    setObjectName("ConfirmOrderResponseElement");
    m_order->setObjectName("Order");
    m_merchantInformation->setObjectName("MerchantInformation");
}

MerchantInformationType *ConfirmOrderResponseItem::merchantInformationType() const
{
    return m_merchantInformation;
}

OrderType *ConfirmOrderResponseItem::orderType() const
{
    return m_order;
}

void ConfirmOrderResponseItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void ConfirmOrderResponseItem::setOrderType(OrderType *orderType)
{
    if (m_order == orderType)
        return;

    m_order = orderType;
    emit orderTypeChanged(orderType);
}
