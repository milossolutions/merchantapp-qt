#include "findorderresponseitem.h"

#include "ordertype.h"

#include <QXmlStreamReader>
#include <QStack>
#include <QMetaProperty>

FindOrderResponseItem::FindOrderResponseItem(QObject *parent) :
    ResponseItemInterface(parent)
{
    setObjectName("FindOrderResponseElement");
}

QList<OrderType *> FindOrderResponseItem::orderListType() const
{
    return m_orderList;
}

void FindOrderResponseItem::appendOrder(OrderType *order)
{
    order->setObjectName("Order");
    m_orderList.append(order);
}

void FindOrderResponseItem::setOrderListType(QList<OrderType *> orderListType)
{
    if (m_orderList == orderListType)
        return;

    m_orderList = orderListType;
    emit orderListTypeChanged(orderListType);
}

void FindOrderResponseItem::parse(QByteArray msgData)
{
    QXmlStreamReader xml(msgData);
    xml.setNamespaceProcessing(true);
    QObjectList childrens;
    if (!this->children().isEmpty())
        getAllChildrensOfObject(this->children(), childrens);
    childrens.append(this);

    const QObjectList objectList = childrens;

    QStack<QString> stack;
    bool isStartElement = false;
    while (!xml.atEnd()) {
        QString name = xml.name().toString();
        QString parent = "";
        isStartElement = false;
        if (xml.isStartElement()) {
            isStartElement = true;
            if (stack.count() != 0) {
                parent = stack.top();
            }
            stack.push(name);
        } else if (xml.isEndElement()) {
            stack.pop();
        }

        xml.readNext();
        foreach (QObject *object, childrens) {
            ItemInterface *itemInterface = qobject_cast<ItemInterface*>(object);
            if (itemInterface) {
                itemInterface->createPropertyList();
                itemInterface->parseElementAttributes(xml);
            }
        }
        if(xml.name().isEmpty() && !xml.text().isEmpty()){
            foreach (QObject *object, childrens) {
                QList<QMetaProperty> metaPropertyList = getPropertyListOfObject(object);
                foreach (QMetaProperty property, metaPropertyList) {
                    QString propertyName(property.name());
                    if (!(property.type() == QVariant::String && propertyName.toLower() == name.toLower())) {
                        continue;
                    }
                    if (!(parent.toLower() == object->objectName().toLower())) {
                        continue;
                    }
                    QVariant variant = QVariant::fromValue(xml.text().toString());
                    property.write(object, variant);
                    break;
                }
            }
        } else if (name.toLower() == "order" && isStartElement) {
            childrens = objectList;
            OrderType *orderType = new OrderType(this);
            appendOrder(orderType);

            QObjectList list;
            if (!orderType->children().isEmpty())
                getAllChildrensOfObject(orderType->children(), list);
            list.append(orderType);

            childrens.append(list);
        }
    }
}
