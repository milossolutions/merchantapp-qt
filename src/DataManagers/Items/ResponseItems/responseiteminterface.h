#ifndef MODELITEMINTERFACE_H
#define MODELITEMINTERFACE_H

#include "iteminterface.h"

/**
 * \class ResponseItemInterface
 * \brief Objects inheriting after ResponseItemInterface contains different members
 * which are then used in parse response received from Twint server.
 */
class ResponseItemInterface : public ItemInterface
{
    Q_OBJECT
public:
    explicit ResponseItemInterface(QObject *parent = 0);

signals:

public slots:
};

#endif // MODELITEMINTERFACE_H
