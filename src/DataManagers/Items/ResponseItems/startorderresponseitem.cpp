#include "startorderresponseitem.h"

StartOrderResponseItem::StartOrderResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_orderStatus(new OrderStatusType(this)),
    m_customerInformation(new CustomerInformationType(this))
{
    setObjectName("StartOrderResponseElement");
    m_orderStatus->setObjectName("OrderStatus");
    m_customerInformation->setObjectName("CustomerInformation");
}

QString StartOrderResponseItem::orderUuid() const
{
    return m_orderUuid;
}

OrderStatusType *StartOrderResponseItem::orderStatusType() const
{
    return m_orderStatus;
}

QString StartOrderResponseItem::token() const
{
    return m_token;
}

QString StartOrderResponseItem::QRCode() const
{
    return m_QRCode;
}

CustomerInformationType *StartOrderResponseItem::customerInformationType() const
{
    return m_customerInformation;
}

QString StartOrderResponseItem::pairingStatus() const
{
    return m_pairingStatus;
}

void StartOrderResponseItem::setOrderUuid(const QString orderUuid)
{
    if (m_orderUuid == orderUuid)
        return;

    m_orderUuid = orderUuid;
    emit orderUuidChanged(orderUuid);
}

void StartOrderResponseItem::setOrderStatusType(OrderStatusType *orderStatusType)
{
    if (m_orderStatus == orderStatusType)
        return;

    m_orderStatus = orderStatusType;
    emit orderStatusTypeChanged(orderStatusType);
}

void StartOrderResponseItem::setToken(const QString token)
{
    if (m_token == token)
        return;

    m_token = token;
    emit tokenChanged(token);
}

void StartOrderResponseItem::setQRCode(const QString QRCode)
{
    if (m_QRCode == QRCode)
        return;

    m_QRCode = QRCode;
    emit QRCodeChanged(QRCode);
}

void StartOrderResponseItem::setCustomerInformationType(CustomerInformationType *customerInformationType)
{
    if (m_customerInformation == customerInformationType)
        return;

    m_customerInformation = customerInformationType;
    emit customerInformationTypeChanged(customerInformationType);
}

void StartOrderResponseItem::setPairingStatus(QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}
