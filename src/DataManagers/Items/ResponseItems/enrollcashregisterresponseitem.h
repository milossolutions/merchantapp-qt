#ifndef ENROLLCASHREGISTERITEM_H
#define ENROLLCASHREGISTERITEM_H

#include "responseiteminterface.h"

class BeaconSecurityType;

/**
 * \class EnrollCashRegisterResponseItem
 * \brief Class represents EnrollCashRegisterResponseElement. Object of this class is properly filled
 * based on EnrollCashRegisterResponseElement data that are received in the server response.
 */

class EnrollCashRegisterResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(BeaconSecurityType* beaconSecurityType READ beaconSecurityType WRITE setBeaconSecurityType NOTIFY beaconSecurityTypeChanged)

public:
    explicit EnrollCashRegisterResponseItem(QObject *parent = 0);

    BeaconSecurityType *beaconSecurityType() const;

signals:
    void beaconSecurityTypeChanged(BeaconSecurityType* beaconSecurityType) const;

public slots:
    void setBeaconSecurityType(BeaconSecurityType* beaconSecurityType);

private:
    BeaconSecurityType *m_beaconSecurity;
};

#endif // ENROLLCASHREGISTERITEM_H
