#ifndef MONITORORDERSTATUSITEM_H
#define MONITORORDERSTATUSITEM_H

#include "responseiteminterface.h"

class MerchantInformationType;
class OrderType;

/**
 * \class MonitorOrderResponseItem
 * \brief Class represents MonitorOrderResponseElement. Object of this class is properly filled
 * based on MonitorOrderResponseElement data that are received in the server response.
 */
class MonitorOrderResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(OrderType* orderType READ orderType WRITE setOrderType NOTIFY orderTypeChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)
    Q_PROPERTY(QString customerRelationUuid READ customerRelationUuid WRITE setCustomerRelationUuid NOTIFY customerRelationUuidChanged)

public:
    explicit MonitorOrderResponseItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    OrderType *orderType() const;
    QString pairingStatus() const;
    QString customerRelationUuid() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void orderTypeChanged(OrderType* orderType);
    void pairingStatusChanged(const QString pairingStatus) const;
    void customerRelationUuidChanged(const QString customerRelationUuid) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setOrderType(OrderType* orderType);
    void setPairingStatus(const QString pairingStatus);
    void setCustomerRelationUuid(const QString customerRelationUuid);

private:
    QString m_pairingStatus;
    QString m_customerRelationUuid;

    MerchantInformationType *m_merchantInformation;
    OrderType *m_order;
};

#endif // MONITORORDERSTATUSITEM_H
