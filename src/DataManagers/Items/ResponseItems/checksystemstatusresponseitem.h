#ifndef CHECKSYSTEMSTATUSITEM_H
#define CHECKSYSTEMSTATUSITEM_H

#include "responseiteminterface.h"

/**
 * \class CheckSystemStatusResponseItem
 * \brief based on represents CheckSystemStatusResponseElement. Object of this class is properly filled
 * from CheckSystemStatusResponseElement data that are received in the server response.
 */
class CheckSystemStatusResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)

public:
    explicit CheckSystemStatusResponseItem(QObject *parent = 0);

    QString status() const;

signals:
    void statusChanged(const QString status) const;

public slots:
    void setStatus(const QString status);

private:
    QString m_status;
};

#endif // CHECKSYSTEMSTATUSITEM_H
