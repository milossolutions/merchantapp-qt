#ifndef CONFIRMORDERITEM_H
#define CONFIRMORDERITEM_H

#include "responseiteminterface.h"

class MerchantInformationType;
class OrderType;

/**
 * \class ConfirmOrderResponseItem
 * \brief Class represents ConfirmOrderResponseElement. Object of this class is properly filled
 * based on ConfirmOrderResponseElement data that are received in the server response.
 */

class ConfirmOrderResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(MerchantInformationType* merchantInformationType READ merchantInformationType WRITE setMerchantInformationType NOTIFY merchantInformationTypeChanged)
    Q_PROPERTY(OrderType* orderType READ orderType WRITE setOrderType NOTIFY orderTypeChanged)

public:
    explicit ConfirmOrderResponseItem(QObject *parent = 0);

    MerchantInformationType *merchantInformationType() const;
    OrderType *orderType() const;

signals:
    void merchantInformationTypeChanged(MerchantInformationType* merchantInformationType) const;
    void orderTypeChanged(OrderType* orderType) const;

public slots:
    void setMerchantInformationType(MerchantInformationType* merchantInformationType);
    void setOrderType(OrderType* orderType);

private:
    MerchantInformationType *m_merchantInformation;
    OrderType *m_order;
};

#endif // CONFIRMORDERITEM_H
