#include "cancelorderresponseitem.h"

#include "ordertype.h"

CancelOrderResponseItem::CancelOrderResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_order(new OrderType(this))
{
    setObjectName("CancelOrderResponseElement");
    m_order->setObjectName("Order");
}

OrderType *CancelOrderResponseItem::orderType() const
{
    return m_order;
}

void CancelOrderResponseItem::setOrderType(OrderType *orderType)
{
    if (m_order == orderType)
        return;

    m_order = orderType;
    emit orderTypeChanged(orderType);
}
