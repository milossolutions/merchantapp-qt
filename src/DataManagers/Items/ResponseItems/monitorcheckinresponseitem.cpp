#include "monitorcheckinresponseitem.h"

#include "checkinnotificationtype.h"

MonitorCheckInResponseItem::MonitorCheckInResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_checkInNotification(new CheckInNotificationType(this))
{
    setObjectName("MonitorCheckInResponseElement");
    m_checkInNotification->setObjectName("CheckInNotification");
}

CheckInNotificationType *MonitorCheckInResponseItem::checkInNotificationType()
{
    return m_checkInNotification;
}

void MonitorCheckInResponseItem::setCheckInNotificationType(CheckInNotificationType *checkInNotificationType)
{
    if (m_checkInNotification == checkInNotificationType)
        return;

    m_checkInNotification = checkInNotificationType;
    emit checkInNotificationTypeChanged(checkInNotificationType);
}
