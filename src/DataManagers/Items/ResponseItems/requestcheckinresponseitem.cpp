#include "requestcheckinresponseitem.h"

#include "checkinnotificationtype.h"

RequestCheckInResponseItem::RequestCheckInResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_checkInNotification(new CheckInNotificationType(this))
{
    setObjectName("RequestCheckInResponseElement");
    m_checkInNotification->setObjectName("CheckInNotification");
}

QString RequestCheckInResponseItem::token() const
{
    return m_token;
}

QString RequestCheckInResponseItem::QRCode() const
{
    return m_QRCode;
}

void RequestCheckInResponseItem::setCheckInNotificationType(CheckInNotificationType *checkInNotificationType)
{
    if (m_checkInNotification == checkInNotificationType)
        return;

    m_checkInNotification = checkInNotificationType;
    emit checkInNotificationTypeChanged(checkInNotificationType);
}

CheckInNotificationType *RequestCheckInResponseItem::checkInNotificationType() const
{
    return m_checkInNotification;
}

void RequestCheckInResponseItem::setToken(QString token)
{
    if (m_token == token)
        return;

    m_token = token;
    emit tokenChanged(token);
}

void RequestCheckInResponseItem::setQRCode(QString QRCode)
{
    if (m_QRCode == QRCode)
        return;

    m_QRCode = QRCode;
    emit QRCodeChanged(QRCode);
}
