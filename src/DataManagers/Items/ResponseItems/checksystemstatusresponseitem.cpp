#include "checksystemstatusresponseitem.h"

CheckSystemStatusResponseItem::CheckSystemStatusResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_status(QString::null)
{
    setObjectName("CheckSystemStatusResponseElement");
}

QString CheckSystemStatusResponseItem::status() const
{
    return m_status;
}

void CheckSystemStatusResponseItem::setStatus(const QString status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(status);
}
