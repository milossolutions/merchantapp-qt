#include "monitororderresponseitem.h"

#include "merchantinformationtype.h"
#include "ordertype.h"

MonitorOrderResponseItem::MonitorOrderResponseItem(QObject *parent) :
    ResponseItemInterface(parent),
    m_merchantInformation(new MerchantInformationType(this)),
    m_order(new OrderType(this))
{
    setObjectName("MonitorOrderResponseElement");
    m_merchantInformation->setObjectName("MerchantInformation");
    m_order->setObjectName("Order");
}

MerchantInformationType *MonitorOrderResponseItem::merchantInformationType() const
{
    return m_merchantInformation;
}

OrderType *MonitorOrderResponseItem::orderType() const
{
    return m_order;
}

QString MonitorOrderResponseItem::pairingStatus() const
{
    return m_pairingStatus;
}

QString MonitorOrderResponseItem::customerRelationUuid() const
{
    return m_customerRelationUuid;
}

void MonitorOrderResponseItem::setMerchantInformationType(MerchantInformationType *merchantInformationType)
{
    if (m_merchantInformation == merchantInformationType)
        return;

    m_merchantInformation = merchantInformationType;
    emit merchantInformationTypeChanged(merchantInformationType);
}

void MonitorOrderResponseItem::setOrderType(OrderType *orderType)
{
    if (m_order == orderType)
        return;

    m_order = orderType;
    emit orderTypeChanged(orderType);
}

void MonitorOrderResponseItem::setPairingStatus(const QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}

void MonitorOrderResponseItem::setCustomerRelationUuid(const QString customerRelationUuid)
{
    if (m_customerRelationUuid == customerRelationUuid)
        return;

    m_customerRelationUuid = customerRelationUuid;
    emit customerRelationUuidChanged(customerRelationUuid);
}
