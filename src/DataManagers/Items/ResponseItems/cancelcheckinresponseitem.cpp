#include "cancelcheckinresponseitem.h"

CancelCheckInResponseItem::CancelCheckInResponseItem(QObject *parent) :
    ResponseItemInterface(parent)
{
    setObjectName("CancelCheckInResponseElement");
}

QString CancelCheckInResponseItem::status() const
{
    return m_status;
}

void CancelCheckInResponseItem::setStatus(const QString status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(status);
}
