#ifndef STARTORDERITEM_H
#define STARTORDERITEM_H

#include "responseiteminterface.h"

#include "orderstatustype.h"
#include "customerinformationtype.h"

/**
 * \class StartOrderResponseItem
 * \brief Class represents StartOrderResponseElement. Object of this class is properly filled
 * based on StartOrderResponseElement data that are received in the server response.
 */

class StartOrderResponseItem : public ResponseItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString orderUuid READ orderUuid WRITE setOrderUuid NOTIFY orderUuidChanged)
    Q_PROPERTY(OrderStatusType* orderStatusType READ orderStatusType WRITE setOrderStatusType NOTIFY orderStatusTypeChanged)
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)
    Q_PROPERTY(QString QRCode READ QRCode WRITE setQRCode NOTIFY QRCodeChanged)
    Q_PROPERTY(CustomerInformationType* customerInformationType READ customerInformationType WRITE setCustomerInformationType NOTIFY customerInformationTypeChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)

public:
    explicit StartOrderResponseItem(QObject *parent = 0);

    QString orderUuid() const;
    OrderStatusType *orderStatusType() const;
    QString token() const;
    QString QRCode() const;
    CustomerInformationType *customerInformationType() const;
    QString pairingStatus() const;

signals:
    void orderUuidChanged(QString orderUuid) const;
    void orderStatusTypeChanged(OrderStatusType* orderStatusType) const;
    void tokenChanged(QString token) const;
    void QRCodeChanged(QString QRCode) const;
    void customerInformationTypeChanged(CustomerInformationType* customerInformationType) const;
    void pairingStatusChanged(QString pairingStatus) const;

public slots:
    void setOrderUuid(const QString orderUuid);
    void setOrderStatusType(OrderStatusType* orderStatusType);
    void setToken(const QString token);
    void setQRCode(const QString QRCode);
    void setCustomerInformationType(CustomerInformationType* customerInformationType);
    void setPairingStatus(QString pairingStatus);

private:
    QString m_orderUuid;
    QString m_token;
    QString m_QRCode;
    QString m_pairingStatus;

    OrderStatusType *m_orderStatus;
    CustomerInformationType *m_customerInformation;
};

#endif // STARTORDERITEM_H
