#ifndef CONTAINERITEMINTERFACE_H
#define CONTAINERITEMINTERFACE_H

#include <QObject>

class ResponseItemInterface;
class RequestItemInterface;

/**
 * \class ContainerItemInterface
 * \brief Objects inheriting after ContainerItemInterface are filled with data from request and response and are used in various functions.
 */
class ContainerItemInterface : public QObject
{
    Q_OBJECT
public:
    explicit ContainerItemInterface(QObject *parent = 0);

signals:

public slots:
    virtual void fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem) = 0;
};

#endif // CONTAINERITEMINTERFACE_H
