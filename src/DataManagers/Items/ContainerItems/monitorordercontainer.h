#ifndef MONITORORDERCONTAINER_H
#define MONITORORDERCONTAINER_H

#include "containeriteminterface.h"
#include "globalsettings.h"

/**
 * \class MonitorOrderContainer
 * \brief Objects of this class are filled with data from MonitorOrderRequestItem and MonitorOrderResponseItem objects.
 */

class MonitorOrderContainer : public ContainerItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString orderId READ orderId WRITE setOrderId NOTIFY orderIdChanged)
    Q_PROPERTY(GlobalSettings::Enums::OrderStatus orderStatus READ orderStatus WRITE setOrderStatus NOTIFY orderStatusChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)

public:
    explicit MonitorOrderContainer(QObject *parent = 0);

    QString orderId() const;
    GlobalSettings::Enums::OrderStatus orderStatus() const;
    QString pairingStatus() const;

signals:
    void orderIdChanged(QString orderId);
    void orderStatusChanged(GlobalSettings::Enums::OrderStatus orderStatus);
    void pairingStatusChanged(QString pairingStatus);

public slots:
    void fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem);

    void setOrderId(QString orderId);
    void setOrderStatus(GlobalSettings::Enums::OrderStatus orderStatus);
    void setPairingStatus(QString pairingStatus);

private:
    QString m_orderId;
    GlobalSettings::Enums::OrderStatus m_orderStatus;
    QString m_pairingStatus;
};

#endif // MONITORORDERCONTAINER_H
