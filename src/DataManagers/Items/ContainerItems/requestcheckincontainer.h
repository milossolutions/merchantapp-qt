#ifndef REQUESTCHECKINCONTAINER_H
#define REQUESTCHECKINCONTAINER_H

#include <containeriteminterface.h>

class LoyaltyType;
class CouponType;

/**
 * \class RequestCheckInContainer
 * \brief Objects of this class are filled with data from RequestCheckInResponseItem object.
 */

class RequestCheckInContainer : public ContainerItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString pairingUuid READ pairingUuid WRITE setPairingUuid NOTIFY pairingUuidChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)
    Q_PROPERTY(QList<LoyaltyType *> loyaltyListType READ loyaltyListType WRITE setLoyaltyListType NOTIFY loyaltyListTypeChanged)
    Q_PROPERTY(QList<CouponType *> couponListType READ couponListType WRITE setCouponListType NOTIFY couponListTypeChanged)
    Q_PROPERTY(QString qrCode READ qrCode WRITE setQrCode NOTIFY qrCodeChanged)
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)

public:
    explicit RequestCheckInContainer(QObject *parent = 0);
    ~RequestCheckInContainer();

    QString pairingUuid() const;
    QString pairingStatus() const;
    QList<LoyaltyType *> loyaltyListType() const;
    QList<CouponType *> couponListType() const;
    QString qrCode() const;
    QString token() const;

signals:
    void pairingUuidChanged(QString pairingUuid);
    void pairingStatusChanged(QString pairingStatus);
    void loyaltyListTypeChanged(QList<LoyaltyType *> loyaltyListType);
    void couponListTypeChanged(QList<CouponType *> couponListType);
    void qrCodeChanged(QString qrCode);
    void tokenChanged(QString token);

public slots:
    void fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem);

    void setPairingUuid(QString pairingUuid);
    void setPairingStatus(QString pairingStatus);
    void setLoyaltyListType(QList<LoyaltyType *> loyaltyListType);
    void setCouponListType(QList<CouponType *> couponListType);
    void setQrCode(QString qrCode);
    void setToken(QString token);

private:
    QString m_pairingUuid;
    QString m_pairingStatus;
    QList<LoyaltyType *> m_loyaltyListType;
    QList<CouponType *> m_couponListType;
    QString m_qrCode;
    QString m_token;
};

#endif // REQUESTCHECKINCONTAINER_H
