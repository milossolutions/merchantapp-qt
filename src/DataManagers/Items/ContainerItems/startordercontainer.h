#ifndef STARTORDERCONTAINER_H
#define STARTORDERCONTAINER_H

#include "containeriteminterface.h"
#include "globalsettings.h"

/**
 * \class StartOrderContainer
 * \brief Objects of this class are filled with data from StartOrderRequestItem and StartOrderResponseItem objects.
 */
class StartOrderContainer : public ContainerItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString orderId READ orderId WRITE setOrderId NOTIFY orderIdChanged)
    Q_PROPERTY(GlobalSettings::Enums::OrderStatus orderStatus READ orderStatus WRITE setOrderStatus NOTIFY orderStatusChanged)
    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString discount READ discount WRITE setDiscount NOTIFY discountChanged)
    Q_PROPERTY(QString currency READ currency WRITE setCurrency NOTIFY currencyChanged)
    Q_PROPERTY(QString cashierUuid READ cashierUuid WRITE setCashierUuid NOTIFY cashierUuidChanged)
    Q_PROPERTY(QString cashierName READ cashierName WRITE setCashierName NOTIFY cashierNameChanged)
    Q_PROPERTY(QString cashierAlias READ cashierAlias WRITE setCashierAlias NOTIFY cashierAliasChanged)
    Q_PROPERTY(QString cashRegisterId READ cashRegisterId WRITE setCashRegisterId NOTIFY cashRegisterIdChanged)
    Q_PROPERTY(QString serviceAgentId READ serviceAgentId WRITE setServiceAgentId NOTIFY serviceAgentIdChanged)
    Q_PROPERTY(QString token READ token WRITE setToken NOTIFY tokenChanged)
    Q_PROPERTY(QString QRCode READ QRCode WRITE setQRCode NOTIFY QRCodeChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)
    Q_PROPERTY(GlobalSettings::Enums::OrderType orderType READ orderType WRITE setOrderType NOTIFY orderTypeChanged)
    Q_PROPERTY(QString linkOrder READ linkOrder WRITE setLinkOrder NOTIFY linkOrderChanged)

public:
    explicit StartOrderContainer(QObject *parent = 0);
    ~StartOrderContainer();

    QString orderId() const;
    GlobalSettings::Enums::OrderStatus orderStatus() const;
    QString value() const;
    QString discount() const;
    QString currency() const;
    QString cashierUuid() const;
    QString cashierName() const;
    QString cashierAlias() const;
    QString cashRegisterId() const;
    QString serviceAgentId() const;
    QString token() const;
    QString QRCode() const;
    QString pairingStatus() const;
    GlobalSettings::Enums::OrderType orderType();
    QString linkOrder();

signals:
    void orderIdChanged(QString orderId);
    void orderStatusChanged(GlobalSettings::Enums::OrderStatus orderStatus);
    void valueChanged(QString value);
    void discountChanged(QString discount);
    void currencyChanged(QString currency);
    void cashierUuidChanged(QString cashierUuid);
    void cashierNameChanged(QString cashierName);
    void cashierAliasChanged(QString cashierAlias);
    void cashRegisterIdChanged(QString cashRegisterId);
    void serviceAgentIdChanged(QString serviceAgentId);
    void tokenChanged(QString token);
    void QRCodeChanged(QString QRCode);
    void pairingStatusChanged(QString pairingStatus);
    void orderTypeChanged(GlobalSettings::Enums::OrderType orderType);
    void linkOrderChanged(QString linkOrder);

public slots:
    void fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem);

    void setOrderId(QString orderId);
    void setOrderStatus(GlobalSettings::Enums::OrderStatus orderStatus);
    void setValue(QString value);
    void setDiscount(QString discount);
    void setCurrency(QString currency);
    void setCashierUuid(QString cashierUuid);
    void setCashierName(QString cashierName);
    void setCashierAlias(QString cashierAlias);
    void setCashRegisterId(QString cashRegisterId);
    void setServiceAgentId(QString serviceAgentId);
    void setToken(QString token);
    void setQRCode(QString QRCode);
    void setPairingStatus(QString pairingStatus);
    void setOrderType(GlobalSettings::Enums::OrderType orderType);
    void setLinkOrder(QString linkOrder);

private:
    QString m_orderId;
    GlobalSettings::Enums::OrderStatus m_orderStatus;
    QString m_value;
    QString m_discount;
    QString m_currency;
    QString m_cashierUuid;
    QString m_cashierName;
    QString m_cashierAlias;
    QString m_cashRegisterId;
    QString m_serviceAgentId;
    QString m_token;
    QString m_QRCode;
    QString m_pairingStatus;
    GlobalSettings::Enums::OrderType m_orderType;
    QString m_linkOrder;
};

#endif // STARTORDERCONTAINER_H
