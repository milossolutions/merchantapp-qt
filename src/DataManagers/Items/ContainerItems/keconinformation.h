#ifndef KECONINFORMATION_H
#define KECONINFORMATION_H

#include <QObject>
#include "containeriteminterface.h"

/**
 * \class KeconInformation
 * \brief Objects of this class are filled with data from EnrollCashRegisterResponseItem object.
 */
class KeconInformation : public ContainerItemInterface
{
    Q_OBJECT

public:
    KeconInformation(QObject *parent = 0);

    QString m_beaconUuid;
    QString m_majorId;
    QString m_minorId;
    QString m_beaconInitString;
    QString m_beaconSecret;

    bool isDataValid() const;

public slots:
    void fillData(RequestItemInterface *, ResponseItemInterface *responseItem);
};

#endif // KECONINFORMATION_H
