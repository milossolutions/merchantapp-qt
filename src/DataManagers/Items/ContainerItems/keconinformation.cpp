#include "keconinformation.h"

#include "enrollcashregisterresponseitem.h"
#include "beaconsecuritytype.h"
#include "globalsettings.h"

/*!
 * \brief Kecon Information
 * Information obtained from TWINT with all keacon settings
 */

KeconInformation::KeconInformation(QObject *parent) :
    ContainerItemInterface( parent),
    m_beaconUuid(QString::null),
    m_majorId(QString::null),
    m_minorId(QString::null),
    m_beaconInitString(QString::null),
    m_beaconSecret(QString::null)
{

}

bool KeconInformation::isDataValid() const
{
    if(m_beaconUuid == QString::null || m_beaconInitString == QString::null){
        LOGD()<<m_beaconUuid<<m_beaconInitString;
        return false;
    }
    return true;
}

/*!
 * \brief fill data
 * Update object with correct infromation received and stored in responseItem
 */

void KeconInformation::fillData(RequestItemInterface *,
                                ResponseItemInterface *responseItem)
{
    EnrollCashRegisterResponseItem *response =
            qobject_cast<EnrollCashRegisterResponseItem*>(responseItem);

    if (!response) {
        LOGD() << "Response invalid data";
        return;
    }

    BeaconSecurityType* sec = response->beaconSecurityType();

    if (!sec) {
        LOGD() << "Security invalid data";
        return;
    }

    m_beaconUuid = sec->beaconUuid();
    m_majorId = sec->majorId();
    m_minorId = sec->minorId();
    m_beaconInitString = sec->beaconInitString();
    m_beaconSecret = sec->beaconSecret();
}
