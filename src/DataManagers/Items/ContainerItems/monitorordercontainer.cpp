#include "monitorordercontainer.h"

#include "monitororderrequestitem.h"
#include "monitororderresponseitem.h"
#include "ordertype.h"
#include "orderstatustype.h"

MonitorOrderContainer::MonitorOrderContainer(QObject *parent) :
    ContainerItemInterface(parent)
{

}

QString MonitorOrderContainer::orderId() const
{
    return m_orderId;
}

GlobalSettings::Enums::OrderStatus MonitorOrderContainer::orderStatus() const
{
    return m_orderStatus;
}

QString MonitorOrderContainer::pairingStatus() const
{
    return m_pairingStatus;
}

void MonitorOrderContainer::fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem)
{
    MonitorOrderRequestItem *request = qobject_cast<MonitorOrderRequestItem*>(requestItem);
    MonitorOrderResponseItem *response = qobject_cast<MonitorOrderResponseItem*>(responseItem);

    if(!request || !response){
        LOGD()<<"Invalid data";
        return;
    }

    setOrderId(request->orderUuid());
    setOrderStatus(GlobalSettings::Enums::OrderStatus(response->orderType()->orderStatusType()->codeReasonAttribute().toInt()));
    setPairingStatus(response->pairingStatus());
}

void MonitorOrderContainer::setOrderId(QString orderId)
{
    if (m_orderId == orderId)
        return;

    m_orderId = orderId;
    emit orderIdChanged(orderId);
}

void MonitorOrderContainer::setOrderStatus(GlobalSettings::Enums::OrderStatus orderStatus)
{
    if (m_orderStatus == orderStatus)
        return;

    m_orderStatus = orderStatus;
    emit orderStatusChanged(orderStatus);
}

void MonitorOrderContainer::setPairingStatus(QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}
