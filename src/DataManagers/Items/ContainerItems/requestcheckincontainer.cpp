#include "requestcheckincontainer.h"

#include "requestcheckinrequestitem.h"
#include "requestcheckinresponseitem.h"
#include "checkinnotificationtype.h"
#include "customerinformationtype.h"
#include "loyaltytype.h"
#include "coupontype.h"
#include "currencyamounttype.h"

RequestCheckInContainer::RequestCheckInContainer(QObject *parent) :
    ContainerItemInterface(parent),
    m_pairingUuid(QString::null),
    m_pairingStatus(QString::null),
    m_qrCode(QString::null),
    m_token(QString::null)
{

}

RequestCheckInContainer::~RequestCheckInContainer()
{

}

QString RequestCheckInContainer::pairingUuid() const
{
    return m_pairingUuid;
}

QString RequestCheckInContainer::pairingStatus() const
{
    return m_pairingStatus;
}

QList<LoyaltyType *> RequestCheckInContainer::loyaltyListType() const
{
    return m_loyaltyListType;
}

QList<CouponType *> RequestCheckInContainer::couponListType() const
{
    return m_couponListType;
}

QString RequestCheckInContainer::qrCode() const
{
    return m_qrCode;
}

QString RequestCheckInContainer::token() const
{
    return m_token;
}

void RequestCheckInContainer::fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem)
{
    RequestCheckInRequestItem *request = qobject_cast<RequestCheckInRequestItem*>(requestItem);
    RequestCheckInResponseItem *response = qobject_cast<RequestCheckInResponseItem*>(responseItem);

    this->setPairingUuid(response->checkInNotificationType()->pairingUuid());
    this->setPairingStatus(response->checkInNotificationType()->pairingStatus());
    this->setQrCode(response->QRCode());
    this->setToken(response->token());

    QList<LoyaltyType *> loyaltyTypeList;
    for(int i = 0; i<response->checkInNotificationType()->customerInformationType()->loyaltyListType().count(); i++){
        LoyaltyType *loyaltyTypePointer = response->checkInNotificationType()->customerInformationType()->loyaltyListType().at(i);
        LoyaltyType *loyaltyType = new LoyaltyType(this);
        loyaltyType->setProgram(loyaltyTypePointer->program());
        loyaltyType->setReference(loyaltyTypePointer->reference());
        loyaltyTypeList.append(loyaltyType);
    }
    this->setLoyaltyListType(loyaltyTypeList);

    QList<CouponType *> couponTypeList;
    for(int i = 0; i<response->checkInNotificationType()->customerInformationType()->couponListType().count(); i++){
        CouponType *couponTypePointer = response->checkInNotificationType()->customerInformationType()->couponListType().at(i);
        CouponType *couponType = new CouponType(this);
        couponType->setCouponId(couponTypePointer->couponId());
        CurrencyAmountType *couponValue = new CurrencyAmountType(this);
        couponValue->setAmount(couponTypePointer->couponValueType()->amount());
        couponValue->setCurrency(couponTypePointer->couponValueType()->currency());
        couponType->setCouponValueType(couponValue);

        couponTypeList.append(couponType);
    }
    this->setCouponListType(couponTypeList);
}

void RequestCheckInContainer::setPairingUuid(QString pairingUuid)
{
    if (m_pairingUuid == pairingUuid)
        return;

    m_pairingUuid = pairingUuid;
    emit pairingUuidChanged(pairingUuid);
}

void RequestCheckInContainer::setPairingStatus(QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}

void RequestCheckInContainer::setLoyaltyListType(QList<LoyaltyType *> loyaltyListType)
{
    if (m_loyaltyListType == loyaltyListType)
        return;

    m_loyaltyListType = loyaltyListType;
    emit loyaltyListTypeChanged(loyaltyListType);
}

void RequestCheckInContainer::setCouponListType(QList<CouponType *> couponListType)
{
    if (m_couponListType == couponListType)
        return;

    m_couponListType = couponListType;
    emit couponListTypeChanged(couponListType);
}

void RequestCheckInContainer::setQrCode(QString qrCode)
{
    if (m_qrCode == qrCode)
        return;

    m_qrCode = qrCode;
    emit qrCodeChanged(qrCode);
}

void RequestCheckInContainer::setToken(QString token)
{
    if (m_token == token)
        return;

    m_token = token;
    emit tokenChanged(token);
}
