#include "startordercontainer.h"

#include "startorderrequestitem.h"
#include "startorderresponseitem.h"
#include "orderstatustype.h"
#include "orderrequesttype.h"
#include "currencyamounttype.h"
#include "merchantinformationtype.h"
#include "orderlinktype.h"
#include "couponlisttype.h"
#include "coupontype.h"

StartOrderContainer::StartOrderContainer(QObject *parent) :
    ContainerItemInterface(parent),
    m_orderId(QString::null),
    m_orderStatus(GlobalSettings::Enums::NO_STATUS),
    m_value(QString::null),
    m_discount(QString::null),
    m_currency(QString::null),
    m_cashierUuid(QString::null),
    m_cashierName(QString::null),
    m_cashierAlias(QString::null),
    m_cashRegisterId(QString::null),
    m_serviceAgentId(QString::null),
    m_token(QString::null),
    m_QRCode(QString::null),
    m_pairingStatus(QString::null),
    m_orderType(GlobalSettings::Enums::NoOrderType),
    m_linkOrder(QString::null)
{

}

StartOrderContainer::~StartOrderContainer()
{

}

QString StartOrderContainer::orderId() const
{
    return m_orderId;
}

GlobalSettings::Enums::OrderStatus StartOrderContainer::orderStatus() const
{
    return m_orderStatus;
}

QString StartOrderContainer::value() const
{
    return m_value;
}

QString StartOrderContainer::discount() const
{
    return m_discount;
}

QString StartOrderContainer::currency() const
{
    return m_currency;
}

QString StartOrderContainer::cashierUuid() const
{
    return m_cashierUuid;
}

QString StartOrderContainer::cashierName() const
{
    return m_cashierName;
}

QString StartOrderContainer::cashierAlias() const
{
    return m_cashierAlias;
}

QString StartOrderContainer::cashRegisterId() const
{
    return m_cashRegisterId;
}

QString StartOrderContainer::serviceAgentId() const
{
    return m_serviceAgentId;
}

QString StartOrderContainer::token() const
{
    return m_token;
}

QString StartOrderContainer::QRCode() const
{
    return m_QRCode;
}

QString StartOrderContainer::pairingStatus() const
{
    return m_pairingStatus;
}

GlobalSettings::Enums::OrderType StartOrderContainer::orderType()
{
    return m_orderType;
}

QString StartOrderContainer::linkOrder()
{
    return m_linkOrder;
}

void StartOrderContainer::fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem)
{
    StartOrderRequestItem *request = qobject_cast<StartOrderRequestItem*>(requestItem);
    StartOrderResponseItem *response = qobject_cast<StartOrderResponseItem*>(responseItem);

    if(!request || !response){
        LOGD()<<"Invalid data";
        return;
    }

    setOrderId(response->orderUuid());
    setOrderStatus(GlobalSettings::Enums::OrderStatus(response->orderStatusType()->codeReasonAttribute().toInt()));
    setValue(request->orderType()->requestedAmountType()->amount());
    if(request->couponsType()->processedCouponListType().count() > 0){
        double rabatAmount = 0;
        for(int i = 0; i < request->couponsType()->processedCouponListType().count(); ++i){
            rabatAmount += request->couponsType()->processedCouponListType().at(i)->couponValueType()->amount().toDouble();
        }
        setDiscount(QString::number(rabatAmount));
        setCurrency(request->couponsType()->processedCouponListType().at(0)->couponValueType()->currency());
    }
//    setDiscount(request->orderType()->customerBenefitType()->amount());
//    setCurrency(request->orderType()->requestedAmountType()->currency());
    setCashierUuid(request->merchantInformationType()->merchantUuid());
    setCashierName(request->merchantInformationType()->merchantAliasId());
    setCashierAlias(request->merchantInformationType()->merchantAliasId());
    setCashRegisterId(request->merchantInformationType()->cashRegisterId());
    setServiceAgentId(request->merchantInformationType()->serviceAgentUuid());
    setToken(response->token());
    setQRCode(response->QRCode());
    setPairingStatus(response->pairingStatus());
    if (request->orderType()->typeAttribute() == "PAYMENT_IMMEDIATE") {
        setOrderType(GlobalSettings::Enums::OrderType::PAYMENT_IMMEDIATE);
    } else if (request->orderType()->typeAttribute() == "REVERSAL") {
        setOrderType(GlobalSettings::Enums::OrderType::REVERSAL);
    }
    setLinkOrder(request->orderType()->linkType()->orderUuid());
}

void StartOrderContainer::setOrderId(QString orderId)
{
    if (m_orderId == orderId)
        return;

    m_orderId = orderId;
    emit orderIdChanged(orderId);
}

void StartOrderContainer::setOrderStatus(GlobalSettings::Enums::OrderStatus orderStatus)
{
    if (m_orderStatus == orderStatus)
        return;

    m_orderStatus = orderStatus;
    emit orderStatusChanged(orderStatus);
}

void StartOrderContainer::setValue(QString value)
{
    if (m_value == value)
        return;

    m_value = value;
    emit valueChanged(value);
}

void StartOrderContainer::setDiscount(QString discount)
{
    if (m_discount == discount)
        return;

    m_discount = discount;
    emit discountChanged(discount);
}

void StartOrderContainer::setCurrency(QString currency)
{
    if (m_currency == currency)
        return;

    m_currency = currency;
    emit currencyChanged(currency);
}

void StartOrderContainer::setCashierUuid(QString cashierUuid)
{
    if (m_cashierUuid == cashierUuid)
        return;

    m_cashierUuid = cashierUuid;
    emit cashierUuidChanged(cashierUuid);
}

void StartOrderContainer::setCashierName(QString cashierName)
{
    if (m_cashierName == cashierName)
        return;

    m_cashierName = cashierName;
    emit cashierNameChanged(cashierName);
}

void StartOrderContainer::setCashierAlias(QString cashierAlias)
{
    if (m_cashierAlias == cashierAlias)
        return;

    m_cashierAlias = cashierAlias;
    emit cashierAliasChanged(cashierAlias);
}

void StartOrderContainer::setCashRegisterId(QString cashRegisterId)
{
    if (m_cashRegisterId == cashRegisterId)
        return;

    m_cashRegisterId = cashRegisterId;
    emit cashRegisterIdChanged(cashRegisterId);
}

void StartOrderContainer::setServiceAgentId(QString serviceAgentId)
{
    if (m_serviceAgentId == serviceAgentId)
        return;

    m_serviceAgentId = serviceAgentId;
    emit serviceAgentIdChanged(serviceAgentId);
}

void StartOrderContainer::setToken(QString token)
{
    if (m_token == token)
        return;

    m_token= token;
    emit tokenChanged(token);
}

void StartOrderContainer::setQRCode(QString QRCode)
{
    if (m_QRCode == QRCode)
        return;

    m_QRCode = QRCode;
    emit QRCodeChanged(QRCode);
}

void StartOrderContainer::setPairingStatus(QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}

void StartOrderContainer::setOrderType(GlobalSettings::Enums::OrderType orderType)
{
    if (m_orderType == orderType)
        return;

    m_orderType = orderType;
    emit orderTypeChanged(orderType);
}

void StartOrderContainer::setLinkOrder(QString linkOrder)
{
    if (m_linkOrder == linkOrder)
        return;

    m_linkOrder = linkOrder;
    emit linkOrderChanged(linkOrder);
}
