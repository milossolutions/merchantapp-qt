#ifndef MONITORCHECKINCONTAINER_H
#define MONITORCHECKINCONTAINER_H

#include <containeriteminterface.h>

class LoyaltyType;
class CouponType;

/*!
 * \class MonitorCheckInContainer
 * \brief MonitorCheckInContainer is filled with data from MonitorCheckIn request and response.
 * Hold coupons avalible for redemption by customer.
 */
class MonitorCheckInContainer : public ContainerItemInterface
{
    Q_OBJECT
    Q_PROPERTY(QString pairingUuid READ pairingUuid WRITE setPairingUuid NOTIFY pairingUuidChanged)
    Q_PROPERTY(QString pairingStatus READ pairingStatus WRITE setPairingStatus NOTIFY pairingStatusChanged)
    Q_PROPERTY(QList<LoyaltyType *> loyaltyListType READ loyaltyListType WRITE setLoyaltyListType NOTIFY loyaltyListTypeChanged)
    Q_PROPERTY(QList<CouponType *> couponListType READ couponListType WRITE setCouponListType NOTIFY couponListTypeChanged)

public:
    explicit MonitorCheckInContainer(QObject *parent = 0);
    ~MonitorCheckInContainer();

    QString pairingUuid() const;
    QString pairingStatus() const;
    QList<LoyaltyType *> loyaltyListType() const;
    QList<CouponType *> couponListType() const;

signals:
    void pairingUuidChanged(QString pairingUuid);
    void pairingStatusChanged(QString pairingStatus);
    void loyaltyListTypeChanged(QList<LoyaltyType *> loyaltyListType);
    void couponListTypeChanged(QList<CouponType *> couponListType);

public slots:
    void fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem);

    void setPairingUuid(QString pairingUuid);
    void setPairingStatus(QString pairingStatus);
    void setLoyaltyListType(QList<LoyaltyType *> loyaltyListType);
    void setCouponListType(QList<CouponType *> couponListType);

private:
    QString m_pairingUuid;
    QString m_pairingStatus;
    QList<LoyaltyType *> m_loyaltyListType;
    QList<CouponType *> m_couponListType;
};

#endif // MONITORCHECKINCONTAINER_H
