#include "monitorcheckincontainer.h"

#include "monitorcheckinrequestitem.h"
#include "monitorcheckinresponseitem.h"
#include "checkinnotificationtype.h"
#include "customerinformationtype.h"
#include "loyaltytype.h"
#include "coupontype.h"
#include "currencyamounttype.h"

#include "globalsettings.h"

MonitorCheckInContainer::MonitorCheckInContainer(QObject *parent) :
    ContainerItemInterface(parent)
{

}

MonitorCheckInContainer::~MonitorCheckInContainer()
{

}

QString MonitorCheckInContainer::pairingUuid() const
{
    return m_pairingUuid;
}

QString MonitorCheckInContainer::pairingStatus() const
{
    return m_pairingStatus;
}

QList<LoyaltyType *> MonitorCheckInContainer::loyaltyListType() const
{
    return m_loyaltyListType;
}

QList<CouponType *> MonitorCheckInContainer::couponListType() const
{
    return m_couponListType;
}

void MonitorCheckInContainer::fillData(RequestItemInterface *requestItem, ResponseItemInterface *responseItem)
{
    MonitorCheckInRequestItem *request = qobject_cast<MonitorCheckInRequestItem*>(requestItem);
    MonitorCheckInResponseItem *response = qobject_cast<MonitorCheckInResponseItem*>(responseItem);

    this->setPairingUuid(response->checkInNotificationType()->pairingUuid());
    this->setPairingStatus(response->checkInNotificationType()->pairingStatus());

    QList<LoyaltyType *> loyaltyTypeList;
    for(int i = 0; i<response->checkInNotificationType()->customerInformationType()->loyaltyListType().count(); i++){
        LoyaltyType *loyaltyTypePointer = response->checkInNotificationType()->customerInformationType()->loyaltyListType().at(i);
        LoyaltyType *loyaltyType = new LoyaltyType(this);
        loyaltyType->setProgram(loyaltyTypePointer->program());
        loyaltyType->setReference(loyaltyTypePointer->reference());
        loyaltyTypeList.append(loyaltyType);
    }
    this->setLoyaltyListType(loyaltyTypeList);

    //LOGD()<<"HERE COUPON COUNT LIST"<<response->checkInNotificationType()->customerInformationType()->couponListType().count();
    QList<CouponType *> couponTypeList;
    for(int i = 0; i<response->checkInNotificationType()->customerInformationType()->couponListType().count(); i++){
        CouponType *couponTypePointer = response->checkInNotificationType()->customerInformationType()->couponListType().at(i);
        CouponType *couponType = new CouponType(this);
        //LOGD()<<"COUPON ID "<<couponTypePointer->couponId();
        couponType->setCouponId(couponTypePointer->couponId());
        CurrencyAmountType *couponValue = new CurrencyAmountType(this);
        couponValue->setAmount(couponTypePointer->couponValueType()->amount());
        couponValue->setCurrency(couponTypePointer->couponValueType()->currency());
        couponType->setCouponValueType(couponValue);

        couponTypeList.append(couponType);
    }
    this->setCouponListType(couponTypeList);
}

void MonitorCheckInContainer::setPairingUuid(QString pairingUuid)
{
    if (m_pairingUuid == pairingUuid)
        return;

    m_pairingUuid = pairingUuid;
    emit pairingUuidChanged(pairingUuid);
}

void MonitorCheckInContainer::setPairingStatus(QString pairingStatus)
{
    if (m_pairingStatus == pairingStatus)
        return;

    m_pairingStatus = pairingStatus;
    emit pairingStatusChanged(pairingStatus);
}

void MonitorCheckInContainer::setLoyaltyListType(QList<LoyaltyType *> loyaltyListType)
{
    if (m_loyaltyListType == loyaltyListType)
        return;

    m_loyaltyListType = loyaltyListType;
    emit loyaltyListTypeChanged(loyaltyListType);
}

void MonitorCheckInContainer::setCouponListType(QList<CouponType *> couponListType)
{
    if (m_couponListType == couponListType)
        return;

    m_couponListType = couponListType;
    emit couponListTypeChanged(couponListType);
}
