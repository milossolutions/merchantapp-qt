#ifndef ITEMINTERFACE_H
#define ITEMINTERFACE_H

#include <QObject>

#include <QMetaProperty>

class QXmlStreamReader;

/**
 * \class ItemInterface
 * \brief This is the base class for each data type, request element and response element.
 */
class ItemInterface : public QObject
{
    Q_OBJECT
public:
    explicit ItemInterface(QObject *parent = 0);

    /**
    * \brief Function responsible for filling object properties based on msgData
    * \param msgData - response data from the server
    * \return
    */
    virtual void parse(QByteArray msgData);
    /**
    * \brief Function responsible for parsing each type, which are included in the element.
    * \param msgData - response data from the server
    * \return
    */
    void parseChildrens(QByteArray msgData);
    /**
    * \brief Function gets childerns of object.
    * \param childrens - input objects.
    * \param output - list of children object.
    * \return
    */
    void getAllChildrensOfObject(QObjectList childrens, QObjectList &output);
    /**
    * \brief Function checks whether any property is set.
    * \param objectList
    * \return true if any property is set, otherwise false
    */
    bool isAnyPropertySet(QObjectList objectList);
    /**
    * \brief Function parses the attributes of the elements.
    * \param xmlReader
    * \return
    */
    virtual void parseElementAttributes(QXmlStreamReader &xmlReader);
    /**
    * \brief Function creates a list of object properties. Properties are stored in m_metaPropertyList.
    * \return
    */
    void createPropertyList();

signals:

public slots:

protected:
    /**
    * \brief Function gets all properties of object.
    * \return
    */
    QList<QMetaProperty> getPropertyListOfObject(QObject *object);

    QList<QMetaProperty> m_metaPropertyList;
};

#endif // ITEMINTERFACE_H
