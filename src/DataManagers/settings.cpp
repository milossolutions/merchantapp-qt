#include "settings.h"

#include "globalsettings.h"

#include <QDir>
#include <QStandardPaths>
#include <QApplication>
#include <QSettings>
#include <QWriteLocker>
#include <QReadLocker>
#include <QApplication>
#include <QMessageBox>
#include <QProcess>

Settings* Settings::m_instance = Q_NULLPTR;

/**
 * \class Settings
 * \brief Settings singleton class that is used to store application settings in system registry
 */
Settings::Settings(QObject *parent) :
    QObject(parent),
    m_translator(new QTranslator(this))
{
    init();
}

void Settings::init()
{
    loadSettings();
}

/**
 * \brief Function responsible for allowing starting new orders.
 * Button will be disabled untill settings are filled.
 */
void Settings::checkIfDataIsFilled()
{
    if(!merchantUuid().isEmpty() && !cashRegisterId().isEmpty()){
        setIsDataFilled(true);
    }else{
        setIsDataFilled(false);
    }
}

Settings::~Settings()
{
    storeSettings();
}

Settings *Settings::instance()
{
    if (!m_instance)
        m_instance = new Settings(QCoreApplication::instance());

    return m_instance;
}

/**
 * \brief Loads settings from system registry and fills properties
 */
void Settings::loadSettings()
{
    QSettings settings("TWINT AG", "TWINT Merchant");
    setLanguage(settings.value("LANGUAGE","").toString());
    setApplicationName(settings.value("APPLICATION_NAME","TWINT Merchant").toString());
    m_lastLoginDateTime = settings.value("LAST_USER_LOGIN_DATETIME", "").toDateTime();
    setMerchantAlias(settings.value("MERCHANT_ALIAS","").toString());
    setMerchantUuid(settings.value("MERCHANT_UUID","").toString());
    setCashRegisterId(settings.value("CASH_REGISTER_ID","").toString());
    setCerfiticatePath(settings.value("CERTIFICATE_PATH","").toString());
    setCertificatePassword(settings.value("CERTIFICATE_PASSWORD","").toString());
    setDeviceType(settings.value("DEVICE_TYPE","USB").toString());
    setUsbPort(settings.value("USB_PORT","").toString());
    setKeackonUrl(settings.value("KEACKON_URL","").toString());
    setUrlPort(settings.value("URL_PORT","").toString());
    setRunOnStartup(settings.value("RUN_ON_STARTUP",false).toBool());
    setIsSelectUser(settings.value("IS_SELECTED_USER",false).toBool());
    setCashRegisterIdType(settings.value("CASH_REGISTER_ID_TYPE","").toString());
    setShowKeyboard(settings.value("SHOW_KEYBOARD",false).toBool());
    setCertificateData(settings.value("CERTIFICATE_DATA","").toByteArray());
    setCertificateKeyData(settings.value("CERTIFICATE_KEY_DATA","").toByteArray());
    setNextVersionCheckDate(settings.value("NEXT_VERSION_CHECK_DATE","0").toInt());

    checkIfDataIsFilled();
}

/**
 * \brief Saves settings in system registry
 */
void Settings::storeSettings()
{
    QSettings settings("TWINT AG", "TWINT Merchant");
    settings.setValue("LANGUAGE", language());
    settings.setValue("APPLICATION_NAME", applicationName());
    settings.setValue("LAST_USER_LOGIN_DATETIME", m_lastLoginDateTime);
    settings.setValue("MERCHANT_ALIAS", merchantAlias());
    settings.setValue("MERCHANT_UUID", merchantUuid());
    settings.setValue("CASH_REGISTER_ID", cashRegisterId());
    settings.setValue("CERTIFICATE_PATH", cerfiticatePath());
    settings.setValue("CERTIFICATE_PASSWORD", certificatePassword());
    settings.setValue("DEVICE_TYPE", deviceType());
    settings.setValue("USB_PORT", usbPort());
    settings.setValue("KEACKON_URL", keackonUrl());
    settings.setValue("URL_PORT", urlPort());
    settings.setValue("RUN_ON_STARTUP", runOnStartup());
    settings.setValue("IS_SELECTED_USER", isSelectUser());
    settings.setValue("CASH_REGISTER_ID_TYPE", cashRegisterIdType());
    settings.setValue("SHOW_KEYBOARD", showKeyboard());
    settings.setValue("NEXT_VERSION_CHECK_DATE", nextVersionCheckDate());

    checkIfDataIsFilled();

    emit settingsChanged();
}

/*!
 * \brief Stores certificate data in system registry when new certificate was properly read
 */
void Settings::storeCertificateData()
{
    QSettings settings("TWINT AG", "TWINT Merchant");
    settings.setValue("CERTIFICATE_DATA", certificateData());
    settings.setValue("CERTIFICATE_KEY_DATA", certificateKeyData());
}

/*!
 * \brief Updates value of next version check date
 */
void Settings::updateNextVersionCheck()
{
    QSettings settings("TWINT AG", "TWINT Merchant");
    settings.setValue("NEXT_VERSION_CHECK_DATE", certificateData());
}

/*!
 * \brief Stores prefix of selected language.
 * Used in translations
 * \return QString with prefix
 */
QString Settings::language()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_language;
    }
}

/*!
 * \brief Helper method to be able dynamic translation in QML
 * see https://wiki.qt.io/How_to_do_dynamic_translation_in_QML
 * \return
 */
QString Settings::emptyString() const
{
    return QStringLiteral("");
}

void Settings::setDefaults()
{
    //setLanguage(language());
}

void Settings::setLanguage(QString language)
{
    {
        QWriteLocker locker(&m_readWriteLocker);

        m_language = language;
    }
    installTranslator(language);
    emit languageChanged(m_language);
}

void Settings::setApplicationName(QString applicationName)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_applicationName == applicationName)
            return;

        m_applicationName = applicationName;
    }
    emit applicationNameChanged(applicationName);
}

void Settings::setApplicationVersion(QString applicationVersion)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_applicationVersion == applicationVersion)
            return;

        m_applicationVersion = applicationVersion;
    }
    emit applicationVersionChanged(applicationVersion);
}

/*!
 * \brief Used for storing data about last successfull login.
 * Currently unused due to login system changes
 */
void Settings::logIn()
{
    m_lastLoginDateTime = QDateTime::currentDateTime();
}

int Settings::timestampForLastLogin()
{
    QDateTime current = QDateTime::currentDateTime();
    if (!m_lastLoginDateTime.isValid()) {
        return -1;
    }
    return current.toTime_t() - m_lastLoginDateTime.toTime_t();
}

void Settings::setMerchantAlias(QString merchantAlias)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_merchantAlias == merchantAlias)
            return;

        m_merchantAlias = merchantAlias;
    }
    emit merchantAliasChanged(merchantAlias);
}

void Settings::setMerchantUuid(QString merchantUuid)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_merchantUuid == merchantUuid)
            return;

        m_merchantUuid = merchantUuid;
    }
    emit merchantUuidChanged(merchantUuid);
}

void Settings::setCashRegisterId(QString cashRegisterId)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_cashRegisterId == cashRegisterId)
            return;

        m_cashRegisterId = cashRegisterId;
    }
    emit cashRegisterIdChanged(cashRegisterId);
}

void Settings::setCerfiticatePath(QString cerfiticatePath)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_cerfiticatePath == cerfiticatePath)
            return;

        m_cerfiticatePath = cerfiticatePath;
    }
    emit cerfiticatePathChanged(cerfiticatePath);
}

void Settings::setCertificatePassword(QString certificatePassword)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_certificatePassword == certificatePassword)
            return;

        m_certificatePassword = certificatePassword;
    }
    emit certificatePasswordChanged(certificatePassword);
}

void Settings::setDeviceType(QString deviceType)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_deviceType == deviceType)
            return;

        m_deviceType = deviceType;
    }
    emit deviceTypeChanged(deviceType);
}

void Settings::setUsbPort(QString usbPort)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_usbPort == usbPort)
            return;

        m_usbPort = usbPort;
    }
    emit usbPortChanged(usbPort);
}

void Settings::setKeackonUrl(QString keackonUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_keackonUrl == keackonUrl)
            return;

        m_keackonUrl = keackonUrl;
    }
    emit keackonUrlChanged(keackonUrl);
}

void Settings::setUrlPort(QString urlPort)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_urlPort == urlPort)
            return;

        m_urlPort = urlPort;
    }
    emit urlPortChanged(urlPort);
}

void Settings::setRunOnStartup(bool runOnStartup)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_runOnStartup == runOnStartup)
            return;

        m_runOnStartup = runOnStartup;
    }
    emit runOnStartupChanged(runOnStartup);
}

void Settings::setIsDataFilled(bool isDataFilled)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_isDataFilled == isDataFilled)
            return;

        m_isDataFilled = isDataFilled;
    }
    emit isDataFilledChanged(isDataFilled);
}

void Settings::setIsSelectUser(bool isSelectUser)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_isSelectUser == isSelectUser)
            return;

        m_isSelectUser = isSelectUser;
    }
    emit isSelectUserChanged(isSelectUser);
}

void Settings::setCashRegisterIdType(QString cashRegisterIdType)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_cashRegisterIdType == cashRegisterIdType)
            return;

        m_cashRegisterIdType = cashRegisterIdType;
    }
    emit cashRegisterIdTypeChanged(cashRegisterIdType);
}

void Settings::setCertificateData(QByteArray certificateData)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_certificateData == certificateData)
            return;

        m_certificateData = certificateData;
    }
    emit certificateDataChanged(certificateData);
}

void Settings::setCertificateKeyData(QByteArray certificateKeyData)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_certificateKeyData == certificateKeyData)
            return;

        m_certificateKeyData = certificateKeyData;
    }
    emit certificateKeyDataChanged(certificateKeyData);
}

void Settings::setNextVersionCheckDate(int nextVersionCheckDate)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_nextVersionCheckDate == nextVersionCheckDate)
            return;

        m_nextVersionCheckDate = nextVersionCheckDate;
    }
    emit nextVersionCheckDateChanged(nextVersionCheckDate);
}

void Settings::setShowKeyboard(bool showKeyboard)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_showKeyboard == showKeyboard)
            return;

        m_showKeyboard = showKeyboard;
    }
    emit showKeyboardChanged(showKeyboard);
}

/*!
 * \brief Install language of application based on chosen language
 * \param lang Chosen language option
 * \return true if translator was installed properly
 *
 * On fresh application start function scans local language system and automatically sets language if supported.
 * If not is uses english as default.
 */
bool Settings::installTranslator(const QString &lang)
{
    QString prefix = lang;
    if(lang.isEmpty()){
        //Load system language
        LOGD()<<"Loading system language";
        QString defaultLocale = QLocale::system().name(); // e.g. "de_DE"
        LOGD()<<defaultLocale;
        if(defaultLocale == "de_CH"){
            prefix= "de";
        }else if(defaultLocale == "fr_CH"){
            prefix= "fr";
        }else{
            defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"
            prefix = defaultLocale;
        }
        LOGD()<<"System language"<<prefix;
    }

    if(prefix != QLatin1String("en") &&
            prefix != QLatin1String("de") &&
            prefix != QLatin1String("fr") &&
            prefix != QLatin1String("it")){
        //If prefix is different from application langagues set default english
        prefix = QStringLiteral("en");
    }

    m_language = prefix;

    if (!m_translator->load(":/translations/tr_" + prefix)){
        LOGD()<<"Load failed prefix = "<<prefix;
        return false;
    }

    bool translator = QCoreApplication::installTranslator(m_translator);
    LOGD()<<"Was translation successfull?"<<translator<<prefix<<lang;
    return translator;
}

/*!
 * \brief Application name
 * \return
 */
QString Settings::applicationName()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_applicationName;
    }
}

/*!
 * \brief Current application version
 * \return
 */
QString Settings::applicationVersion()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_applicationVersion;
    }
}

/*!
 * \brief Merchant alias
 * \return
 * A simple text alias for the merchant can be assigned in addition to the MerchantUuid.
 * It can be used to identify the merchant as an alternative to the UUID.
 * Was removed from settings screen in process.
 */
QString Settings::merchantAlias()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_merchantAlias;
    }
}

/*!
 * \brief Merchant uuid number. Inditificates person assigned to certificate. Used in all requests.
 * \return QString of MerchantUuid
 */
QString Settings::merchantUuid()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_merchantUuid;
    }
}

/*!
 * \brief Cash register name.
 * \return QString with CashRegisterId
 *
 * Multiple cash register can be used with one certificate.
 * Cash register id should be unique with correlation of merchant uuid.
 * If two cash register with the same name and certificate will be used at the same time it will cause InternalErrors in TWINT api.
 */
QString Settings::cashRegisterId()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_cashRegisterId;
    }
}

/*!
 * \brief Path to certificate file
 * \return
 */
QString Settings::cerfiticatePath()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_cerfiticatePath;
    }
}

/*!
 * \brief Certificate file password used to decypher it's content
 * \return
 */
QString Settings::certificatePassword()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_certificatePassword;
    }
}

/*!
 * \brief Used beacon type.
 * \return
 *
 * Currently there are two options:
 * No Twint beacon - means we will not use beacon for pairng
 * TWINT beacon - user can use beacon device in pairing process
 * WIFI - removed option
 */
QString Settings::deviceType()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_deviceType;
    }
}

/*!
 * \brief Port where usb beacon is connected to.
 * \return
 */
QString Settings::usbPort()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_usbPort;
    }
}

/*!
 * \brief WIFI keacon address currently unused
 * \return
 */
QString Settings::keackonUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_keackonUrl;
    }
}

/*!
 * \brief WIFI url port currently unused
 * \return
 */
QString Settings::urlPort()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_urlPort;
    }
}

/*!
 * \brief If set to true application will be started on system start up
 * \return
 */
bool Settings::runOnStartup()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_runOnStartup;
    }
}

/**
 * \brief Function adds records to registry system settings on Windows and Mac OS if run on startup is choosen.
 * \param runOnStartup
 */
void Settings::createAutorunRegistry(bool runOnStartup)
{
#ifdef Q_OS_WIN
    QSettings settings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
    if (runOnStartup) {
        settings.setValue("TWINT_APP", QDir::toNativeSeparators(QApplication::applicationFilePath()));
    } else {
        settings.remove("TWINT_APP");
    }
#elif defined(Q_OS_MAC)
    QFile file(QDir::homePath() + "/Library/LaunchAgents/com.twintMerchant.plist");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    if (runOnStartup) {
        out <<"<plist version=\"1.0\"><dict><key>Label</key><string>com.twintMerchant</string><key>RunAtLoad</key><true/><key>Program</key><string>"+QApplication::applicationFilePath()+"</string></dict></plist>";
    } else {
        out <<"<plist version=\"1.0\"><dict><key>Label</key><string>com.twintMerchant</string><key>RunAtLoad</key><false/><key>Program</key><string>"+QApplication::applicationFilePath()+"</string></dict></plist>";
    }
    file.close();
    QProcess *myProcess = new QProcess(this);
    QStringList arg;
    arg.append("load");
    arg.append(QDir::homePath() +"/Library/LaunchAgents/com.twintMerchant.plist");
    myProcess->start("launchctl", arg);
#endif
}

/*!
 * \brief Set to true if Merchant uuid and cash register id was set
 * \return
 */
bool Settings::isDataFilled()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_isDataFilled;
    }
}

/*!
 * \brief Timestamp of date when can next "Standard update avalible be displayed"
 * \return
 */
int Settings::nextVersionCheckDate()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_nextVersionCheckDate;
    }
}

/*!
 * \brief Certificate QByteArray data saved in system.
 *
 * Allows authorization process even when certificate file was removed from hard drive.
 * \return
 */
QByteArray Settings::certificateData()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_certificateData;
    }
}

/*!
 * \brief Private key QByteArray data saved in system.
 * \return
 */
QByteArray Settings::certificateKeyData()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_certificateKeyData;
    }
}

/*!
 * \brief If set to true system keyboard will be showed on entering input fields
 * \return
 */
bool Settings::showKeyboard()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_showKeyboard;
    }
}

/*!
 * \brief Value of select user checkbox
 * \return
 *
 * If set to true input field will be displayed in application header that allows to eaisly change cash register id between creating orders request.
 */
bool Settings::isSelectUser()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_isSelectUser;
    }
}

/*!
 * \brief Value differs between Numeric and Alphanumeric cash register name
 * \return
 *
 * Numeric - cash register id must be 3 characters long integer
 * Aplhanumeric - From 1 to 15 characters string
 */
QString Settings::cashRegisterIdType()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_cashRegisterIdType;
    }
}
