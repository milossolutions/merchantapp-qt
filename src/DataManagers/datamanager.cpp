#include "datamanager.h"
#include "globalsettings.h"

#include "neworderlistmodel.h"
#include "currentorderitem.h"
#include "startordercontainer.h"
#include "startorderresponseitem.h"
#include "couponlistmodel.h"
#include "coupontype.h"
#include "currencyamounttype.h"

#include "requestcheckincontainer.h"
#include "monitorcheckincontainer.h"

/*!
 * \class DataManager
 * \brief Class stores data about models, sql database
 */
DataManager::DataManager(QString connectionName, QObject *parent) :
    QObject(parent),
    m_currentOrderItem(new CurrentOrderItem(this)),
    m_couponListModel(new CouponListModel(this)),
    m_newOrderListModel(new NewOrderListModel(connectionName, this))
{
    prepareConnections();
}

DataManager::~DataManager()
{

}

/*!
 * \brief Object used to store data about on going transaction.
 * \return
 */
CurrentOrderItem *DataManager::currentOrderItem() const
{
    return m_currentOrderItem;
}

/*!
 * \brief Model that store information about downloaded coupons that can be used in transaction.
 * \return
 */
CouponListModel *DataManager::couponListModel() const
{
    return m_couponListModel;
}

/*!
 * \brief Transactions order list model. Stores and provide data about completed transactions.
 * \return
 */
NewOrderListModel *DataManager::newOrderListModel() const
{
    return m_newOrderListModel;
}

void DataManager::setCurrentOrderItem(CurrentOrderItem *currentOrderItem)
{
    if (m_currentOrderItem == currentOrderItem)
        return;

    m_currentOrderItem = currentOrderItem;
    emit currentOrderItemChanged(currentOrderItem);
}

void DataManager::setCouponListModel(CouponListModel *couponListModel)
{
    if (m_couponListModel == couponListModel)
        return;

    m_couponListModel = couponListModel;
    emit couponListModelChanged(couponListModel);
}

/*!
 * \brief Parses received data from RequestCheckIn and fills Coupons model with it.
 * \param containerItemInterface RequestCheckInContainer
 */
void DataManager::receivedCouponsDataFromRequest(ContainerItemInterface *containerItemInterface)
{
    RequestCheckInContainer *requestCheckInContainer = qobject_cast<RequestCheckInContainer*>(containerItemInterface);
    if(!requestCheckInContainer){
        return;
    }

    m_couponListModel->clear();

    for(int i = 0; i < requestCheckInContainer->couponListType().count(); ++i){
        CouponType *couponPointer = requestCheckInContainer->couponListType().at(i);
        CouponData couponData;
        couponData.setTitle(couponPointer->couponId());
        couponData.setDetailDescription(couponPointer->couponId());
        couponData.setCouponId(couponPointer->couponId());
        couponData.setAmount(couponPointer->couponValueType()->amount().toDouble());
        m_couponListModel->addCoupon(couponData);
    }

    if(m_couponListModel->count() > 0){
        emit showCouponsList();
    } else {
        if(!(currentOrderItem()->orderValue() == QString::null)){
            emit makeRequest(GlobalSettings::Enums::StartOrder, QStringList{currentOrderItem()->orderValue(), "PAYMENT_IMMEDIATE"});
        }
    }

    requestCheckInContainer->deleteLater();
}

/*!
 * \brief Parses received data from MonitorCheckIn and fills Coupons model with it.
 * \param containerItemInterface MonitorCheckInContainer
 */
void DataManager::receivedCouponsDataFromMonitor(ContainerItemInterface *containerItemInterface)
{
    MonitorCheckInContainer *requestCheckInContainer = qobject_cast<MonitorCheckInContainer*>(containerItemInterface);
    if(!requestCheckInContainer){
        return;
    }

    m_couponListModel->clear();

    for(int i = 0; i < requestCheckInContainer->couponListType().count(); ++i){
        CouponType *couponPointer = requestCheckInContainer->couponListType().at(i);
        CouponData couponData;
        couponData.setTitle(couponPointer->couponId());
        couponData.setDetailDescription(couponPointer->couponId());
        couponData.setCouponId(couponPointer->couponId());
        couponData.setAmount(couponPointer->couponValueType()->amount().toDouble());
        m_couponListModel->addCoupon(couponData);
    }

    if(m_couponListModel->count() > 0){
        emit showCouponsList();
    } else {
        if(!(currentOrderItem()->orderValue() == QString::null)){
            emit makeRequest(GlobalSettings::Enums::StartOrder, QStringList{currentOrderItem()->orderValue(), "PAYMENT_IMMEDIATE"});
        }
    }

    requestCheckInContainer->deleteLater();
}

void DataManager::setNewOrderListModel(NewOrderListModel *newOrderListModel)
{
    if (m_newOrderListModel == newOrderListModel)
        return;

    m_newOrderListModel = newOrderListModel;
    emit newOrderListModelChanged(newOrderListModel);
}

void DataManager::prepareConnections()
{
    connect(this, &DataManager::refreshTransactionsModel, m_newOrderListModel, &NewOrderListModel::refreshModel, Qt::QueuedConnection);
}
