#ifndef GLOBALSETTINGS_H
#define GLOBALSETTINGS_H

#include <QObject>
#include <QCoreApplication>
#include <QStandardPaths>
#include <QDebug>
#include <QDateTime>
#include <QReadWriteLock>

#define FUNCTION_DESC Q_FUNC_INFO
#ifdef QT_DEBUG
#define LOG_CONTENT << QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss:zzz")\
    << "[" << FUNCTION_DESC << "] "
#else
#define LOG_CONTENT << "[" << FUNCTION_DESC << "] "
#endif
#define LOGI() qInfo() << "[I]" LOG_CONTENT
#define LOGD() qDebug() << "[D]" LOG_CONTENT
#define LOGW() qWarning() << "[W]" LOG_CONTENT
#define LOGC() qCritical() << "[C]" LOG_CONTENT
#define LOGF(value) QString log; \
    log.append("[F] "); \
    log.append(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss:zzz")); \
    log.append(" ["); \
    log.append(FUNCTION_DESC); \
    log.append("] "); \
    log.append(value); \
    qFatal(log.toLatin1().constData());

namespace GlobalSettings {

/**
 * \class GlobalSettings::Paths::Paths
 * \brief Singleton class stat stores paths of application.
 */
class Paths : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString TwintServerUrl READ TwintServerUrl WRITE setTwintServerUrl NOTIFY TwintServerUrlChanged)
    Q_PROPERTY(QString TwintIntUrl READ TwintIntUrl WRITE setTwintIntUrl NOTIFY TwintIntUrlChanged)
    Q_PROPERTY(QString TwintPatUrl READ TwintPatUrl WRITE setTwintPatUrl NOTIFY TwintPatUrlChanged)
    Q_PROPERTY(QString TwintLiveUrl READ TwintLiveUrl WRITE setTwintLiveUrl NOTIFY TwintLiveUrlChanged)
    Q_PROPERTY(QString TwintRefUrl READ TwintRefUrl WRITE setTwintRefUrl NOTIFY TwintRefUrlChanged)
    Q_PROPERTY(QString TwintMajorUrl READ TwintMajorUrl WRITE setTwintMajorUrl NOTIFY TwintMajorUrlChanged)
    Q_PROPERTY(int twintServerId READ twintServerId WRITE setTwintServerId NOTIFY twintServerIdChanged)
    Q_PROPERTY(QString TwintVersionUrl READ TwintVersionUrl WRITE setTwintVersionUrl NOTIFY TwintVersionUrlChanged)
    Q_PROPERTY(QString TwintApplicationUrl READ TwintApplicationUrl WRITE setTwintApplicationUrl NOTIFY TwintApplicationUrlChanged)

public:
    static Paths* instance();

    /**
     * \brief AppDirName
     * Application name
     */
    const QString AppDirName;
    /**
     * \brief DocumentPath
     * System documents path
     */
    const QString DocumentPath;
    /**
     * \brief DatabasePath
     * Database file path
     */
    const QString DatabasePath;


    /**
    * \brief TwintServerUrl
    * Here we store adress to server that we connect to
    */
    QString TwintServerUrl();
    /**
     * \brief TwintIntUrl
     * Twint test server path
     */
    QString TwintIntUrl();
    /**
     * \brief TwintPatUrl
     */
    QString TwintPatUrl();
    /**
     * \brief TwintLiveUrl
     */
    QString TwintLiveUrl();

    /**
     * \brief TwintRefUrl
     */
    QString TwintRefUrl();

    int twintServerId();

    /**
     * \brief TwintVersionUrl ulr where new version file can be downloaded
     */
    QString TwintVersionUrl();

    /**
     * \brief TwintApplicationUrl url where new application version is stored
     */
    QString TwintApplicationUrl();

    /**
     * \brief TwintMajorUrl
     */
    QString TwintMajorUrl();

signals:
    void TwintServerUrlChanged(QString TwintServerUrl);
    void TwintIntUrlChanged(QString TwintIntUrl);
    void TwintPatUrlChanged(QString TwintPatUrl);
    void TwintLiveUrlChanged(QString TwintLiveUrl);
    void TwintRefUrlChanged(QString TwintRefUrl);
    void twintServerIdChanged(int twintServerId);
    void TwintVersionUrlChanged(QString TwintVersionUrl);
    void TwintMajorUrlChanged(QString TwintMajorUrl);

    void TwintApplicationUrlChanged(QString TwintApplicationUrl);

public slots:
    void setTwintServerUrl(QString TwintServerUrl);
    void setTwintIntUrl(QString TwintIntUrl);
    void setTwintPatUrl(QString TwintPatUrl);
    void setTwintLiveUrl(QString TwintLiveUrl);
    void setTwintRefUrl(QString TwintRefUrl);
    void setTwintServerId(int twintServerId);
    void setTwintVersionUrl(QString TwintVersionUrl);
    void setTwintApplicationUrl(QString TwintApplicationUrl);
    void setTwintMajorUrl(QString TwintMajorUrl);

private:
    Paths(QObject *parent = 0);
    static Paths *m_instance;

    QReadWriteLock m_readWriteLocker;

    QString m_TwintServerUrl;
    QString m_TwintIntUrl;
    QString m_TwintPatUrl;
    QString m_TwintLiveUrl;
    QString m_TwintRefUrl;
    int m_twintServerId;
    QString m_TwintVersionUrl;
    QString m_TwintApplicationUrl;
    QString m_TwintMajorUrl;
};

/**
 * \brief The Enums class
 * Enums class stores information about application enums.
 */
class Enums : public QObject
{
    Q_OBJECT
    Q_ENUMS(SoapMessageType QmlScreens PopUpType OrderType
            Language ErrorCheck LogMessageType TwintServerAddress
            CertificateCheckResult VersionCheckResult BeaconConnectionStatus)
public: //place for enums
    /**
     * \brief The SoapMessageType enum
     * Determines which kind of message is being processed
     */
    enum SoapMessageType {
        RequestCheckIn = 0,
        MonitorCheckIn = 1,
        CancelCheckIn = 2,
        StartOrder = 3,
        MonitorOrder = 4,
        ConfirmOrder = 5,
        CancelOrder = 6,
        FindOrder = 7,
        EnrollCashRegister = 8,
        CheckSystemStatus = 9
    };

    /**
     * \brief The QmlScreens enum
     * QmlScreens enum used in qml to push screen on StackView
     */
    enum QmlScreens {
        LoginScreen = 0,
        MainWindow,
        TransactionsDetailsScreen,
        CashierScreen,
        PaymentScreen,
        DetailsScreen,
        NumpadScreen,
        QrCodeScreen,
        PopUpScreen,
        SettingsScreen,
        KeyboardScreen,
        CashRegisterPopUp,
        GearSettingsScreen
    };

    /**
     * \brief The OrderStatus enum
     * Order status enum, ORDER_REVERSAL added for distinguishing Reversed orders
     */
    enum OrderStatus {
        NO_STATUS = -1, //order waiting for status
        ORDER_OK = 0, //Payment completed successfully
        ORDER_REVERSAL = 2,
        ORDER_PARTIAL_OK = 1, //Split function. The requested amount could not be approved, but only a partial amount
        ORDER_RECEIVED = 80, //Payment order being processed. Waiting for customer pairing
        ORDER_PENDING = 90, //Payment order being processed. Waiting for payment approval by the customer
        ORDER_CONFIRMATION_PENDING = 91, //Payment order being processed. The order was approved by the customer, but not yet confirmed by the merchant
        GENERAL_ERROR = 100, //General error without detailed information
        CLIENT_TIMEOUT = 101, //The customer has not authorized the payment in the specified time, or a customer for the payment could not be identified
        MERCHANT_ABORT = 102, //Cancellation of the payment by the merchant
        CLIENT_ABORT = 103 //Payment aborted by the customer
    };

    /**
     * \brief The PopUpType enum
     * Used in showing different style pop-ups screen
     */
    enum PopUpType {
        ConfirmPopUp = 0,
        ErrorPopUp = 1,
    };

    /**
     * \brief The OrderType enum
     * PAYMENT_IMMEDIATE - used in normal transactions
     * REVERSAL - return paid amount to customer account and cancels order
     */
    enum OrderType {
        NoOrderType,
        PAYMENT_IMMEDIATE,
        REVERSAL
    };

    /**
     * \brief The Language enum
     * Supported languages
     */
    enum Language {
        English = 0,
        German,
        French,
        Italian
    };

    /*!
     * \brief The ErrorCheck enum
     * Used to validate data on saving settings
     */
    enum ErrorCheck {
        PasswordCheck = 0,
        UsbCheck,
        SystemStatusCheck
    };

    /*!
     * \brief The LogMessageType enum
     * Used to distinguish log message types
     */
    enum LogMessageType {
        LogRequestMessage = 0,
        LogResponseMessage,
        LogErrorMessage
    };

    /*!
     * \brief The TwintServerAddress enum
     * Points to supported server.
     * TWINT merchant can connect to different server.
     *
     * To change them add "--server name" argument or use different shortcuts.
     * Avalible servers:
     * int, pat, ref, live
     * Application connects to live by default
     */
    enum TwintServerAddress {
        TwintLiveServer = 1,
        TwintIntServer = 2,
        TwintPatServer = 3,
        TwintRefServer = 4,
        TwintMajorServer = 5
    };

    /*!
     * \brief The CertificateCheckResult enum
     * Represents result of certificate authorization
     */
    enum CertificateCheckResult {
        WrongPassword = 0,
        ImportSuccess = 1,
        FileNotExist = 2
    };

    /*!
     * \brief The VersionCheckResult enum
     * Represents result of version check
     */
    enum VersionCheckResult {
        UpdateError = 0,
        ApplicationUpToDate,
        StandardUpdateAvalible,
        MandatoryUpdateAvalible
    };

    enum BeaconConnectionStatus {
        NotConnected = 0,
        Connected,
        Connecting,
    };

};

}

Q_DECLARE_METATYPE(GlobalSettings::Enums::SoapMessageType)
Q_DECLARE_METATYPE(GlobalSettings::Enums::QmlScreens)
Q_DECLARE_METATYPE(GlobalSettings::Enums::PopUpType)
Q_DECLARE_METATYPE(GlobalSettings::Enums::OrderType)
Q_DECLARE_METATYPE(GlobalSettings::Enums::Language)
Q_DECLARE_METATYPE(GlobalSettings::Enums::ErrorCheck)
Q_DECLARE_METATYPE(GlobalSettings::Enums::LogMessageType)
Q_DECLARE_METATYPE(GlobalSettings::Enums::TwintServerAddress)
Q_DECLARE_METATYPE(GlobalSettings::Enums::CertificateCheckResult)
Q_DECLARE_METATYPE(GlobalSettings::Enums::VersionCheckResult)
Q_DECLARE_METATYPE(GlobalSettings::Enums::BeaconConnectionStatus)

#endif // GLOBALSETTINGS_H
