#ifndef NEWORDERLISTMODEL_H
#define NEWORDERLISTMODEL_H

#include <QAbstractListModel>

#include "orderlistitem.h"

class NewOrderListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum ModelRole
    {
        OrderDate = Qt::UserRole + 1,
        PkId,
        TrxId,
        OrderId,
        BillStatus,
        Value,
        Discount,
        CurrencyName,
        CashierName,
        OrderHour,
        SummaryValue,
        DiscountValue,
        IsSumarryItem,
        TwintServerId
    };

    Q_ENUMS(ModelRole)

    enum DateFormat {
        Yearly = 0,
        Hourly
    };

    enum ModelColumn {
        OrderDateColumn = 0,
        PkIdColumn = 1,
        TrxIdColumn = 2,
        OrderIdColumn = 3,
        BillStatusColumn = 4,
        TwintServerIdColumn = 5,
        ValueColumn = 6,
        DiscountColumn = 7,
        CurrencyNameColumn = 8,
        CashierNameColumn = 9,
        OrderHourColumn = OrderDateColumn,
    };

    NewOrderListModel(QString connectionName, QObject *parent = 0);
    NewOrderListModel();
    ~NewOrderListModel();

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QHash<int, QByteArray> roleNames() const;

signals:
    void modelRefreshed() const;

public slots:
    Q_INVOKABLE QString getTransactionDetails(int index, ModelRole role);
    Q_INVOKABLE QString getTotalPrice(int index);
    QString getSelectStatement();
    void refreshModel();

private:
    void init();
    void clearValueMaps();
    QString getStringDate(int timestamp, DateFormat option) const;
    QString m_connectionName;
    QHash<int, QByteArray> m_customNames;
    QList<OrderListItem*> m_orderList;

};

#endif // NEWORDERLISTMODEL_H
