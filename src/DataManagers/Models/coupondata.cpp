#include "coupondata.h"

/*!
 * \brief Coupon Data
 * Data used in model data of CouponListModel providing information about
 * available coupons applicable to client.
 * No default constructor because CouponData can't be empty.
 *
 * \param title Short coupon title
 * \param description Detailed coupon description
 * \param amount Total amount for coupon that is applicable
 */
CouponData::CouponData(const QString& title,
                       const QString& description,
                       const qreal amount) :
    m_title( title),
    m_detailDescription( description),
    m_amount( amount),
    m_currentAmount( amount),
    m_usingCoupon( false)
{
    /* nothing */
}

CouponData::CouponData() :
    m_title(QString::null),
    m_detailDescription(QString::null),
    m_amount(0),
    m_currentAmount(0),
    m_usingCoupon(false)
{

}

void CouponData::setCouponId(const QString &couponId)
{
    m_couponId = couponId;
}

void CouponData::setUsingCoupon(bool usingCoupon)
{
    m_usingCoupon = usingCoupon;
}

void CouponData::setCurrentAmount(const qreal &currentAmount)
{
    m_currentAmount = currentAmount;
}

void CouponData::setAmount(const qreal &amount)
{
    m_amount = amount;
}

void CouponData::setDetailDescription(const QString &detailDescription)
{
    m_detailDescription = detailDescription;
}

void CouponData::setTitle(const QString &title)
{
    m_title = title;
}
