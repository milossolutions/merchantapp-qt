#include "currentorderitem.h"

#include "startorderresponseitem.h"
#include "qrcodeimageprovider.h"
#include "startordercontainer.h"
#include "monitororderresponseitem.h"
#include "ordertype.h"
#include "monitorordercontainer.h"
#include "requestcheckincontainer.h"

#include "globalsettings.h"
#include "settings.h"
#include "applicationstatus.h"

#include <QTimer>
#include <QMetaProperty>

CurrentOrderItem::CurrentOrderItem(QObject *parent) :
    QObject(parent),
    m_orderValue(QString::null),
    m_orderStatus(GlobalSettings::Enums::NO_STATUS),
    m_imageProvider(new QrCodeImageProvider()),
    m_mainTimer(new QTimer(this)),
    m_intervalTimer(new QTimer(this)),
    m_monitorStatus(NoStatus)
{
    connect(m_mainTimer, &QTimer::timeout, this, &CurrentOrderItem::mainMonitorTimerTimeout);
    connect(m_intervalTimer, &QTimer::timeout, this, &CurrentOrderItem::intervalMonitorTimerTimeout);
}

CurrentOrderItem::~CurrentOrderItem()
{

}

QString CurrentOrderItem::orderValue() const
{
    return m_orderValue;
}

QString CurrentOrderItem::orderToken() const
{
    return m_orderToken;
}

QString CurrentOrderItem::imageData() const
{
    return m_imageData;
}

QString CurrentOrderItem::orderId() const
{
    return m_orderId;
}

GlobalSettings::Enums::OrderStatus CurrentOrderItem::orderStatus() const
{
    return m_orderStatus;
}

void CurrentOrderItem::startMonitorOrder()
{
    m_mainTimer->setSingleShot(true);
    m_mainTimer->start(30000);
    m_intervalTimer->start(2000);
}

void CurrentOrderItem::stopMonitorOrder()
{
    m_mainTimer->stop();
    m_intervalTimer->stop();
}

void CurrentOrderItem::abortOrder()
{
    stopMonitorOrder();
    showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, tr("The payment is terminated"));
    m_monitorStatus = MerchantCancel;
    clearData();
    //emit sendRequest(GlobalSettings::Enums::MonitorOrder, QStringList{orderId()});
}

QrCodeImageProvider *CurrentOrderItem::imageProvider() const
{
    return m_imageProvider;
}

void CurrentOrderItem::setData(StartOrderContainer *startOrderContainer)
{
    LOGD()<<"Setting data";
    setOrderValue(startOrderContainer->value());
    setOrderId(startOrderContainer->orderId());
}

void CurrentOrderItem::requestCheckInSlot(ContainerItemInterface *requestCheckInContainer)
{
    RequestCheckInContainer *checkInContainer = qobject_cast<RequestCheckInContainer*>(requestCheckInContainer);
    if(!checkInContainer){
        LOGD()<<"Invalid container";
        return;
    }

    setOrderToken(checkInContainer->token());
    setImageData(checkInContainer->qrCode());

    emit sendRequest(GlobalSettings::Enums::MonitorCheckIn, QStringList{checkInContainer->pairingUuid()});
}

void CurrentOrderItem::setOrderValue(QString orderValue)
{
    if (m_orderValue == orderValue)
        return;

    m_orderValue = orderValue;
    emit orderValueChanged(orderValue);
}

void CurrentOrderItem::setOrderToken(QString orderToken)
{
    if (m_orderToken == orderToken){
        return;
    }

    m_orderToken = orderToken;
    emit orderTokenChanged(orderToken);
}

void CurrentOrderItem::setImageData(QString imageData)
{
    if (m_imageData == imageData){
        return;
    }

    m_imageData = imageData;

    m_imageProvider->receiveImageData(m_imageData);

    emit imageDataChanged(imageData);
}

void CurrentOrderItem::setOrderId(QString orderId)
{
    if (m_orderId == orderId)
        return;

    m_orderId = orderId;
    emit orderIdChanged(orderId);
}

void CurrentOrderItem::setOrderStatus(GlobalSettings::Enums::OrderStatus orderStatus)
{
    if (m_orderStatus == orderStatus)
        return;

    m_orderStatus = orderStatus;

    emit orderStatusChanged(orderStatus);
}

void CurrentOrderItem::setCurrentOrderItemData(ContainerItemInterface *itemContainer)
{
    StartOrderContainer *orderContainer = qobject_cast<StartOrderContainer*>(itemContainer);
    if(!orderContainer){
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        clearData();
        showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, tr("Internal error!"));
        return;
    }

    if (orderContainer->orderType() == GlobalSettings::Enums::OrderType::REVERSAL) {
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        clearData();
        if (orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_OK ||
                orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_PARTIAL_OK) {
            ApplicationStatus::instance()->setPairingOnGoingStatus(false);
            emit showMessage(GlobalSettings::Enums::PopUpType::ConfirmPopUp, tr("The transaction was successfully reverted"));
        } else {
            emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, tr("Validation error"));
        }
        return;
    }

    LOGD() << "NEW ORDER, Status:"<<orderContainer->orderStatus();
    setOrderStatus(GlobalSettings::Enums::NO_STATUS);

    setData(orderContainer);

    emit updateDatabaseOrderStatus(orderId(), orderContainer->orderStatus());
    setOrderStatus(orderContainer->orderStatus());

    m_monitorStatus = NoStatus;
    setOrderStatus(orderContainer->orderStatus());
    createMessage(orderContainer->orderStatus());
    checkResponse(orderContainer);
}

void CurrentOrderItem::monitorOrderSlot(ContainerItemInterface *containerItem)
{
    MonitorOrderContainer *monitorOrderContainer = qobject_cast<MonitorOrderContainer*>(containerItem);
    if (!monitorOrderContainer) {
        LOGD() << "MONITOR ERROR";
        return;
    }

    GlobalSettings::Enums::OrderStatus status = monitorOrderContainer->orderStatus();
    if (status != m_orderStatus) {
        emit updateDatabaseOrderStatus(orderId(), status);
        setOrderStatus(status);
    }

    if (m_monitorStatus == MerchantCancel) {
        clearData();
        return;
    }

    if(monitorOrderContainer->pairingStatus() != "PAIRING_ACTIVE"){
        stopMonitorOrder();
        createMessage(GlobalSettings::Enums::GENERAL_ERROR);
        m_monitorStatus = Cancel;
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
    }


    if (monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_OK ||
            monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_PARTIAL_OK) {
        stopMonitorOrder();
        createMessage(m_orderStatus);
        m_monitorStatus = Success;
        ApplicationStatus::instance()->setPairingOnGoingStatus(false);
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        clearData();
        LOGD() << "TWINT_PAYMENT_SUCCESFUL";
    } else if (monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::GENERAL_ERROR ||
               monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::CLIENT_TIMEOUT ||
               monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::MERCHANT_ABORT ||
               monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::CLIENT_ABORT) {
        LOGD() << "Customer cancel transaction etc.";
        stopMonitorOrder();
        createMessage(m_orderStatus);
        m_monitorStatus = Cancel;
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        ApplicationStatus::instance()->setPairingOnGoingStatus(false);
    } else if (monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_RECEIVED ||
               monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_PENDING ||
               monitorOrderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_CONFIRMATION_PENDING) {
        LOGD()<<"ORDER_CONFIRMATION_PENDING";
    }
    monitorOrderContainer->deleteLater();
}

void CurrentOrderItem::mainMonitorTimerTimeout()
{
    LOGD() << "TIMEOUT of main monitor ";
    m_monitorStatus = Timeout;
    stopMonitorOrder();
    emit showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("Payment timeout"));
    ApplicationStatus::instance()->setPaymentOngoingStatus(false);
    clearData();
}

void CurrentOrderItem::intervalMonitorTimerTimeout()
{
    if (m_monitorStatus != Checking)
        return;

    emit sendRequest(GlobalSettings::Enums::MonitorOrder, QStringList{orderId()});
}

void CurrentOrderItem::checkResponse(StartOrderContainer *orderContainer)
{
    LOGD() << "Positive Response: YES";
    LOGD() << "STATUS: " << orderContainer->orderStatus();
    LOGD() << "PAIRING_STATUS: " << orderContainer->pairingStatus();
    if (orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_OK ||
            orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_PARTIAL_OK) {
        m_monitorStatus = Success;
        emit updateDatabaseOrderStatus(orderId(), m_orderStatus);
        setOrderStatus(m_orderStatus);
        createMessage(m_orderStatus);
        LOGD() << "TWINT_PAYMENT_SUCCESFUL";
    } else if (orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::GENERAL_ERROR ||
               orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::CLIENT_TIMEOUT ||
               orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::MERCHANT_ABORT ||
               orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::CLIENT_ABORT) {
        createMessage(m_orderStatus);
    } else if (orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_RECEIVED ||
               orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_PENDING ||
               orderContainer->orderStatus() == GlobalSettings::Enums::OrderStatus::ORDER_CONFIRMATION_PENDING){
        LOGD() << "PENDING";
        if (orderContainer->pairingStatus() == "NO_PAIRING") {
            LOGD() << "PAIRING_STATUS_ACTIVE: NO";
            showMessage(GlobalSettings::Enums::PopUpType::ErrorPopUp, QObject::tr("The payment is terminated"));
            emit sendRequest(GlobalSettings::Enums::CancelCheckIn, QStringList{"PAYMENT_ABORT"});
        } else {
            LOGD() << "PAIRING_STATUS_ACTIVE: YES";
            LOGD() << "ORDER UUID: " << orderContainer->orderId();
            m_monitorStatus = Checking;
            emit updateDatabaseOrderStatus(orderId(), m_orderStatus);
            setOrderStatus(m_orderStatus);
            startMonitorOrder();
        }
    }
    orderContainer->deleteLater();
}

void CurrentOrderItem::createMessage(GlobalSettings::Enums::OrderStatus orderStatus)
{
    QString message = "";
    GlobalSettings::Enums::PopUpType popUpType = GlobalSettings::Enums::ConfirmPopUp;
    if (orderStatus == 0) {
        message = tr("The payment is collected successfully");// + orderValue() + tr(" is collected successfully");
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        clearData();
    } else if (orderStatus == 1) {
        message = "Split function. The requested amount could not be approved, but only a partial amount";
        ApplicationStatus::instance()->setPaymentOngoingStatus(false);
        clearData();
    } else if (orderStatus == 90) {
//        popUpType = GlobalSettings::Enums::ErrorPopUp;
//        message = tr("The payment is terminated");
    } else if (orderStatus == 100) {
        popUpType = GlobalSettings::Enums::ErrorPopUp;
        message = tr("The payment is terminated");
    } else if (orderStatus == 101) {
        popUpType = GlobalSettings::Enums::ErrorPopUp;
        message = "The customer has not authorized the payment in the specified time, or a customer for the payment could not be identified";
    } else if (orderStatus == 102) {
        popUpType = GlobalSettings::Enums::ErrorPopUp;
        message = tr("The payment is terminated");
    } else if (orderStatus == 103) {
        popUpType = GlobalSettings::Enums::ErrorPopUp;
        message = tr("The payment is terminated");
    }

    if (!message.isEmpty()) {
        emit showMessage(popUpType, message);
    }
}

void CurrentOrderItem::setImageProvider(QrCodeImageProvider *imageProvider)
{
    if (m_imageProvider == imageProvider)
        return;

    m_imageProvider = imageProvider;
    emit imageProviderChanged(imageProvider);
}

void CurrentOrderItem::clearData()
{
    m_orderStatus = GlobalSettings::Enums::NO_STATUS;
    m_orderId = QString::null;
    m_monitorStatus = NoStatus;
    m_orderToken = QString::null;
    m_imageData = QString::null;
    m_orderValue = QString::null;
    ApplicationStatus::instance()->setOfflineCode("");

    if(m_mainTimer->isActive()){
        stopMonitorOrder();
    }
}
