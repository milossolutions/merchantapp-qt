#include "qrcodeimageprovider.h"
#include "globalsettings.h"

QrCodeImageProvider::QrCodeImageProvider() :
    QQuickImageProvider(QQuickImageProvider::Image)
{
}

/**
 * @brief QrCodeImageProvider::requestImage
 * @param id - need to be diffrent each time new data is provided to refresh image
 * @param size
 * @param requestedSize - required for scaling in qml
 * @return
 */
QImage QrCodeImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    QImage image;
    if(m_qrcCodeData.isEmpty()){
        return image;

    }
    image.loadFromData(QByteArray::fromBase64(m_qrcCodeData));
    QImage result;

    result = image.scaled(requestedSize, Qt::KeepAspectRatioByExpanding);

    *size = result.size();
    return result;
}

QByteArray QrCodeImageProvider::qrcCodeData() const
{
    return m_qrcCodeData;
}

void QrCodeImageProvider::setQrcCodeData(const QByteArray &qrcCodeData)
{
    m_qrcCodeData = qrcCodeData;
}

void QrCodeImageProvider::receiveImageData(QString imageData)
{
    QStringList data = imageData.split(",", QString::SkipEmptyParts);

    setQrcCodeData(QByteArray::fromStdString(data.last().toStdString()));
}
