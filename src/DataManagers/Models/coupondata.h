#ifndef COUPONDATA_H
#define COUPONDATA_H

#include <QString>

class CouponData
{
public:
    CouponData();
    CouponData(const QString& title,
               const QString& description,
               const qreal amount);

    /*!< Coupon description */
    QString m_title;

    /*!< Coupon detailed description */
    QString m_detailDescription;

    /*!< Coupon discount maximum amount */
    qreal m_amount;

    /*!< Currently used discount amount */
    qreal m_currentAmount;

    /*!< Determine whether coupon is used in current purchase */
    bool m_usingCoupon;

    /*!< Coupon id required in StartOrderMessage */
    QString m_couponId;

    void setTitle(const QString &title);
    void setDetailDescription(const QString &detailDescription);
    void setAmount(const qreal &amount);
    void setCurrentAmount(const qreal &currentAmount);
    void setUsingCoupon(bool usingCoupon);
    void setCouponId(const QString &couponId);
};

#endif // COUPONDATA_H
