#ifndef CURRENTORDERITEM_H
#define CURRENTORDERITEM_H

#include <QObject>

#include "globalsettings.h"

class StartOrderResponseItem;
class ResponseItemInterface;
class QrCodeImageProvider;
class QTimer;
class ContainerItemInterface;
class MonitorOrderContainer;
class StartOrderContainer;

enum MonitorStatus {
    NoStatus,
    Checking,
    Timeout,
    Success,
    Cancel,
    MerchantCancel
};

/**
 * @brief The CurrentOrderItem class
 * Contins information and data about on-going transaction.
 */
class CurrentOrderItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString orderValue READ orderValue WRITE setOrderValue NOTIFY orderValueChanged)
    Q_PROPERTY(QString orderToken READ orderToken WRITE setOrderToken NOTIFY orderTokenChanged)
    Q_PROPERTY(QString imageData READ imageData WRITE setImageData NOTIFY imageDataChanged)
    Q_PROPERTY(QString orderId READ orderId WRITE setOrderId NOTIFY orderIdChanged)
    Q_PROPERTY(GlobalSettings::Enums::OrderStatus orderStatus READ orderStatus WRITE setOrderStatus NOTIFY orderStatusChanged)
    Q_PROPERTY(QrCodeImageProvider* imageProvider READ imageProvider WRITE setImageProvider NOTIFY imageProviderChanged)
public:
    explicit CurrentOrderItem(QObject *parent = 0);
    ~CurrentOrderItem();

    QString orderValue() const;
    QString orderToken() const;
    QString imageData() const;
    QString orderId() const;
    GlobalSettings::Enums::OrderStatus orderStatus() const;
    QrCodeImageProvider* imageProvider() const;

    void startMonitorOrder();
    void stopMonitorOrder();

    Q_INVOKABLE void abortOrder();

signals:
    void orderValueChanged(QString orderValue);
    void orderTokenChanged(QString orderToken);
    void imageDataChanged(QString imageData);
    void orderIdChanged(QString orderId);
    void imageProviderChanged(QrCodeImageProvider* imageProvider);
    void orderStatusChanged(GlobalSettings::Enums::OrderStatus orderStatus);
    void offlineCodeChanged(QString offlineCode);

    void updateDatabaseOrderStatus(QString orderUuid, int status);
    void sendRequest(GlobalSettings::Enums::SoapMessageType messageType, QStringList list);
    void showMessage(GlobalSettings::Enums::PopUpType popUpType, QString message);

public slots:
    void setData(StartOrderContainer *startOrderContainer);
    void requestCheckInSlot(ContainerItemInterface *requestCheckInContainer);
    void setOrderValue(QString orderValue);
    void setOrderToken(QString orderToken);
    void setImageData(QString imageData);
    void setOrderId(QString orderId);
    void setOrderStatus(GlobalSettings::Enums::OrderStatus orderStatus);
    void setImageProvider(QrCodeImageProvider* imageProvider);
    void clearData();

    /**
     * @brief setCurrentOrderItemData
     * @param itemContainer
     * Parses data and sets values of current order received in response
     */
    void setCurrentOrderItemData(ContainerItemInterface *itemContainer);
    /**
     * @brief monitorOrderSlot
     * @param containerItem
     * Function responsible for parsing monitor order actions
     */
    void monitorOrderSlot(ContainerItemInterface *containerItem);

private slots:
    /**
     * @brief mainMonitorTimerTimeout
     * Called after payment timeout
     */
    void mainMonitorTimerTimeout();
    /**
     * @brief intervalMonitorTimerTimeout
     * Send message on interval to monitor current order
     */
    void intervalMonitorTimerTimeout();

    /**
     * @brief checkResponse
     * @param orderContainer
     * Check status of started order
     */
    void checkResponse(StartOrderContainer *orderContainer);
    /**
     * @brief createMessage
     * @param orderStatus
     * Displays message on pop-up screen depending on order status
     */
    void createMessage(GlobalSettings::Enums::OrderStatus orderStatus);

private:
    QString m_orderValue;
    QString m_orderToken;
    QString m_imageData;
    QString m_orderId;
    GlobalSettings::Enums::OrderStatus m_orderStatus;
    QrCodeImageProvider* m_imageProvider;

    QTimer *m_mainTimer;
    QTimer *m_intervalTimer;

    MonitorStatus m_monitorStatus;
};

#endif // CURRENTORDERITEM_H
