#ifndef COUPONLISTMODEL_H
#define COUPONLISTMODEL_H

#include <QAbstractListModel>
#include "coupondata.h"

class CouponListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    CouponListModel(QObject *parent = Q_NULLPTR);

    enum CouponModelRole {
        Title = Qt::UserRole + 1,
        Description,
        Amount,
        UsingCoupon,
    };
    Q_ENUMS(CouponModelRole)

    Q_INVOKABLE void addCoupon(const CouponData &coupon);
    Q_INVOKABLE void addCoupon(const QString &title,
                               const QString& description,
                               const qreal amount);
    Q_INVOKABLE qreal getTotalDiscount() const;
    Q_INVOKABLE QList<CouponData> getCheckedCoupons();
    Q_INVOKABLE int count();

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_FINAL;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_FINAL;
    Q_INVOKABLE void setModelData(const int &row, const QVariant &value, int role = CouponModelRole::UsingCoupon);
    virtual QHash<int, QByteArray> roleNames() const Q_DECL_FINAL;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_FINAL;

public slots:
    Q_INVOKABLE void clear();

private:
    void init();
    void removeItem(const int row);

    QList<CouponData> m_modelData;
    QHash<int, QByteArray> m_modelRoles;
};

#endif // COUPONLISTMODEL_H
