#include "neworderlistmodel.h"

#include "globalsettings.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QMap>
#include <QDateTime>

NewOrderListModel::NewOrderListModel(QString connectionName, QObject *parent) :
    QAbstractListModel(parent),
    m_connectionName(connectionName)
{
    init();
}

NewOrderListModel::NewOrderListModel()
{

}

NewOrderListModel::~NewOrderListModel()
{

}

int NewOrderListModel::rowCount(const QModelIndex &parent) const
{
    return m_orderList.count();
}

QVariant NewOrderListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        LOGD() << "invalid index";
        return QVariant();
    }

    int row = index.row();

    switch (role) {
        case OrderDate:
        {
            return m_orderList.at(row)->orderDate();
        }
        case OrderHour:
        {
            return m_orderList.at(row)->orderHour();
        }
        case PkId:
        {
            return m_orderList.at(row)->pkId();
        }
        case TrxId:
        {
            return m_orderList.at(row)->trxId();
        }
        case OrderId:
        {
            return m_orderList.at(row)->orderId();
        }
        case BillStatus:
        {
            return m_orderList.at(row)->billStatus();
        }
        case TwintServerId:
        {
            return m_orderList.at(row)->twintServerId();
        }
        case Value:
        {
            return m_orderList.at(row)->value();
        }
        case Discount:
        {
            return m_orderList.at(row)->discount();
        }
        case CurrencyName:
        {
            return m_orderList.at(row)->currencyName();
        }
        case CashierName:
        {
            return m_orderList.at(row)->cashierName();
        }
        case SummaryValue:
        {
            return m_orderList.at(row)->summaryValue();
        }
        case DiscountValue:
        {
            return m_orderList.at(row)->summaryDiscount();
        }
        case IsSumarryItem:
        {
            return m_orderList.at(row)->isSummaryItem();
        }
        default:
        {
            LOGD() << "invalid role";
            return QVariant();
        }
    }

    return QVariant();
}

QHash<int, QByteArray> NewOrderListModel::roleNames() const
{
    return m_customNames;
}

QString NewOrderListModel::getTransactionDetails(int index, NewOrderListModel::ModelRole role)
{
    QModelIndex modelIndex;
    modelIndex = this->index(index,0);
    return data(modelIndex, role).toString();
}

QString NewOrderListModel::getTotalPrice(int index)
{
    QModelIndex modelIndex;
    modelIndex = this->index(index,0);

    if (!modelIndex.isValid())
        return "";

    double sum = data(modelIndex, ModelRole::Value).toDouble() + data(modelIndex, ModelRole::Discount).toDouble();

    return QString::number(sum);
}

QString NewOrderListModel::getSelectStatement()
{
    //b - bill table
    //ba - bill_amount table
    //cu - currency table
    //ca - cashier table
    return QString("SELECT b.date, b.pk_id, b.trx_id, b.order_id, b.bill_status, b.twint_server_id, "
                   "ba.value, ba.discount, "
                   "cu.name,"
                   "ca.name "
                   "FROM bill b "
                   "INNER JOIN bill_amount ba ON b.bill_amount_id = ba.id "
                   "INNER JOIN currency cu ON ba.currency_id = cu.id "
                   "INNER JOIN cashier ca ON b.cashier_id = ca.id "
                   "WHERE (b.bill_status=0 OR b.bill_status=1 OR b.bill_status=2) "
                   "AND b.twint_server_id=" + QString::number(GlobalSettings::Paths::instance()->twintServerId()) +
                   " ORDER BY b.date DESC");
}

void NewOrderListModel::refreshModel()
{
    beginResetModel();
    clearValueMaps();

    QSqlQuery statement(QSqlDatabase::database(m_connectionName));
    statement.prepare(getSelectStatement());
    statement.exec();

    int columnDate = OrderDateColumn;
    int columnValue = ValueColumn;
    int columnDiscount = DiscountColumn;
    int columnStatus = BillStatusColumn;
    QString currentDate = QString::null;
    double valueSummary = 0;
    double discountSummary = 0;
    //LOGD()<<"Refresh called"<<m_daySummaryValue<<m_daySummaryDiscount;

    if(statement.next()){
        currentDate = getStringDate(statement.value(columnDate).toInt(), DateFormat::Yearly);
        OrderListItem *orderItem= new OrderListItem(currentDate,
                                                    statement.value(PkIdColumn).toString(),
                                                    statement.value(TrxIdColumn).toString(),
                                                    statement.value(OrderIdColumn).toString(),
                                                    statement.value(BillStatusColumn).toString(),
                                                    statement.value(ValueColumn).toString(),
                                                    statement.value(DiscountColumn).toString(),
                                                    statement.value(CurrencyNameColumn).toString(),
                                                    statement.value(CashierNameColumn).toString(),
                                                    getStringDate(statement.value(columnDate).toInt(), DateFormat::Hourly),
                                                    statement.value(TwintServerIdColumn).toInt());
        m_orderList.append(orderItem);

        if(statement.value(columnStatus).toInt() != GlobalSettings::Enums::ORDER_REVERSAL){
            valueSummary = statement.value(columnValue).toDouble();
            discountSummary = statement.value(columnDiscount).toDouble();
        }

    }

    //checking if there if there are more records
    while (statement.next()) {
        if(getStringDate(statement.value(columnDate).toInt(), DateFormat::Yearly) == currentDate){
            OrderListItem *orderItem= new OrderListItem(currentDate,
                                                        statement.value(PkIdColumn).toString(),
                                                        statement.value(TrxIdColumn).toString(),
                                                        statement.value(OrderIdColumn).toString(),
                                                        statement.value(BillStatusColumn).toString(),
                                                        statement.value(ValueColumn).toString(),
                                                        statement.value(DiscountColumn).toString(),
                                                        statement.value(CurrencyNameColumn).toString(),
                                                        statement.value(CashierNameColumn).toString(),
                                                        getStringDate(statement.value(columnDate).toInt(), DateFormat::Hourly),
                                                        statement.value(TwintServerIdColumn).toInt());
            m_orderList.append(orderItem);

            if(statement.value(columnStatus).toInt() != GlobalSettings::Enums::ORDER_REVERSAL){
                valueSummary += statement.value(columnValue).toDouble();
                discountSummary += statement.value(columnDiscount).toDouble();
            }
        }else{
            //if dates are different add summary order item

            OrderListItem *orderItem = new OrderListItem();
            //LOGD()<<"currentDate"<<currentDate<<valueSummary<<discountSummary;
            orderItem->setOrderDate(currentDate);
            orderItem->setIsSummaryItem(true);
            orderItem->setSummaryDiscount(QString::number(discountSummary));
            orderItem->setSummaryValue(QString::number(valueSummary));
            m_orderList.append(orderItem);

            currentDate = getStringDate(statement.value(columnDate).toInt(), DateFormat::Yearly);

            OrderListItem *orderItem2 = new OrderListItem(currentDate,
                                                         statement.value(PkIdColumn).toString(),
                                                         statement.value(TrxIdColumn).toString(),
                                                         statement.value(OrderIdColumn).toString(),
                                                         statement.value(BillStatusColumn).toString(),
                                                         statement.value(ValueColumn).toString(),
                                                         statement.value(DiscountColumn).toString(),
                                                         statement.value(CurrencyNameColumn).toString(),
                                                         statement.value(CashierNameColumn).toString(),
                                                         getStringDate(statement.value(columnDate).toInt(), DateFormat::Hourly),
                                                         statement.value(TwintServerIdColumn).toInt());
            m_orderList.append(orderItem2);

            //reset values with new data
            if(statement.value(columnStatus).toInt() != GlobalSettings::Enums::ORDER_REVERSAL){
                valueSummary = statement.value(columnValue).toDouble();
                discountSummary = statement.value(columnDiscount).toDouble();
            }else{
                valueSummary=0;
                discountSummary=0;
            }
        }
    }
    //add data from last section
//    if(statement.value(columnStatus).toInt() != GlobalSettings::Enums::ORDER_REVERSAL){
//        m_daySummaryValue.append(dailySummary{currentDate,valueSummary});
//        m_daySummaryDiscount.append(dailySummary{currentDate,discountSummary});
//    }
    OrderListItem *orderItem = new OrderListItem();
    orderItem->setOrderDate(currentDate);
    orderItem->setIsSummaryItem(true);
    orderItem->setSummaryDiscount(QString::number(discountSummary));
    orderItem->setSummaryValue(QString::number(valueSummary));
    m_orderList.append(orderItem);
    //LOGD()<<"Refreshed values"<<m_daySummaryValue;

    emit modelRefreshed();

    //LOGD()<<m_daySummaryValue<<m_daySummaryDiscount;
    endResetModel();
}

void NewOrderListModel::init()
{
    m_customNames[OrderDate] = "orderDate";
    m_customNames[OrderHour] = "orderHour";
    m_customNames[PkId] = "pkId";
    m_customNames[TrxId] = "trxId";
    m_customNames[OrderId] = "orderId";
    m_customNames[BillStatus] = "billStatus";
    m_customNames[Value] = "value";
    m_customNames[Discount] = "discount";
    m_customNames[CurrencyName] = "currencyName";
    m_customNames[CashierName] = "cashierName";
    m_customNames[SummaryValue] = "summaryValue";
    m_customNames[DiscountValue] = "summaryDiscount";
    m_customNames[IsSumarryItem] = "isSumarryItem";
    m_customNames[TwintServerId] = "twintServerId";

    refreshModel();
}

void NewOrderListModel::clearValueMaps()
{
    for(int i = 0; i < m_orderList.count(); ++i){
        m_orderList.at(i)->deleteLater();
    }
    m_orderList.clear();
}

QString NewOrderListModel::getStringDate(int timestamp, NewOrderListModel::DateFormat option) const
{
    QDateTime date;
    date.setTime_t(timestamp);

    if(option == DateFormat::Yearly){
        return date.toString("dd.MM.yyyy");
    }else if(option == DateFormat::Hourly){
        return date.toString("hh:mm");
    }

    return QStringLiteral("Invalid");
}
