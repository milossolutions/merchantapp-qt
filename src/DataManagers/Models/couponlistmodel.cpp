#include "couponlistmodel.h"

#include "globalsettings.h"

/*!
 * \brief Coupon list model
 * List model, coupons are retrieved from web server then model is
 * updated on user data change
 *
 * Example usage:
 * \code
   CouponListModel* model = applicationController->dataManager()->couponListModel();
   model->addCoupon( "Example Title", "Example description", 50);
   CouponData couponFromShop = CouponData( "Title", "Description", 50);
   model->addCoupon( couponFromShop);
   // clear model data
   model->clear();
 * \endcode
 *
 * \param parent
 */

CouponListModel::CouponListModel(QObject *parent) :
    QAbstractListModel( parent)
{
    init();
}

/*!
 * \brief init initialize members
 */

void CouponListModel::init()
{
    m_modelRoles[CouponModelRole::Title] =        "Title";
    m_modelRoles[CouponModelRole::Description] =  "Description";
    m_modelRoles[CouponModelRole::Amount] =       "Amount";
}

/*!
 * \brief rowCount Return current model row count
 * \return
 */

int CouponListModel::rowCount(const QModelIndex &/*parent*/) const
{
    return m_modelData.count();
}

/*!
 * \brief data Return model data for specified role
 * Coupon amount is returned with two decimal places precision
 *
 * \param index model data index, using only row in this list mode
 * \param role model data role
 * \return Model data
 */

QVariant CouponListModel::data(const QModelIndex &index, int role) const
{
    if (   index.row() > m_modelData.count() - 1
        || index.row() < 0) {
        LOGD() << "CouponListModel: error data item incorrect index";
        return QVariant();
    }

    const CouponData& data = m_modelData.at( index.row());

    switch( role)
    {
        case CouponModelRole::Title :
        {
            return data.m_title;
        } break;
        case CouponModelRole::Description :
        {
            return data.m_detailDescription;
        } break;
        case CouponModelRole::Amount :
        {
            return QString::number(data.m_currentAmount, 'f', 2);
        } break;
        default :
        {
            return QVariant();
        } break;
    }

    return QVariant();
}

/*!
 * \brief set model data
 * Sets model data changed by user
 *
 * When setting new coupon value check is performed if new value is
 *
 * \param row model row
 * \param value new data
 * \param role data role
 */

void CouponListModel::setModelData(const int &row,
                                   const QVariant &value,
                                   int role)
{
    if ( row > m_modelData.count() - 1 || row < 0) {
        LOGD() << "CouponListModel: error set data item incorrect index";
        return;
    }

    if (role == CouponModelRole::UsingCoupon)
    {
        m_modelData[ row].m_usingCoupon = value.toBool();
    }
    else if (role == CouponModelRole::Amount)
    {
        bool converted = false;
        qreal newValue =  value.toDouble( &converted);

        if ( converted && (newValue >= 0.) )
            //Coupons rabat can be higher than their amount
            //Amount field doesn't have to be filled when downloading coupons
//            && (newValue <= m_modelData[ row].m_amount) )
        {
            m_modelData[ row].m_currentAmount = newValue;
        }

        // notify view about changes, view can't set data on value changed
        // programatically, i.e. on line edits use finishedEditing instead of
        // text / valueChanged
        // this implements validator under hood, so vied don't neccesserly need
        // to have one installed

        emit dataChanged( createIndex(row, 0),  createIndex(row, 0));

    } else {
        LOGD() << "CouponListModel: error set data item role not supported";
    }
}

/*!
 * \brief roleNames Available model role names
 * \return Hash of available model role names
 */

QHash<int, QByteArray> CouponListModel::roleNames() const
{
    return m_modelRoles;
}

Qt::ItemFlags CouponListModel::flags(const QModelIndex &/*index*/) const
{
    return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable;
}

/*!
 * \brief addCoupon Add new coupon to model
 * \param coupon Coupon that should be added to data model
 */

void CouponListModel::addCoupon(const CouponData &coupon)
{
    beginInsertRows( QModelIndex(), m_modelData.count(), m_modelData.count());

        m_modelData.append( coupon);

    endInsertRows();
}

/*!
 * \brief addCoupon Add new coupon to model
 * \param title Coupon title
 * \param description Coupon description
 * \param amount Coupon amount
 */

void CouponListModel::addCoupon(const QString &title,
                                const QString &description,
                                const qreal amount)
{
    beginInsertRows( QModelIndex(), m_modelData.count(), m_modelData.count());
        m_modelData.append( CouponData( title, description, amount));

    endInsertRows();
}

/*!
 * \brief get total discount currently selected
 * \return total discount amount
 */

qreal CouponListModel::getTotalDiscount() const
{
    qreal discount = 0.;

    for (auto coupon : m_modelData) {
        //if (coupon.m_usingCoupon)
            discount += coupon.m_currentAmount;
    }

    return discount;
}

//Checkbox was removed we will use coupons when discount value is higher then 0.00
QList<CouponData> CouponListModel::getCheckedCoupons()
{
    QList<CouponData> coupons;

    for (CouponData coupon : m_modelData) {
        if (coupon.m_currentAmount > 0.00)
            coupons.append(coupon);
    }

    return coupons;
}

int CouponListModel::count()
{
    return m_modelData.size();
}

/*!
 * \brief removeItem Remove item from model located at index row
 * \param row Model row index
 */

void CouponListModel::removeItem(const int row)
{
    if (   row < 0
        || row > m_modelData.count()) {
        LOGD() << "CouponListModel: error removing item incorrect index";
        return;
    }

    beginRemoveRows( QModelIndex(), row, row);

    m_modelData.removeAt( row);

    endRemoveRows();
}

/*!
 * \brief clear Clears model data
 */

void CouponListModel::clear()
{
    while ( !m_modelData.isEmpty()) {
        removeItem( 0);
    }
}
