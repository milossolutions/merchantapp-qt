#ifndef QRCODEIMAGEPROVIDER_H
#define QRCODEIMAGEPROVIDER_H

#include <QQuickImageProvider>

/**
 * @brief The QrCodeImageProvider class
 * This class is responsible for parsing qr code data of on-going transaction
 * and displaying it on screen.
 */
class QrCodeImageProvider : public QQuickImageProvider
{
public:
    QrCodeImageProvider();
    QImage requestImage(const QString & id, QSize * size, const QSize & requestedSize);

    QByteArray qrcCodeData() const;
    void setQrcCodeData(const QByteArray &qrcCodeData);

public slots:
    void receiveImageData(QString imageData);

private:
    QByteArray m_qrcCodeData;
};

#endif // QRCODEIMAGEPROVIDER_H
