#include "globalsettings.h"

#include <QDir>

GlobalSettings::Paths* GlobalSettings::Paths::m_instance = Q_NULLPTR;

GlobalSettings::Paths::Paths(QObject *parent) :
    QObject(parent),
    AppDirName( "TwintMerchant"),
    DocumentPath( QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation) + "/" + AppDirName),
    DatabasePath( DocumentPath + "/resources/"),
    m_TwintServerUrl("https://service.twint.ch/merchant/service/TWINTMerchantServiceV2"),
    m_TwintIntUrl("https://service-int.twint.ch/merchant/service/TWINTMerchantServiceV2"),
    m_TwintPatUrl("https://service-pat.twint.ch/merchant/service/TWINTMerchantServiceV2"),
    m_TwintLiveUrl("https://service.twint.ch/merchant/service/TWINTMerchantServiceV2"),
    m_TwintRefUrl("https://twint-service-ref.adnovum.ch/merchant/service/TWINTMerchantServiceV2"),
    m_TwintMajorUrl("https://twint-service-major.adnovum.ch/merchant/service/TWINTMerchantServiceV2"),
    m_twintServerId(1),
    m_TwintVersionUrl("https://download.k3x.io/version.ini")
{
    // check if document path exists, if not create
    if (!QDir().mkpath( DatabasePath)) {
        LOGD() << "Error, unable to create document directory:"
               << DatabasePath;
    }

#ifdef Q_OS_WIN
    setTwintApplicationUrl("http://download.k3x.io/?file=TWINT-Setup-MA-WIN");
#elif defined(Q_OS_MAC)
    //TODO switch link
    setTwintApplicationUrl("http://download.k3x.io/?file=TWINT-Setup-MA-OSX");
#endif
}

GlobalSettings::Paths *GlobalSettings::Paths::instance()
{
    if (!m_instance)
        m_instance = new Paths(QCoreApplication::instance());

    return m_instance;
}

QString GlobalSettings::Paths::TwintServerUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintServerUrl;
    }
}

QString GlobalSettings::Paths::TwintIntUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintIntUrl;
    }
}

QString GlobalSettings::Paths::TwintPatUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintPatUrl;
    }
}

QString GlobalSettings::Paths::TwintLiveUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintLiveUrl;
    }
}

QString GlobalSettings::Paths::TwintRefUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintRefUrl;
    }
}

int GlobalSettings::Paths::twintServerId()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_twintServerId;
    }
}

QString GlobalSettings::Paths::TwintVersionUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintVersionUrl;
    }
}

QString GlobalSettings::Paths::TwintApplicationUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintApplicationUrl;
    }
}

QString GlobalSettings::Paths::TwintMajorUrl()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_TwintMajorUrl;
    }
}

void GlobalSettings::Paths::setTwintServerUrl(QString TwintServerUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintServerUrl == TwintServerUrl)
            return;

        m_TwintServerUrl = TwintServerUrl;
    }
    emit TwintServerUrlChanged(TwintServerUrl);
}

void GlobalSettings::Paths::setTwintIntUrl(QString TwintIntUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintIntUrl == TwintIntUrl)
            return;

        m_TwintIntUrl = TwintIntUrl;
    }
    emit TwintIntUrlChanged(TwintIntUrl);
}

void GlobalSettings::Paths::setTwintPatUrl(QString TwintPatUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintPatUrl == TwintPatUrl)
            return;

        m_TwintPatUrl = TwintPatUrl;
    }
    emit TwintPatUrlChanged(TwintPatUrl);
}

void GlobalSettings::Paths::setTwintLiveUrl(QString TwintLiveUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintLiveUrl == TwintLiveUrl)
            return;

        m_TwintLiveUrl = TwintLiveUrl;
    }
    emit TwintLiveUrlChanged(TwintLiveUrl);
}

void GlobalSettings::Paths::setTwintRefUrl(QString TwintRefUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintRefUrl == TwintRefUrl)
            return;

        m_TwintRefUrl = TwintRefUrl;
    }
    emit TwintRefUrlChanged(TwintRefUrl);
}

void GlobalSettings::Paths::setTwintServerId(int twintServerId)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_twintServerId == twintServerId)
            return;

        m_twintServerId = twintServerId;
    }
    emit twintServerIdChanged(twintServerId);
}

void GlobalSettings::Paths::setTwintVersionUrl(QString TwintVersionUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintVersionUrl == TwintVersionUrl)
            return;

        m_TwintVersionUrl = TwintVersionUrl;
    }
    emit TwintVersionUrlChanged(TwintVersionUrl);
}

void GlobalSettings::Paths::setTwintApplicationUrl(QString TwintApplicationUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintApplicationUrl == TwintApplicationUrl)
            return;

        m_TwintApplicationUrl = TwintApplicationUrl;
    }
    emit TwintApplicationUrlChanged(TwintApplicationUrl);
}

void GlobalSettings::Paths::setTwintMajorUrl(QString TwintMajorUrl)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_TwintMajorUrl == TwintMajorUrl)
            return;

        m_TwintMajorUrl = TwintMajorUrl;
    }
    emit TwintMajorUrlChanged(TwintMajorUrl);
}
