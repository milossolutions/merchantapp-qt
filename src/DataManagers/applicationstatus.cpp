#include "applicationstatus.h"

#include "globalsettings.h"

#include <QWriteLocker>
#include <QReadLocker>
#include <QApplication>

ApplicationStatus* ApplicationStatus::m_instance = Q_NULLPTR;

/*!
 * \class ApplicationStatus
 * \brief ApplicationStatus is singleton class that stores values of perfomed live actions.
 *
 * It's used to monitor status of on going transactions and pairing with customer.
 * Based on values of properties different action will be taken.
 */
ApplicationStatus::ApplicationStatus(QObject *parent) :
    QObject(parent),
    m_currentEnrollStatus(false),
    m_loggerStatus(false),
    m_credentialsStatus(false),
    m_paymentOngoingStatus(false),
    m_requestCheckInStatus(false),
    m_singleReconnectRequest(true),
    m_pairingOnGoingStatus(false),
    m_hostConnection(true),
    m_mandatoryUpdateRequired(false),
    m_beaconConnectionStatus(GlobalSettings::Enums::NotConnected),
    m_firmwareVersion(QString::null),
    m_couponScreen(false)
{

}

ApplicationStatus::~ApplicationStatus()
{

}

ApplicationStatus *ApplicationStatus::instance()
{
    if (!m_instance)
        m_instance = new ApplicationStatus(QCoreApplication::instance());

    return m_instance;
}

void ApplicationStatus::setDiscoverdPort(QString discoverdPort)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        m_discoverdPort = discoverdPort;
    }
    emit discoverdPortChanged(discoverdPort);
}

void ApplicationStatus::setCurrentCashRegister(QString currentCashRegister)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_currentCashRegister == currentCashRegister)
            return;

        m_currentCashRegister = currentCashRegister;
    }
    emit currentCashRegisterChanged(currentCashRegister);
}

void ApplicationStatus::setCurrentEnrollStatus(bool currentEnrollStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_currentEnrollStatus == currentEnrollStatus)
            return;

        m_currentEnrollStatus = currentEnrollStatus;
    }
    emit currentEnrollStatusChanged(currentEnrollStatus);
}

void ApplicationStatus::setBeaconConnectionStatus(GlobalSettings::Enums::BeaconConnectionStatus beaconConnectionStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_beaconConnectionStatus == beaconConnectionStatus)
            return;

        m_beaconConnectionStatus = beaconConnectionStatus;
    }
    emit beaconConnectionStatusChanged(beaconConnectionStatus);
}

void ApplicationStatus::setLoggerStatus(bool loggerStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_loggerStatus == loggerStatus)
            return;

        m_loggerStatus = loggerStatus;
    }
    emit loggerStatusChanged(loggerStatus);
}

void ApplicationStatus::setCredentialsStatus(bool credentialsStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_credentialsStatus == credentialsStatus)
            return;

        m_credentialsStatus = credentialsStatus;
    }
    emit credentialsStatusChanged(credentialsStatus);
}

void ApplicationStatus::setPaymentOngoingStatus(bool paymentOngoingStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_paymentOngoingStatus == paymentOngoingStatus)
            return;

        m_paymentOngoingStatus = paymentOngoingStatus;
    }
    emit paymentOngoingStatusChanged(paymentOngoingStatus);
}

void ApplicationStatus::setRequestCheckInStatus(bool requestCheckInStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_requestCheckInStatus == requestCheckInStatus)
            return;

        m_requestCheckInStatus = requestCheckInStatus;
    }
    emit requestCheckInStatusChanged(requestCheckInStatus);
}

void ApplicationStatus::setSingleReconnectRequest(bool singleReconnectRequest)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_singleReconnectRequest == singleReconnectRequest)
            return;

        m_singleReconnectRequest = singleReconnectRequest;
    }
    emit singleReconnectRequestChanged(singleReconnectRequest);
}

void ApplicationStatus::setPairingOnGoingStatus(bool pairingOnGoingStatus)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_pairingOnGoingStatus == pairingOnGoingStatus)
            return;

        m_pairingOnGoingStatus = pairingOnGoingStatus;
    }
    emit pairingOnGoingStatusChanged(pairingOnGoingStatus);
}

void ApplicationStatus::setHostConnection(bool hostConnection)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_hostConnection == hostConnection)
            return;

        m_hostConnection = hostConnection;
    }
    emit hostConnectionChanged(hostConnection);
}

void ApplicationStatus::setOfflineCode(QString offlineCode)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_offlineCode == offlineCode)
            return;

        m_offlineCode = offlineCode;
    }
    emit offlineCodeChanged(offlineCode);
}

void ApplicationStatus::setFirmwareVersion(QString firmwareVersion)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_firmwareVersion == firmwareVersion)
            return;

        m_firmwareVersion = firmwareVersion;
    }
    emit firmwareVersionChanged(firmwareVersion);
}

void ApplicationStatus::setCouponScreen(bool couponScreen)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_couponScreen == couponScreen)
            return;

        m_couponScreen = couponScreen;
    }
    emit couponScreenChanged(couponScreen);
}

void ApplicationStatus::setMandatoryUpdateRequired(bool mandatoryUpdateRequired)
{
    {
        QWriteLocker locker(&m_readWriteLocker);
        if (m_mandatoryUpdateRequired == mandatoryUpdateRequired)
            return;

        m_mandatoryUpdateRequired = mandatoryUpdateRequired;
    }
    emit mandatoryUpdateRequiredChanged(mandatoryUpdateRequired);
}

/*!
 * \brief Stores result of discovered Usb beacon port
 */
QString ApplicationStatus::discoverdPort()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_discoverdPort;
    }
}

/*!
 * \brief Stores value of current cash register id from input
 * \return
 */
QString ApplicationStatus::currentCashRegister()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_currentCashRegister;
    }
}

/*!
 * \brief Returns true if cash register was succesfully enrolled with current settings
 * \return
 */
bool ApplicationStatus::currentEnrollStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_currentEnrollStatus;
    }
}

/*!
 * \brief Set to true if beacon was connected succesfully. Set to false on lost connection.
 * \return
 */
GlobalSettings::Enums::BeaconConnectionStatus ApplicationStatus::beaconConnectionStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_beaconConnectionStatus;
    }
}

/*!
 * \brief If set to true soap messages will be saved to file
 * \return
 */
bool ApplicationStatus::loggerStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_loggerStatus;
    }
}

/*!
 * \brief Sets to true if authorization process ends with success
 * \return
 */
bool ApplicationStatus::credentialsStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_credentialsStatus;
    }
}

/*!
 * \brief Variable that tell us if order is being proccessed
 * \return
 */
bool ApplicationStatus::paymentOngoingStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_paymentOngoingStatus;
    }
}

/*!
 * \brief Set to true if customer connection with beacon was estblished. Currently unsed in logic.
 * \return
 */
bool ApplicationStatus::requestCheckInStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_requestCheckInStatus;
    }
}

/*!
 * \brief Responsible for calling reconnect signal only once after receiving usb event.
 * \return
 */
bool ApplicationStatus::singleReconnectRequest()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_singleReconnectRequest;
    }
}

/*!
 * \brief Status points to on going pairing
 *
 * After paring customer with merchant and receiving coupons list.
 * Additional MonitorOrder messages are being sent to check if customer hasn't canceled payment.
 * \return
 */
bool ApplicationStatus::pairingOnGoingStatus()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_pairingOnGoingStatus;
    }
}

/*!
 * \brief Set to true after mandatory update is avalible
 *
 * Property block application from usage by displaying dark overlay on top of main screen
 * \return
 */
bool ApplicationStatus::mandatoryUpdateRequired()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_mandatoryUpdateRequired;
    }
}

/*!
 * \brief Value responsible for storing information about current network connectivity.
 *
 * Displays proper errors if set to false.
 * \return
 */
bool ApplicationStatus::hostConnection()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_hostConnection;
    }
}

/*!
 * \brief Value that stores offline code of transaction generated by customer with no internet connectivity.
 *
 * Used to request orders with different type of pairing.
 * \return
 */
QString ApplicationStatus::offlineCode()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_offlineCode;
    }
}

/*!
 * \brief Beacon firmware version. Displayed in seetings screen if beacon is connected
 * \return
 */
QString ApplicationStatus::firmwareVersion()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_firmwareVersion;
    }
}

bool ApplicationStatus::couponScreen()
{
    {
        QReadLocker locker(&m_readWriteLocker);
        return m_couponScreen;
    }
}
