#ifndef APPLICATIONSTATUS_H
#define APPLICATIONSTATUS_H

#include "globalsettings.h"
#include <QObject>
#include <QMutex>
#include <QVariantMap>
#include <QReadWriteLock>
#include <QDateTime>

class ApplicationStatus : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString discoverdPort READ discoverdPort WRITE setDiscoverdPort NOTIFY discoverdPortChanged)
    Q_PROPERTY(QString currentCashRegister READ currentCashRegister WRITE setCurrentCashRegister NOTIFY currentCashRegisterChanged)
    Q_PROPERTY(bool currentEnrollStatus READ currentEnrollStatus WRITE setCurrentEnrollStatus NOTIFY currentEnrollStatusChanged)
    Q_PROPERTY(GlobalSettings::Enums::BeaconConnectionStatus beaconConnectionStatus READ beaconConnectionStatus WRITE setBeaconConnectionStatus NOTIFY beaconConnectionStatusChanged)
    Q_PROPERTY(bool loggerStatus READ loggerStatus WRITE setLoggerStatus NOTIFY loggerStatusChanged)
    Q_PROPERTY(bool credentialsStatus READ credentialsStatus WRITE setCredentialsStatus NOTIFY credentialsStatusChanged)
    Q_PROPERTY(bool paymentOngoingStatus READ paymentOngoingStatus WRITE setPaymentOngoingStatus NOTIFY paymentOngoingStatusChanged)
    Q_PROPERTY(bool requestCheckInStatus READ requestCheckInStatus WRITE setRequestCheckInStatus NOTIFY requestCheckInStatusChanged)
    Q_PROPERTY(bool singleReconnectRequest READ singleReconnectRequest WRITE setSingleReconnectRequest NOTIFY singleReconnectRequestChanged)
    Q_PROPERTY(bool pairingOnGoingStatus READ pairingOnGoingStatus WRITE setPairingOnGoingStatus NOTIFY pairingOnGoingStatusChanged)
    Q_PROPERTY(bool mandatoryUpdateRequired READ mandatoryUpdateRequired WRITE setMandatoryUpdateRequired NOTIFY mandatoryUpdateRequiredChanged)
    Q_PROPERTY(bool hostConnection READ hostConnection WRITE setHostConnection NOTIFY hostConnectionChanged)
    Q_PROPERTY(QString offlineCode READ offlineCode WRITE setOfflineCode NOTIFY offlineCodeChanged)
    Q_PROPERTY(QString firmwareVersion READ firmwareVersion WRITE setFirmwareVersion NOTIFY firmwareVersionChanged)
    Q_PROPERTY(bool couponScreen READ couponScreen WRITE setCouponScreen NOTIFY couponScreenChanged)

public:
    ~ApplicationStatus();
    static ApplicationStatus* instance();

    QString discoverdPort();
    QString currentCashRegister();
    bool currentEnrollStatus();
    GlobalSettings::Enums::BeaconConnectionStatus beaconConnectionStatus();
    bool loggerStatus();
    bool credentialsStatus();
    bool paymentOngoingStatus();
    bool requestCheckInStatus();
    bool singleReconnectRequest();
    bool pairingOnGoingStatus();
    bool mandatoryUpdateRequired();
    bool hostConnection();
    QString offlineCode();
    QString firmwareVersion();
    bool couponScreen();

signals:
    void discoverdPortChanged(QString discoverdPort);
    void currentCashRegisterChanged(QString currentCashRegister);
    void currentEnrollStatusChanged(bool currentEnrollStatus);
    void beaconConnectionStatusChanged(GlobalSettings::Enums::BeaconConnectionStatus beaconConnectionStatus);
    void loggerStatusChanged(bool loggerStatus);
    void credentialsStatusChanged(bool credentialsStatus);
    void paymentOngoingStatusChanged(bool paymentOngoingStatus);
    void requestCheckInStatusChanged(bool requestCheckInStatus);
    void pairingOnGoingStatusChanged(bool pairingOnGoingStatus);
    Q_INVOKABLE void paymentTimeout();
    void singleReconnectRequestChanged(bool singleReconnectRequest);
    void mandatoryUpdateRequiredChanged(bool mandatoryUpdateRequired);
    void hostConnectionChanged(bool hostConnection);
    void offlineCodeChanged(QString offlineCode);
    void firmwareVersionChanged(QString firmwareVersion);
    void couponScreenChanged(bool couponScreen);

public slots:
    void setDiscoverdPort(QString discoverdPort);
    void setCurrentCashRegister(QString currentCashRegister);
    void setCurrentEnrollStatus(bool currentEnrollStatus);
    void setBeaconConnectionStatus(GlobalSettings::Enums::BeaconConnectionStatus beaconConnectionStatus);
    void setLoggerStatus(bool loggerStatus);
    void setCredentialsStatus(bool credentialsStatus);
    void setPaymentOngoingStatus(bool paymentOngoingStatus);
    void setRequestCheckInStatus(bool requestCheckInStatus);
    void setSingleReconnectRequest(bool singleReconnectRequest);
    void setPairingOnGoingStatus(bool pairingOnGoingStatus);
    void setMandatoryUpdateRequired(bool mandatoryUpdateRequired);
    void setHostConnection(bool hostConnection);
    void setOfflineCode(QString offlineCode);
    void setFirmwareVersion(QString firmwareVersion);
    void setCouponScreen(bool couponScreen);

private:
    static ApplicationStatus *m_instance;
    ApplicationStatus(QObject *parent = 0);

    QReadWriteLock m_readWriteLocker;

    QString m_discoverdPort;
    QString m_currentCashRegister;
    bool m_currentEnrollStatus;
    GlobalSettings::Enums::BeaconConnectionStatus m_beaconConnectionStatus;
    bool m_loggerStatus;
    bool m_credentialsStatus;
    bool m_paymentOngoingStatus;
    bool m_requestCheckInStatus;
    bool m_singleReconnectRequest;
    bool m_mandatoryUpdateRequired;
    bool m_hostConnection;
    bool m_pairingOnGoingStatus;
    QString m_offlineCode;
    QString m_firmwareVersion;
    bool m_couponScreen;
};

#endif // APPLICATIONSTATUS_H
