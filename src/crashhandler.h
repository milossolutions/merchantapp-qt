#ifndef CRASHHANDLER_H
#define CRASHHANDLER_H

#include <QObject>

class CrashHandler : public QObject
{
    Q_OBJECT
public:
    static CrashHandler* instance(QObject *parent = 0);
    static void crashHandler(int sig = 0);

signals:
    void crashDetected();

private:
    static CrashHandler* m_instance;
    explicit CrashHandler(QObject *parent = 0);

    void dumpBacktrace();
};

#endif // CRASHHANDLER_H
