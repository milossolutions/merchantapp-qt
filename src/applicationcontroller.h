#ifndef APPLICATIONCONTROLLER_H
#define APPLICATIONCONTROLLER_H

#include <QObject>
#include <QThread>
#include <QAbstractNativeEventFilter>

#include "globalsettings.h"

class DataManager;
class DatabaseManager;
class UiController;
class CommunicationManager;
class QSystemTrayIcon;
class QProcess;


class ApplicationController : public QObject, public QAbstractNativeEventFilter
{
    Q_OBJECT
    Q_PROPERTY(DataManager* dataManager READ dataManager WRITE setDataManager NOTIFY dataManagerChanged)
public:
    explicit ApplicationController(QObject *parent = 0);
    ~ApplicationController();

    DataManager* dataManager() const;

    DatabaseManager *databaseManager() const;
    void setDatabaseManager(DatabaseManager *databaseManager);

    CommunicationManager *communicationManager() const;
    void setCommunicationManager(CommunicationManager *communicationManager);

    UiController *uiController() const;
    void setUiController(UiController *uiController);

    bool nativeEventFilter(const QByteArray &eventType, void *message, long *result) Q_DECL_OVERRIDE;
    bool eventFilter(QObject *object, QEvent *event);

public slots:
    void setDataManager(DataManager* dataManager);
    void quitHandler();
    void appRunningHandler();
    void updateSystemTray();
    void showSystemKeyboard();
    void showUpdatePopUp(GlobalSettings::Enums::VersionCheckResult checkResult);

signals:
    void dataManagerChanged(DataManager* dataManager);
    void raiseWindow() const;
    void usbEventOccured() const;

private slots:
    void showQuitMessageBox();

private:
    void init();
    void prepareConnections();
    /**
     * @brief startCommunicationManager
     * Function puts communication and database manager in different thread.
     */
    void startCommunicationManager();
    void configureSystemTray();

    DataManager* m_dataManager;
    DatabaseManager *m_databaseManager;
    CommunicationManager *m_communicationManager;
    UiController *m_uiController;
    QSystemTrayIcon *m_systemTrayIcon;
    QProcess *m_keyboardProcess;

    QThread m_commLibThread;
};

#endif // APPLICATIONCONTROLLER_H
