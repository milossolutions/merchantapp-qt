#include "crashhandler.h"

#include <signal.h>

#ifdef Q_OS_WIN32
#include "Windows.h"

/*!
 * \brief Windows exception (aka crash) handler.
 * This is used to clean up anything after application crash (like closing
 * communication with serial keacon).
 **/

LONG WINAPI unhandledExceptionFilter(PEXCEPTION_POINTERS)
{
    CrashHandler::instance()->crashHandler();
    return EXCEPTION_EXECUTE_HANDLER;
}
#endif

// include after windows.h
#ifdef USE_BACKTRACE
#ifdef Q_OS_WIN32
    #include "DbgHelp.h"
#else
    #include <execinfo.h>
    #include <unistd.h>
#endif
#endif

CrashHandler* CrashHandler::m_instance = Q_NULLPTR;

/*!
 * \brief instance
 * Instance of crash handler, singleton
 */

CrashHandler* CrashHandler::instance(QObject *parent)
{
    if (!m_instance) {
        m_instance = new CrashHandler( parent);
    }
    return m_instance;
}

/*!
 * \brief Crash handler
 * Detect crash and notify about detected application crash.
 * Can be used co i.e. cleanly close kecon device (it's adviced to close device
 * when application quit, with crash handler also crash will close connection),
 * close db, networkign, log for debugging, etc...
 */

CrashHandler::CrashHandler(QObject *parent) : QObject(parent)
{
#ifdef Q_OS_WIN32
    SetUnhandledExceptionFilter(unhandledExceptionFilter);
#endif

    signal( SIGSEGV, CrashHandler::crashHandler);
    signal( SIGTERM, CrashHandler::crashHandler);
    signal( SIGABRT, CrashHandler::crashHandler);
}

/*!
 * \brief crash handler
 * Static method that is invoked when crash is detected
 */

void CrashHandler::crashHandler(int sig)
{
    qDebug( "crash detected");
    emit CrashHandler::instance()->crashDetected();
    CrashHandler::instance()->dumpBacktrace();

#ifdef Q_OS_WIN
    Q_UNUSED( sig)
#else
    // on OsX / Linux change default handle to SIG_DFL to prevent inf. recursion
    signal(sig, SIG_DFL);
#endif
}

/*!
 * \brief dump backtrace
 * Output stack backtrace to pinpoint location of crash.
 * Used only for debugging
 */

void CrashHandler::dumpBacktrace()
{
#ifdef USE_BACKTRACE
#ifdef Q_OS_WIN32
    const int stackSize = 100;
    void         * stack[ stackSize];
    unsigned short frames;
    SYMBOL_INFO  * symbol;
    HANDLE         process;

    process = GetCurrentProcess();
    SymInitialize( process, NULL, TRUE );
    frames               = CaptureStackBackTrace( 0, stackSize, stack, NULL );
    symbol               = ( SYMBOL_INFO * )calloc( sizeof( SYMBOL_INFO) + 256 * sizeof( char), 1 );
    symbol->MaxNameLen   = 255;
    symbol->SizeOfStruct = sizeof( SYMBOL_INFO);

    for (uint i = 0; i < frames; ++i) {
        SymFromAddr( process, ( DWORD64)( stack[i]), 0, symbol);
        qDebug( "%i: %s - 0x%0X\n", frames - i - 1, symbol->Name, symbol->Address);
    }
    free( symbol );
#else
    const int asize = 10;
    void *array[asize];
    size_t size;

    size = backtrace(array, asize);
    backtrace_symbols_fd(array, size, STDERR_FILENO);
#endif
#endif
}
